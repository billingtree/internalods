﻿





CREATE VIEW [dbo].[vRevenueATIByYear]
AS

  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.MID)  AS MID
        ,ATI.Config                  AS Config
        ,ATI.Name                    AS Name
        ,ATILookup.CRM_Id            AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.Revenue)            AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.RevenueATI20082010         AS ATI
  LEFT JOIN dbo.RevenueATILookup      AS ATILookup ON ATI.MID = ATILookup.MID
                                                AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.MID)
          ,ATI.Config
          ,ATI.Name
          ,ATILookup.CRM_Id
          ,YEAR(ATI.TranDate)
          
  UNION ALL

  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.MID)  AS MID
        ,ATI.Config                  AS Config
        ,ATI.Name                    AS Name
        ,ATILookup.CRM_Id            AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.Revenue)            AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.RevenueATI2011JanJun       AS ATI
  LEFT JOIN dbo.RevenueATILookup      AS ATILookup ON ATI.MID = ATILookup.MID
                                                AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.MID)
          ,ATI.Config
          ,ATI.Name
          ,ATILookup.CRM_Id
          ,YEAR(ATI.TranDate)
          
  UNION ALL
  
  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.MID)  AS MID
        ,ATI.Config                  AS Config
        ,ATI.Name                    AS Name
        ,ATILookup.CRM_Id            AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.Revenue)            AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.RevenueATI2011JulDec       AS ATI
  LEFT JOIN dbo.RevenueATILookup      AS ATILookup ON ATI.MID = ATILookup.MID
                                                AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.MID)
          ,ATI.Config
          ,ATI.Name
          ,ATILookup.CRM_Id
          ,YEAR(ATI.TranDate)
          
  UNION ALL
  
  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.MID)  AS MID
        ,ATI.Config                  AS Config
        ,ATI.Name                    AS Name
        ,ATILookup.CRM_Id            AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.Revenue)            AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.RevenueATI2012             AS ATI
  LEFT JOIN dbo.RevenueATILookup      AS ATILookup ON ATI.MID = ATILookup.MID
                                                AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.MID)
          ,ATI.Config
          ,ATI.Name
          ,ATILookup.CRM_Id
          ,YEAR(ATI.TranDate)
          
  UNION ALL
  
  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.MID)  AS MID
        ,ATI.Config                  AS Config
        ,ATI.Name                    AS Name
        ,ATILookup.CRM_Id            AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.Revenue)            AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.RevenueATI2013       AS ATI
  LEFT JOIN dbo.RevenueATILookup      AS ATILookup ON ATI.MID = ATILookup.MID
                                                AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.MID)
          ,ATI.Config
          ,ATI.Name
          ,ATILookup.CRM_Id
          ,YEAR(ATI.TranDate)
      
  UNION ALL
  
  SELECT DISTINCT 
         CONVERT(NVARCHAR, ATI.ProcessingNbr) 
                                     AS MID
        ,ATI.ConfigNbr               AS Config
        ,ATI.CRMActLevelName_GPParentName     
                                     AS Name
        ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
                                     AS CRM_ID
        ,YEAR(ATI.TranDate)          AS TranDate
        ,SUM(ATI.RevenueAmount)      AS Revenue
        ,'ATI'                       AS Source  
  FROM dbo.ATIDailyRevenueGLFeed      AS ATI

  WHERE ATI.RevenueAmount <> 0
  GROUP BY CONVERT(NVARCHAR, ATI.ProcessingNbr)
          ,ATI.ConfigNbr
          ,ATI.CRMActLevelName_GPParentName
          ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
          ,YEAR(ATI.TranDate)
            
    








