﻿

-- =============================================
-- Author:		Yaseen Jamaludeen
-- Create date: 2015-06-01
-- Description:	Procedure to carry out Consolidated Billing Workflow
-- WARNING : This is to be used in the SSIS main workflow. Do not execute independently.
-- =============================================
CREATE PROCEDURE [Billing].[spUSAePay] --'2016-06-25'
(
	@RunDate date = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	IF @RunDate IS NOT NULL
		BEGIN
			DECLARE @RunDateYearMonth int = LEFT(REPLACE(CONVERT(char(10),@RunDate),'-',''),6)
			DECLARE @RunDateYear smallint = YEAR(@RunDate)
			DECLARE @RunDateMonth tinyint = MONTH(@RunDate)
			DECLARE @RunDateMonthBegin date = DATEADD(D,1,EOMONTH(@RunDate,-1))
			DECLARE @RunDateMonthEnd date = EOMONTH(@RunDate)
			
			DELETE	FROM Billing.CDT_CustomerDailyTran
			WHERE	CDT_PRT_ID = 4
					AND CDT_RunDateYYYYMM = @RunDateYearMonth

			DECLARE @MaxID AS int = NULL

			SELECT	@MaxID=ISNULL(MAX(CDT_ID),0)
			FROM	Billing.CDT_CustomerDailyTran
			DBCC CHECKIDENT ('Billing.CDT_CustomerDailyTran', RESEED, @MaxID)

			DECLARE @LoopDate date = @RunDateMonthBegin -- Begining of month
			WHILE @LoopDate <= @RunDateMonthEnd  -- End of month
				BEGIN
					--PRINT @LoopDate
					INSERT INTO Billing.CDT_CustomerDailyTran
							(CDT_CPP_ID
							,CDT_RunDateYYYYMM
							,CDT_TranDate
							,CDT_TranType
							,CDT_TranAmount
							,CDT_TranCount
							,CDT_PRT_ID)
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Debit'				AS CDT_TranType
							,U.Amount			AS CDT_TranAmount
							,U.Sales			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Sales > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate
					UNION ALL
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Credit'			AS CDT_TranType
							,0					AS CDT_TranAmount
							,U.Credits			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Credits > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate
					UNION ALL
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Decline'			AS CDT_TranType
							,0					AS CDT_TranAmount
							,U.Declines			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Declines > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate

	
					UPDATE	CDT
					SET		CDT.CDT_CTF_ID = CTF.CTF_ID
					FROM	Billing.CDT_CustomerDailyTran AS CDT
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON CDT.CDT_CPP_ID = CPP.CPP_ID
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
								INNER JOIN Billing.CTF_CustomerTranFee AS CTF
									ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
									AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
									AND CDT.CDT_CTF_ID IS NULL
									AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
									AND CDT.CDT_TranDate = @LoopDate

					SET @LoopDate=DATEADD(d,1,@LoopDate)
				END




			UPDATE	Billing.PSL_ProcessStatusLog 
			SET		PSL_Status = 'Finished'
			WHERE	PSL_PRT_ID = 4
					AND PSL_RunDateYYYYMM = @RunDateYearMonth

			DECLARE @PRLID AS int
			SELECT	@PRLID = MAX(R.PRL_ID)
			FROM	Billing.PRL_ProcessRunList AS R
						INNER JOIN Billing.PSL_ProcessStatusLog AS S
							ON R.PRL_PSL_ID = S.PSL_ID
							AND S.PSL_PRT_ID = 4
							AND S.PSL_RunDateYYYYMM = @RunDateYearMonth

			UPDATE	Billing.PRL_ProcessRunList
			SET		PRL_EndDateTime = GETDATE()
			WHERE	PRL_ID = @PRLID
		END
END








