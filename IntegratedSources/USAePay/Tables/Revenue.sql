﻿CREATE TABLE [USAePay].[Revenue] (
    [FileName]            NVARCHAR (255) NULL,
    [USAePaySourceID]     NVARCHAR (255) NULL,
    [Merchant]            NVARCHAR (255) NULL,
    [MerchantID]          NVARCHAR (255) NULL,
    [Volume]              MONEY          NULL,
    [TotalTransactions]   INT            NULL,
    [MonthlyRevenue]      MONEY          NULL,
    [TransactionsRevenue] MONEY          NULL,
    [TranDate]            DATE           NULL,
    [TranDateYYYYMM]      INT            NULL,
    [CRM_PPDID]           NVARCHAR (36)  NULL,
    [UsabilityIndex]      SMALLINT       NULL,
    [MashupOutcome]       TINYINT        NULL,
    [InsertDate]          SMALLDATETIME  CONSTRAINT [DF__USAepayRevenue__Inser__2DDCB077] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_TranDateYYYYMM_UsabilityIndex]
    ON [USAePay].[Revenue]([TranDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([USAePaySourceID], [MerchantID]) WITH (FILLFACTOR = 80);

