﻿CREATE TABLE [SF].[OpportunityHistory] (
    [Amount]           NUMERIC (18, 2) NULL,
    [CloseDate]        DATE            NULL,
    [CreatedById]      NVARCHAR (18)   NULL,
    [CreatedDate]      DATETIME        NULL,
    [ExpectedRevenue]  NUMERIC (18, 2) NULL,
    [ForecastCategory] NVARCHAR (40)   NULL,
    [Id]               NVARCHAR (18)   NULL,
    [IsDeleted]        BIT             NULL,
    [OpportunityId]    NVARCHAR (18)   NULL,
    [Probability]      NUMERIC (3)     NULL,
    [StageName]        NVARCHAR (40)   NULL,
    [SystemModstamp]   DATETIME        NULL
);

