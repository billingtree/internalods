﻿CREATE TABLE [dbo].[MIDSalesPerson] (
    [CRM_PPDID]     UNIQUEIDENTIFIER NULL,
    [ParentAccount] NVARCHAR (255)   NULL,
    [ChildAccount]  NVARCHAR (255)   NULL,
    [MID]           NVARCHAR (255)   NULL,
    [ConfigSource]  NVARCHAR (255)   NULL,
    [FullName]      NVARCHAR (255)   NULL,
    [FirstName]     NVARCHAR (255)   NULL,
    [LastName]      NVARCHAR (255)   NULL,
    [StatusCode]    NVARCHAR (255)   NULL,
    [CreatedDate]   SMALLDATETIME    CONSTRAINT [DF_MIDSalesPerson_CreatedDate] DEFAULT (getdate()) NOT NULL
);

