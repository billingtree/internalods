﻿CREATE TABLE [dbo].[RevenueInStep] (
    [TranDateYYYYMM]       FLOAT (53)     NULL,
    [MID]                  FLOAT (53)     NULL,
    [Config]               NVARCHAR (255) NULL,
    [Name]                 NVARCHAR (255) NULL,
    [TranRate]             FLOAT (53)     NULL,
    [ReturnRate]           FLOAT (53)     NULL,
    [CreditOriginationFee] FLOAT (53)     NULL,
    [CreditReturnFee]      FLOAT (53)     NULL,
    [TranDate]             DATETIME       NULL,
    [TranCount]            FLOAT (53)     NULL,
    [K]                    DATETIME       NULL,
    [L]                    FLOAT (53)     NULL,
    [M]                    FLOAT (53)     NULL,
    [ReturnCount]          FLOAT (53)     NULL,
    [O]                    FLOAT (53)     NULL,
    [P]                    FLOAT (53)     NULL,
    [TranFee]              FLOAT (53)     NULL,
    [ReturnFee]            FLOAT (53)     NULL,
    [Revenue]              FLOAT (53)     NULL
);

