﻿CREATE TABLE [dbo].[ATIMonthlyCosts] (
    [ProcessingNumber]     NVARCHAR (8)   NULL,
    [Name]                 NVARCHAR (255) NULL,
    [StartDate]            DATE           NULL,
    [EndDate]              DATE           NULL,
    [EnforceEndDate]       DATE           NULL,
    [Inactive]             BIT            NULL,
    [StatusID]             TINYINT        NULL,
    [ConfigNumber]         TINYINT        NULL,
    [Fee]                  MONEY          NULL,
    [TransactionYearMonth] INT            NULL,
    [MainGLSegment]        NVARCHAR (10)  CONSTRAINT [DF_ATIMonthlyCosts_MainGLSegment] DEFAULT ((41000)) NULL,
    [InsertDate]           SMALLDATETIME  CONSTRAINT [DF_ATIMonthlyCosts_InsertDate] DEFAULT (getdate()) NULL
);

