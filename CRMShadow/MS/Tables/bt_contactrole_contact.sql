﻿CREATE TABLE [MS].[bt_contactrole_contact] (
    [bt_contactrole_contactid] UNIQUEIDENTIFIER NULL,
    [bt_contactroleid]         UNIQUEIDENTIFIER NULL,
    [contactid]                UNIQUEIDENTIFIER NULL,
    [versionnumber]            BIGINT           NULL,
    [InsertDate]               SMALLDATETIME    DEFAULT (getdate()) NULL
);

