﻿CREATE TABLE [dbo].[BTWebPayUsersByMID] (
    [MID]            NVARCHAR (255) NULL,
    [SourceID]       NVARCHAR (255) NULL,
    [SourceValue]    NVARCHAR (255) NULL,
    [BTWebPayUserYN] CHAR (1)       NULL,
    [RecCounter]     INT            NULL,
    [CreatedDate]    SMALLDATETIME  CONSTRAINT [DF_BTWebPayUsersByMID_CreatedDate] DEFAULT (getdate()) NOT NULL
);

