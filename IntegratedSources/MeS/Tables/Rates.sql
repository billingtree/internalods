﻿CREATE TABLE [MeS].[Rates] (
    [ReportYearMonth] INT            NOT NULL,
    [MID]             NVARCHAR (20)  NOT NULL,
    [DESCRIPTION]     NVARCHAR (255) NOT NULL,
    [CARD_TYPE]       CHAR (2)       NULL,
    [RATE]            MONEY          NOT NULL,
    [PER_ITEM]        MONEY          NOT NULL,
    [InsertDate]      SMALLDATETIME  DEFAULT (getdate()) NOT NULL
);

