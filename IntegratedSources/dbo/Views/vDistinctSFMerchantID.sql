﻿

CREATE VIEW [dbo].[vDistinctSFMerchantID]
AS
SELECT	DISTINCT
        CC_Merchant_number__c
		,Account__c
		FROM	SalesForce.PaymentProcessing

