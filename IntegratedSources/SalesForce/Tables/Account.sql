﻿CREATE TABLE [SalesForce].[Account] (
    [Account_Number__c]                               DECIMAL (10)     NULL,
    [Account_Number_External__c]                      NVARCHAR (10)    NULL,
    [AccountSource]                                   NVARCHAR (40)    NULL,
    [Alliance_Code__c]                                NVARCHAR (150)   NULL,
    [Alliance_Rating__c]                              NVARCHAR (1300)  NULL,
    [Alliance_Score__c]                               NVARCHAR (255)   NULL,
    [AnnualRevenue]                                   DECIMAL (18)     NULL,
    [Anticipated_Transaction_Volume__c]               DECIMAL (18)     NULL,
    [ArrowpointeMaps__Geocode_Accuracy_Code__c]       NVARCHAR (10)    NULL,
    [ArrowpointeMaps__Geocode_Outdated_By_AP_Maps__c] DECIMAL (18, 2)  NULL,
    [ArrowpointeMaps__Last_Updated_AP_Maps__c]        SMALLDATETIME    NULL,
    [ArrowpointeMaps__Latitude_AP_Maps__c]            DECIMAL (14, 10) NULL,
    [ArrowpointeMaps__Longitude_AP_Maps__c]           DECIMAL (14, 10) NULL,
    [ArrowpointeMaps__Process_ID_AP_Maps__c]          NVARCHAR (100)   NULL,
    [BillingCity]                                     NVARCHAR (40)    NULL,
    [BillingCountry]                                  NVARCHAR (80)    NULL,
    [BillingPostalCode]                               NVARCHAR (20)    NULL,
    [BillingState]                                    NVARCHAR (80)    NULL,
    [BillingStreet]                                   NVARCHAR (255)   NULL,
    [Call_Center_Seats__c]                            DECIMAL (18)     NULL,
    [Collection_Seats__c]                             DECIMAL (18)     NULL,
    [Company_DBA__c]                                  NVARCHAR (150)   NULL,
    [Concurforce__AccruedUnApprovedExpenseTotal__c]   DECIMAL (18, 2)  NULL,
    [Concurforce__FinalExpenseTotal__c]               DECIMAL (18, 2)  NULL,
    [Contact_Info_Validated__c]                       BIT              NULL,
    [CreatedById]                                     NVARCHAR (18)    NULL,
    [CreatedDate]                                     SMALLDATETIME    NULL,
    [D_U_N_S_Number__c]                               NVARCHAR (150)   NULL,
    [Data_Quality_Description__c]                     NVARCHAR (1300)  NULL,
    [Data_Quality_Score__c]                           DECIMAL (18)     NULL,
    [Description]                                     NVARCHAR (MAX)   NULL,
    [Existing_Clients_Profile__c]                     NVARCHAR (255)   NULL,
    [Fax]                                             NVARCHAR (40)    NULL,
    [FonalityUAE__Days_since_contacted__c]            DECIMAL (18)     NULL,
    [FonalityUAE__Days_since_created__c]              DECIMAL (18)     NULL,
    [FonalityUAE__Last_Contact_Date__c]               DATE             NULL,
    [Id]                                              NVARCHAR (36)    NOT NULL,
    [Inactive__c]                                     BIT              NULL,
    [Inactive_SA_Partner__c]                          BIT              NULL,
    [Industry]                                        NVARCHAR (255)   NULL,
    [IsDeleted]                                       BIT              NULL,
    [ISO_Parent__c]                                   NVARCHAR (20)    NULL,
    [IsPartner]                                       BIT              NULL,
    [IVR_licenses__c]                                 DECIMAL (18)     NULL,
    [Jigsaw]                                          NVARCHAR (20)    NULL,
    [JigsawCompanyId]                                 NVARCHAR (20)    NULL,
    [Known_and_Expected_Benefits__c]                  NVARCHAR (255)   NULL,
    [LastActivityDate]                                DATE             NULL,
    [LastModifiedById]                                NVARCHAR (18)    NULL,
    [LastModifiedDate]                                SMALLDATETIME    NULL,
    [Location_Type__c]                                NVARCHAR (255)   NULL,
    [Locations__c]                                    DECIMAL (18)     NULL,
    [Maintenance_Date__c]                             DATE             NULL,
    [Maintenance_Tier__c]                             NVARCHAR (255)   NULL,
    [MasterRecordId]                                  NVARCHAR (18)    NULL,
    [MCC_Designation__c]                              NVARCHAR (255)   NULL,
    [Name]                                            NVARCHAR (255)   NULL,
    [Next_Review_Date__c]                             DATE             NULL,
    [Next_Review_Date2__c]                            DATE             NULL,
    [Next_Review_Date3__c]                            DATE             NULL,
    [Next_Review_Date4__c]                            DATE             NULL,
    [Number_of_Users__c]                              DECIMAL (18)     NULL,
    [NumberOfEmployees]                               INT              NULL,
    [OwnerId]                                         NVARCHAR (18)    NULL,
    [ParentId]                                        NVARCHAR (36)    NULL,
    [Phone]                                           NVARCHAR (40)    NULL,
    [Physical_Address__c]                             NVARCHAR (150)   NULL,
    [Physical_City__c]                                NVARCHAR (20)    NULL,
    [Physical_Location__c]                            NVARCHAR (255)   NULL,
    [Physical_State__c]                               NVARCHAR (20)    NULL,
    [Physical_Zip__c]                                 NVARCHAR (10)    NULL,
    [Potential_Clients_Profile__c]                    NVARCHAR (255)   NULL,
    [RecordTypeId]                                    NVARCHAR (18)    NULL,
    [Referenceable__c]                                BIT              NULL,
    [Referral_Account__c]                             NVARCHAR (18)    NULL,
    [ShippingCity]                                    NVARCHAR (40)    NULL,
    [ShippingCountry]                                 NVARCHAR (80)    NULL,
    [ShippingPostalCode]                              NVARCHAR (20)    NULL,
    [ShippingState]                                   NVARCHAR (80)    NULL,
    [ShippingStreet]                                  NVARCHAR (255)   NULL,
    [SicDesc]                                         NVARCHAR (80)    NULL,
    [Sub_ISO_Parent__c]                               NVARCHAR (50)    NULL,
    [SystemModstamp]                                  SMALLDATETIME    NULL,
    [Ticker_Symbol__c]                                NVARCHAR (4)     NULL,
    [Tier__c]                                         NVARCHAR (255)   NULL,
    [Toll_Free_Phone__c]                              NVARCHAR (40)    NULL,
    [Type]                                            NVARCHAR (255)   NULL,
    [Validation_Date__c]                              DATE             NULL,
    [Website]                                         NVARCHAR (255)   NULL,
    [Year_Established__c]                             NVARCHAR (4)     NULL,
    [InsertDate]                                      DATETIME         CONSTRAINT [DF_Account_InsertDate] DEFAULT (getdate()) NOT NULL,
    [UpdateDate]                                      DATETIME         NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY NONCLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20130521-123516]
    ON [SalesForce].[Account]([Id] ASC, [Inactive__c] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_Type]
    ON [SalesForce].[Account]([Type] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_Name]
    ON [SalesForce].[Account]([Name] ASC)
    INCLUDE([Id], [Existing_Clients_Profile__c]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_IsDeletedType]
    ON [SalesForce].[Account]([IsDeleted] ASC, [Type] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_IsDeleted_Type]
    ON [SalesForce].[Account]([IsDeleted] ASC, [Type] ASC)
    INCLUDE([Id]) WITH (FILLFACTOR = 80);

