﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spInStepATIProcessingNbrDetailReport] 
(@Month1 int
,@Year1  int
,@ProcessingNbr INT
,@ConfigNbr INT
,@Source VARCHAR (6) 
,@Type   VARCHAR(1)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Month int, @Year int, @vProcessingNbr INT, @vConfigNbr INT, 
	        @vSource VARCHAR (6), @vType VARCHAR(1)
	SET @Month=@Month1
	SET @Year=@Year1
	SET @vProcessingNbr=@ProcessingNbr
	SET @vConfigNbr=@ConfigNbr
	SET @vSource=@Source
	SET @vType=@Type		
	
    DECLARE @YearMonth                 INT
    DECLARE @txtYear                   VARCHAR (4)
    DECLARE @txtMonth                  VARCHAR (2)
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))  	
	
-----------------------------------------------------------------------------------
--   This is ATI / InStep detail records for Config #, Processing # and Month    --                                   --
-----------------------------------------------------------------------------------
  BEGIN
    IF @vSource = 'ATI' 
        IF @vType = 'O'	
	        ------------------------------------------------------------------------------
         	--   This is ATI Monthly Originations                                       --                                   --
	        -----------------------------------------------------------------------------
	        SELECT	'ATI'                             AS ACHSource
	            ,ATI.ConfigNumber                     AS ConfigNbr
		     	,ATI.ProcessingNumber                 AS ProcessingNbr
			    ,ATI.CompanyName                      AS ACHName
			    ,ATI.FormattedAmount                  AS Volume
			    ,ATI.FileCreationDate                 AS TranDate
			    ,ATI.FileCreationTime                 AS TranTime
			    ,ATI.TransactionCode                  AS TranCode
			    ,ATI.StandardEntryClassCode           AS SEC
			    ,ATI.IdentificationNumber             AS IDNbr
			    ,ATI.ReceivingCompanyName             AS Name
	        FROM NACHA.Originations        AS ATI
	        WHERE ATI.FileCreationDateYYYYMM = @YearMonth
	          AND ATI.UsabilityIndex = 1
	          AND ATI.SourceType = 4
              AND ATI.ConfigNumber = @vConfigNbr
              AND ATI.ProcessingNumber = @vProcessingNbr
			  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
        ELSE
            SELECT	'ATI'                             AS ACHSource
	            ,ATIreturn.ConfigNumber               AS ConfigNbr
		     	,ATIreturn.ProcessingNumber           AS ProcessingNbr
			    ,ATIreturn.CompanyName                AS ACHName
			    ,ATIreturn.FormattedAmount            AS Volume
			    ,ATIreturn.FileCreationDate           AS TranDate
			    ,ATIreturn.FileCreationTime           AS TranTime
			    ,CONCAT(ATIreturn.TransactionCode,'-',ATIreturn.ReturnCode)            
			                                          AS TranCode
			    ,ATIreturn.StandardEntryClassCode     AS SEC
			    ,ATIreturn.IdentificationNumber       AS IDNbr
			    ,ATIreturn.ReceivingCompanyName       AS Name
			    
	        FROM NACHA.Returns        AS ATIreturn
	        WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
	          AND ATIreturn.UsabilityIndex = 1
	          AND ATIreturn.SourceType = 4
              AND ATIreturn.ConfigNumber = @vConfigNbr
              AND ATIreturn.ProcessingNumber = @vProcessingNbr
          ----------------------------------
    ELSE  ---   This is Instep            --
          ----------------------------------
        IF @vType = 'O'	
	        ------------------------------------------------------------------------------
         	--   This is InStep Monthly Originations                               --                                   --
	        -----------------------------------------------------------------------------
	        SELECT	'InStep'                             AS ACHSource
	            ,InStep.ConfigNumber                     AS ConfigNbr
		     	,InStep.ProcessingNumber                 AS ProcessingNbr
			    ,InStep.CompanyName                      AS ACHName
			    ,InStep.FormattedAmount                  AS Volume
			    ,InStep.FileCreationDate                 AS TranDate
			    ,InStep.FileCreationTime                 AS TranTime
			    ,InStep.TransactionCode                  AS TranCode
			    ,InStep.StandardEntryClassCode           AS SEC
			    ,InStep.IdentificationNumber             AS IDNbr
			    ,InStep.ReceivingCompanyName             AS Name
	        FROM NACHA.Originations        AS InStep
	        WHERE InStep.FileCreationDateYYYYMM = @YearMonth
			  AND InStep.UsabilityIndex = 1
	          AND InStep.SourceType = 1
              AND InStep.ConfigNumber = @vConfigNbr
              AND InStep.ProcessingNumber = @vProcessingNbr
			  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
		
		
		ELSE
	        ----------------------------------------------------------------------
         	--   This is InStep Monthly Returns                                 --                                   --
	        ----------------------------------------------------------------------
		
            SELECT	'InStep'                             AS ACHSource
	            ,InStepreturn.ConfigNumber               AS ConfigNbr
		     	,InStepreturn.ProcessingNumber           AS ProcessingNbr
			    ,InStepreturn.CompanyName                AS ACHName
			    ,InStepreturn.FormattedAmount            AS Volume
			    ,InStepreturn.FileCreationDate           AS TranDate
			    ,InStepreturn.FileCreationTime           AS TranTime
			    ,CONCAT(InStepreturn.TransactionCode,'-',InStepreturn.ReturnCode)            
			                                             AS TranCode
			    ,InStepreturn.StandardEntryClassCode     AS SEC
			    ,InStepreturn.IdentificationNumber       AS IDNbr
			    ,InStepreturn.ReceivingCompanyName       AS Name
			    
	        FROM NACHA.Returns           AS InStepreturn
	        WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
	          AND InStepreturn.UsabilityIndex = 1
	          AND InStepreturn.SourceType = 1
              AND InStepreturn.ConfigNumber = @vConfigNbr
              AND InStepreturn.ProcessingNumber = @vProcessingNbr	        
  
  END



END
