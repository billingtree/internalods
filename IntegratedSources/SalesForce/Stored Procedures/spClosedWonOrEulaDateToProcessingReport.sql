﻿











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spClosedWonOrEulaDateToProcessingReport]
(
@FromDate DATE,
@ToDate   DATE,
@AsOfDate DATE,
@Mode     AS CHAR
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @StartDate DATE
	       ,@EndDate   DATE
	       ,@CalcDate  DATE
	       ,@RunMode   CHAR
	SET @StartDate=@FromDate
	SET @EndDate=@ToDate
    SET @CalcDate=@AsOfDate
    SET @RunMode=@Mode



BEGIN
    IF @RunMode = 'S'   --   Setup Date Mode, drive through Sales Force Processing Stage History looking for Setup Dates 
                        --   that fall in between the Start and End Dates supplied 
----------------------------------------------------------------------------------------------------------------------
--   SINCE ProcessingStageHistory WAS NOT CONVERTED TO CRM, WE NEED TO CONTINUE TO USE IT FOR HISTORICAL PURPOSES   --
----------------------------------------------------------------------------------------------------------------------
                        
--------------------------------------------------------------------------
--    USAePay - get all MIDs that were Setup in the Timeframe supplied  --
--------------------------------------------------------------------------
		SELECT PSH.Processing__c                                          AS ProcessingID
			  ,'CC'                                                       AS "Source"
			  ,SFA.Existing_Clients_Profile__c                            AS Parent
			  ,SFA.Name                                                   AS Child
			  ,SFP.CC_Merchant_Number__c                                  AS MerchantProcessingNbr
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               SFP.Fee_Collected__c
		--           ELSE
		--               OH.bt_Stamp
		--       END                                                       AS FeeCollectedEULADate
			  ,I.InitialProcessingDate                                     AS InitialTranDate
			  ,DATEDIFF(DAY,PSH.Date_Stage_Entered__c,I.InitialProcessingDate)
																		 AS SetupTranDays
			  ,CASE
				   WHEN SO.bt_ApplicationStageName > ' ' THEN
					   SO.bt_ApplicationStageName
				   ELSE 
					   CONVERT(NVARCHAR, SO.bt_ApplicationStage)
			   END                                                       AS AppStage
			  ,CASE 
				   WHEN I.InitialProcessingDate IS NULL  THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)
				   WHEN I.InitialProcessingDate = ' ' THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)        
				   ELSE
					   NULL
			   END                                                       AS NotProcessingDays   
			  ,PSH.Date_Stage_Entered__c                                 AS SetupDate
			  ,CASE 
				   WHEN Opp.Name > '' THEN
					   Opp.Name    
				   ELSE
					   O.Name   
			   END                                                       AS OppName
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               DATEDIFF(DAY,SFP.Fee_Collected__c,PSH.Date_Stage_Entered__c)
		--           ELSE
		--               DATEDIFF(DAY,OH.bt_Stamp,PSH.Date_Stage_Entered__c) 
		--       END                                                       AS EULAToSetupDays
			  ,DATEDIFF(DAY,Opp.ActualCloseDate,PSH.Date_Stage_Entered__c)  AS CloseToSetupDays
			  ,ROW_NUMBER() OVER(PARTITION BY SFA.Existing_Clients_Profile__c
											 ,SFA.Name
											 ,SFP.CC_Merchant_Number__c 
								 ORDER BY PSH.Date_Stage_Entered__c DESC) AS RecCounter  -- Count multiple Setup Dates  
			  ,SU.FullName                                                AS SalesPerson    
			  ,Opp.bt_TypeName                                            AS "Type"
			  ,Opp.bt_OpportunityStageName                                AS OpportunityStage
			  ,A.bt_SubTypeName                                           AS "SubType(Account)"
			  ,Opp.ActualCloseDate                                        AS ActualCloseDate
			  ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
			  ,Opp.EstimatedValue                                         AS EstimatedRevenue
			  ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
			  ,SO.CreatedOn                                               AS ServiceOrderCreateDate                                 
		FROM SalesForce.ProcessingStageHistory        AS PSH  
		JOIN SalesForce.PaymentProcessing             AS SFP   ON PSH.Processing__c = SFP.Id 
															  AND SFP.CC_Merchant_Number__c IS NOT NULL
		JOIN SalesForce.Account                       AS SFA   ON SFA.Id = SFP.Account__c
															  AND SFA.Existing_Clients_Profile__c <> 'BillingTree' 
		LEFT JOIN SalesForce.Opportunity              AS O     ON O.Id = SFP.Opportunity__c  
		LEFT JOIN dbo.InitialProcessingDateByMID      AS I     ON SFP.Id = I.CRM_PPDID                                                       
		LEFT JOIN CRMShadow.MS.bt_ProcessingProfile   AS PP    ON PSH.Processing__c = PP.bt_ProcessingProfileId   
		LEFT JOIN CRMShadow.MS.bt_ProcessingAccount   AS PA    ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN CRMShadow.MS.bt_ServiceOrder        AS SO    ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
		LEFT JOIN CRMShadow.MS.Opportunity            AS Opp   ON PA.bt_Opportunity = Opp.OpportunityId
		--LEFT JOIN CRMShadow.MS.bt_OpportunityHistory  AS OH    ON Opp.OpportunityId = OH.bt_Opportunity
		--                                                     AND OH.bt_NewLabel = 'EULA Signed'
		LEFT JOIN CRMShadow.MS.SystemUser             AS SU    ON Opp.OwnerId = SU.SystemUserId 
		LEFT JOIN CRMShadow.MS.Account                AS A     ON PA.bt_Account = A.AccountId
		WHERE PSH.Date_Stage_Entered__c BETWEEN @StartDate AND @EndDate
		  AND (PSH.Name = 'Set Up' OR PSH.Name = 'Setup')  


		UNION ALL


		--------------------------------------------------------------------------
		--    ATI     - get all MIDs that were Setup in the Timeframe supplied  --
		--------------------------------------------------------------------------
		SELECT PSH.Processing__c                                          AS ProcessingID
			  ,'ATI'                                                      AS "Source"
			  ,SFA.Existing_Clients_Profile__c                            AS Parent
			  ,SFA.Name                                                   AS Child
			  ,SFP.ACH_Processing_Number__c                               AS MerchantProcessingNbr
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               SFP.Fee_Collected__c
		--           ELSE
		--               OH.bt_Stamp
		--       END                                                       AS FeeCollectedEULADate
			  ,I.InitialProcessingDate                                   AS InitialTranDate
			  ,DATEDIFF(DAY,PSH.Date_Stage_Entered__c,I.InitialProcessingDate)
																		 AS SetupTranDays
			  ,CASE
				   WHEN SO.bt_ApplicationStageName > ' ' THEN
					   SO.bt_ApplicationStageName
				   ELSE 
					   CONVERT(NVARCHAR, SO.bt_ApplicationStage)
			   END                                                       AS AppStage
			  ,CASE 
				   WHEN I.InitialProcessingDate IS NULL  THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)
				   WHEN I.InitialProcessingDate = ' ' THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)        
				   ELSE
					   NULL
			   END                                                       AS NotProcessingDays   
			  ,PSH.Date_Stage_Entered__c                                 AS SetupDate
			  ,CASE 
				   WHEN Opp.Name > '' THEN
					   Opp.Name    
				   ELSE
					   O.Name   
			   END                                                       AS OppName
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               DATEDIFF(DAY,SFP.Fee_Collected__c,PSH.Date_Stage_Entered__c)
		--           ELSE
		--               DATEDIFF(DAY,OH.bt_Stamp,PSH.Date_Stage_Entered__c) 
		--       END                                                       AS EULAToSetupDays
			  ,DATEDIFF(DAY,Opp.ActualCloseDate,PSH.Date_Stage_Entered__c)  AS CloseToSetupDays
			  ,ROW_NUMBER() OVER(PARTITION BY SFA.Existing_Clients_Profile__c
											 ,SFA.Name
											 ,SFP.ACH_Processing_Number__c 
								 ORDER BY PSH.Date_Stage_Entered__c DESC) AS RecCounter  -- Count multiple Setup Dates    
			  ,SU.FullName                                                AS SalesPerson 
			  ,Opp.bt_TypeName                                            AS "Type"
			  ,Opp.bt_OpportunityStageName                                AS OpportunityStage
			  ,A.bt_SubTypeName                                           AS "SubType(Account)"
			  ,Opp.ActualCloseDate                                        AS ActualCloseDate
			  ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
			  ,Opp.EstimatedValue                                         AS EstimatedRevenue
			  ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
			  ,SO.CreatedOn                                               AS ServiceOrderCreateDate                                                      
		FROM SalesForce.ProcessingStageHistory        AS PSH  
		JOIN SalesForce.PaymentProcessing             AS SFP   ON PSH.Processing__c = SFP.Id 
															AND SFP.CC_Merchant_Number__c IS NULL
															AND SFP.ACH_Platform__c LIKE 'ACH%'
		JOIN SalesForce.Account                       AS SFA   ON SFA.Id = SFP.Account__c
															  AND SFA.Existing_Clients_Profile__c <> 'BillingTree' 
		LEFT JOIN SalesForce.Opportunity              AS O     ON O.Id = SFP.Opportunity__c                                                         
		LEFT JOIN dbo.InitialProcessingDateByMID      AS I     ON SFP.Id = I.CRM_PPDID                                                       
		LEFT JOIN CRMShadow.MS.bt_ProcessingProfile   AS PP    ON PSH.Processing__c = PP.bt_ProcessingProfileId   
		LEFT JOIN CRMShadow.MS.bt_ProcessingAccount   AS PA    ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN CRMShadow.MS.bt_ServiceOrder        AS SO    ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
		LEFT JOIN CRMShadow.MS.Opportunity            AS Opp   ON PA.bt_Opportunity = Opp.OpportunityId
		--LEFT JOIN CRMShadow.MS.bt_OpportunityHistory  AS OH    ON Opp.OpportunityId = OH.bt_Opportunity
		--                                                     AND OH.bt_NewLabel = 'EULA Signed'
		LEFT JOIN CRMShadow.MS.SystemUser             AS SU    ON Opp.OwnerId = SU.SystemUserId 
		LEFT JOIN CRMShadow.MS.Account                AS A     ON PA.bt_Account = A.AccountId                                                     
		WHERE PSH.Date_Stage_Entered__c BETWEEN @StartDate AND @EndDate
		  AND (PSH.Name = 'Set Up' OR PSH.Name = 'Setup') 


		UNION ALL

		--------------------------------------------------------------------------
		--    InStep - get all MIDs that were Setup in the Timeframe supplied  --
		--------------------------------------------------------------------------
		SELECT PSH.Processing__c                                          AS ProcessingID
			  ,'InStep'                                                   AS "Source"
			  ,SFA.Existing_Clients_Profile__c                            AS Parent
			  ,SFA.Name                                                   AS Child
			  ,SFP.ACH_Processing_Number__c                               AS MerchantProcessingNbr
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               SFP.Fee_Collected__c
		--           ELSE
		--               OH.bt_Stamp
		--       END                                                       AS FeeCollectedEULADate
			  ,I.InitialProcessingDate                                   AS InitialTranDate
			  ,DATEDIFF(DAY,PSH.Date_Stage_Entered__c,I.InitialProcessingDate)
																		 AS SetupTranDays
			  ,CASE
				   WHEN SO.bt_ApplicationStageName > ' ' THEN
					   SO.bt_ApplicationStageName
				   ELSE 
					   CONVERT(NVARCHAR, SO.bt_ApplicationStage)
			   END                                                       AS AppStage
			  ,CASE 
				   WHEN I.InitialProcessingDate IS NULL  THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)
				   WHEN I.InitialProcessingDate = ' ' THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c,@CalcDate)        
				   ELSE
					   NULL
			   END                                                       AS NotProcessingDays   
			  ,PSH.Date_Stage_Entered__c                                 AS SetupDate
			  ,CASE 
				   WHEN Opp.Name > '' THEN
					   Opp.Name    
				   ELSE
					   O.Name   
			   END                                                       AS OppName
		--      ,CASE
		--           WHEN SFP.Fee_Collected__c > '' THEN
		--               DATEDIFF(DAY,SFP.Fee_Collected__c,PSH.Date_Stage_Entered__c)
		--           ELSE
		--               DATEDIFF(DAY,OH.bt_Stamp,PSH.Date_Stage_Entered__c) 
		--       END                                                       AS EULAToSetupDays   
			  ,DATEDIFF(DAY,Opp.ActualCloseDate,PSH.Date_Stage_Entered__c)  AS CloseToSetupDays 
			  ,ROW_NUMBER() OVER(PARTITION BY SFA.Existing_Clients_Profile__c
											 ,SFA.Name
											 ,SFP.ACH_Processing_Number__c 
								 ORDER BY PSH.Date_Stage_Entered__c DESC) AS RecCounter   -- Count multiple Setup Dates
			  ,SU.FullName                                                AS SalesPerson  
			  ,Opp.bt_TypeName                                            AS "Type"
			  ,Opp.bt_OpportunityStageName                                AS OpportunityStage
			  ,A.bt_SubTypeName                                           AS "SubType(Account)"
			  ,Opp.ActualCloseDate                                        AS ActualCloseDate
			  ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
			  ,Opp.EstimatedValue                                         AS EstimatedRevenue
			  ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
			  ,SO.CreatedOn                                               AS ServiceOrderCreateDate                                                            
		FROM SalesForce.ProcessingStageHistory        AS PSH  
		JOIN SalesForce.PaymentProcessing             AS SFP    ON PSH.Processing__c = SFP.Id 
															 AND SFP.CC_Merchant_Number__c IS NULL
															 AND SFP.ACH_Platform__c LIKE 'InStep%'
		JOIN SalesForce.Account                       AS SFA    ON SFA.Id = SFP.Account__c
															   AND SFA.Existing_Clients_Profile__c <> 'BillingTree' 
		LEFT JOIN SalesForce.Opportunity              AS O      ON O.Id = SFP.Opportunity__c                                                         
		LEFT JOIN dbo.InitialProcessingDateByMID      AS I      ON SFP.Id = I.CRM_PPDID                                                       
		LEFT JOIN CRMShadow.MS.bt_ProcessingProfile   AS PP     ON PSH.Processing__c = PP.bt_ProcessingProfileId   
		LEFT JOIN CRMShadow.MS.bt_ProcessingAccount   AS PA     ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN CRMShadow.MS.bt_ServiceOrder        AS SO     ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
		LEFT JOIN CRMShadow.MS.Opportunity            AS Opp    ON PA.bt_Opportunity = Opp.OpportunityId
		--LEFT JOIN CRMShadow.MS.bt_OpportunityHistory  AS OH     ON Opp.OpportunityId = OH.bt_Opportunity
		--                                                      AND OH.bt_NewLabel = 'EULA Signed'
		LEFT JOIN CRMShadow.MS.SystemUser             AS SU    ON Opp.OwnerId = SU.SystemUserId 
		LEFT JOIN CRMShadow.MS.Account                AS A     ON PA.bt_Account = A.AccountId                                                      
		WHERE PSH.Date_Stage_Entered__c BETWEEN @StartDate AND @EndDate
		  AND (PSH.Name = 'Set Up' OR PSH.Name = 'Setup') 






ELSE     -----------------------------------------------------------------
         --                       Closed Date Mode                      --
         -----------------------------------------------------------------

-------------------------------------------------------------------------------
--    USAePay - get all MIDs that were Closed Won in the Timeframe supplied  --
-------------------------------------------------------------------------------
		SELECT PSH.Processing__c                                          AS ProcessingID
			  ,CASE PP.bt_ProductLineName
				   WHEN 'Credit Card' THEN 
					   'CC' 
				   WHEN 'ACH' THEN
					   'ACH'
				   ELSE
					   ' '
			   END                                                        AS "Source"
			  ,CASE
				   WHEN A.ParentAccountIdName > '' THEN
					   A.ParentAccountIdName
				   ELSE 
					   A.Name
			   END                                                        AS Parent            
			  ,A.Name                                                     AS Child
			  ,PP.bt_BankMID                                              AS MerchantProcessingNbr
			  ,I.InitialProcessingDate                                    AS InitialTranDate
			  ,DATEDIFF(DAY,PSH.Date_Stage_Entered__c,I.InitialProcessingDate)
																		  AS SetupTranDays
			  ,CASE
				   WHEN SO.bt_ApplicationStageName > ' ' THEN
					   SO.bt_ApplicationStageName
				   ELSE 
					   CONVERT(NVARCHAR, SO.bt_ApplicationStage)
			   END                                                        AS AppStage
			  ,CASE 
				   WHEN I.InitialProcessingDate IS NULL  THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c, @CalcDate)
				   WHEN I.InitialProcessingDate = ' ' THEN 
					   DATEDIFF(DAY,PSH.Date_Stage_Entered__c, @CalcDate)        
				   ELSE
					   NULL
			   END                                                        AS NotProcessingDays   

			  ,PSH.Date_Stage_Entered__c                                  AS SetupDate
			  ,Opp.Name                                                   AS OppName
			  ,DATEDIFF(DAY,Opp.ActualCloseDate,PSH.Date_Stage_Entered__c)  AS CloseToSetupDays
			  ,ROW_NUMBER() OVER(PARTITION BY A.ParentAccountIdName
											 ,A.Name
											 ,Opp.Name
											 ,Opp.ActualCloseDate
											 ,PP.bt_BankMID 
								 ORDER BY PSH.Date_Stage_Entered__c DESC) AS RecCounter   -- Count multiple Setup Dates
			  ,SU.FullName                                                AS SalesPerson  
			  ,Opp.bt_TypeName                                            AS "Type"
			  ,Opp.bt_OpportunityStageName                                AS OpportunityStage
			  ,A.bt_SubTypeName                                           AS "SubType(Account)"
			  ,Opp.ActualCloseDate                                        AS ActualCloseDate
			  ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
			  ,Opp.EstimatedValue                                         AS EstimatedRevenue
			  ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
			  ,SO.CreatedOn                                               AS ServiceOrderCreateDate                
		FROM CRMShadow.MS.Opportunity                      AS Opp
		LEFT JOIN CRMShadow.MS.bt_ProcessingAccount        AS PA    ON Opp.OpportunityId = PA.bt_Opportunity
		LEFT JOIN CRMShadow.MS.bt_ProcessingProfile        AS PP    ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
																   --AND PP.bt_BankMID IS NOT NULL
		LEFT JOIN CRMShadow.MS.Account                     AS A     ON PA.bt_Account = A.AccountId    
		LEFT JOIN CRMShadow.MS.bt_ServiceOrder             AS SO    ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
		LEFT JOIN SalesForce.ProcessingStageHistory        AS PSH   ON PP.bt_ProcessingProfileId = PSH.Processing__c 
															   AND (PSH.Name = 'Set Up' OR PSH.Name = 'Setup') 
		LEFT JOIN CRMShadow.MS.SystemUser                  AS SU    ON Opp.OwnerId = SU.SystemUserId 
		LEFT JOIN dbo.InitialProcessingDateByMID           AS I     ON PP.bt_ProcessingProfileId = I.CRM_PPDID                                                       
		WHERE CONVERT(DATE, Opp.ActualCloseDate) BETWEEN @StartDate AND @EndDate
		  AND Opp.bt_OpportunityStageName = 'Closed Won'
		  AND SU.FullName <> 'CRM Dev'
 -- AND PP.bt_ProductLineName = 'Credit Card'
/*****************   We were noticing that there were Closed Won Opportunities that had no Processing Account
UNION ALL            Nor Processing Profiles, so we chose not to split it out, and use the PP.ProductLineName
                     and when there was no Processing Profile, show a blank value for the Source column

-------------------------------------------------------------------------------
--    ATI - get all MIDs that were Closed Won in the Timeframe supplied  --
-------------------------------------------------------------------------------
SELECT PSH.Processing__c                                          AS ProcessingID
      ,'ACH'                                                      AS "Source"
      ,CASE
           WHEN A.ParentAccountIdName > '' THEN
               A.ParentAccountIdName
           ELSE 
               A.Name
       END                                                        AS Parent            
      ,A.Name                                                     AS Child
      ,PP.bt_BankMID                                              AS MerchantProcessingNbr
      ,I.InitialProcessingDate                                    AS InitialTranDate
      ,DATEDIFF(DAY,PSH.Date_Stage_Entered__c,I.InitialProcessingDate)
                                                                  AS SetupTranDays
      ,CASE
           WHEN SO.bt_ApplicationStageName > ' ' THEN
               SO.bt_ApplicationStageName
           ELSE 
               CONVERT(NVARCHAR, SO.bt_ApplicationStage)
       END                                                        AS AppStage
      ,CASE 
           WHEN I.InitialProcessingDate IS NULL  THEN 
               DATEDIFF(DAY,PSH.Date_Stage_Entered__c, @CalcDate)
           WHEN I.InitialProcessingDate = ' ' THEN 
               DATEDIFF(DAY,PSH.Date_Stage_Entered__c, @CalcDate)        
           ELSE
               NULL
       END                                                        AS NotProcessingDays   

      ,PSH.Date_Stage_Entered__c                                  AS SetupDate
      ,Opp.Name                                                   AS OppName
      ,DATEDIFF(DAY,Opp.ActualCloseDate,PSH.Date_Stage_Entered__c)  AS CloseToSetupDays
      ,ROW_NUMBER() OVER(PARTITION BY A.ParentAccountIdName
                                     ,A.Name
                                     ,Opp.Name
                                     ,Opp.ActualCloseDate
                                     ,PP.bt_BankMID 
                         ORDER BY PSH.Date_Stage_Entered__c DESC) AS RecCounter   -- Count multiple Setup Dates
      ,SU.FullName                                                AS SalesPerson  
      ,Opp.bt_TypeName                                            AS "Type"
      ,Opp.bt_OpportunityStageName                                AS OpportunityStage
      ,A.bt_SubTypeName                                           AS "SubType(Account)"
      ,Opp.ActualCloseDate                                        AS ActualCloseDate
      ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
      ,Opp.EstimatedValue                                         AS EstimatedRevenue
      ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
 
FROM CRMShadow.MS.Opportunity                      AS Opp
LEFT JOIN CRMShadow.MS.bt_ProcessingAccount        AS PA    ON Opp.OpportunityId = PA.bt_Opportunity
LEFT JOIN CRMShadow.MS.bt_ProcessingProfile        AS PP    ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
                                                           --AND PP.bt_BankMID IS NOT NULL
LEFT JOIN CRMShadow.MS.Account                     AS A     ON PA.bt_Account = A.AccountId    
LEFT JOIN CRMShadow.MS.bt_ServiceOrder             AS SO    ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
LEFT JOIN SalesForce.ProcessingStageHistory        AS PSH   ON PP.bt_ProcessingProfileId = PSH.Processing__c 
                                                       AND (PSH.Name = 'Set Up' OR PSH.Name = 'Setup') 
LEFT JOIN CRMShadow.MS.SystemUser                  AS SU    ON Opp.OwnerId = SU.SystemUserId 
LEFT JOIN dbo.InitialProcessingDateByMID           AS I     ON PP.bt_ProcessingProfileId = I.CRM_PPDID  
WHERE Opp.ActualCloseDate BETWEEN @StartDate AND @EndDate
  AND Opp.bt_OpportunityStageName = 'Closed Won'
  AND SU.FullName <> 'CRM Dev'
  AND PP.bt_ProductLineName = 'ACH'
******/  
  

END






END
















