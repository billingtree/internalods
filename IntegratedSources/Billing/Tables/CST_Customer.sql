﻿CREATE TABLE [Billing].[CST_Customer] (
    [CST_ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [CST_RunDateYYYYMM]        INT              NOT NULL,
    [CST_CRMAccountID]         UNIQUEIDENTIFIER NOT NULL,
    [CST_ParentAccountNumber]  NVARCHAR (20)    NULL,
    [CST_AccountNumber]        NVARCHAR (20)    NULL,
    [CST_AccountName]          NVARCHAR (160)   NULL,
    [CST_SubType]              NVARCHAR (255)   NULL,
    [CST_Status]               NVARCHAR (255)   NULL,
    [CST_SalesRepFullName]     NVARCHAR (255)   NULL,
    [CST_SalesRepFirstName]    NVARCHAR (255)   NULL,
    [CST_SalesRepLastName]     NVARCHAR (255)   NULL,
    [CST_ContactAddressString] NVARCHAR (1000)  NULL,
    [CST_CreateDate]           SMALLDATETIME    DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CST_Customer] PRIMARY KEY CLUSTERED ([CST_ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NCI_CST_CRMAccountID]
    ON [Billing].[CST_Customer]([CST_CRMAccountID] ASC)
    INCLUDE([CST_ID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_CST_RunDateYYYYMM]
    ON [Billing].[CST_Customer]([CST_RunDateYYYYMM] ASC)
    INCLUDE([CST_ID]) WITH (FILLFACTOR = 80);

