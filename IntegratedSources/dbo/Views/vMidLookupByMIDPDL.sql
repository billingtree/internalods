﻿


--DROP VIEW [dbo].[vMidLookupByMIDPDL]

CREATE VIEW [dbo].[vMidLookupByMIDPDL]
AS
      
SELECT DISTINCT
       UMS.MID                   AS MID 
      ,CONVERT(INT, StartYYYYMM) AS StartYYYYMM
      ,CONVERT(INT, EndYYYYMM)   AS EndYYYYMM
      ,CASE 
          WHEN PDL.MID > '' THEN
              'Y'
          ELSE
              'N'
       END                       AS PDL
FROM dbo.MidLookupUMS       AS UMS
LEFT JOIN dbo.RevenueCCPDL  AS PDL  ON CONVERT(VARCHAR, CONVERT(BIGINT, UMS.MID)) = CONVERT(VARCHAR, CONVERT(BIGINT, PDL.MID))
                                   OR CONVERT(VARCHAR, CONVERT(BIGINT, UMS.MID)) = RIGHT(CONVERT(VARCHAR, CONVERT(BIGINT, PDL.MID)), 11) 
                                   OR LEFT(CONVERT(VARCHAR, CONVERT(BIGINT, UMS.MID)), 13) = CONVERT(VARCHAR, CONVERT(BIGINT, PDL.MID))
                                   OR RIGHT(CONVERT(VARCHAR, CONVERT(BIGINT, UMS.MID)), 11) = CONVERT(VARCHAR, CONVERT(BIGINT, PDL.MID))
                                   OR RIGHT(CONVERT(VARCHAR, CONVERT(BIGINT, UMS.MID)), 5) = CONVERT(VARCHAR, CONVERT(BIGINT, PDL.MIDPartial))
UNION
SELECT DISTINCT
       MeS.MID                       AS MID
      ,CONVERT(INT, MeS.StartYYYYMM) AS StartYYYYMM
      ,CONVERT(INT, MeS.EndYYYYMM)   AS EndYYYYMM
      ,CASE 
          WHEN PDL.MID > '' THEN
              'Y'
          ELSE
              'N'
       END                       AS PDL
FROM dbo.MidLookupMeS       AS MeS
LEFT JOIN dbo.RevenueCCPDL  AS PDL  ON MeS.MID = PDL.MID





