﻿





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spProcessingWhileNotComplete]
(
@Mode     AS VARCHAR (24)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @RunMode   VARCHAR(24)

    SET @RunMode=@Mode



BEGIN
    IF @RunMode = 'ProcessingNowNotComplete'   --   Drive through CRM Service Order looking for MIDs that are not in 
                                               --   Application Stage Complete, but are Processing
 
        SELECT CASE
                   WHEN A.ParentAccountIdName > '' THEN
                       A.ParentAccountIdName
                   ELSE
                       A.Name
               END                                               AS Parent
              ,A.Name                                            AS Child
              ,PP.bt_BankMID                                     AS MID
              ,CASE
                   WHEN PP.bt_Config > '' THEN
                       PP.bt_Config
                   ELSE
                       PP.bt_SourceId
               END                                               AS SourceIdConfig
              ,I.InitialProcessingDate                           AS InitialTranDate
              ,SO.bt_ApplicationStageName                        AS AppStage
              ,''                                                AS CompleteDate
              ,PP.bt_ProcessingProfileId                         AS CRM_PPDID
              ,PP.StatusCodeName                                 AS ProfileStatus
              ,PP.bt_Name                                        AS ProfileName
        FROM [$(CRMShadow)].MS.bt_ServiceOrder             AS SO
        JOIN [$(CRMShadow)].MS.bt_ProcessingAccount        AS PA    ON SO.bt_ServiceOrderId = PA.bt_CurrentServiceOrder
        LEFT JOIN [$(CRMShadow)].MS.Account                AS A     ON PA.bt_Account = A.AccountId
        JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP    ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
        JOIN dbo.InitialProcessingDateByMID           AS I     ON PP.bt_ProcessingProfileId = I.CRM_PPDID

        WHERE UPPER(SO.bt_ApplicationStageName) NOT IN ('COMPLETE','INACTIVE') 



    ELSE     --   ProcessingBeforeComplete
             --   Drive through Sales Force Processing Stage History looking for 
             --   MIDs that went to App Stage Complete after their Initial Processing Date
----------------------------------------------------------------------------------------------------------------------
--   SINCE ProcessingStageHistory WAS NOT CONVERTED TO CRM, WE NEED TO CONTINUE TO USE IT FOR HISTORICAL PURPOSES   --
----------------------------------------------------------------------------------------------------------------------

        SELECT CASE
                   WHEN A.ParentAccountIdName > '' THEN
                       A.ParentAccountIdName
                   ELSE
                       A.Name
               END                                               AS Parent
              ,A.Name                                            AS Child
              ,PP.bt_BankMID                                     AS MID
              ,CASE
                   WHEN PP.bt_Config > '' THEN
                       PP.bt_Config
                   ELSE
                       PP.bt_SourceId
               END                                               AS SourceIdConfig
              ,I.InitialProcessingDate                           AS InitialTranDate
              ,PSH.Name                                          AS AppStage
              ,CONVERT(DATE, PSH.Date_Stage_Entered__c)          AS CompleteDate
              ,PP.bt_ProcessingProfileId                         AS CRM_PPDID
              ,PP.StatusCodeName                                 AS ProfileStatus
              ,PP.bt_Name                                        AS ProfileName
        FROM SalesForce.ProcessingStageHistory        AS PSH  
        JOIN dbo.InitialProcessingDateByMID           AS I     ON PSH.Processing__c = CONVERT(NVARCHAR (36), I.CRM_PPDID)
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile   AS PP    ON I.CRM_PPDID = PP.bt_ProcessingProfileId   
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount   AS PA    ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
        LEFT JOIN [$(CRMShadow)].MS.Account                AS A     ON PA.bt_Account = A.AccountId

        WHERE UPPER(PSH.Name) = 'COMPLETE' 
          AND CONVERT(DATE, PSH.Date_Stage_Entered__c) > I.InitialProcessingDate
  
  

    END






END
















