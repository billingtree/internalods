﻿CREATE TABLE [Billing].[CTF_CustomerTranFee] (
    [CTF_ID]                         INT              IDENTITY (1, 1) NOT NULL,
    [CTF_RunDateYYYYMM]              INT              NOT NULL,
    [CTF_CPA_ID]                     INT              NOT NULL,
    [CTF_CRMServiceProductFeeID]     UNIQUEIDENTIFIER NOT NULL,
    [CTF_ProductName]                NVARCHAR (100)   NULL,
    [CTF_FeeType]                    NVARCHAR (255)   NULL,
    [CTF_FeeAmount]                  MONEY            NULL,
    [CTF_StartDate]                  DATETIME         NULL,
    [CTF_EndDate]                    DATETIME         NULL,
    [CTF_EffectiveStartDateForMonth] DATE             NULL,
    [CTF_EffectiveEndDateForMonth]   DATE             NULL,
    [CTF_DaysInEffectForMonth]       INT              NULL,
    [CTF_CreateDate]                 SMALLDATETIME    DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTF_CustomerTranFee] PRIMARY KEY CLUSTERED ([CTF_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CTF_CustomerTranFee_CPA_CustomerProcessingAccount] FOREIGN KEY ([CTF_CPA_ID]) REFERENCES [Billing].[CPA_CustomerProcessingAccount] ([CPA_ID]) ON DELETE CASCADE
);

