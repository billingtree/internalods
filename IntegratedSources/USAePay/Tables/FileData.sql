﻿CREATE TABLE [USAePay].[FileData] (
    [ID]              INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileName]        VARCHAR (255)  NOT NULL,
    [TransType]       CHAR (1)       NULL,
    [Result]          CHAR (1)       NULL,
    [ApiType]         VARCHAR (20)   NOT NULL,
    [TransDate]       DATE           NOT NULL,
    [TransTime]       TIME (7)       NOT NULL,
    [Software]        VARCHAR (255)  NULL,
    [SourceID]        VARCHAR (60)   NULL,
    [AuthCode]        VARCHAR (50)   NULL,
    [MerchantName]    VARCHAR (255)  NULL,
    [USAePayID]       VARCHAR (8)    NOT NULL,
    [MerchantID]      VARCHAR (16)   NULL,
    [Platform]        VARCHAR (25)   NOT NULL,
    [RefNum]          CHAR (10)      NULL,
    [AccountHolder]   VARCHAR (175)  NULL,
    [Amount]          MONEY          NOT NULL,
    [Invoice]         VARCHAR (15)   NULL,
    [OrderID]         VARCHAR (70)   NULL,
    [Description]     VARCHAR (300)  NULL,
    [Comments]        VARCHAR (2000) NULL,
    [Currency]        VARCHAR (3)    NULL,
    [Tax]             VARCHAR (10)   NULL,
    [Tip]             VARCHAR (10)   NULL,
    [Shipping]        VARCHAR (10)   NULL,
    [Subtotal]        VARCHAR (10)   NULL,
    [CardType]        VARCHAR (10)   NULL,
    [AVSZip]          VARCHAR (50)   NULL,
    [CustomerID]      VARCHAR (50)   NULL,
    [ErrorCode]       VARCHAR (10)   NULL,
    [ErrorReason]     VARCHAR (250)  NULL,
    [TransDateYYYYMM] INT            NOT NULL,
    [CRM_PPDID]       NVARCHAR (36)  NULL,
    [UsabilityIndex]  SMALLINT       NULL,
    [MashupOutcome]   TINYINT        NULL,
    [InsertDate]      DATETIME       CONSTRAINT [DF__FileData__Insert__607D3EDD] DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_Result_UsabilityIndex_TransDate]
    ON [USAePay].[FileData]([TransType] ASC, [Result] ASC, [UsabilityIndex] ASC, [TransDate] ASC)
    INCLUDE([CRM_PPDID], [MerchantID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_Result_TransDate_UsabilityIndex]
    ON [USAePay].[FileData]([TransType] ASC, [Result] ASC, [TransDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([CRM_PPDID], [Amount]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_TransDateYYYYMM]
    ON [USAePay].[FileData]([TransType] ASC, [Result] ASC, [UsabilityIndex] ASC, [TransDateYYYYMM] ASC)
    INCLUDE([USAePayID], [MerchantID], [ID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [IX_UsabilityIndex]
    ON [USAePay].[FileData]([UsabilityIndex] ASC)
    INCLUDE([USAePayID], [MerchantID], [TransDate], [TransType], [Result]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_Result_Platform_TransDateYYYYMM_UsabilityIndex]
    ON [USAePay].[FileData]([TransType] ASC, [Result] ASC, [Platform] ASC, [TransDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([CRM_PPDID], [AuthCode], [MerchantID], [Amount], [AccountHolder], [CardType], [MerchantName], [TransDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_Result_UsabilityIndex_TransDateYYYYMM_INC_Amount_CRM_PPDID]
    ON [USAePay].[FileData]([TransType] ASC, [Result] ASC, [UsabilityIndex] ASC, [TransDateYYYYMM] ASC)
    INCLUDE([Amount], [CRM_PPDID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransDate]
    ON [USAePay].[FileData]([TransDate] ASC)
    INCLUDE([SourceID], [CRM_PPDID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_UsabilityIndex_TransDate]
    ON [USAePay].[FileData]([UsabilityIndex] ASC, [TransDate] ASC)
    INCLUDE([SourceID], [USAePayID], [MerchantID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_InsertDate]
    ON [USAePay].[FileData]([InsertDate] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_InsertDateMeSSettlement]
    ON [USAePay].[FileData]([InsertDate] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_FileName]
    ON [USAePay].[FileData]([FileName] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_TransDateYYYYMM_UsabilityIndex]
    ON [USAePay].[FileData]([TransType] ASC, [TransDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([Result], [TransDate], [TransTime], [SourceID], [MerchantName], [MerchantID], [AccountHolder], [Amount], [Invoice], [Description], [AVSZip]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_MerchantID_UsabilityIndex]
    ON [USAePay].[FileData]([TransType] ASC, [MerchantID] ASC, [UsabilityIndex] ASC)
    INCLUDE([Result], [TransDate], [TransTime], [SourceID], [MerchantName], [AccountHolder], [Amount], [Invoice], [Description], [AVSZip]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransType_MerchantID_TransDateYYYYMM_UsabilityIndex_INC_Result_TransDate_TransTime_SourceID_MerchantName_AccountHolder_Amo]
    ON [USAePay].[FileData]([TransType] ASC, [MerchantID] ASC, [TransDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([Result], [TransDate], [TransTime], [SourceID], [MerchantName], [AccountHolder], [Amount], [Invoice], [Description], [AVSZip]) WITH (FILLFACTOR = 80);

