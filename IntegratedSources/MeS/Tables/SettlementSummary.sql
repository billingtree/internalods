﻿CREATE TABLE [MeS].[SettlementSummary] (
    [ID]                    INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReportReferenceCode]   INT           NOT NULL,
    [MerchantID]            NVARCHAR (20) NOT NULL,
    [DBAName]               NVARCHAR (30) NULL,
    [TermNumber]            NVARCHAR (10) NULL,
    [BatchNumber]           VARCHAR (5)   NULL,
    [BatchDate]             DATE          NULL,
    [TransactionDate]       DATE          NOT NULL,
    [CardType]              VARCHAR (5)   NOT NULL,
    [CardNumber]            VARCHAR (50)  NOT NULL,
    [Reference]             VARCHAR (30)  NULL,
    [PurchaseID]            NVARCHAR (50) NULL,
    [AuthorizationCode]     VARCHAR (6)   NULL,
    [EntryMode]             NVARCHAR (50) NOT NULL,
    [TransactionAmount]     MONEY         NOT NULL,
    [TridentTransactionID]  NVARCHAR (35) NULL,
    [ClientReferenceNumber] VARCHAR (30)  NULL,
    [TransDateYYYYMM]       INT           NOT NULL,
    [BatchDateYYYYMM]       INT           NOT NULL,
    [CRM_PPDID]             NVARCHAR (36) NULL,
    [UsabilityIndex]        SMALLINT      NULL,
    [MashupOutcome]         TINYINT       NULL,
    [InsertDate]            DATETIME      CONSTRAINT [DF_MeSSettlementSummary_InsertDate] DEFAULT (getdate()) NOT NULL,
	[BankCode]				VARCHAR(10)	  NOT NULL DEFAULT 'MES'
    CONSTRAINT [PK_SettlementSummary] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NCI_UsabilityIndex]
    ON [MeS].[SettlementSummary]([UsabilityIndex] ASC)
    INCLUDE([MerchantID], [DBAName], [TransactionDate], [AuthorizationCode], [TransactionAmount], [CRM_PPDID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_UsabilityIndexIncluded]
    ON [MeS].[SettlementSummary]([UsabilityIndex] ASC)
    INCLUDE([MerchantID], [DBAName], [BatchDate], [TransactionDate], [AuthorizationCode], [TransactionAmount]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI-MerchantID_TransactionDate_AuthorizationCode_TransactionAmount_UsabilityIndex]
    ON [MeS].[SettlementSummary]([MerchantID] ASC, [TransactionDate] ASC, [AuthorizationCode] ASC, [TransactionAmount] ASC, [UsabilityIndex] ASC)
    INCLUDE([DBAName], [BatchDate]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_TransDateYYYYMM_UsabilityIndex]
    ON [MeS].[SettlementSummary]([TransDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([TransactionDate], [CRM_PPDID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_BatchDateYYYYMM_UsabilityIndex_MerchantID]
    ON [MeS].[SettlementSummary]([BatchDateYYYYMM] ASC, [UsabilityIndex] ASC, [MerchantID] ASC)
    INCLUDE([DBAName], [BatchDate], [TransactionDate], [CardNumber], [Reference], [AuthorizationCode], [TransactionAmount]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_MerchantID_UsabilityIndex]
    ON [MeS].[SettlementSummary]([MerchantID] ASC, [UsabilityIndex] ASC)
    INCLUDE([DBAName], [BatchDate], [TransactionDate], [CardNumber], [Reference], [AuthorizationCode], [TransactionAmount]) WITH (FILLFACTOR = 80);

