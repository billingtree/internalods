﻿








CREATE VIEW [dbo].[vRptQuarantinePrepView]
AS
SELECT         'USAePay'                           AS "Source",
               MerchantName                        AS "Name",
               CONVERT(NVARCHAR(20),USAePayID)     AS "SourceID",
               CONVERT(NVARCHAR(20),MerchantID)    AS "MerchantID",
               'Transaction'                       AS "TransType"
              ,CASE MashupOutcome
    		       WHEN 1 THEN 'Spurious'
			       WHEN 2 THEN 'UnMatched'
			       WHEN 3 THEN 'Matched'
			       WHEN 4 THEN 'Duplicates'
		       END                                 AS "Reason",
               TransDate                           AS InsertDate
               
FROM           USAePay.FileData
WHERE          UsabilityIndex = 0 


UNION ALL

SELECT        'USAePay'                                        AS Source
              ,UDS.MerchantName                                AS Name
              ,UDS.SourceID                                    AS SourceID
              ,UDS.MerchantID                                  AS MerchantID
              ,'Daily Summary'                                 AS TransType
              ,CASE UDS.MashupOutcome
    		       WHEN 1 THEN 'Spurious'
			       WHEN 2 THEN 'UnMatched'
			       WHEN 3 THEN 'Matched'
			       WHEN 4 THEN 'Duplicates'
		       END                                             AS Reason
              ,UDS.TransDate                                   AS InsertDate
               
FROM           USAePay.DailySummary      AS UDS
WHERE          UDS.UsabilityIndex = 0


--------------------------------------------------------
--                Global commented out
--------------------------------------------------------
/*****
UNION ALL

SELECT         'Global'                                    AS "Source",
               MerchantName                                AS "Name",
               ''                                          AS "SourceID",
               CONVERT(NVARCHAR(20),RIGHT(MerchantID,13))  AS "MerchantID",
               'Transaction'                               AS "TransType"
              ,CASE MashupOutcome
    		       WHEN 1 THEN 'Spurious'
			       WHEN 2 THEN 'UnMatched'
			       WHEN 3 THEN 'Matched'
			       WHEN 4 THEN 'Duplicates'
		       END                                         AS "Reason",
               TranDate                                    AS InsertDate
               
FROM           Global.FileDataPersisted
WHERE          UsabilityIndex = 0 
*****/

UNION ALL

SELECT         'MeS'                               AS "Source",
               DBAName                             AS "Name",
               ''                                  AS "SourceID",
               CONVERT(NVARCHAR(20),MerchantID)    AS "MerchantID",
               'Transaction'                       AS "TransType"
              ,CASE MashupOutcome
    		       WHEN 1 THEN 'Spurious'
			       WHEN 2 THEN 'UnMatched'
			       WHEN 3 THEN 'Matched'
			       WHEN 4 THEN 'Duplicates'
		       END                                 AS "Reason",
               TransactionDate                     AS InsertDate
               
FROM           MeS.SettlementSummary
WHERE          UsabilityIndex = 0

UNION ALL

SELECT         'MeS'                               AS "Source",
               DBAName                             AS "Name",
               ''                                  AS "SourceID",
               CONVERT(NVARCHAR(20),MerchantID)    AS "MerchantID",
               'ChargeBack'                        AS "TransType"
              ,CASE MashupOutcome
    		       WHEN 1 THEN 'Spurious'
			       WHEN 2 THEN 'UnMatched'
			       WHEN 3 THEN 'Matched'
			       WHEN 4 THEN 'Duplicates'
		       END                                 AS "Reason",
               TransactionDate                     AS InsertDate
               
FROM           MeS.ChargebackAdjustments
WHERE          UsabilityIndex = 0

UNION ALL
 
SELECT         'ATI'                                      AS "Source",
               ATIOrig.CompanyName                        AS "Name",
               CONVERT(NVARCHAR(20),ATIOrig.ConfigNumber) AS "SourceID",
               CONVERT(NVARCHAR(20),ATIorig.ProcessingNumber)
                                                          AS "MerchantID",
               'Origination'                              AS "TransType",
               ATIOrig.MashupOutcomeDescription           AS "Reason",
               ATIOrig.FilecreationDate                   AS InsertDate
               
FROM           NACHA.Originations ATIOrig
WHERE          ATIOrig.UsabilityIndex = 0
  AND          ATIOrig.SourceType = 4
  
UNION ALL

SELECT         'ATI'                                         AS "Source",
               ATIReturn.CompanyName                         AS "Name",
               CONVERT(NVARCHAR(20),ATIReturn.ConfigNumber)  AS "SourceID",
               CONVERT(NVARCHAR(20),ATIReturn.ProcessingNumber)
                                                             AS "MerchantID",
               'Return'                                      AS "TransType",
               ATIReturn.MashupOutcomeDescription            AS "Reason",
               ATIReturn.FilecreationDate                    AS InsertDate
               
FROM           NACHA.Returns                   AS ATIReturn
WHERE          ATIReturn.UsabilityIndex = 0
  AND          ATIReturn.SourceType = 4                               

UNION ALL

SELECT         'InStep'                                      AS "Source",
               InStepOrig.CompanyName                        AS "Name",
               CONVERT(NVARCHAR(20),InStepOrig.ConfigNumber) AS "SourceID",
               CONVERT(NVARCHAR(20),InStepOrig.ProcessingNumber)
                                                             AS "MerchantID",
               'Origination'                                 AS "TransType",
               InStepOrig.MashupOutcomeDescription           AS "Reason",
               InStepOrig.FilecreationDate                   AS InsertDate
               
FROM           NACHA.Originations InStepOrig
WHERE          InStepOrig.UsabilityIndex = 0
  AND          InStepOrig.SourceType = 1

UNION ALL

SELECT         'InStep'                                        AS "Source",
               InStepReturn.CompanyName                        AS "Name",
               CONVERT(NVARCHAR(20),InStepReturn.ConfigNumber) AS "SourceID",
               CONVERT(NVARCHAR(20),InStepReturn.ProcessingNumber)
                                                               AS "MerchantID",
               'Return'                                        AS "TransType",
               InStepReturn.MashupOutcomeDescription           AS "Reason",
               InStepReturn.FilecreationDate                   AS InsertDate
               
FROM           NACHA.Returns InStepReturn
WHERE          InStepReturn.UsabilityIndex = 0
  AND          InStepReturn.SourceType = 1


UNION ALL
 
SELECT         'ATI API'                                  AS "Source",
               'Name Not Provided'                        AS "Name",
               CONVERT(NVARCHAR(20),ATIAPI.ConfigNumber)  AS "SourceID",
               CONVERT(NVARCHAR(20),ATIAPI.AccountID)     AS "MerchantID",
               'Transaction'                              AS "TransType",
               CASE
	               WHEN MashupOutcome = 2 THEN
		               'Not Found'
                   WHEN MashupOutcome = 3 THEN
		               'Match'
                   WHEN MashupOutcome = 4 THEN
		              'Duplicates'
                   ELSE
		               'Unknown'
               END                                       AS "Reason",
               ATIAPI.BatchDate                          AS InsertDate
               
FROM           Billing.ACHTransByBatchDate ATIAPI
WHERE          ATIAPI.UsabilityIndex = 0
  
UNION ALL

SELECT         'ATI API'                                  AS "Source",
               'Name Not Provided'                        AS "Name",
               CONVERT(NVARCHAR(20),ATIAPI.ConfigNumber)  AS "SourceID",
               CONVERT(NVARCHAR(20),ATIAPI.AccountID)     AS "MerchantID",
               'Return'                                   AS "TransType",
               CASE
	               WHEN MashupOutcome = 2 THEN
		               'Not Found'
                   WHEN MashupOutcome = 3 THEN
		               'Match'
                   WHEN MashupOutcome = 4 THEN
		              'Duplicates'
                   ELSE
		               'Unknown'
               END                                        AS "Reason",
               ATIAPI.ReturnDate                          AS InsertDate
               
FROM           Billing.ACHReturnsByReturnDate ATIAPI
WHERE          ATIAPI.UsabilityIndex = 0
      








