﻿











-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spMIDsWithRevenuePartnersOnly](
 @Year       INT
,@Month      INT
,@ToYear     INT
,@ToMonth    INT
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
SET NOCOUNT ON; 

    DECLARE @vYear            VARCHAR(4)
    DECLARE @vMonth           VARCHAR(2)
    DECLARE @vToYear          VARCHAR(4)
    DECLARE @vToMonth         VARCHAR(2)
    DECLARE @vFromDate        DATE
    DECLARE @vToDate          DATE
    
    SET @vYear = CONVERT(VARCHAR, @Year)
    SET @vMonth = CONVERT(VARCHAR, @Month)
    SET @vFromDate = CONVERT(DATE, CONCAT(@vYear, '-', @vMonth, '-', '01'))
    
    SET @vToYear = CONVERT(VARCHAR, @ToYear)
    SET @vToMonth = CONVERT(VARCHAR, @ToMonth) 
    SET @vToDate = CONVERT(DATE, CONCAT(@vToYear, '-', @vToMonth, '-', '01'))  
    SET @vToDate = EOMONTH(@vToDate) 

SELECT '*** All Customers'                          AS PartnerName
UNION ALL
SELECT DISTINCT PartProd.PartnerName            AS PartnerName

FROM 
    (
     SELECT DISTINCT AllRevSources.CRM_Id
     FROM (
			 SELECT DISTINCT 
					CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
			 FROM MeSRevenueMonthlyGLFeed
			 WHERE TranDate BETWEEN @vFromDate AND @vToDate
			 UNION ALL
			 SELECT DISTINCT 
					CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
			 FROM ATIDailyRevenueGLFeed
			 WHERE TranDate BETWEEN @vFromDate AND @vToDate
			 UNION ALL
			 SELECT DISTINCT 
					CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
			 FROM USAePayRevenueMonthlyGLFeed
			 WHERE TranDate BETWEEN @vFromDate AND @vToDate
           )	 AllRevSources	            
    )                                         AS Rev   
JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP         ON Rev.CRM_Id = PP.bt_ProcessingProfileId  
JOIN [$(CRMShadow)].MS.bt_ProcessingAccount        AS PA         ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId          
JOIN CRM.vPartnerWithAllSubTypes              AS PartProd   ON PA.bt_CurrentServiceOrder = PartProd.ServiceOrderId

ORDER BY 1

END












