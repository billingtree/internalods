﻿


--drop view [dbo].[vCRMSetupDateByMerchantID]

CREATE VIEW [dbo].[vCRMSetupDateByMerchantID]
AS
SELECT DISTINCT 
       SFP.Id                            AS "CRM_ID"
      ,SFP.CC_Merchant_Number__c         AS "MerchantID"
      ,SFP.CC_Source_ID__c               AS "SourceID"
      ,SFA.Existing_Clients_Profile__c   AS "Parent"
      ,SFA.Name                          AS "Child"
      ,CONVERT(DATE, PSH.SetupDate)      AS "SetupDate"
      ,SFP.Application_Stage__c          AS "Stage"
      ,SFP.Estimated_Monthly_Transactions__c
                                         AS "EstMonthCount"
      ,SFP.Estimated_Monthly_Volume__c   AS "EstMonthVolume"  
      ,' '                               AS "MultipleSetupDates" 
FROM  dbo.SetupDateByMID                PSH 
JOIN  SalesForce.PaymentProcessing      SFP   ON SFP.Id = PSH.CRM_PPDID
                                          AND SFP.IsDeleted = 0
                                          AND SFP.CC_Merchant_Number__c IS NOT NULL
                                          AND SFP.Application_Stage__c <> 'Inactive'
                                          --  This logic is to remove the Buckeye 
                                          --  Accounts that are Complete, but
                                          --  have no Source ID because they 
                                          --  process without using the Gateway,
                                          --  i.e. USAePay, but we want to leave 
                                          --  in those Accounts that are still
                                          --  in Setup or Pass Back - Setup
                                          AND (SFP.CC_Source_ID__c IS NOT NULL
                                                       OR 
                                               (SFP.CC_Source_ID__c IS NULL
                                                        AND 
                                                SFP.Application_Stage__c <> 'Complete'))                
JOIN SalesForce.Account                 SFA   ON SFA.Id = SFP.Account__c
                                          AND SFA.Existing_Clients_Profile__c <> 'BillingTree'

WHERE YEAR(PSH.SetupDate) > 2012



