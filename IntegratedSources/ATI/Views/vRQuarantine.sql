﻿





CREATE VIEW [ATI].[vRQuarantine] AS
SELECT	R1.ReferenceCode
		,R1.ConfigNumber
		,R1.FileCreationDate
		,R1.FileCreationDateYYYYMM
		,R1.FileCreationTime
		,R5.ServiceClassCode
		,R5.CompanyName
		,R5.CompanyIdentification
		,R5.StandardEntryClassCode
		,R5.EffectiveEntryDate
		,R6.*
		,CASE R6.TransactionCode
			WHEN 22 THEN CONVERT(numeric(16,2), R6.Amount)/100
			WHEN 23 THEN CONVERT(numeric(16,2), R6.Amount)/100
			WHEN 24 THEN CONVERT(numeric(16,2), R6.Amount)/100       
			WHEN 32 THEN CONVERT(numeric(16,2), R6.Amount)/100
			WHEN 33 THEN CONVERT(numeric(16,2), R6.Amount)/100           
			WHEN 34 THEN CONVERT(numeric(16,2), R6.Amount)/100       
			WHEN 27 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1
			WHEN 28 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1
			WHEN 29 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1       
			WHEN 37 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1
			WHEN 38 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1
			WHEN 39 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1              
			ELSE CONVERT(numeric(16,2), R6.Amount)/100     
		END AS FormattedAmount
		,CASE R6.MashupOutcome
			WHEN 0 THEN 'Matched'
			WHEN 1 THEN 'CRM Duplicates'
			WHEN 2 THEN 'Concordance Not Found'
			WHEN 3 THEN 'CRM Not Found'
		END AS MashupOutcomeDescription
FROM	(
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6ARCEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6CCDEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6COREntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyNameORIDNumber
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6CTXEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6PPDEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6RCKEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6TELEntryDetail
			WHERE	UsabilityIndex = 0
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6WEBEntryDetail
			WHERE	UsabilityIndex = 0
		) AS R6
			LEFT JOIN ATI.vR5BlockHeader AS R5
				ON R6.FileReferenceCodeID = R5.FileReferenceCodeID
				AND R5.BlockNumber=R6.BlockNumber
			LEFT JOIN ATI.vR1FileHeader AS R1
				ON R1.FileReferenceCodeID = R5.FileReferenceCodeID
