﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spPayRazrToODSReport](
@RunYear   INT,
@RunMonth  INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @YearMonth INT
    DECLARE @txtYear   VARCHAR (4)
    DECLARE @txtMonth  VARCHAR (2)
    
 
    SET @txtYear = CONVERT(VARCHAR (4), @RunYear)
    IF @RunMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @RunMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @RunMonth) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))   
     

    -- Insert statements for procedure here
    
SELECT MIDs.Name           AS Name
      ,MIDs.MID            AS MID
      ,MIDs.SourceId       AS SourceId
      ,MIDs.TranYYYYMM     AS TranYYYYMM
      ,Sales.TranCount     AS SaleCount
      ,Sales.Volume        AS SaleVolume
      ,Credits.TranCount   AS CreditCount
      ,Credits.Volume      AS CreditVolume
      ,Declines.TranCount  AS DeclineCount
      ,Declines.Volume     AS DeclineVolume
      ,ISNULL(Sales.TranCount, 0) +
       ISNULL(Credits.TranCount, 0) +
       ISNULL(Declines.TranCount, 0)  AS TotalCount
      ,ISNULL(Sales.Volume, 0) -
       ISNULL(Credits.Volume, 0)      AS TotalVolume       
FROM (	
    ---  Get a list of MIDs for the Month from PayRazr
	SELECT DISTINCT 
	       USA.MerchantName           AS Name
	      ,USA.MerchantID             AS MID  
		  ,USA.USAePayID              AS SourceId
		  ,USA.TransDateYYYYMM        AS TranYYYYMM  
   
	FROM USAePay.FileData      AS USA
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND LOWER(USA.FileName) LIKE 'payrazr%'

     )    AS MIDs	
     
LEFT JOIN (  
    ---  Get a list of Sales for the Month from PayRazr       
     SELECT USA.MerchantID                    AS MID  
           ,USA.USAePayID                     AS SourceId
           ,USA.TransDateYYYYMM               AS TranYYYYMM  
           ,'Sale'                            AS TranType
           ,COUNT(*)                          AS TranCount  
           ,SUM(USA.Amount)                   AS Volume 
	FROM USAePay.FileData      AS USA
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND LOWER(USA.FileName) LIKE 'payrazr%'
	  AND USA.TransType = 'S'
	  AND USA.Result = 'A'
	GROUP BY USA.MerchantID  
			,USA.USAePayID 
			,USA.TransDateYYYYMM	
         )   AS Sales                  ON MIDs.MID = Sales.MID
                                      AND MIDs.SourceId = Sales.SourceId
                                      AND MIDs.TranYYYYMM = Sales.TranYYYYMM	
                                     
LEFT JOIN (   
    ---  Get a list of Credits for the Month from PayRazr      
     SELECT USA.MerchantID                    AS MID  
           ,USA.USAePayID                     AS SourceId
           ,USA.TransDateYYYYMM               AS TranYYYYMM  
           ,'Credit'                          AS TranType
           ,COUNT(*)                          AS TranCount 
           ,SUM(USA.Amount)                   AS Volume    
	FROM USAePay.FileData      AS USA
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND LOWER(USA.FileName) LIKE 'payrazr%'
	  AND USA.TransType = 'C'
	  AND USA.Result = 'A'
	GROUP BY USA.MerchantID  
			,USA.USAePayID 
			,USA.TransDateYYYYMM	
         )   AS Credits                 ON MIDs.MID = Credits.MID
                                       AND MIDs.SourceId = Credits.SourceId
                                       AND MIDs.TranYYYYMM = Credits.TranYYYYMM	     

LEFT JOIN ( 
    ---  Get a list of Sales and Credits that were Declines for the Month from PayRazr        
     SELECT USA.MerchantID                    AS MID  
           ,USA.USAePayID                     AS SourceId
           ,USA.TransDateYYYYMM               AS TranYYYYMM  
           ,'Decline'                         AS TranType
           ,COUNT(*)                          AS TranCount 
           ,SUM(USA.Amount)                   AS Volume    
	FROM USAePay.FileData      AS USA
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND LOWER(USA.FileName) LIKE 'payrazr%'
	  AND USA.TransType IN ('S','C')
	  AND USA.Result = 'D'
	GROUP BY USA.MerchantID  
			,USA.USAePayID 
			,USA.TransDateYYYYMM	
         )   AS Declines                ON MIDs.MID = Declines.MID
                                       AND MIDs.SourceId = Declines.SourceId
                                       AND MIDs.TranYYYYMM = Declines.TranYYYYMM	     

END
















