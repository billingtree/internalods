﻿CREATE TABLE [dbo].[RevenueUMS20082010] (
    [TranDate]           DATETIME       NULL,
    [TranDateYYYYMM]     NVARCHAR (255) NULL,
    [MID]                NVARCHAR (255) NULL,
    [Name]               NVARCHAR (255) NULL,
    [Status]             MONEY          NULL,
    [Volume]             FLOAT (53)     NULL,
    [Disc Inc]           NVARCHAR (255) NULL,
    [Surchg Inc]         NVARCHAR (255) NULL,
    [20% Surchg Exp]     NVARCHAR (255) NULL,
    [IC]                 NVARCHAR (255) NULL,
    [Discount Income]    NVARCHAR (255) NULL,
    [Trans Nbr]          NVARCHAR (255) NULL,
    [Trans Income]       NVARCHAR (255) NULL,
    [DurbinVolume]       NVARCHAR (255) NULL,
    [DurbinNbr]          NVARCHAR (255) NULL,
    [Durbin Disc Inc]    NVARCHAR (255) NULL,
    [Durbin IC Exp]      NVARCHAR (255) NULL,
    [Durbin Disc Income] NVARCHAR (255) NULL,
    [Batch Income]       NVARCHAR (255) NULL,
    [Total Income]       FLOAT (53)     NULL,
    [Revenue]            FLOAT (53)     NULL
);

