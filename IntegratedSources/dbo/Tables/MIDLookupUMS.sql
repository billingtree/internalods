﻿CREATE TABLE [dbo].[MIDLookupUMS] (
    [MID]             FLOAT (53)     NULL,
    [CRMParent]       NVARCHAR (255) NULL,
    [DBA]             NVARCHAR (255) NULL,
    [Legal Name]      NVARCHAR (255) NULL,
    [Date Open]       DATETIME       NULL,
    [UMS Date Closed] DATETIME       NULL,
    [CRM Closed Date] NVARCHAR (255) NULL,
    [On Cancel List]  NVARCHAR (255) NULL,
    [StartYYYYMM]     FLOAT (53)     NULL,
    [EndYYYYMM]       FLOAT (53)     NULL
);

