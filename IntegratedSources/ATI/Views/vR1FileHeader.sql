﻿
CREATE VIEW [ATI].[vR1FileHeader] WITH SCHEMABINDING AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,2) AS PriorityCode
		,SUBSTRING(SourceRecord,4,10) AS ImmediateDestination
		,SUBSTRING(SourceRecord,14,10) AS ImmediateOrigin
		,CONVERT(date,SUBSTRING(SourceRecord,24,6),121) AS FileCreationDate
		,CONVERT(time(0),SUBSTRING(SourceRecord,30,2)+':'+SUBSTRING(SourceRecord,32,2),108) AS FileCreationTime--SUBSTRING(SourceRecord,30,4) AS FileCreationTime
		,SUBSTRING(SourceRecord,34,1) AS FileIDModifier
		,SUBSTRING(SourceRecord,35,3) AS RecordSize
		,SUBSTRING(SourceRecord,38,2) AS BlockingFactor
		,SUBSTRING(SourceRecord,40,1) AS FormatCode
		,SUBSTRING(SourceRecord,41,23) AS ImmediateDestinationName
		,SUBSTRING(SourceRecord,64,23) AS ImmediateOriginName
		,SUBSTRING(SourceRecord,87,8) AS ReferenceCode
		,CONVERT(int,'20'+SUBSTRING(SourceRecord,24,4)) AS FileCreationDateYYYYMM
		,CRM_PPDID
		,InsertDate
FROM	ATI.ReturnFileData
WHERE	RecordType = 1;
