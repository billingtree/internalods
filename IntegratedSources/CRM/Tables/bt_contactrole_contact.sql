﻿CREATE TABLE [CRM].[bt_contactrole_contact] (
    [bt_contactrole_contactid] UNIQUEIDENTIFIER NULL,
    [bt_contactroleid]         UNIQUEIDENTIFIER NULL,
    [contactid]                UNIQUEIDENTIFIER NULL,
    [versionnumber]            BIGINT           NULL,
    [InsertDate]               SMALLDATETIME    DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_bt_contactroleid]
    ON [CRM].[bt_contactrole_contact]([bt_contactroleid] ASC)
    INCLUDE([contactid]) WITH (FILLFACTOR = 80);

