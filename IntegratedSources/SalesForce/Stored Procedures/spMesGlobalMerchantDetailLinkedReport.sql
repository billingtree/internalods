﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spMesGlobalMerchantDetailLinkedReport](
@Month       INT,
@Year        INT,
@MerchantID  FLOAT,
@Source      VARCHAR (6),
@Mode        CHAR
)

-- Insert statements for procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vMode        CHAR (1)
    SET @vMode = @Mode
    
    DECLARE @vSource      VARCHAR (6)
    SET @vSource = @Source    
    
  	DECLARE @vMonth int, @vYear int, @vMerchantID FLOAT
	SET @vMonth=@Month
	SET @vYear=@Year
	SET @vMerchantID=@MerchantID

    
    -- Concatenate Year and Month in order to use the Index
    DECLARE @vYearMonth INT
    DECLARE @txtYear   VARCHAR (4)
    DECLARE @txtMonth  VARCHAR (2)
    
 
    SET @txtYear = CONVERT(VARCHAR (4), @vYear)
    IF @vMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @vMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @vMonth) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))       

-------------------------------------------------------------------------------
--   This is MeS and Global detail records for a given Merchant and Month    --                                   --
-------------------------------------------------------------------------------
  BEGIN
      IF @vMode = 'T'   --   Get by Transaction Date  
          IF @vSource = 'Global'     -- Get Global here
              SELECT G.MerchantID                      AS "MerchantID", 
                     G.MerchantName                    AS "DBAName", 
                     G.TranDate                        AS "TransactionDate",
                     G.TranTime                        AS "TransactionTime",
                     G.Amount                          AS "TransactionAmount", 
                     G.CardType                        AS "CardType", 
                     G.CardHolderNumber                AS "CardNumber",
                     G.AuthCode                        AS "AuthorizationCode", 
                     G.DepositDate                     AS "BatchNumber"
             FROM Global.FileDataPersisted G
             WHERE G.TransDateYYYYMM = @vYearMonth
               AND G.UsabilityIndex = 1
       --   The incoming Merchant ID is the Sales Force Merchant ID  
       --   and therefore needs to be Substringed in this way
              and (
              RIGHT(CONVERT(NUMERIC (16,0), G.MerchantID),13) = 
               LEFT(CONVERT(NUMERIC (16,0), @vMerchantID),13)
                              OR
               RIGHT(CONVERT(NUMERIC (16,0), G.MerchantID),11) = 
               RIGHT(CONVERT(NUMERIC (16,0), @vMerchantID),11)  
                  )                       
                             
         ELSE                       -- Get MeS here 
             SELECT MeS.MerchantID                     AS "MerchantID", 
                    MeS.DBAName                        AS "DBAName", 
                    MeS.TransactionDate                AS "TransactionDate",
                    ''                                 AS "TransactionTime",
                    MeS.TransactionAmount              AS "TransactionAmount", 
                    MeS.CardType                       AS "CardType", 
                    MeS.CardNumber                     AS "CardNumber",
                    MeS.AuthorizationCode              AS "AuthorizationCode", 
                    MeS.BatchDate                      AS "BatchNumber"
             FROM MeS.SettlementSummary MeS
             WHERE MeS.TransDateYYYYMM = @vYearMonth
               AND MeS.UsabilityIndex = 1
               AND CONVERT(NUMERIC (16,0), MeS.MerchantID) = CONVERT(NUMERIC (16,0), @vMerchantID)


        ELSE          --   Get by Batch Date (Deposit Date)


          IF @vSource = 'Global'       --  Get Global here
              SELECT G.MerchantID                      AS "MerchantID", 
                     G.MerchantName                    AS "DBAName", 
                     G.TranDate                        AS "TransactionDate",
                     G.TranTime                        AS "TransactionTime",
                     G.Amount                          AS "TransactionAmount", 
                     G.CardType                        AS "CardType", 
                     G.CardHolderNumber                AS "CardNumber",
                     G.AuthCode                        AS "AuthorizationCode", 
                     G.DepositDate                     AS "BatchNumber"
             FROM Global.FileDataPersisted G
             WHERE G.DepositDateYYYYMM = @vYearMonth
              AND G.UsabilityIndex = 1
       --   The incoming Merchant ID is the Sales Force Merchant ID  
       --   and therefore needs to be Substringed in this way
              and (
              RIGHT(CONVERT(NUMERIC (16,0), G.MerchantID),13) = 
               LEFT(CONVERT(NUMERIC (16,0), @vMerchantID),13)
                              OR
               RIGHT(CONVERT(NUMERIC (16,0), G.MerchantID),11) = 
               RIGHT(CONVERT(NUMERIC (16,0), @vMerchantID),11)  
                  )                       
                             
         ELSE                       -- Get MeS here     
             SELECT MeS.MerchantID                     AS "MerchantID", 
                    MeS.DBAName                        AS "DBAName", 
                    MeS.TransactionDate                AS "TransactionDate",
                    ''                                 AS "TransactionTime",
                    MeS.TransactionAmount              AS "TransactionAmount", 
                    MeS.CardType                       AS "CardType", 
                    MeS.CardNumber                     AS "CardNumber",
                    MeS.AuthorizationCode              AS "AuthorizationCode", 
                    MeS.BatchDate                      AS "BatchNumber"
             FROM MeS.SettlementSummary MeS
             WHERE MeS.BatchDateYYYYMM = @vYearMonth
               AND MeS.UsabilityIndex = 1
               AND CONVERT(NUMERIC (16,0), MeS.MerchantID) = CONVERT(NUMERIC (16,0), @vMerchantID)


  END

END
