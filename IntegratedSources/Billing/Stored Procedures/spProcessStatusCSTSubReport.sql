﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessStatusCSTSubReport](
@PRLID          INT,
@IssueType      NVARCHAR (50)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vPRLID          INT
    DECLARE @vIssueType      NVARCHAR (50)
    SET @vPRLID = @PRLID   
    SET @vIssueType = RTRIM(@IssueType)

    -- Insert statements for procedure here

--------------------------------------------------------------------------------
--         This 1 Issue is handled here
--------------------------------------------------------------------------------
    --IF @vIssueType = 'Missing Customer eMail Contacts'
    
    
		SELECT DISTINCT PSL.PSL_RunDateYYYYMM
			  ,PRT.PRT_Name
			  ,PSL.PSL_Status
			  ,PSL.PSL_InsertDateTime
			  ,PRL.PRL_StartDateTime
			  ,PRL.PRL_EndDateTime
			  ,PIT.PIT_Category
			  ,PIT.PIT_Description
			  ,PIT.PIT_Name
			  ,CST.CST_AccountNumber
			  ,CST.CST_AccountName
			  ,CPP.CPP_SourceStep
		FROM Billing.PSL_ProcessStatusLog                AS PSL
		JOIN Billing.PRT_ProcessRunType                  AS PRT   ON PSL.PSL_PRT_ID = PRT.PRT_ID
		JOIN Billing.PRL_ProcessRunList                  AS PRL   ON PSL.PSL_ID = PRL.PRL_PSL_ID
		JOIN Billing.PIL_ProcessIssueList                AS PIL   ON PRL.PRL_ID = PIL.PIL_PRL_ID
		JOIN Billing.PIT_ProcessIssueType                AS PIT   ON PIL.PIL_PIT_ID = PIT.PIT_ID
		LEFT JOIN Billing.PED_ProcessErrorDetail         AS PED   ON PIL.PIL_ID = PED.PED_PIL_ID
		LEFT JOIN Billing.CST_Customer                   AS CST   ON PED.PED_CST_ID = CST.CST_ID
		LEFT JOIN Billing.CPA_CustomerProcessingAccount  AS CPA   ON CST.CST_ID = CPA.CPA_CST_ID
		LEFT JOIN Billing.CPP_CustomerProcessingProfile  AS CPP   ON CPA.CPA_ID = CPP.CPP_CPA_ID
		WHERE PRL.PRL_ID = @vPRLID 
		  AND PIT.PIT_Name = @vIssueType
		  
		  
	OPTION (OPTIMIZE FOR UNKNOWN)--(RECOMPILE)
	
	
	
END






