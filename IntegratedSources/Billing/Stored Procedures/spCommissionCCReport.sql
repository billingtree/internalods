﻿-- =============================================
-- Author:        Rick Stohle
-- Create date:   Oct-2015
-- Modified:      22-Dec-2015
-- Description:   Create this Interim Commission Report for Finance
--                On 22-Dec-2015 the change was to now report on Fees
-- =============================================
CREATE PROCEDURE [Billing].[spCommissionCCReport] ( @YearMonth INT )

-- Insert statements for procedure here
AS
    BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @RunYearMonth INT;
        SET @RunYearMonth = @YearMonth;




        SELECT  CommissionUniverse.CPA_ID AS CPA_ID ,
                CommissionUniverse.CPP_ID AS CPP_ID ,
                CommissionUniverse.Merchant AS Merchant ,
                CommissionUniverse.MID AS MID ,
                CASE WHEN CommissionUniverse.SourceIdConfig IS NOT NULL
                     THEN CommissionUniverse.SourceIdConfig
                     ELSE ZID.bt_ZID
                END AS SourceIdConfig ,
                CommissionUniverse.ManualBillYN ,
                ISNULL(SalesCount.TotalCount, 0) AS Sales ,
                ISNULL(CreditCount.TotalCount, 0) AS Credits ,
                ISNULL(DeclineCount.TotalCount, 0) AS Declines ,
                CommissionUniverse.FeeAmount AS TranFee ,
                ISNULL(SalesCount.TotalCount, 0)
                * CommissionUniverse.FeeAmount AS SalesRevenue ,
                ISNULL(CreditCount.TotalCount, 0)
                * CommissionUniverse.FeeAmount AS CreditsRevenue ,
                ISNULL(DeclineCount.TotalCount, 0)
                * CommissionUniverse.FeeAmount AS DeclinesRevenue ,
                ( ( ISNULL(SalesCount.TotalCount, 0)
                    * CommissionUniverse.FeeAmount )
                  + ( ISNULL(CreditCount.TotalCount, 0)
                      * CommissionUniverse.FeeAmount )
                  + ( ISNULL(DeclineCount.TotalCount, 0)
                      * CommissionUniverse.FeeAmount ) ) AS TotalRevenue
        FROM    (
		----------------------------------------------------------------------------------------------------------------------
		--   The Commission Report for Credit Card needs to supply the Sale Count, the Credit Count and the Decline Count  
		--   for each Processing Profile, along with a Single CRM Gateway Submission Fee (ignore multiple Fees).
		--   This means, we can only have 1 Record per SourceID in the Commission Report, even though 2 would be Correct 
		--   Things to Consider:
		--       1) In the ARI table, we have collapsed all 3 Counts into 1 record (CDT has the 3 Details)
		--       2) In the ARI table, we can have multiple Gateway Submission Fees in effect (daily changing fees)
		--       3) The ARI table has a Foreign Key reference to CPA
		--       4) The multiple fees in ARI (as well as CTF) are still tied to this 1 CPA record
		--       5) We are to choose the Most Recent Fee in effect  
		--       6) The Commission Report is to also include Manually Billed Customers
		--       7) ARI Does NOT Include Manually Billed Customers, CDT does though
		--       8) Since Manually Billed Fees are not verified during the Billing Process, Only Counts WERE reported,
		--          but on 22-Dec-2015 we changed this to now report the Most Recent Fee for Manually Billed too
        --   It is understood then:
		--       1) The ARI does not give us all we need in order to produce the report, as we need to go to CDT
		--          for Detail Counts and for Manually Billed Customer Counts in CDT
		--       2) The ARI can give us the Most Recent Gateway Submission Fee in effect
		--       3) We do not need the Gateway Submission Fee for Manually Billed Customers
		--   Therefore: 
		--       1) We have chosen to drive through ARI (with single fee) to CPA, to CPP to CDT for Non-Manual Bill
		--       2) And, For Manual Bill, drive through CDT to CPP to CPA for just Counts, and then to CTF for Most Recent Fee 
		--       3) This avoids resolving the single Gateway Submission Fee using CTF
		----------------------------------------------------------------------------------------------------------------------
                  SELECT DISTINCT
                            OneRateUniverse.RunDateYYYYMM AS RunDateYYYYMM ,
                            OneRateUniverse.CPA_ID AS CPA_ID ,
                            OneRateUniverse.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            OneRateUniverse.Merchant AS Merchant ,
                            OneRateUniverse.ProductLine AS ProductLine ,
                            OneRateUniverse.ManualBillYN AS ManualBillYN ,
                            OneRateUniverse.CPP_ID AS CPP_ID ,
                            OneRateUniverse.MID AS MID ,
                            OneRateUniverse.SourceIdConfig AS SourceIdConfig ,
                            OneRateUniverse.FeeType AS FeeType  -- This is the Gateway Submission Fee
                            ,
                            OneRateUniverse.FeeAmount AS FeeAmount
                  FROM      ( SELECT    ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                                        ARI.ARI_CPA_ID AS CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName AS Merchant ,
                                        CPA.CPA_ProductLine AS ProductLine ,
                                        CPA.CPA_ManualBillYN AS ManualBillYN ,
                                        CPP.CPP_ID AS CPP_ID ,
                                        CPP.CPP_MID AS MID ,
                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                        'BillingTree Gateway Submission Fee' AS FeeType ,
                                        MAX(ARI.ARI_TranDate) AS StartDate ,
                                        MAX(ARI.ARI_UnitPrice) AS FeeAmount ,
                                        SUM(ARI.ARI_Quantity) AS TotalCount
                              FROM      Billing.ARI_AccountReceivableInvoice
                                        AS ARI
                                        JOIN Billing.CPA_CustomerProcessingAccount
                                        AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                                        JOIN Billing.CPP_CustomerProcessingProfile
                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                  AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                              WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                                        AND ARI.ARI_ItemNumber = 'CCTRANS'
                                        AND CPA.CPA_ProductLine = 'Credit Card'
                                        AND CPA.CPA_ManualBillYN = 'N'
                                        AND CPA.CPA_DoNotBillYN = 'N'
                              GROUP BY  ARI.ARI_RunDateYYYYMM ,
                                        ARI.ARI_CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName ,
                                        CPA.CPA_ProductLine ,
                                        CPA.CPA_ManualBillYN ,
                                        CPP.CPP_ID ,
                                        CPP.CPP_MID ,
                                        CPP.CPP_SourceIdConfig
                              UNION ALL
                              SELECT    CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID AS CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName AS Merchant ,
                                        CPA.CPA_ProductLine AS ProductLine ,
                                        CPA.CPA_ManualBillYN AS ManualBillYN ,
                                        CPP.CPP_ID AS CPP_ID ,
                                        CPP.CPP_MID AS MID ,
                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                        'BillingTree Gateway Subnmission Fee' AS FeeType ,
                                        TranFee.LatestStartDate AS StartDate ,
                                        TranFee.RecentFee AS FeeAmount ,
                                        SUM(CDT.CDT_TranCount) AS TotalCount
                              FROM      Billing.CDT_CustomerDailyTran AS CDT
                                        JOIN Billing.CPP_CustomerProcessingProfile
                                        AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                        JOIN Billing.CPA_CustomerProcessingAccount
                                        AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                                        LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                                            'TranFee' AS FeeType ,
                                                            CTF_FeeAmount AS RecentFee ,
                                                            MAX(CTF_StartDate) AS LatestStartDate
                                                    FROM    Billing.CTF_CustomerTranFee
                                                    WHERE   CTF_RunDateYYYYMM = @RunYearMonth
                                                            AND CTF_FeeType = 'BillingTree Gateway Submission Fee'
                                                    GROUP BY CTF_CPA_ID ,
                                                            CTF_FeeAmount
                                                  ) AS TranFee ON CPA.CPA_ID = TranFee.CPA_ID
                              WHERE     CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                        AND CDT.CDT_TranType IN ( 'Debit',
                                                              'Credit',
                                                              'Decline' )
                                        AND CPA.CPA_ProductLine = 'Credit Card'
                                        AND CPA.CPA_ManualBillYN = 'Y'
                              GROUP BY  CDT.CDT_RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName ,
                                        CPA.CPA_ProductLine ,
                                        CPA.CPA_ManualBillYN ,
                                        CPP.CPP_ID ,
                                        CPP.CPP_MID ,
                                        CPP.CPP_SourceIdConfig ,
                                        TranFee.LatestStartDate ,
                                        TranFee.RecentFee
                            ) AS OneRateUniverse
                ) AS CommissionUniverse
                LEFT JOIN -- Get Sales Count for Regular and Manuall Billed customers
                ( SELECT    OneRateUniverse.RunDateYYYYMM AS RunDateYYYYMM ,
                            OneRateUniverse.CPA_ID AS CPA_ID ,
                            OneRateUniverse.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            OneRateUniverse.Merchant AS Merchant ,
                            OneRateUniverse.ProductLine AS ProductLine ,
                            OneRateUniverse.ManualBillYN AS ManualBillYN ,
                            OneRateUniverse.CPP_ID AS CPP_ID ,
                            OneRateUniverse.MID AS MID ,
                            OneRateUniverse.SourceIdConfig AS SourceIdConfig ,
                            NULL AS FeeType ,
                            NULL AS FeeAmount ,
                            OneRateUniverse.TotalCount AS TotalCount
                  FROM      ( SELECT    CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID AS CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName AS Merchant ,
                                        CPA.CPA_ProductLine AS ProductLine ,
                                        CPA.CPA_ManualBillYN AS ManualBillYN ,
                                        CDT.CDT_CPP_ID AS CPP_ID ,
                                        CPP.CPP_MID AS MID ,
                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                        SUM(CDT.CDT_TranCount) AS TotalCount
                              FROM      Billing.CDT_CustomerDailyTran AS CDT
                                        JOIN Billing.CPP_CustomerProcessingProfile
                                        AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                        JOIN Billing.CPA_CustomerProcessingAccount
                                        AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                              WHERE     CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                        AND CDT.CDT_TranType = 'Debit'
                                        AND CPA.CPA_ProductLine = 'Credit Card'
                                        AND CPA.CPA_DoNotBillYN <> 'Y'
                              GROUP BY  CDT.CDT_RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName ,
                                        CPA.CPA_ProductLine ,
                                        CPA.CPA_ManualBillYN ,
                                        CDT.CDT_CPP_ID ,
                                        CPP.CPP_MID ,
                                        CPP.CPP_SourceIdConfig
                            ) AS OneRateUniverse
                ) AS SalesCount ON CommissionUniverse.CPP_ID = SalesCount.CPP_ID
                LEFT JOIN -- Get Credit Count for Regular and Manuall Billed customers 
                ( SELECT    OneRateUniverse.RunDateYYYYMM AS RunDateYYYYMM ,
                            OneRateUniverse.CPA_ID AS CPA_ID ,
                            OneRateUniverse.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            OneRateUniverse.Merchant AS Merchant ,
                            OneRateUniverse.ProductLine AS ProductLine ,
                            OneRateUniverse.ManualBillYN AS ManualBillYN ,
                            OneRateUniverse.CPP_ID AS CPP_ID ,
                            OneRateUniverse.MID AS MID ,
                            OneRateUniverse.SourceIdConfig AS SourceIdConfig ,
                            NULL AS FeeType ,
                            NULL AS FeeAmount ,
                            OneRateUniverse.TotalCount AS TotalCount
                  FROM      ( SELECT    CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID AS CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName AS Merchant ,
                                        CPA.CPA_ProductLine AS ProductLine ,
                                        CPA.CPA_ManualBillYN AS ManualBillYN ,
                                        CDT.CDT_CPP_ID AS CPP_ID ,
                                        CPP.CPP_MID AS MID ,
                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                        SUM(CDT.CDT_TranCount) AS TotalCount
                              FROM      Billing.CDT_CustomerDailyTran AS CDT
                                        JOIN Billing.CPP_CustomerProcessingProfile
                                        AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                        JOIN Billing.CPA_CustomerProcessingAccount
                                        AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                              WHERE     CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                        AND CDT.CDT_TranType = 'Credit'
                                        AND CPA.CPA_ProductLine = 'Credit Card'
                                        AND CPA.CPA_DoNotBillYN <> 'Y'
                              GROUP BY  CDT.CDT_RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName ,
                                        CPA.CPA_ProductLine ,
                                        CPA.CPA_ManualBillYN ,
                                        CDT.CDT_CPP_ID ,
                                        CPP.CPP_MID ,
                                        CPP.CPP_SourceIdConfig
                            ) AS OneRateUniverse
                ) AS CreditCount ON CommissionUniverse.CPP_ID = CreditCount.CPP_ID
                LEFT JOIN -- Get Decline Count for Regular and Manuall Billed customers 
                ( SELECT    OneRateUniverse.RunDateYYYYMM AS RunDateYYYYMM ,
                            OneRateUniverse.CPA_ID AS CPA_ID ,
                            OneRateUniverse.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            OneRateUniverse.Merchant AS Merchant ,
                            OneRateUniverse.ProductLine AS ProductLine ,
                            OneRateUniverse.ManualBillYN AS ManualBillYN ,
                            OneRateUniverse.CPP_ID AS CPP_ID ,
                            OneRateUniverse.MID AS MID ,
                            OneRateUniverse.SourceIdConfig AS SourceIdConfig ,
                            NULL AS FeeType ,
                            NULL AS FeeAmount ,
                            OneRateUniverse.TotalCount AS TotalCount
                  FROM      ( SELECT    CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID AS CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName AS Merchant ,
                                        CPA.CPA_ProductLine AS ProductLine ,
                                        CPA.CPA_ManualBillYN AS ManualBillYN ,
                                        CDT.CDT_CPP_ID AS CPP_ID ,
                                        CPP.CPP_MID AS MID ,
                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                        SUM(CDT.CDT_TranCount) AS TotalCount
                              FROM      Billing.CDT_CustomerDailyTran AS CDT
                                        JOIN Billing.CPP_CustomerProcessingProfile
                                        AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                        JOIN Billing.CPA_CustomerProcessingAccount
                                        AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                              WHERE     CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                        AND CDT.CDT_TranType = 'Decline'
                                        AND CPA.CPA_ProductLine = 'Credit Card'
                                        AND CPA.CPA_DoNotBillYN <> 'Y'
                              GROUP BY  CDT.CDT_RunDateYYYYMM ,
                                        CPP.CPP_CPA_ID ,
                                        CPA.CPA_CRMProcessingAccountID ,
                                        CPA.CPA_TrustName ,
                                        CPA.CPA_ProductLine ,
                                        CPA.CPA_ManualBillYN ,
                                        CDT.CDT_CPP_ID ,
                                        CPP.CPP_MID ,
                                        CPP.CPP_SourceIdConfig
                            ) AS OneRateUniverse
                ) AS DeclineCount ON CommissionUniverse.CPP_ID = DeclineCount.CPP_ID
                LEFT JOIN -- Get a List of ZIDs by Processsing Account ID
                ( SELECT DISTINCT
                            PA.bt_ProcessingAccountId ,
                            PA.bt_ZID
                  FROM      CRMShadow.MS.bt_ProcessingAccount AS PA
                  WHERE     PA.bt_ZID IS NOT NULL
                ) AS ZID ON ZID.bt_ProcessingAccountId = CommissionUniverse.CPA_CRMProcessingAccountID;






    END;









