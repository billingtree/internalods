﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spUSAePayDrillDownReport](
@StartYearMonth INT,
@EndYearMonth   INT 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
   
SELECT sfa.Existing_Clients_Profile__c    "Parent", 
       sfa.Name                           "Child", 
       LEFT(sfp.CC_Merchant_number__c,13) "MerchantID",
       COUNT(*),                           
       'A - Sales'                        "Type"
FROM USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account           sfa  on sfp.Account__c = sfa.Id
WHERE usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDateYYYYMM BETWEEN @StartYearMonth AND @EndYearMonth
  and usa.UsabilityIndex = 1  
GROUP BY sfa.Existing_Clients_Profile__c, 
         sfa.Name, 
         LEFT(sfp.CC_Merchant_number__c,13)

 
UNION 

SELECT sfa.Existing_Clients_Profile__c    "Parent", 
       sfa.Name                           "Child", 
       LEFT(sfp.CC_Merchant_number__c,13) "MerchantID",
       COUNT(*),                          
       'B - Credits'                      "Type"
FROM USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account           sfa  on sfp.Account__c = sfa.Id
WHERE usa.Result = 'A'
  and usa.TransType = 'C'
  and usa.TransDateYYYYMM BETWEEN @StartYearMonth AND @EndYearMonth
  and usa.UsabilityIndex = 1
GROUP BY sfa.Existing_Clients_Profile__c, 
         sfa.Name, 
         LEFT(sfp.CC_Merchant_number__c,13)     
         
UNION 

SELECT sfa.Existing_Clients_Profile__c    "Parent", 
       sfa.Name                           "Child", 
       LEFT(sfp.CC_Merchant_number__c,13) "MerchantID",
       COUNT(*),                          
       'C - Auths'                      "Type"
FROM USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account           sfa  on sfp.Account__c = sfa.Id
WHERE usa.Result = 'A'
  and usa.TransType = 'A'
  and usa.TransDateYYYYMM BETWEEN @StartYearMonth AND @EndYearMonth
  and usa.UsabilityIndex = 1  
GROUP BY sfa.Existing_Clients_Profile__c, 
         sfa.Name, 
         LEFT(sfp.CC_Merchant_number__c,13)             
           
UNION 

SELECT sfa.Existing_Clients_Profile__c    "Parent", 
       sfa.Name                           "Child", 
       LEFT(sfp.CC_Merchant_number__c,13) "MerchantID",
       COUNT(*),                          
       'D - Declines'                      "Type"
FROM USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account           sfa  on sfp.Account__c = sfa.Id
WHERE usa.Result = 'D'
  and usa.TransType IN ('A','C','S')
  and usa.TransDateYYYYMM BETWEEN @StartYearMonth AND @EndYearMonth
  and usa.UsabilityIndex = 1  
GROUP BY sfa.Existing_Clients_Profile__c, 
         sfa.Name, 
         LEFT(sfp.CC_Merchant_number__c,13)             


END
