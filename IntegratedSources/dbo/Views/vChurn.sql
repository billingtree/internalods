﻿


CREATE VIEW [dbo].[vChurn]
AS
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jan 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201101
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201101) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201101))
UNION   
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Feb 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201102
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201102) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201102))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Mar 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201103
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201103) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201103))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Apr 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201104
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201104) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201104))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'May 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201105
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201105) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201105))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jun 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201106
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201106) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201106))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jul 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201107
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201107) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201107))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Aug 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201108
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201108) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201108))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Sep 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201109
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201109) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201109))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Oct 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201110
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201110) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201110))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Nov 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201111
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201111) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201111))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Dec 2011'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201112
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201112) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201112))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jan 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201201
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201201) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201201))
UNION   
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Feb 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201202
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201202) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201202))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Mar 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201203
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201203) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201203))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Apr 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201204
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201204) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201204))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'May 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201205
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201205) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201205))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jun 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201206
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201206) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201206))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jul 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201207
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201207) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201207))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Aug 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201208
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201208) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201208))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Sep 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201209
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201209) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201209))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Oct 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201210
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201210) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201210))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Nov 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201211
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201211) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201211))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Dec 2012'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201212
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201212) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201212))
UNION
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jan 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201301
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201301) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201301))
UNION   
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Feb 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201302
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201302) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201302))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Mar 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201303
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201303) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201303))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Apr 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201304
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201304) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201304))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'May 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201305
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201305) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201305))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jun 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201306
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201306) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201306))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jul 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201307
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201307) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201307))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Aug 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201308
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201308) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201308))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Sep 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201309
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201309) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201309))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Oct 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201310
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201310) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201310))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Nov 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201311
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201311) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201311))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Dec 2013'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201312
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201312) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201312))
UNION
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Jan 2014'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201401
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201401) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201401))
UNION   
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Feb 2014'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201402
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201402) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201402))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Mar 2014'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201403
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201403) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201403))
UNION 
SELECT DISTINCT CRMParent    AS Parent
      ,EndYYYYMM             AS YYYYMM
      ,'Apr 2014'            AS MonYear
FROM dbo.vMIDLookup 
WHERE EndYYYYMM = 201404
  AND CRMParent NOT IN (SELECT DISTINCT CRMParent 
                        FROM dbo.vMIDLookup 
                        WHERE (EndYYYYMM IS NULL OR EndYYYYMM > 201404) 
                          AND (StartYYYYMM IS NULL OR StartYYYYMM <= 201404))
                          


