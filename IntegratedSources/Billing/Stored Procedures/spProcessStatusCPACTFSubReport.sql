﻿







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessStatusCPACTFSubReport](
@PRLID          INT,
@IssueType      NVARCHAR (50)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vPRLID          INT
    DECLARE @vIssueType      NVARCHAR (50)
    DECLARE @vFeeType        NVARCHAR(255)
    SET @vPRLID = @PRLID   
    SET @vIssueType = RTRIM(@IssueType)
    
    IF @vIssueType LIKE '%(Submission Fee%' 
        SET @vFeeType = 'BillingTree Gateway Submission Fee'
    ELSE
    IF @vIssueType LIKE '%(Monthly Fee%' 
        SET @vFeeType = 'BillingTree Monthly Fee' 
    ELSE
    IF @vIssueType LIKE '%(Return Fee%' 
        SET @vFeeType = 'Return Fee'  
    ELSE
    IF @vIssueType LIKE '%(Per Batch Fee%' 
        SET @vFeeType = 'Per Batch Fee/Offset'  
    ELSE
    IF @vIssueType LIKE '%(Un-Authorized Return Fee%' 
        SET @vFeeType = 'Un-Authorized Return Fee'                             
        

    -- Insert statements for procedure here

-----------------------------------------------------------------------------------
--       These 10 (5 for each Fee) Issue Types are handled by this Procedure
-----------------------------------------------------------------------------------
    --IF @vIssueType LIKE 'Fees Duplicated%'
    --IF @vIssueType LIKE 'Fees Missing%'

    
    
		SELECT PSL.PSL_RunDateYYYYMM
			  ,PRT.PRT_Name
			  ,PSL.PSL_Status
			  ,PSL.PSL_InsertDateTime
			  ,PRL.PRL_StartDateTime
			  ,PRL.PRL_EndDateTime
			  ,PIT.PIT_Category
			  ,PIT.PIT_Description
			  ,PIT.PIT_Name
			  ,CST.CST_AccountName
			  ,CPA.CPA_TrustName
			  ,CPA.CPA_ManualBillStartDate
			  ,CPA.CPA_ManualBillEndDate
			  ,CPP.CPP_MID
			  ,CPP.CPP_SourceIdConfig
			  ,CPP.CPP_InitialTranDateForMonth
			  ,CPP.CPP_SourceStep
			  ,CPP.CPP_StartDate
			  ,CPP.CPP_EndDate
			  ,CPP.CPP_CRMProcessingProfileID
			  ,CPA.CPA_CRMProcessingAccountID
			  ,CTF.CTF_ProductName
			  ,CTF.CTF_FeeType
			  ,CTF.CTF_StartDate
			  ,CTF.CTF_EndDate
			  ,CTF.CTF_FeeAmount
			  ,PIT.PIT_ID
			  ,CASE 
			       WHEN PIN.PIN_ID IS NULL THEN 
			           'Add a Note'
			       ELSE
			           PIN.PIN_Note
			   END                                                          AS Note
		FROM Billing.PSL_ProcessStatusLog                AS PSL
		JOIN Billing.PRT_ProcessRunType                  AS PRT   ON PSL.PSL_PRT_ID = PRT.PRT_ID
		JOIN Billing.PRL_ProcessRunList                  AS PRL   ON PSL.PSL_ID = PRL.PRL_PSL_ID
		JOIN Billing.PIL_ProcessIssueList                AS PIL   ON PRL.PRL_ID = PIL.PIL_PRL_ID
		JOIN Billing.PIT_ProcessIssueType                AS PIT   ON PIL.PIL_PIT_ID = PIT.PIT_ID
		LEFT JOIN Billing.PED_ProcessErrorDetail         AS PED   ON PIL.PIL_ID = PED.PED_PIL_ID
		LEFT JOIN Billing.CPA_CustomerProcessingAccount  AS CPA   ON PED.PED_CPA_ID = CPA.CPA_ID
		LEFT JOIN Billing.CPP_CustomerProcessingProfile  AS CPP   ON CPA.CPA_ID = CPP.CPP_CPA_ID
		LEFT JOIN Billing.CST_Customer                   AS CST   ON CPA.CPA_CST_ID = CST.CST_ID
		LEFT JOIN Billing.CTF_CustomerTranFee            AS CTF   ON CPA.CPA_ID = CTF.CTF_CPA_ID
		                                                         AND ISNULL(CTF.CTF_FeeType, 'Missing') IN (@vFeeType, 'Missing')
		LEFT JOIN Billing.PIN_ProcessIssueNote           AS PIN   ON PIT.PIT_ID = PIN.PIN_PIT_ID
		                                                         AND CPA.CPA_CRMProcessingAccountID = PIN.PIN_CPA_CRMProcessingAccountID
		WHERE PRL.PRL_ID = @vPRLID 
		  AND PIT.PIT_Name = @vIssueType

	OPTION (OPTIMIZE FOR UNKNOWN)--(RECOMPILE)
	



		
END














