﻿



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessIssueNoteCPA]
(
@PIT_ID          INT,
@CPA_CRMID       UniqueIdentifier,
@Note            NVARCHAR (1000)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @vPIT_ID          INT
	DECLARE @vCPA_CRMID       UniqueIdentifier
	       
	SET @vPIT_ID = @PIT_ID
	SET @vCPA_CRMID = @CPA_CRMID
	
	DECLARE @vNoteDate      DATE
	DECLARE @vNote          NVARCHAR (1000)

	SET @vNote = @Note

	SELECT @vNoteDate = CONVERT(DATE, PIN.PIN_InsertDateTime)
	FROM Billing.PIN_ProcessIssueNote  AS PIN
	WHERE PIN.PIN_PIT_ID = @vPIT_ID
	  AND PIN.PIN_CPA_CRMProcessingAccountID = @vCPA_CRMID

    
		IF @vNoteDate IS NULL                   --   Insert Note Mode
		    BEGIN       
				BEGIN TRANSACTION
					INSERT INTO Billing.PIN_ProcessIssueNote 
					SELECT @vPIT_ID                   AS PIN_PIT_ID
					      ,NULL                       AS PIN_CTF_CRMServiceProductFeeID
					      ,NULL                       AS PIN_CPP_CRMProcessingProfileID
					      ,@vCPA_CRMID                AS PIN_CPA_CRMProcessingAccountID
					      ,NULL                       AS PIN_CST_CRMAccountID
					      ,@vNote                     AS PIN_Note
					      ,GETDATE()                  AS PIN_InsertDateTime
				COMMIT  
            END
	    ELSE                                   --   Update Note Mode
	        BEGIN
				BEGIN TRANSACTION
					UPDATE Billing.PIN_ProcessIssueNote
					SET PIN_Note = @vNote
					WHERE PIN_PIT_ID = @vPIT_ID
					  AND PIN_CPA_CRMProcessingAccountID = @vCPA_CRMID                   
				COMMIT  		  
	        END		


    SELECT @vNote                        AS EnteredNote
          ,CONVERT(DATE, GetDate())      AS OnThisDate


END













