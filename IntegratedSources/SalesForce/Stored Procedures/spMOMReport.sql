﻿










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spMOMReport](
@Month1 INT,
@Month2 INT,
@Year1  INT,
@Year2  INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @YearMonth1 INT
    DECLARE @YearMonth2 INT
    DECLARE @txtYear1   VARCHAR (4)
    DECLARE @txtYear2   VARCHAR (4)
    DECLARE @txtMonth1  VARCHAR (2)
    DECLARE @txtMonth2  VARCHAR (2)
    
 
    SET @txtYear1 = CONVERT(VARCHAR (4), @Year1)
    IF @Month1 < 10 
        SET @txtMonth1 = CONCAT('0', CONVERT(VARCHAR(1), @Month1)) 
    ELSE
        SET @txtMonth1 = CONVERT(VARCHAR(2), @Month1) 
    
    SET @YearMonth1 = CONVERT(INT, CONCAT(@txtYear1, @txtMonth1))   
     
     
    SET @txtYear2 = CONVERT(VARCHAR (4), @Year2)
    IF @Month2 < 10 
        SET @txtMonth2 = CONCAT('0', CONVERT(VARCHAR(1), @Month2)) 
    ELSE
        SET @txtMonth2 = CONVERT(VARCHAR(2), @Month2) 
    
    SET @YearMonth2 = CONVERT(INT, CONCAT(@txtYear2, @txtMonth2))        
     

    -- Insert statements for procedure here
    
SELECT Both.Type                                     AS "Type"
      ,Both.Parent                                   AS "Parent"
      ,Both.Child                                    AS "Child"
      ,Both.ParentIndustry                           AS ParentIndustry
      ,Both.ParentNumberOfEmployees                  AS ParentNumberOfEmployees
	  ,Both.Industry                                 AS Industry
	  ,Both.NumberOfEmployees                        AS NumberOfEmployees      
	  ,Both.MIDProcessingNbr                         AS "MIDProcessingNbr"
	  ,Both.SourceConfigNbr                          AS "SourceConfigNbr"
	  ,Both.SetupDate                                AS SetupDate
	  ,Both.ProcessingDate                           AS ProcessingDate
	  ,Both.ClosedDate                               AS ClosedDate
      ,(ISNULL(Month1.Volume1,0)+
        ISNULL(Month2.Volume2,0))                    AS "TotalVolume"
      ,(ISNULL(Month1.Count1,0)+
        ISNULL(Month2.Count2,0))                     AS "TotalCount"
      ,(ISNULL(Month1.Volume1,0))                    AS "Volume1"
      ,(ISNULL(Month1.Count1,0))                     AS "Count1"
      ,(ISNULL(Month2.Volume2,0))                    AS "Volume2"
      ,(ISNULL(Month2.Count2,0))                     AS "Count2"
FROM
     -----------------------------------------------------------------------------
     --    Get All CRM Processing Accounts with Numeric Merchant IDs            --
     -----------------------------------------------------------------------------
    (  SELECT sfp.Id                              AS CRM_ID
      ,A.Name                              AS Child
      ,CASE 
           WHEN A.ParentAccountIdName IS NULL THEN
               A.Name 
           ELSE 
               A.ParentAccountIdName
       END                                 AS Parent
      ,SFP.CC_Merchant_Number__c           AS MIDProcessingNbr
      ,SFP.CC_Source_ID__c                 AS SourceConfigNbr
      ,CASE 
           WHEN A2.bt_SICCodeName IS NULL THEN 
               A.bt_SICCodeName
           ELSE
               A2.bt_SICCodeName
       END                                 AS ParentIndustry
      ,CASE 
           WHEN A2.NumberOfEmployees IS NULL THEN
               A.NumberOfEmployees
           ELSE
               A2.NumberOfEmployees
       END                                 AS ParentNumberOfEmployees
      ,A.bt_SICCodeName                    AS Industry
      ,A.NumberOfEmployees                 AS NumberOfEmployees
      ,S.SetupDate                         AS SetupDate
      ,I.InitialProcessingDate             AS ProcessingDate
      ,Opp.ActualCloseDate                 AS ClosedDate
      ,'CC'                                AS Type
     FROM SalesForce.PaymentProcessing                  AS sfp
     --JOIN SalesForce.Account                            AS sfa ON sfp.Account__c = sfa.Id
     LEFT JOIN dbo.SetupDateByMID                       AS S   ON SFP.Id = S.CRM_PPDID
     LEFT JOIN dbo.InitialProcessingDateByMID           AS I   ON SFP.Id = I.CRM_PPDID
     LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP  ON SFP.Id = PP.bt_ProcessingProfileId
     LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount        AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
     LEFT JOIN [$(CRMShadow)].MS.Opportunity                 AS Opp ON PA.bt_Opportunity = Opp.OpportunityId 
     JOIN [$(CRMShadow)].MS.Account                          AS A   ON PA.bt_Account = A.AccountId      -- the Child Account
     LEFT JOIN [$(CRMShadow)].MS.Account                     AS A2  ON A.ParentAccountId = A2.AccountId -- Maybe a Parent Account
     WHERE ISNUMERIC(SFP.CC_Merchant_Number__c) = 1   
			 
  ) 	Both 

LEFT JOIN
-------------------------------------------------
--    Get Month 1                              --
-------------------------------------------------
(select sfp.Id                                    "CRM_ID"                                
       ,sfa.Name                                  "Child1" 
       ,sfa.Existing_Clients_Profile__c           "Parent1"
       ,SUM(usa.amount)                           "Volume1" 
       ,count(*)                                  "Count1" 
       ,'CC'                                      "Type"
from USAePay.FileData             AS usa
JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDateYYYYMM = @YearMonth1
  and usa.UsabilityIndex = 1
group by sfp.Id
        ,sfa.Name
        ,sfa.Existing_Clients_Profile__c) Month1  ON Both.CRM_ID = Month1.CRM_ID

LEFT JOIN
-------------------------------------------------
--    Get Month 2                              --
-------------------------------------------------
(select sfp.Id                                      "CRM_ID"
       ,sfa.Name                                    "Child2" 
       ,sfa.Existing_Clients_Profile__c             "Parent2"
       ,SUM(usa.amount)                             "Volume2"
       ,count(*)                                    "Count2"
       ,'CC'                                        "Type"
from USAePay.FileData             AS usa
JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDateYYYYMM = @YearMonth2
  and usa.UsabilityIndex = 1
group by sfp.Id
        ,sfa.Name
        ,sfa.Existing_Clients_Profile__c) Month2  ON Both.CRM_ID = Month2.CRM_ID
-----------------------------------------------------------------------
--  Do a Group By so Having can weed out Accounts with no Activity   --
------------------------------------------------------------------------
GROUP BY Both.Type
      ,Both.Parent
      ,Both.Child
      ,Both.ParentIndustry 
      ,Both.ParentNumberOfEmployees
	  ,Both.Industry 
	  ,Both.NumberOfEmployees  
	  ,Both.MIDProcessingNbr
	  ,Both.SourceConfigNbr    
	  ,Both.SetupDate
	  ,Both.ProcessingDate
	  ,Both.ClosedDate 
      ,(ISNULL(Month1.Volume1,0)+
        ISNULL(Month2.Volume2,0)) 
      ,(ISNULL(Month1.Count1,0)+
        ISNULL(Month2.Count2,0))  
      ,(ISNULL(Month1.Volume1,0))   
      ,(ISNULL(Month1.Count1,0)) 
      ,(ISNULL(Month2.Volume2,0))  
      ,(ISNULL(Month2.Count2,0))   
----------------------------------------------------------------------
--  Only show those Accounts that have a Total Count > 0            --
----------------------------------------------------------------------  
  HAVING (ISNULL(Month1.Count1,0)) + 
         (ISNULL(Month2.Count2,0)) > 0




UNION ALL
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
--                         ACH Data   (ATI and InStep)                         --
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
  SELECT Both.Type                                    AS "Type"
        ,Both.Parent                                  AS "Parent"
        ,Both.Child                                   AS "Child"
        ,Both.ParentIndustry                          AS ParentIndustry
        ,Both.ParentNumberOfEmployees                 AS ParentNumberOfEmployees
	    ,Both.Industry                                AS Industry
	    ,Both.NumberOfEmployees                       AS NumberOfEmployees           
	    ,Both.MIDProcessingNbr                        AS MIDProcessingNbr
	    ,Both.SourceConfigNbr                         AS SourceConfigNbr    
	    ,Both.SetupDate                               AS SetupDate
	    ,Both.ProcessingDate                          AS ProcessingDate	 
	    ,Both.ClosedDate                              AS ClosedDate       
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0)+
          ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             AS "TotalVolume"
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              AS "TotalCount"
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0))             AS "Volume1"
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0))              AS "Count1"
        ,(ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             AS "Volume2"
        ,(ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              AS "Count2"

  FROM
     -----------------------------------------------------------------------------
     --    Get All CRM Processing Accounts with Numeric Config & Processing #s  --
     -----------------------------------------------------------------------------
    (  SELECT sfp.Id                              AS CRM_ID
      ,A.Name                              AS Child
      ,CASE 
           WHEN A.ParentAccountIdName IS NULL THEN
               A.Name 
           ELSE 
               A.ParentAccountIdName
       END                                 AS Parent
      ,SFP.ACH_Processing_Number__c        AS MIDProcessingNbr
	  ,SFP.ACH_Config_Number__c            AS SourceConfigNbr
	   ,CASE 
           WHEN A2.bt_SICCodeName IS NULL THEN 
               A.bt_SICCodeName
           ELSE
               A2.bt_SICCodeName
       END                                 AS ParentIndustry
      ,CASE 
           WHEN A2.NumberOfEmployees IS NULL THEN
               A.NumberOfEmployees
           ELSE
               A2.NumberOfEmployees
       END                                 AS ParentNumberOfEmployees
      ,A.bt_SICCodeName                    AS Industry
      ,A.NumberOfEmployees                 AS NumberOfEmployees
	  ,S.SetupDate                         AS SetupDate
	  ,I.InitialProcessingDate             AS ProcessingDate	 
	  ,Opp.ActualCloseDate                 AS ClosedDate     	          
	  ,'ACH'                               AS Type
     FROM SalesForce.PaymentProcessing                  AS sfp
     --JOIN SalesForce.Account                            AS sfa ON sfp.Account__c = sfa.Id
     LEFT JOIN dbo.SetupDateByMID                       AS S   ON SFP.Id = S.CRM_PPDID
     LEFT JOIN dbo.InitialProcessingDateByMID           AS I   ON SFP.Id = I.CRM_PPDID   
     LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP  ON SFP.Id = PP.bt_ProcessingProfileId
     LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount        AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
     LEFT JOIN [$(CRMShadow)].MS.Opportunity                 AS Opp ON PA.bt_Opportunity = Opp.OpportunityId   
     LEFT JOIN [$(CRMShadow)].MS.Account                     AS A   ON PA.bt_Account = A.AccountId      -- the Child Account
     LEFT JOIN [$(CRMShadow)].MS.Account                     AS A2  ON A.ParentAccountId = A2.AccountId -- Maybe a Parent Account          
     WHERE ISNUMERIC(SFP.ACH_Processing_Number__c) = 1
       AND ISNUMERIC(SFP.ACH_Config_Number__c) = 1   
			 
  ) 	Both 



LEFT JOIN
---------------------------------------------------------
--    Get ATI for Month 1                              --
---------------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child1
	    ,sfa.Existing_Clients_Profile__c      AS Parent1
	    ,SUM(ATI.FormattedAmount)             AS Volume1
	    ,COUNT(*)                             AS Count1
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS ATI
	JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE ATI.FileCreationDateYYYYMM = @YearMonth1
	  AND ATI.UsabilityIndex = 1
      AND ATI.SourceType = 4
	  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	GROUP BY sfp.Id
		    ,sfa.Name 
		    ,sfa.Existing_Clients_Profile__c)  ATIMonth1  ON Both.CRM_ID = ATIMonth1.CRM_ID


LEFT JOIN
-------------------------------------------------
--    Get ATI Month 2                          --
-------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child2
	    ,sfa.Existing_Clients_Profile__c      AS Parent2
	    ,SUM(ATI.FormattedAmount)             AS Volume2
	    ,COUNT(*)                             AS Count2
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS ATI
	JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE ATI.FileCreationDateYYYYMM = @YearMonth2
	  AND ATI.UsabilityIndex = 1
      AND ATI.SourceType = 4
	  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	GROUP BY sfp.Id
		    ,sfa.Name 
			,sfa.Existing_Clients_Profile__c)  ATIMonth2  ON Both.CRM_ID = ATIMonth2.CRM_ID

LEFT JOIN
---------------------------------------------------------
--    Get InStep for Month 1                           --
---------------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child1
	    ,sfa.Existing_Clients_Profile__c      AS Parent1
	    ,SUM(InStep.FormattedAmount)          AS Volume1
	    ,COUNT(*)                             AS Count1
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS InStep
	JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE InStep.FileCreationDateYYYYMM = @YearMonth1
	  AND InStep.UsabilityIndex = 1
      AND InStep.SourceType = 1
	  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record	
	GROUP BY sfp.Id
		    ,sfa.Name 
		    ,sfa.Existing_Clients_Profile__c)  InStepMonth1  ON Both.CRM_ID = InStepMonth1.CRM_ID

LEFT JOIN
---------------------------------------------------------
--    Get InStep for Month 2                           --
---------------------------------------------------------
(  SELECT sfp.Id                              AS CRM_ID
         ,sfa.Name                            AS Child2
	     ,sfa.Existing_Clients_Profile__c     AS Parent2
	     ,SUM(InStep.FormattedAmount)         AS Volume2
	     ,COUNT(*)                            AS Count2
	     ,'ACH'                               AS Type
	FROM NACHA.Originations           AS InStep
	JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE InStep.FileCreationDateYYYYMM = @YearMonth2
	  AND InStep.UsabilityIndex = 1
      AND InStep.SourceType = 1
      AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
	GROUP BY sfp.Id 
		    ,sfa.Name 
			,sfa.Existing_Clients_Profile__c)  InStepMonth2  ON Both.CRM_ID = InStepMonth2.CRM_ID
-----------------------------------------------------------------------
--  Do a Group By so Having can weed out Accounts with no Activity   --
------------------------------------------------------------------------
GROUP BY Both.Type
        ,Both.Parent
        ,Both.Child
        ,Both.ParentIndustry 
        ,Both.ParentNumberOfEmployees
	    ,Both.Industry   
	    ,Both.NumberOfEmployees
        ,Both.MIDProcessingNbr   
	    ,Both.SourceConfigNbr 
	    ,Both.SetupDate  
	    ,Both.ProcessingDate 
	    ,Both.ClosedDate
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0)+
          ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0))             
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0))             
        ,(ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))                     
        ,(ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))  
          
----------------------------------------------------------------------
--  Only show those Accounts that have a Total Count > 0            --
----------------------------------------------------------------------          
HAVING (ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0)) > 0       

END












