﻿-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spACHReturnAnalysisReport](
@Month1 INT,
@Year1  INT
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      DECLARE @Month int, @Year int
      SET @Month=@Month1
      SET @Year=@Year1
      
      
    DECLARE @YearMonth                 INT
    DECLARE @txtYear                   VARCHAR (4)
    DECLARE @txtMonth                  VARCHAR (2)
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))    

    
SELECT AllVendors.Source                   AS "Source"
      ,AllVendors.Parent                   AS Parent
      ,AllVendors.Child                    AS Child
      ,AllVendors.ProcessingNbr            AS ProcessingNbr
      ,AllVendors.ConfigNbr                AS ConfigNbr
      ,(ISNULL(ACHOrig.Count,0)+ISNULL(InStepOrig.Count,0))
                                           AS "ACH Originations"
      ,(ISNULL(ACHReturns.Count,0)+ISNULL(InStepReturns.Count,0))
                                           AS "ACH Returns"
      ,(ISNULL(ACHR01R09.Count,0)+ISNULL(InStepR01R09.Count,0))
                                           AS "R01 and R09"
      ,(ISNULL(ACHR02.Count,0)+ISNULL(InStepR02.Count,0))
                                           AS "R02"
      ,(ISNULL(ACHR03R04.Count,0)+ISNULL(InStepR03R04.Count,0))
                                           AS "R03 and R04"
      ,(ISNULL(ACHR08.Count,0)+ISNULL(InStepR08.Count,0))
                                           AS "R08"
      ,(ISNULL(ACHUnAuth.Count,0)+ISNULL(InStepUnAuth.Count,0))
                                           AS "Un Auth"
      ,(ISNULL(ACHOther.Count,0)+ISNULL(InStepOther.Count,0))
                                           As "Other"

FROM 
-------------------------------------------------------------------------
--    Get any Merchant who processed during the Time Period            --
--   Look first at ATI                                                 -- 
--     At Originations first (exclude OFFSETs)                         --
--     Then Returns (exclude SEC code of COR)                          --
--   Then Look at InStep                                               -- 
--     At Originations first (exclude OFFSETs)                         --
--     Then Returns (exclude SEC code of COR)                          --
-------------------------------------------------------------------------
(     SELECT DISTINCT   
           'ATI'                                AS "Source"
          ,sfp.Id                               AS AcctID
          ,sfa.Name                             AS Child
          ,sfa.Existing_Clients_Profile__c      AS Parent
          ,sfp.ACH_Processing_Number__c         AS ProcessingNbr
          ,sfp.ACH_Config_Number__c             AS ConfigNbr
            
      FROM      NACHA.Originations             AS ATIorig
      LEFT JOIN SalesForce.PaymentProcessing   AS sfp ON ATIorig.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account             AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIorig.FileCreationDateYYYYMM = @YearMonth
        AND ATIorig.UsabilityIndex = 1
        AND ATIorig.SourceType = 4
      AND UPPER(ATIorig.IdentificationNumber) NOT LIKE '%OFFSET%'
                  
    UNION
    
    SELECT DISTINCT     
           'ATI'                                AS "Source" 
          ,sfp.Id                               AS AcctID
          ,sfa.Name                             AS Child
          ,sfa.Existing_Clients_Profile__c      AS Parent
          ,sfp.ACH_Processing_Number__c         AS ProcessingNbr
          ,sfp.ACH_Config_Number__c             AS ConfigNbr
              
      FROM    NACHA.Returns                 AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing  AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account            AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'

    UNION
    ----------------------------------------------------------
    --               InStep Originations                    --
    ----------------------------------------------------------
    SELECT DISTINCT     
          'InStep'                              AS "Source"
          ,sfp.Id                               AS AcctID
          ,sfa.Name                             AS Child
         ,sfa.Existing_Clients_Profile__c      AS Parent
          ,sfp.ACH_Processing_Number__c         AS ProcessingNbr
          ,sfp.ACH_Config_Number__c             AS ConfigNbr
            
      FROM    NACHA.Originations           AS InSteporig
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InSteporig.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InSteporig.FileCreationDateYYYYMM = @YearMonth
        AND InSteporig.UsabilityIndex = 1
        AND InSteporig.SourceType = 1
        AND InSteporig.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
                  
    UNION
        ----------------------------------------------------------
        --               InStep Returns                         --
        ----------------------------------------------------------
    SELECT DISTINCT     
           'InStep'                             AS "Source"
          ,sfp.Id                               AS AcctID
          ,sfa.Name                             AS Child
         ,sfa.Existing_Clients_Profile__c      AS Parent
          ,sfp.ACH_Processing_Number__c         AS ProcessingNbr
          ,sfp.ACH_Config_Number__c             AS ConfigNbr
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
    JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1               
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'                     
                  
 )            AS AllVendors
  
LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's ATI Originations           --
-------------------------------------------------------------------------
( SELECT sfp.Id                               AS AcctID
            ,COUNT(*)                            AS Count
            
      FROM    NACHA.Originations           AS ATIorig
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIorig.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIorig.FileCreationDateYYYYMM = @YearMonth
        AND ATIorig.UsabilityIndex = 1
        AND ATIorig.SourceType = 4
        AND UPPER(ATIorig.IdentificationNumber) NOT LIKE '%OFFSET%'
    GROUP BY sfp.ID                 

)             AS ACHOrig           ON AllVendors.AcctID = ACHOrig.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's ACH Returns                --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4                  
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
    GROUP BY sfp.ID     

)                AS ACHReturns          ON AllVendors.AcctID = ACHReturns.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R01 and R09               --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4      
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode IN ('R01','R09')
    GROUP BY sfp.ID     

)               AS ACHR01R09           ON AllVendors.AcctID = ACHR01R09.AcctID


LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R02                        --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4                        
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode = 'R02'
    GROUP BY sfp.ID     
)                  AS ACHR02        ON AllVendors.AcctID = ACHR02.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R03 and R04                 --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4      
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode IN ('R03','R04')
    GROUP BY sfp.ID     
    
)                  AS ACHR03R04        ON AllVendors.AcctID = ACHR03R04.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R08                        --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4      
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode = 'R08'
    GROUP BY sfp.ID     
)                 AS ACHR08        ON AllVendors.AcctID = ACHR08.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's UnAuthorized Returns       --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4      
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode IN ('R05','R07','R10','R29','R51')
    GROUP BY sfp.ID     
)                  AS ACHUnAuth        ON AllVendors.AcctID = ACHUnAuth.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's All Other Returns          --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS ATIreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
        AND ATIreturn.UsabilityIndex = 1
        AND ATIreturn.SourceType = 4      
        AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
        AND ATIreturn.ReturnCode NOT IN ('R01','R02','R03','R04','R05','R07',
                                         'R08','R09','R10','R29','R51')
    GROUP BY sfp.ID     
)                  AS ACHOther        ON AllVendors.AcctID = ACHOther.AcctID
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--                                                                           --
--                      Do the same for InStep as ATI above                  --
--                                                                           --
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's InStep Originations        --
-------------------------------------------------------------------------
( SELECT sfp.Id                               AS AcctID
            ,COUNT(*)                            AS Count
            
      FROM    NACHA.Originations           AS InSteporig
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InSteporig.CRM_PPDID = sfp.Id
    JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InSteporig.FileCreationDateYYYYMM = @YearMonth
        AND InSteporig.UsabilityIndex = 1
        AND InSteporig.SourceType = 1
        AND InSteporig.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
    GROUP BY sfp.ID                 

)             AS InStepOrig           ON AllVendors.AcctID = InStepOrig.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's InStep Returns                --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
      AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
    GROUP BY sfp.ID     

)                AS InStepReturns          ON AllVendors.AcctID = InStepReturns.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R01 and R09               --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode IN ('R01','R09')
    GROUP BY sfp.ID     

)               AS InStepR01R09           ON AllVendors.AcctID = InStepR01R09.AcctID


LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R02                        --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode = 'R02'
    GROUP BY sfp.ID     
)                  AS InStepR02        ON AllVendors.AcctID = InStepR02.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R03 and R04                 --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode IN ('R03','R04')
    GROUP BY sfp.ID     
    
)                  AS InStepR03R04        ON AllVendors.AcctID = InStepR03R04.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's R08                        --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode = 'R08'
    GROUP BY sfp.ID     
)                 AS InStepR08        ON AllVendors.AcctID = InStepR08.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's UnAuthorized Returns       --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode IN ('R05','R07','R10','R29','R51')
    GROUP BY sfp.ID     
)                  AS InStepUnAuth        ON AllVendors.AcctID = InStepUnAuth.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's All Other Returns          --
-------------------------------------------------------------------------
(  SELECT sfp.Id                              AS AcctID
            ,COUNT(*)                            AS Count
              
      FROM    NACHA.Returns                AS InStepreturn
      LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStepreturn.CRM_PPDID = sfp.Id
      JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
      WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
        AND InStepreturn.UsabilityIndex = 1
        AND InStepreturn.SourceType = 1
        AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
        AND InStepreturn.ReturnCode NOT IN ('R01','R02','R03','R04','R05','R07',
                                            'R08','R09','R10','R29','R51')
    GROUP BY sfp.ID     
)                  AS InStepOther        ON AllVendors.AcctID = InStepOther.AcctID


END
