﻿









CREATE VIEW [dbo].[vRevenueByCCByYearMonth]
AS

SELECT DISTINCT CCRev.TranDate      AS TranDate
               ,CCRev.CRM_ID        AS CRM_ID
               ,CCRev.Source        AS Source
               ,SUM(CCRev.Revenue)  AS Revenue
FROM 
  (
  -- MeS 2011/2012 Revenue
  SELECT MeS.TranDateYYYYMM   AS TranDate
        ,MeSLookup.CRM_Id     AS CRM_ID
        ,'MeS'                AS Source
        ,SUM(MeS.Revenue)     AS Revenue 			  
  FROM dbo.RevenueMeS20112012      AS MeS
  LEFT JOIN dbo.RevenueMeSLookup   AS MeSLookup ON MeS.MID = MeSLookup.MID
  GROUP BY MeS.TranDateYYYYMM 
          ,MeSLookup.CRM_Id 
  
  UNION ALL
  -- MeS 2012/2013 Revenue
  SELECT MeS.TranDateYYYYMM   AS TranDate
        ,MeSLookup.CRM_Id     AS CRM_ID
        ,'MeS'                AS Source
        ,SUM(MeS.Revenue)     AS Revenue 
  FROM dbo.RevenueMeS20122013      AS MeS
  LEFT JOIN dbo.RevenueMeSLookup   AS MeSLookup ON MeS.MID = MeSLookup.MID
  GROUP BY MeS.TranDateYYYYMM
          ,MeSLookup.CRM_Id 	
 
         
  UNION ALL
  --                       MeS GL Revenue Feed
  SELECT CONCAT(YEAR(MeS.TranDate), MONTH(MeS.TranDate))     
                                AS TranDate
        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard     
                                AS CRM_ID
        ,'MeS'                  AS Source
        ,SUM(MeS.RevenueAmount) AS Revenue 
  FROM dbo.MeSRevenueMonthlyGLFeed      AS MeS
  WHERE MONTH(MeS.TranDate) > 9
    AND MeS.RevenueAmount <> 0  -- Exclude these Customers and their large Negative Revenue amount
  	AND MeS.CRMActLevelName_GPParentName NOT IN ('Orbit Leasing, Inc.'
	                                            ,'Security Auto Loans'
	                                            ,'First Financial Asset Management, Inc. (FFAM)'
	                                            ,'Reliable Auto Finance, Inc.'
	                                            ,'BillingTree Admin'
	                                            ,'Coastal Credit, LLC')
  GROUP BY CONCAT(YEAR(MeS.TranDate), MONTH(MeS.TranDate)) 
          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
  UNION ALL
  SELECT CONCAT(YEAR(MeS.TranDate), MONTH(MeS.TranDate))     
                                AS TranDate
        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard     
                                AS CRM_ID
        ,'MeS'                  AS Source
        ,SUM(MeS.RevenueAmount) AS Revenue 
  FROM dbo.MeSRevenueMonthlyGLFeed      AS MeS
  WHERE MONTH(MeS.TranDate) > 9
    AND MeS.RevenueAmount > 0    -- Only include these Customers if they have Positive Revenue Ammount
	AND MeS.CRMActLevelName_GPParentName IN ('Orbit Leasing, Inc.'
	                                         ,'Security Auto Loans'
	                                         ,'First Financial Asset Management, Inc. (FFAM)'
	                                         ,'Reliable Auto Finance, Inc.'
	                                         ,'BillingTree Admin'
	                                         ,'Coastal Credit, LLC')
  GROUP BY CONCAT(YEAR(MeS.TranDate), MONTH(MeS.TranDate)) 
          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
  UNION ALL        
  SELECT CONCAT(YEAR(MeS.TranDate), 0, MONTH(MeS.TranDate))     
                                AS TranDate
        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard     
                                AS CRM_ID
        ,'MeS'                  AS Source
        ,SUM(MeS.RevenueAmount) AS Revenue 
  FROM dbo.MeSRevenueMonthlyGLFeed      AS MeS
  WHERE MONTH(MeS.TranDate) < 10
    AND MeS.RevenueAmount <> 0  -- Exclude these Customers and their large Negative Revenue amount  
	AND MeS.CRMActLevelName_GPParentName NOT IN ('Orbit Leasing, Inc.'
	                                            ,'Security Auto Loans'
	                                            ,'First Financial Asset Management, Inc. (FFAM)'
	                                            ,'Reliable Auto Finance, Inc.'
	                                            ,'BillingTree Admin'
	                                            ,'Coastal Credit, LLC')
  GROUP BY CONCAT(YEAR(MeS.TranDate), 0, MONTH(MeS.TranDate)) 
          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
  UNION ALL                    
  SELECT CONCAT(YEAR(MeS.TranDate), 0, MONTH(MeS.TranDate))     
                                AS TranDate
        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard     
                                AS CRM_ID
        ,'MeS'                  AS Source
        ,SUM(MeS.RevenueAmount) AS Revenue 
  FROM dbo.MeSRevenueMonthlyGLFeed      AS MeS
  WHERE MONTH(MeS.TranDate) < 10
    AND MeS.RevenueAmount > 0     -- Only include these Customers if they have Positive Revenue Ammount 
	AND MeS.CRMActLevelName_GPParentName IN ('Orbit Leasing, Inc.'
	                                        ,'Security Auto Loans'
	                                        ,'First Financial Asset Management, Inc. (FFAM)'
	                                        ,'Reliable Auto Finance, Inc.'
	                                        ,'BillingTree Admin'
	                                        ,'Coastal Credit, LLC')
  GROUP BY CONCAT(YEAR(MeS.TranDate), 0, MONTH(MeS.TranDate)) 
          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
          
  
  UNION ALL  
  -- Orion Revenue All Years
  SELECT Orion.TranDateYYYYMM AS TranDate
        ,OrionLookup.CRM_Id   AS CRM_ID
        ,'Orion'              AS Source
        ,SUM(Orion.Revenue)   AS Revenue 
  FROM dbo.RevenueOrion            AS Orion
  LEFT JOIN dbo.RevenueOrionLookup AS OrionLookup ON Orion.MID = OrionLookup.MID
  GROUP BY Orion.TranDateYYYYMM
          ,OrionLookup.CRM_Id
  
  UNION ALL
  -- UMS 2008 to 2010 Revenue
  SELECT UMS.TranDateYYYYMM   AS TranDate
        ,UMSLookup.CRM_Id     AS CRM_ID
        ,'UMS'                AS Source
        ,SUM(UMS.Revenue)     AS Revenue 
  FROM dbo.RevenueUMS20082010      AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  GROUP BY UMS.TranDateYYYYMM
          ,UMSLookup.CRM_Id
  
  UNION ALL  
  -- UMS 2011 Revenue
  SELECT UMS.TranDateYYYYMM   AS TranDate
        ,UMSLookup.CRM_Id     AS CRM_ID
        ,'UMS'                AS Source
        ,SUM(UMS.Revenue)     AS Revenue 
  FROM dbo.RevenueUMS2011          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  GROUP BY UMS.TranDateYYYYMM
          ,UMSLookup.CRM_Id
  
  UNION ALL  
  -- UMS 2012 Revenue
  SELECT UMS.TranDateYYYYMM   AS TranDate
        ,UMSLookup.CRM_Id     AS CRM_ID
        ,'UMS'                AS Source
        ,SUM(UMS.Revenue)     AS Revenue 
  FROM dbo.RevenueUMS2012          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  GROUP BY UMS.TranDateYYYYMM
          ,UMSLookup.CRM_Id
  
  UNION ALL  
  -- UMS 2013 Revenue
  SELECT UMS.TranDateYYYYMM   AS TranDate
        ,UMSLookup.CRM_Id     AS CRM_ID
        ,'UMS'                AS Source
        ,SUM(UMS.Revenue)     AS Revenue 
  FROM dbo.RevenueUMS2013          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  GROUP BY UMS.TranDateYYYYMM
          ,UMSLookup.CRM_Id
  
  UNION ALL  
  -- USAePay 2008 through 2010 Revenue
  SELECT USA.TranDateYYYYMM   AS TranDate
        ,CCG.CRM_Id           AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.Revenue)     AS Revenue 
  FROM dbo.RevenueUSAePay20082010    AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
  GROUP BY USA.TranDateYYYYMM
          ,CCG.CRM_Id
    
  UNION ALL  
  -- USAePay 2011 Revenue
  SELECT USA.TranDateYYYYMM   AS TranDate
        ,CCG.CRM_Id           AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.Revenue)     AS Revenue 
  FROM dbo.RevenueUSAePay2011        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
  GROUP BY USA.TranDateYYYYMM
          ,CCG.CRM_Id
          
  UNION ALL 
  -- USAePay 2012 Revenue
  SELECT USA.TranDateYYYYMM   AS TranDate
        ,CCG.CRM_Id           AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.Revenue)     AS Revenue 			  
  FROM dbo.RevenueUSAePay2012        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
  GROUP BY USA.TranDateYYYYMM
          ,CCG.CRM_Id			  
  
  UNION ALL   
  -- USAePay 2013 Revenue
  SELECT USA.TranDateYYYYMM   AS TranDate
        ,CCG.CRM_Id           AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.Revenue)     AS Revenue 
  FROM dbo.RevenueUSAePay2013        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
  GROUP BY USA.TranDateYYYYMM
          ,CCG.CRM_Id

  UNION ALL   
  --                        USAePay From the GL Revenue Feed
  SELECT CONCAT(YEAR(USA.TranDate), MONTH(USA.TranDate))     
                               AS TranDate
        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
                              AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.RevenueAmount)     AS Revenue 
  FROM dbo.USAePayRevenueMonthlyGLFeed AS USA
  WHERE MONTH(USA.TranDate) > 9  
  GROUP BY CONCAT(YEAR(USA.TranDate), MONTH(USA.TranDate)) 
          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
  UNION ALL
  SELECT CONCAT(YEAR(USA.TranDate), 0, MONTH(USA.TranDate))     
                               AS TranDate
        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
                              AS CRM_ID
        ,'USA'                AS Source
        ,SUM(USA.RevenueAmount)     AS Revenue 
  FROM dbo.USAePayRevenueMonthlyGLFeed AS USA
  WHERE MONTH(USA.TranDate) < 10  
  GROUP BY CONCAT(YEAR(USA.TranDate), 0, MONTH(USA.TranDate)) 
          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard       
      			  
  ) AS CCRev
  
GROUP BY CCRev.TranDate 
      ,CCRev.CRM_ID   
      ,CCRev.Source  










