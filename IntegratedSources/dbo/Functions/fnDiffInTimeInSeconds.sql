﻿





CREATE FUNCTION  [dbo].[fnDiffInTimeInSeconds] 
(@ValueIn1                 VARCHAR(22)
,@ValueIn2                 VARCHAR(22)
)
 RETURNS FLOAT
AS
BEGIN
    DECLARE @DateTime1            DATETIME
    DECLARE @DateTime2            DATETIME    
    DECLARE @DiffInSeconds        FLOAT
    
    SET @DateTime1 = CONCAT(CONVERT(DATE, '22-JUL-2013'), ' ', @ValueIn1)
    SET @DateTime2 = CONCAT(CONVERT(DATE, '22-JUL-2013'), ' ', @ValueIn2)
    
    SET @DiffInSeconds = -1
    
    SET @DiffInSeconds = DATEDIFF(SECOND, @DateTime1, @DateTime2)
  

    RETURN @DiffInSeconds
   
END    
 








