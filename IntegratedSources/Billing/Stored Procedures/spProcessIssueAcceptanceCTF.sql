﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessIssueAcceptanceCTF]
(
@PIT_ID          INT,
@CTF_CRMID       UniqueIdentifier,
@UserName        NVARCHAR(100),
@AcceptedValue   MONEY

)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @vPIT_ID          INT
	DECLARE @vCTF_CRMID       UniqueIdentifier
	DECLARE @vUserName        NVARCHAR(100)
	DECLARE @vAcceptedValue   NVARCHAR(100)
	       
	SET @vPIT_ID = @PIT_ID
	SET @vCTF_CRMID = @CTF_CRMID
	SET @vUserName = @UserName
	SET @vAcceptedValue = CONVERT(NVARCHAR, @AcceptedValue)
	
	DECLARE @vAcceptDate      DATE
	


	SELECT @vAcceptDate = CONVERT(DATE, PIA.PIA_InsertDateTime)
	      ,@vUserName = PIA.PIA_UserName
	FROM Billing.PIA_ProcessIssueAcceptance  AS PIA
	WHERE PIA.PIA_PIT_ID = @vPIT_ID
	  AND PIA.PIA_CTF_CRMServiceProductFeeID = @vCTF_CRMID

    
		IF @vAcceptDate IS NULL  
		    BEGIN       
				BEGIN TRANSACTION
					INSERT INTO Billing.PIA_ProcessIssueAcceptance 
					SELECT @vPIT_ID                   AS PIA_PIT_ID
					      ,@vCTF_CRMID                AS PIA_CTF_CRMServiceProductFeeID
					      ,NULL                       AS PIA_CPP_CRMProcessingProfileID
					      ,NULL                       AS PIA_CPA_CRMProcessingAccountID
					      ,NULL                       AS PIA_CST_CRMAccountID
					      ,@vUserName                 AS PIA_UserName
					      ,@vAcceptedValue            AS PIA_AcceptedValue
					      ,GETDATE()                  AS PIA_InsertDateTime
				COMMIT  
            END
	    ELSE
	        BEGIN
				BEGIN TRANSACTION
					DELETE Billing.PIA_ProcessIssueAcceptance
					WHERE PIA_PIT_ID = @vPIT_ID
					  AND PIA_CTF_CRMServiceProductFeeID = @vCTF_CRMID                   
				COMMIT  		  
				SET @vUserName = 'Removed'  
	        END		


    SELECT @vUserName                    AS Acceptor
          ,@vAcceptedValue               AS AcceptedValue
          ,CONVERT(DATE, GetDate())      AS OnThisDate


END












