﻿CREATE TABLE [Billing].[PIN_ProcessIssueNote] (
    [PIN_ID]                         INT              IDENTITY (1, 1) NOT NULL,
    [PIN_PIT_ID]                     INT              NOT NULL,
    [PIN_CTF_CRMServiceProductFeeID] UNIQUEIDENTIFIER NULL,
    [PIN_CPP_CRMProcessingProfileID] UNIQUEIDENTIFIER NULL,
    [PIN_CPA_CRMProcessingAccountID] UNIQUEIDENTIFIER NULL,
    [PIN_CST_CRMAccountID]           UNIQUEIDENTIFIER NULL,
    [PIN_Note]                       NVARCHAR (1000)  NOT NULL,
    [PIN_InsertDateTime]             DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PIN_ProcessIssueNote] PRIMARY KEY CLUSTERED ([PIN_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PIN_ProcessIssueNote_PIT_ProcessIssueType] FOREIGN KEY ([PIN_PIT_ID]) REFERENCES [Billing].[PIT_ProcessIssueType] ([PIT_ID])
);

