﻿
-- =============================================
-- Author:        Rick Stohle
-- Create date:   Oct-2015
-- Description:   Create an Interim Commission Report for Finance.
--                On 28-Dec-2015, we needed to report on Rates for Manually Billed Customers as well,
--                                and these customers are not in the ARI, but in the CDT, so we get Rates from CTF.
--                                Eventually, Manual Bill will become part of ARI, so we left ARI as the driver 
--                                for Non-Manual Billed customers.
-- =============================================
CREATE PROCEDURE [Billing].[spCommissionACHReport]
    (
      @YearMonth INT ,
      @ConfigNumber VARCHAR(12)
    )

-- Insert statements for procedure here
AS
    BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @RunYearMonth INT;
        SET @RunYearMonth = @YearMonth;
        DECLARE @Config VARCHAR(12);
        SET @Config = @ConfigNumber;
        DECLARE @LowConfig VARCHAR(12);
        DECLARE @HighConfig VARCHAR(12);


        IF @Config = 'All'
            SET @LowConfig = '00';
        SET @HighConfig = '99';

        IF @Config <> 'All'
            SET @LowConfig = @Config;
        SET @HighConfig = @Config;
	
		----------------------------------------------------------------------------------------------------------------------
		--   The Commission Report for ACH needs to supply Origination Count, Most Recent Gateway Submission Fee, Return Count 
		--   (which is a combined Return and UnAuth Return), the Most Recent Return Fee and the Monthly Fee   
		--   for each Processing Profile.  Multiple Fees are to be ignored.
		--   This means, we can only have 1 Record per SourceID in the Commission Report, even though 2 would be Correct 
		--   Things to Consider:
		--       1) In the ARI table, we have Originations, Returns, UnAuths and Monthly Fees
		--       2) In the ARI table, we can have multiple Gateway Submission and Return Fees in effect (daily changing fees)
		--       3) The ARI table has a Foreign Key reference to CPA
		--       4) The multiple fees in ARI (as well as CTF) are still tied to this 1 CPA record
		--       5) We are to choose the Most Recent Fee in effect  
		--       6) The Commission Report is to also include Manually Billed Customers
		--       7) ARI Does NOT Include Manually Billed Customers, CDT does though
		--       8) Since Manually Billed Fees are not verified during the Billing Process, Only Counts need to be reported
        --   It is understood then:
		--       1) The ARI does not give us all we need in order to produce the report, as we need to go to CDT
		--          for Detail Counts and for Manually Billed Customer Counts in CDT
		--       2) The ARI can give us the Most Recent Gateway Submission Fee in effect
		--       3) We do not need the Gateway Submission Fee for Manually Billed Customers, until the 28-Dec-2015 modification
		--   Therefore: 
		--       1) We have chosen to drive through ARI (with single fee) to CPA, to CPP to CDT for Non-Manual Bill
		--       2) And, For Manual Bill, drive through CDT to CPP to CPA for just Counts, and then to CTF for Most Recent Fee  
		--       3) This avoids resolving the single Gateway Submission Fee using CTF
		----------------------------------------------------------------------------------------------------------------------
		--  The Commission Universe below is made up of Unique MIDs and Configs from ARI    --
		--       and from CDT for Manual Billed customers
		--------------------------------------------------------------------------------------
        SELECT  CommissionUniverse.RunDateYYYYMM AS RunDateYYYYMM ,
                CommissionUniverse.CPA_ID AS CPA_ID ,
                CommissionUniverse.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                CommissionUniverse.Merchant AS Merchant ,
                CommissionUniverse.ProductLine AS ProductLine ,
                CommissionUniverse.ManualBillYN AS ManualBillYN ,
                CommissionUniverse.CPP_ID AS CPP_ID ,
                CommissionUniverse.MID AS MID ,
                CommissionUniverse.SourceIdConfig AS SourceIdConfig ,
                Originations.FeeType AS FeeTypeOrig ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Originations.FeeAmount, 0)
                     ELSE ISNULL(MBOriginations.FeeAmount, 0)
                END AS TranFee ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Originations.TotalCount, 0)
                     ELSE ISNULL(MBOriginations.TotalCount, 0)
                END AS Originations ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Originations.FeeAmount, 0)
                          * ISNULL(Originations.TotalCount, 0)
                     ELSE ISNULL(MBOriginations.FeeAmount, 0)
                          * ISNULL(MBOriginations.TotalCount, 0)
                END AS TranRevenue ,
                Returns.FeeType AS FeeTypeRet ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Returns.FeeAmount, 0)
                     ELSE ISNULL(MBReturns.FeeAmount, 0)
                END AS ReturnFee ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Returns.TotalCount, 0)
                          + ISNULL(UnAuths.TotalCount, 0)
                     ELSE ISNULL(MBReturns.TotalCount, 0)
                          + ISNULL(MBUnAuths.TotalCount, 0)
                END AS Returns ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Returns.FeeAmount, 0)
                          * ( ISNULL(Returns.TotalCount, 0)
                              + ISNULL(UnAuths.TotalCount, 0) )
                     ELSE ISNULL(MBReturns.FeeAmount, 0)
                          * ( ISNULL(MBReturns.TotalCount, 0)
                              + ISNULL(MBUnAuths.TotalCount, 0) )
                END AS ReturnRevenue ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(UnAuths.TotalCount, 0)
                     ELSE ISNULL(MBUnAuths.TotalCount, 0)
                END AS UnAuthReturns ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(Returns.FeeAmount, 5.00)
                          * ISNULL(UnAuths.TotalCount, 0)
                     ELSE ISNULL(MBReturns.FeeAmount, 5.00)
                          * ISNULL(MBUnAuths.TotalCount, 0)
                END AS UnAuthRevenue ,
                MFee.FeeType AS FeeTypeMonthly ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ISNULL(MFee.FeeAmount, 0)
                     ELSE NULL
                END AS MonthlyFee ,
                CASE WHEN CommissionUniverse.ManualBillYN = 'N'
                     THEN ( ISNULL(Originations.FeeAmount, 0)
                            * ISNULL(Originations.TotalCount, 0) )
                          + ( ISNULL(Returns.FeeAmount, 0)
                              * ( ISNULL(Returns.TotalCount, 0)
                                  + ISNULL(UnAuths.TotalCount, 0) ) )
                          + ( ISNULL(MFee.FeeAmount, 0) )
                     ELSE ( ISNULL(MBOriginations.FeeAmount, 0)
                            * ISNULL(MBOriginations.TotalCount, 0) )
                          + ( ISNULL(MBReturns.FeeAmount, 0)
                              * ( ISNULL(MBReturns.TotalCount, 0)
                                  + ISNULL(MBUnAuths.TotalCount, 0) ) )
                END AS TotalRevenue
        FROM    ( SELECT DISTINCT
                            ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                            ARI.ARI_CPA_ID AS CPA_ID ,
                            CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            CPA.CPA_TrustName AS Merchant ,
                            CPA.CPA_ProductLine AS ProductLine ,
                            CPA.CPA_ManualBillYN AS ManualBillYN ,
                            CPP.CPP_ID AS CPP_ID ,
                            CPP.CPP_MID AS MID ,
                            CPP.CPP_SourceIdConfig AS SourceIdConfig
                  FROM      Billing.ARI_AccountReceivableInvoice AS ARI
                            JOIN Billing.CPA_CustomerProcessingAccount AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                            JOIN Billing.CPP_CustomerProcessingProfile AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                              AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                  WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                            AND ARI.ARI_Type = 10
                            AND CONVERT(VARCHAR, ARI.ARI_ConfigNbr) BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                            AND CPA.CPA_ProductLine = 'ACH'
                            AND CPA.CPA_ManualBillYN = 'N'
                            AND CPA.CPA_DoNotBillYN = 'N'
                  UNION ALL
                  SELECT DISTINCT
                            CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                            CPP.CPP_CPA_ID AS CPA_ID ,
                            CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                            CPA.CPA_TrustName AS Merchant ,
                            CPA.CPA_ProductLine AS ProductLine ,
                            CPA.CPA_ManualBillYN AS ManualBillYN ,
                            CPP.CPP_ID AS CPP_ID ,
                            CPP.CPP_MID AS MID ,
                            CPP.CPP_SourceIdConfig AS SourceIdConfig
                  FROM      Billing.CDT_CustomerDailyTran AS CDT
                            JOIN Billing.CPP_CustomerProcessingProfile AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                            JOIN Billing.CPA_CustomerProcessingAccount AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                  WHERE     CDT.CDT_RunDateYYYYMM = @RunYearMonth
                            AND CDT.CDT_TranType IN ( 'Origination', 'Return',
                                                      'UnAuth' )
                            AND CPP.CPP_SourceIdConfig BETWEEN @LowConfig
                                                       AND    @HighConfig
                            AND CPA.CPA_ProductLine = 'ACH'
                            AND CPA.CPA_ManualBillYN = 'Y'
                ) AS CommissionUniverse
                LEFT JOIN ( SELECT DISTINCT
                                    OneRateOriginations.RunDateYYYYMM AS RunDateYYYYMM ,
                                    OneRateOriginations.CPA_ID AS CPA_ID ,
                                    OneRateOriginations.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    OneRateOriginations.Merchant AS Merchant ,
                                    OneRateOriginations.ProductLine AS ProductLine ,
                                    OneRateOriginations.ManualBillYN AS ManualBillYN ,
                                    OneRateOriginations.CPP_ID AS CPP_ID ,
                                    OneRateOriginations.MID AS MID ,
                                    OneRateOriginations.SourceIdConfig AS SourceIdConfig ,
                                    OneRateOriginations.FeeType AS FeeType  -- This is the Gateway Submission Fee
                                    ,
                                    OneRateOriginations.FeeAmount AS FeeAmount ,
                                    OneRateOriginations.TotalCount AS TotalCount
                            FROM    ( SELECT    ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID AS CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName AS Merchant ,
                                                CPA.CPA_ProductLine AS ProductLine ,
                                                CPA.CPA_ManualBillYN AS ManualBillYN ,
                                                CPP.CPP_ID AS CPP_ID ,
                                                CPP.CPP_MID AS MID ,
                                                CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                'BillingTree Gateway Submission Fee' AS FeeType ,
                                                MAX(ARI.ARI_TranDate) AS StartDate ,
                                                MAX(ARI.ARI_UnitPrice) AS FeeAmount ,
                                                SUM(ARI.ARI_Quantity) AS TotalCount
                                      FROM      Billing.ARI_AccountReceivableInvoice
                                                AS ARI
                                                JOIN Billing.CPA_CustomerProcessingAccount
                                                AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                                                JOIN Billing.CPP_CustomerProcessingProfile
                                                AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                          AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                                      WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                                                AND ARI.ARI_ItemNumber = 'ACHSUB'
                                                AND CONVERT(VARCHAR, ARI.ARI_ConfigNbr) BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                                AND CPA.CPA_ProductLine = 'ACH'
                                                AND CPA.CPA_ManualBillYN = 'N'
                                                AND CPA.CPA_DoNotBillYN = 'N'
                                      GROUP BY  ARI.ARI_RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName ,
                                                CPA.CPA_ProductLine ,
                                                CPA.CPA_ManualBillYN ,
                                                CPP.CPP_ID ,
                                                CPP.CPP_MID ,
                                                CPP.CPP_SourceIdConfig
                                    ) AS OneRateOriginations
                          ) AS Originations ON CommissionUniverse.CPA_ID = Originations.CPA_ID
                                               AND CommissionUniverse.CPP_ID = Originations.CPP_ID
                LEFT JOIN ( SELECT DISTINCT
                                    OneRateReturns.RunDateYYYYMM AS RunDateYYYYMM ,
                                    OneRateReturns.CPA_ID AS CPA_ID ,
                                    OneRateReturns.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    OneRateReturns.Merchant AS Merchant ,
                                    OneRateReturns.ProductLine AS ProductLine ,
                                    OneRateReturns.ManualBillYN AS ManualBillYN ,
                                    OneRateReturns.CPP_ID AS CPP_ID ,
                                    OneRateReturns.MID AS MID ,
                                    OneRateReturns.SourceIdConfig AS SourceIdConfig ,
                                    OneRateReturns.FeeType AS FeeType  -- This is the Return Fee
                                    ,
                                    OneRateReturns.FeeAmount AS FeeAmount ,
                                    OneRateReturns.TotalCount AS TotalCount
                            FROM    ( SELECT    ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID AS CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName AS Merchant ,
                                                CPA.CPA_ProductLine AS ProductLine ,
                                                CPA.CPA_ManualBillYN AS ManualBillYN ,
                                                CPP.CPP_ID AS CPP_ID ,
                                                CPP.CPP_MID AS MID ,
                                                CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                'Return Fee' AS FeeType ,
                                                MAX(ARI.ARI_TranDate) AS StartDate ,
                                                MAX(ARI.ARI_UnitPrice) AS FeeAmount ,
                                                SUM(ARI.ARI_Quantity) AS TotalCount
                                      FROM      Billing.ARI_AccountReceivableInvoice
                                                AS ARI
                                                JOIN Billing.CPA_CustomerProcessingAccount
                                                AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                                                JOIN Billing.CPP_CustomerProcessingProfile
                                                AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                          AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                                      WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                                                AND ARI.ARI_ItemNumber = 'ACHRET'
                                                AND CONVERT(VARCHAR, ARI.ARI_ConfigNbr) BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                                AND CPA.CPA_ProductLine = 'ACH'
                                                AND CPA.CPA_ManualBillYN = 'N'
                                                AND CPA.CPA_DoNotBillYN = 'N'
                                      GROUP BY  ARI.ARI_RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName ,
                                                CPA.CPA_ProductLine ,
                                                CPA.CPA_ManualBillYN ,
                                                CPP.CPP_ID ,
                                                CPP.CPP_MID ,
                                                CPP.CPP_SourceIdConfig
                                    ) AS OneRateReturns
                          ) AS Returns ON CommissionUniverse.CPA_ID = Returns.CPA_ID
                                          AND CommissionUniverse.CPP_ID = Returns.CPP_ID
                LEFT JOIN ( SELECT DISTINCT
                                    NoRateUnAuths.RunDateYYYYMM AS RunDateYYYYMM ,
                                    NoRateUnAuths.CPA_ID AS CPA_ID ,
                                    NoRateUnAuths.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    NoRateUnAuths.Merchant AS Merchant ,
                                    NoRateUnAuths.ProductLine AS ProductLine ,
                                    NoRateUnAuths.ManualBillYN AS ManualBillYN ,
                                    NoRateUnAuths.CPP_ID AS CPP_ID ,
                                    NoRateUnAuths.MID AS MID ,
                                    NoRateUnAuths.SourceIdConfig AS SourceIdConfig ,
                                    NoRateUnAuths.FeeType AS FeeType  -- This is Blank
                                    ,
                                    NoRateUnAuths.FeeAmount AS FeeAmount ,
                                    NoRateUnAuths.TotalCount AS TotalCount
                            FROM    ( SELECT    ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID AS CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName AS Merchant ,
                                                CPA.CPA_ProductLine AS ProductLine ,
                                                CPA.CPA_ManualBillYN AS ManualBillYN ,
                                                CPP.CPP_ID AS CPP_ID ,
                                                CPP.CPP_MID AS MID ,
                                                CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                ' ' AS FeeType ,
                                                MAX(ARI.ARI_TranDate) AS StartDate ,
                                                MAX(ARI.ARI_UnitPrice) AS FeeAmount ,
                                                SUM(ARI.ARI_Quantity) AS TotalCount
                                      FROM      Billing.ARI_AccountReceivableInvoice
                                                AS ARI
                                                JOIN Billing.CPA_CustomerProcessingAccount
                                                AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                                                JOIN Billing.CPP_CustomerProcessingProfile
                                                AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                          AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                                      WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                                                AND ARI.ARI_ItemNumber = 'ACHUNAUTH'
                                                AND CONVERT(VARCHAR, ARI.ARI_ConfigNbr) BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                                AND CPA.CPA_ProductLine = 'ACH'
                                                AND CPA.CPA_ManualBillYN = 'N'
                                                AND CPA.CPA_DoNotBillYN = 'N'
                                      GROUP BY  ARI.ARI_RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName ,
                                                CPA.CPA_ProductLine ,
                                                CPA.CPA_ManualBillYN ,
                                                CPP.CPP_ID ,
                                                CPP.CPP_MID ,
                                                CPP.CPP_SourceIdConfig
                                    ) AS NoRateUnAuths
                          ) AS UnAuths ON CommissionUniverse.CPA_ID = UnAuths.CPA_ID
                                          AND CommissionUniverse.CPP_ID = UnAuths.CPP_ID
                LEFT JOIN ( SELECT DISTINCT
                                    SumMonthlyFee.RunDateYYYYMM AS RunDateYYYYMM ,
                                    SumMonthlyFee.CPA_ID AS CPA_ID ,
                                    SumMonthlyFee.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    SumMonthlyFee.Merchant AS Merchant ,
                                    SumMonthlyFee.ProductLine AS ProductLine ,
                                    SumMonthlyFee.ManualBillYN AS ManualBillYN ,
                                    SumMonthlyFee.CPP_ID AS CPP_ID ,
                                    SumMonthlyFee.MID AS MID ,
                                    SumMonthlyFee.SourceIdConfig AS SourceIdConfig ,
                                    SumMonthlyFee.FeeType AS FeeType  -- This is Monthly Fee
                                    ,
                                    SumMonthlyFee.FeeAmount AS FeeAmount ,
                                    SumMonthlyFee.TotalCount AS TotalCount
                            FROM    ( SELECT    ARI.ARI_RunDateYYYYMM AS RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID AS CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName AS Merchant ,
                                                CPA.CPA_ProductLine AS ProductLine ,
                                                CPA.CPA_ManualBillYN AS ManualBillYN ,
                                                CPP.CPP_ID AS CPP_ID ,
                                                CPP.CPP_MID AS MID ,
                                                CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                'ACH Monthly Fee' AS FeeType ,
                                                MAX(ARI.ARI_TranDate) AS StartDate ,
                                                SUM(ISNULL(ARI.ARI_ExtPrice, 0)) AS FeeAmount  -- This accounts for Quantity = 0 for waived Monthly Fees
                                                ,
                                                SUM(ARI.ARI_Quantity) AS TotalCount
                                      FROM      Billing.ARI_AccountReceivableInvoice
                                                AS ARI
                                                JOIN Billing.CPA_CustomerProcessingAccount
                                                AS CPA ON ARI.ARI_CPA_ID = CPA.CPA_ID
                                                JOIN Billing.CPP_CustomerProcessingProfile
                                                AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                          AND ARI.ARI_ProcessingProfileID = CPP.CPP_CRMProcessingProfileID
                                      WHERE     ARI.ARI_RunDateYYYYMM = @RunYearMonth
                                                AND ARI.ARI_LineDescription = 'ACH Monthly Fee'
                                                AND CONVERT(VARCHAR, ARI.ARI_ConfigNbr) BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                                AND CPA.CPA_ProductLine = 'ACH'
                                                AND CPA.CPA_ManualBillYN = 'N'
                                                AND CPA.CPA_DoNotBillYN = 'N'
                                      GROUP BY  ARI.ARI_RunDateYYYYMM ,
                                                ARI.ARI_CPA_ID ,
                                                CPA.CPA_CRMProcessingAccountID ,
                                                CPA.CPA_TrustName ,
                                                CPA.CPA_ProductLine ,
                                                CPA.CPA_ManualBillYN ,
                                                CPP.CPP_ID ,
                                                CPP.CPP_MID ,
                                                CPP.CPP_SourceIdConfig
                                    ) AS SumMonthlyFee
                          ) AS MFee ON CommissionUniverse.CPA_ID = MFee.CPA_ID
                                       AND CommissionUniverse.CPP_ID = MFee.CPP_ID
                LEFT JOIN ( SELECT  CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID AS CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName AS Merchant ,
                                    CPA.CPA_ProductLine AS ProductLine ,
                                    CPA.CPA_ManualBillYN AS ManualBillYN ,
                                    CPP.CPP_ID AS CPP_ID ,
                                    CPP.CPP_MID AS MID ,
                                    CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                    'BillingTree Gateway Submission Fee' AS FeeType ,
                                    TranFee.LatestStartDate AS StartDate ,
                                    TranFee.RecentFee AS FeeAmount ,
                                    SUM(CDT.CDT_TranCount) AS TotalCount
                            FROM    Billing.CDT_CustomerDailyTran AS CDT
                                    JOIN Billing.CPP_CustomerProcessingProfile
                                    AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                    JOIN Billing.CPA_CustomerProcessingAccount
                                    AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
			--------------------------------------------------------------------
			--   Added the Most Recent Rate in Effect for Manual Bill as well
			--------------------------------------------------------------------
                                    LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                                        'TranFee' AS FeeType ,
                                                        CTF_FeeAmount AS RecentFee ,
                                                        MAX(CTF_StartDate) AS LatestStartDate
                                                FROM    Billing.CTF_CustomerTranFee
                                                WHERE   CTF_RunDateYYYYMM = @RunYearMonth
                                                        AND CTF_FeeType = 'BillingTree Gateway Submission Fee'
                                                GROUP BY CTF_CPA_ID ,
                                                        CTF_FeeAmount
                                              ) AS TranFee ON CPA.CPA_ID = TranFee.CPA_ID
                            WHERE   CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                    AND CDT.CDT_TranType = 'Origination'
                                    AND CPP.CPP_SourceIdConfig BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                    AND CPA.CPA_ProductLine = 'ACH'
                                    AND CPA.CPA_ManualBillYN = 'Y'
                            GROUP BY CDT.CDT_RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName ,
                                    CPA.CPA_ProductLine ,
                                    CPA.CPA_ManualBillYN ,
                                    CPP.CPP_ID ,
                                    CPP.CPP_MID ,
                                    CPP.CPP_SourceIdConfig ,
                                    TranFee.LatestStartDate ,
                                    TranFee.RecentFee
                          ) AS MBOriginations ON CommissionUniverse.CPA_ID = MBOriginations.CPA_ID
                                                 AND CommissionUniverse.CPP_ID = MBOriginations.CPP_ID
                LEFT JOIN ( SELECT  CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID AS CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName AS Merchant ,
                                    CPA.CPA_ProductLine AS ProductLine ,
                                    CPA.CPA_ManualBillYN AS ManualBillYN ,
                                    CPP.CPP_ID AS CPP_ID ,
                                    CPP.CPP_MID AS MID ,
                                    CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                    'Return Fee' AS FeeType ,
                                    TranFee.LatestStartDate AS StartDate ,
                                    TranFee.RecentFee AS FeeAmount ,
                                    SUM(CDT.CDT_TranCount) AS TotalCount
                            FROM    Billing.CDT_CustomerDailyTran AS CDT
                                    JOIN Billing.CPP_CustomerProcessingProfile
                                    AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                    JOIN Billing.CPA_CustomerProcessingAccount
                                    AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
			--------------------------------------------------------------------
			--   Added the Most Recent Rate in Effect for Manual Bill as well
			--------------------------------------------------------------------
                                    LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                                        'ReturnFee' AS FeeType ,
                                                        CTF_FeeAmount AS RecentFee ,
                                                        MAX(CTF_StartDate) AS LatestStartDate
                                                FROM    Billing.CTF_CustomerTranFee
                                                WHERE   CTF_RunDateYYYYMM = @RunYearMonth
                                                        AND CTF_FeeType = 'Return Fee'
                                                GROUP BY CTF_CPA_ID ,
                                                        CTF_FeeAmount
                                              ) AS TranFee ON CPA.CPA_ID = TranFee.CPA_ID
                            WHERE   CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                    AND CDT.CDT_TranType = 'Return'
                                    AND CPP.CPP_SourceIdConfig BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                    AND CPA.CPA_ProductLine = 'ACH'
                                    AND CPA.CPA_ManualBillYN = 'Y'
                            GROUP BY CDT.CDT_RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName ,
                                    CPA.CPA_ProductLine ,
                                    CPA.CPA_ManualBillYN ,
                                    CPP.CPP_ID ,
                                    CPP.CPP_MID ,
                                    CPP.CPP_SourceIdConfig ,
                                    TranFee.LatestStartDate ,
                                    TranFee.RecentFee
                          ) AS MBReturns ON CommissionUniverse.CPA_ID = MBReturns.CPA_ID
                                            AND CommissionUniverse.CPP_ID = MBReturns.CPP_ID
                LEFT JOIN ( SELECT  CDT.CDT_RunDateYYYYMM AS RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID AS CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID AS CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName AS Merchant ,
                                    CPA.CPA_ProductLine AS ProductLine ,
                                    CPA.CPA_ManualBillYN AS ManualBillYN ,
                                    CPP.CPP_ID AS CPP_ID ,
                                    CPP.CPP_MID AS MID ,
                                    CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                    ' ' AS FeeType ,
                                    TranFee.LatestStartDate AS StartDate ,
                                    TranFee.RecentFee AS FeeAmount ,
                                    SUM(CDT.CDT_TranCount) AS TotalCount
                            FROM    Billing.CDT_CustomerDailyTran AS CDT
                                    JOIN Billing.CPP_CustomerProcessingProfile
                                    AS CPP ON CDT.CDT_CPP_ID = CPP.CPP_ID
                                    JOIN Billing.CPA_CustomerProcessingAccount
                                    AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
			--------------------------------------------------------------------
			--   Added the Most Recent Rate in Effect for Manual Bill as well
			--     - Use the Return Fee, even for UnAuths
			--------------------------------------------------------------------
                                    LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                                        'ReturnFee' AS FeeType ,
                                                        CTF_FeeAmount AS RecentFee ,
                                                        MAX(CTF_StartDate) AS LatestStartDate
                                                FROM    Billing.CTF_CustomerTranFee
                                                WHERE   CTF_RunDateYYYYMM = @RunYearMonth
                                                        AND CTF_FeeType = 'Return Fee'
                                                GROUP BY CTF_CPA_ID ,
                                                        CTF_FeeAmount
                                              ) AS TranFee ON CPA.CPA_ID = TranFee.CPA_ID
                            WHERE   CDT.CDT_RunDateYYYYMM = @RunYearMonth
                                    AND CDT.CDT_TranType = 'UnAuth'
                                    AND CPP.CPP_SourceIdConfig BETWEEN @LowConfig
                                                              AND
                                                              @HighConfig
                                    AND CPA.CPA_ProductLine = 'ACH'
                                    AND CPA.CPA_ManualBillYN = 'Y'
                            GROUP BY CDT.CDT_RunDateYYYYMM ,
                                    CPP.CPP_CPA_ID ,
                                    CPA.CPA_CRMProcessingAccountID ,
                                    CPA.CPA_TrustName ,
                                    CPA.CPA_ProductLine ,
                                    CPA.CPA_ManualBillYN ,
                                    CPP.CPP_ID ,
                                    CPP.CPP_MID ,
                                    CPP.CPP_SourceIdConfig ,
                                    TranFee.LatestStartDate ,
                                    TranFee.RecentFee
                          ) AS MBUnAuths ON CommissionUniverse.CPA_ID = MBUnAuths.CPA_ID
                                            AND CommissionUniverse.CPP_ID = MBUnAuths.CPP_ID;


    END;
