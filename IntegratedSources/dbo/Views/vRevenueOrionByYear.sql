﻿



CREATE VIEW [dbo].[vRevenueOrionByYear]
AS

SELECT DISTINCT 
	   Orion.MID               AS MID
	  ,OrionLookup.SourceId    AS SourceId
	  ,Orion.Name              AS Name  
	  ,OrionLookup.CRM_Id      AS CRM_ID
	  ,YEAR(Orion.TranDate)    AS TranDate
	  ,SUM(Orion.Revenue)      AS Revenue
	  ,'Orion'                 AS Source  
FROM dbo.RevenueOrion              AS Orion
LEFT JOIN dbo.RevenueOrionLookup   AS OrionLookup ON Orion.MID = OrionLookup.MID
WHERE Orion.Revenue <> 0  
GROUP BY Orion.MID
        ,OrionLookup.SourceId
        ,Orion.Name
        ,OrionLookup.CRM_Id
        ,YEAR(Orion.TranDate)



