﻿CREATE TABLE [dbo].[ATIMonthlyVolumeGLFeed] (
    [Type]                                                 TINYINT          NULL,
    [CRMParentTID_GPExtenderNatlAcctID]                    UNIQUEIDENTIFIER NULL,
    [CRMParentName_GPExtenderNatlAcctName]                 NVARCHAR (255)   NULL,
    [BTAssignedActNum_GPParentActNum]                      NVARCHAR (10)    NULL,
    [CRMActLevelName_GPParentName]                         NVARCHAR (255)   NULL,
    [BTAssignedProcessingRecord_GPChildCustomerNum]        NVARCHAR (60)    NULL,
    [CRMProcessingRecordName_GPChildCustomerName]          NVARCHAR (150)   NULL,
    [CRMActLevelNum_GPExtenderParentNum]                   UNIQUEIDENTIFIER NULL,
    [CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard] UNIQUEIDENTIFIER NULL,
    [Main]                                                 NVARCHAR (5)     NULL,
    [BillingStreet]                                        NVARCHAR (255)   NULL,
    [BillingStreet2]                                       NVARCHAR (255)   NULL,
    [BillingStreet3]                                       NVARCHAR (255)   NULL,
    [BillingCity]                                          NVARCHAR (255)   NULL,
    [BillingState]                                         NVARCHAR (255)   NULL,
    [BillingZip]                                           NVARCHAR (255)   NULL,
    [BillingCountry]                                       NVARCHAR (255)   NULL,
    [Phone]                                                NVARCHAR (255)   NULL,
    [Fax]                                                  NVARCHAR (255)   NULL,
    [Email]                                                NVARCHAR (255)   NULL,
    [GPMain]                                               NVARCHAR (255)   NULL,
    [GPBillingStreet]                                      NVARCHAR (255)   NULL,
    [GPBillingStreet2]                                     NVARCHAR (255)   NULL,
    [GPBillingStreet3]                                     NVARCHAR (255)   NULL,
    [GPBillingCity]                                        NVARCHAR (255)   NULL,
    [GPBillingState]                                       NVARCHAR (255)   NULL,
    [GPBillingZip]                                         NVARCHAR (255)   NULL,
    [GPBillingCountry]                                     NVARCHAR (255)   NULL,
    [GPPhone]                                              NVARCHAR (255)   NULL,
    [GPFax]                                                NVARCHAR (255)   NULL,
    [GPEmail]                                              NVARCHAR (255)   NULL,
    [CRMActLvlEECount_GPParentEECount]                     INT              NULL,
    [CRMProcessingActEECount_GPChildEECount]               INT              NULL,
    [MID]                                                  NVARCHAR (255)   NULL,
    [ConfigNbr]                                            TINYINT          NULL,
    [ProcessingNbr]                                        NVARCHAR (8)     NULL,
    [SourceID]                                             NVARCHAR (255)   NULL,
    [Industry]                                             NVARCHAR (5)     NULL,
    [TranDate]                                             DATE             NULL,
    [Volume]                                               MONEY            NULL,
    [Count]                                                INT              NULL,
    [CRMActLevelPartner_GPParentPartner]                   NVARCHAR (255)   NULL,
    [CRMProcessingActPartner_GPChildPartner]               NVARCHAR (255)   NULL,
    [Product]                                              NVARCHAR (255)   NULL,
    [RevenueCategory]                                      TINYINT          NULL,
    [SetupDate]                                            DATE             NULL,
    [ProcessingDate]                                       DATE             NULL,
    [CancelDate]                                           DATE             NULL,
    [SalesPersonID]                                        NVARCHAR (255)   NULL,
    [SalesPersonFirstName]                                 NVARCHAR (255)   NULL,
    [SalesPersonMiddleName]                                NVARCHAR (255)   NULL,
    [SalesPersonLastName]                                  NVARCHAR (255)   NULL,
    [RevenueAmount]                                        MONEY            NULL,
    [Quantity]                                             INT              NULL,
    [MainGLSegment]                                        NVARCHAR (10)    NULL,
    [BatchID]                                              NVARCHAR (15)    NULL,
    [AmountReceived]                                       MONEY            NULL,
    [EFTFlag]                                              BIT              NULL,
    [CheckNumber]                                          NVARCHAR (15)    NULL,
    [PaymentType]                                          NVARCHAR (255)   NULL,
    [LineDescription]                                      NVARCHAR (40)    NULL,
    [Cost]                                                 MONEY            NULL,
    [InsertDate]                                           SMALLDATETIME    CONSTRAINT [DF_ATIMonthlyVolumeGLFeed_InsertDate] DEFAULT (getdate()) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CRM PARENT ID-GP EXTENDER NATL ACCT ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMParentTID_GPExtenderNatlAcctID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM PARENT NAME-GP EXTENDER NATL ACCT NAME]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMParentName_GPExtenderNatlAcctName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[BT ASSIGNED ACCT#-GP PARENT ACCT#]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'BTAssignedActNum_GPParentActNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM ACCT LEVEL NAME-GP PARENT NAME]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMActLevelName_GPParentName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[BT ASSIGNED PROCESSING RECORD#-GP CHILD CUSTOMER#]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'BTAssignedProcessingRecord_GPChildCustomerNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM PROCESSING RECORD NAME-GP CHILD CUSTOMER NAME]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMProcessingRecordName_GPChildCustomerName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM ACCT LEVEL#-GP EXTENDER PARENT#]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMActLevelNum_GPExtenderParentNum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM PROCESSING RECORD#-GP extender field on Customer card]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM ACCT LEVEL EE COUNT-GP PARENT EE COUNT]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMActLvlEECount_GPParentEECount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM PROCESSING ACCT EE COUNT-GP CHILD EE COUNT]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMProcessingActEECount_GPChildEECount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM ACCT LEVEL PARTNER-GP PARENT PARTNER]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMActLevelPartner_GPParentPartner';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[CRM PROCESSING ACCT PARTNER-GP CHILD PARTNER]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'CRMProcessingActPartner_GPChildPartner';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'[Main GL Segment]', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ATIMonthlyVolumeGLFeed', @level2type = N'COLUMN', @level2name = N'MainGLSegment';

