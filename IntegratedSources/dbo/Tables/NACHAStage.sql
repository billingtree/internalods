﻿CREATE TABLE [dbo].[NACHAStage] (
    [LineNumber]         INT           IDENTITY (1, 1) NOT NULL,
    [BlockNumber]        INT           NULL,
    [RecordType]         AS            (CONVERT([tinyint],left([SourceRecord],(1)))),
    [EntryORAddendaType] NVARCHAR (50) NULL,
    [IsCCDOffset]        BIT           NULL,
    [SourceRecord]       NVARCHAR (94) NULL,
    CONSTRAINT [PK_NACHA22] PRIMARY KEY CLUSTERED ([LineNumber] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NCIRecordType]
    ON [dbo].[NACHAStage]([RecordType] ASC)
    INCLUDE([LineNumber]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCIRecordtypeIncluded]
    ON [dbo].[NACHAStage]([RecordType] ASC)
    INCLUDE([LineNumber], [SourceRecord]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCIRecordTypeEntryORAddendaType]
    ON [dbo].[NACHAStage]([RecordType] ASC, [EntryORAddendaType] ASC)
    INCLUDE([LineNumber], [BlockNumber], [IsCCDOffset]) WITH (FILLFACTOR = 80);

