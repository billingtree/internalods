﻿





CREATE VIEW [dbo].[vATIDailyRevenueGLFeed]
AS
SELECT	Type
		,CRMParentTID_GPExtenderNatlAcctID AS [CRM PARENT ID-GP EXTENDER NATL ACCT ID]
		,CRMParentName_GPExtenderNatlAcctName AS [CRM PARENT NAME-GP EXTENDER NATL ACCT NAME]
		,BTAssignedActNum_GPParentActNum AS [BT ASSIGNED ACCT#-GP PARENT ACCT#]
		,CRMActLevelName_GPParentName AS [CRM ACCT LEVEL NAME-GP PARENT NAME]
		,BTAssignedProcessingRecord_GPChildCustomerNum AS [BT ASSIGNED PROCESSING RECORD#-GP CHILD CUSTOMER#]
		,CRMProcessingRecordName_GPChildCustomerName AS [CRM PROCESSING RECORD NAME-GP CHILD CUSTOMER NAME]
		,CRMActLevelNum_GPExtenderParentNum AS [CRM ACCT LEVEL#-GP EXTENDER PARENT#]
		,CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard AS [CRM PROCESSING RECORD#-GP extender field on Customer card]
		,Main
		,BillingStreet
		,BillingStreet2
		,BillingStreet3
		,BillingCity
		,BillingState
		,BillingZip
		,BillingCountry
		,Phone
		,Fax
		,Email
		,GPMain
		,GPBillingStreet
		,GPBillingStreet2
		,GPBillingStreet3
		,GPBillingCity
		,GPBillingState
		,GPBillingZip
		,GPBillingCountry
		,GPPhone
		,GPFax
		,GPEmail
		,CRMActLvlEECount_GPParentEECount AS [CRM ACCT LEVEL EE COUNT-GP PARENT EE COUNT]
		,CRMProcessingActEECount_GPChildEECount AS [CRM PROCESSING ACCT EE COUNT-GP CHILD EE COUNT]
		,MID
		,ConfigNbr
		,ProcessingNbr
		,SourceID
		,Industry
		,TranDate
		,Volume
		,Count
		,CRMActLevelPartner_GPParentPartner AS [CRM ACCT LEVEL PARTNER-GP PARENT PARTNER]
		,CRMProcessingActPartner_GPChildPartner AS [CRM PROCESSING ACCT PARTNER-GP CHILD PARTNER]
		,Product
		,RevenueCategory
		,SetupDate
		,ProcessingDate
		,CancelDate
		,SalesPersonID
		,SalesPersonFirstName
		,SalesPersonMiddleName
		,SalesPersonLastName
		,RevenueAmount
		,Quantity
		,MainGLSegment AS [Main GL Segment]
		,BatchID
		,AmountReceived
		,EFTFlag
		,CheckNumber
		,PaymentType
		,LineDescription
		,Cost
FROM	dbo.ATIDailyRevenueGLFeed







