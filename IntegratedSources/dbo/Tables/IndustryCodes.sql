﻿CREATE TABLE [dbo].[IndustryCodes] (
    [SIC]                     CHAR (10)      NULL,
    [IndustryName]            NVARCHAR (255) NULL,
    [GP_CODE]                 CHAR (2)       NULL,
    [GP_INDUSTRY_DESCRIPTION] NVARCHAR (255) NULL
);

