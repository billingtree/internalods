﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spConvenienceFeeAssociationMeSReport](
--@CustomerMerchantID   FLOAT,
@BTConvFeeMerchantID    FLOAT,
@Month                INT,
@Year                 INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    --DECLARE @nCustomerMerchantID       FLOAT
    DECLARE @nBTConvFeeMerchantID        FLOAT
    --SET @nCustomerMerchantID = @CustomerMerchantID
    SET @nBTConvFeeMerchantID = @BTConvFeeMerchantID      
    
    DECLARE @nYear                     INT
    DECLARE @nMonth                    INT
    SET @nYear = @Year
    SET @nMonth = @Month 
    
    DECLARE @YearMonth                 INT
    DECLARE @txtYear                   VARCHAR (4)
    DECLARE @txtMonth                  VARCHAR (2)
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @nMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @nMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @nMonth) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))     
    
    
SELECT MeSCustomer.DBAName                 AS "CustomerName"
      ,A.Address1_StateOrProvince          AS "CustomerState"   
      ,MeSCustomer.AuthorizationCode       AS "CustomerAuthCode"
      ,MeSCustomer.MerchantID              AS "CustomerMerchantID"
      ,MeSCustomer.BatchDate               AS "CustomerBatchDate"
      ,MeSCustomer.TransactionDate         AS "CustomerTranDate"
      ,MeSCustomer.CardNumber              AS "CustomerCard"
      ,MeSCustomer.TransactionAmount       AS "CustomerAmount"
      ,MeSCustomer.Reference               AS "CustomerReference"
      ,BTConvFee.DBAName                   AS "BTName" 
      ,BTConvFee.AuthorizationCode         AS "BTAuthCode"                                          
      ,BTConvFee.MerchantID                AS "BTMerchantID"
      ,BTConvFee.BatchDate                 AS "BTBatchDate"
      ,BTConvFee.TransactionDate           AS "BTTranDate"
      ,BTConvFee.CardNumber                AS "BTCard"
      ,BTConvFee.TransactionAmount         AS "BTAmount"
      ,BTConvFee.Reference                 AS "BTReference"
      ,@YearMonth                          AS "ParmYYYYMM"

                                         
FROM MeS.SettlementSummary              AS MeSCustomer
JOIN CRMShadow.MS.bt_ProcessingProfile  AS PP ON MeSCustomer.CRM_PPDID = PP.bt_ProcessingProfileID
JOIN CRMShadow.MS.bt_ProcessingAccount  AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
JOIN CRMShadow.MS.Account               AS A  ON PA.bt_Account = A.AccountId
LEFT JOIN MeS.SettlementSummary         AS BTConvFee 
                   ON MeSCustomer.TransactionDate = BTConvFee.TransactionDate
                  AND LOWER(MeSCustomer.CardNumber) = LOWER(BTConvFee.CardNumber)
                  AND BTConvFee.UsabilityIndex = 1
                  AND BTConvFee.MerchantID = @nBTConvFeeMerchantID

WHERE MeSCustomer.BatchDateYYYYMM = @YearMonth
  AND MeSCustomer.UsabilityIndex = 1
  AND MeSCustomer.MerchantID IN ('941000108108','941000108111','941000115005','941000116300','941000118019'
                                ,'941000118018','941000119554','941000119555','941000119553','941000119556'
                                ,'941000119557','941000119558','941000121616','941000119125'
                                ,'941000112089','941000121954','941000122393','941000122394'
                                ,'941000122740','941000117859')
  --@nCustomerMerchantID




END
GO

