﻿





-- =============================================
-- Author:		Yaseen Jamaludeen
-- Create date: 2014-01-16
-- Description:	Procedure to get ATI Daily Revenue data.
-- =============================================
CREATE PROCEDURE [dbo].[spATIDailyRevenue] --'2014-02-28'
(
	@RevenueDate date = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	IF @RevenueDate IS NULL
		SET @RevenueDate = getdate()
	INSERT INTO dbo.ATIDailyRevenue
			   (Description
			   ,Config
			   ,ReceivingCompany
			   ,IDNumber
			   ,ProcessingNbr
			   ,SettleDate
			   ,FormattedAmount
			   ,CheckNumber
			   ,MainGLSegment)
	SELECT	--REPLACE(@RevenueDate,'-','')
			CONVERT(nvarchar(40),Description)
			,CONVERT(tinyint,Config)
			,CONVERT(nvarchar(22),ReceivingCompany)
			,CONVERT(nvarchar(15),IDNumber)
			,CONVERT(nvarchar(8),ProcessingNbr)
			,SettleDate
			,CONVERT(money,FormattedAmount)
			,CONVERT(nvarchar(15),CheckNumber)
			,CONVERT(nvarchar(10),MainGLSegment)
	FROM	(
				------------------------------------------------------
				--   Config 22 Monthly Fees DEPOSITS
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET')													AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22F'				AS CheckNumber
						,'41000'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%RESUBMIT%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'    -- See Comment below in 47F
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0

				UNION ALL
				------------------------------------------------------
				--   Config 22 Monthly Fees WITHDRAWALS
				--     There was only 1 instance of this (a negative amount); 6-Nov-2013 for -$4,800.00
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET Debit')											AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22FD'			AS CheckNumber
						,'42020'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				------------------------------------------------------
				--   Config 47 Monthly Fees DEPOSITS
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET')													AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47F'				AS CheckNumber
						,'41000'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE47%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%RESUBMIT%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'	-- Knox Associates, Settlement Fees were
																					-- returned and then re-submitted using
																					-- 0-NET, but within a Fee Batch.
																					-- Decision was to do the same for Config
																					-- 22 and 57, even though there were no
																					-- instances of this.
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0

				UNION ALL
				------------------------------------------------------
				--   Config 47 Monthly Fees WITHDRAWALS
				--     There are no instances of this (negative Amounts), but did the same as Config 22
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET Debit')											AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47FD'			AS CheckNumber
						,'42020'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE47%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
  								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'      -- See Comment above in 47F
  							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				------------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 47 REFUND of Setup Fees before they are a BT Customer
				--     These Refund Fees should come in under Config 22 files, but at times under 47
				------------------------------------------------------------------------------------
				SELECT      CONCAT(ATI.CompanyName,'  Dummy Acct Refund')									AS Description
							,ATI.ConfigNumber																AS Config
							,ATI.ReceivingCompanyName														AS ReceivingCompany
							,ATI.IdentificationNumber														AS IDNumber
							,'1003'																			AS ProcessingNbr -- Need to use Default Processing #
							,ATI.EffectiveEntryDate															AS SettleDate
							,ATI.FormattedAmount															AS FormattedAmount
							,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47Ref'		AS CheckNumber
							,'41000'																		AS MainGLSegment
				FROM  NACHA.Originations AS ATI
				WHERE UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE47%'
							AND UPPER(ATI.IdentificationNumber) LIKE '0-REFUND%'
							AND ATI.EffectiveEntryDate = @RevenueDate
							AND ATI.UsabilityIndex = 1
							AND ATI.SourceType = 4
							AND ATI.FormattedAmount < 0

				UNION ALL
				------------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 57 REFUND of Setup Fees before they are a BT Customer
				--     These Refund Fees should come in under Config 22 files, but it may under 57
				------------------------------------------------------------------------------------
				SELECT      CONCAT(ATI.CompanyName,'  Dummy Acct Refund')									AS Description
							,ATI.ConfigNumber																AS Config
							,ATI.ReceivingCompanyName														AS ReceivingCompany
							,ATI.IdentificationNumber														AS IDNumber
							,'1004'																			AS ProcessingNbr -- Need to use Default Processing #
							,ATI.EffectiveEntryDate															AS SettleDate
							,ATI.FormattedAmount															AS FormattedAmount
							,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57Ref'		AS CheckNumber
							,'41000'																		AS MainGLSegment
				FROM  NACHA.Originations AS ATI
				WHERE UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE57%'
							AND UPPER(ATI.IdentificationNumber) LIKE '0-REFUND%'
							AND ATI.EffectiveEntryDate = @RevenueDate
							AND ATI.UsabilityIndex = 1
							AND ATI.SourceType = 4
							AND ATI.FormattedAmount < 0

				UNION ALL
				------------------------------------------------------
				--   Config 57 Monthly Fees DEPOSITS
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET')													AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57F'				AS CheckNumber
						,'41000'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE57%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%RESUBMIT%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%ACHMODFEE%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0

				UNION ALL
				------------------------------------------------------
				--   Config 57 Monthly Fees  WITHDRAWALS
				--     There are no instances of this (negative Amounts), but did the same as Config 22
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET Debit')											AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57FD'				AS CheckNumber
						,'42020'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE57%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
  								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%ACHMODFEE%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0
						
				UNION ALL
				------------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 58 REFUND of Setup Fees before they are a BT Customer
				--     These Refund Fees should come in under Config 22 files, but it may under 58    -- ADDED 23-Jun-2014
				------------------------------------------------------------------------------------
				SELECT      CONCAT(ATI.CompanyName,'  Dummy Acct Refund')									AS Description
							,ATI.ConfigNumber																AS Config
							,ATI.ReceivingCompanyName														AS ReceivingCompany
							,ATI.IdentificationNumber														AS IDNumber
							,'1003'																			AS ProcessingNbr -- Need to use Default Processing #
							,ATI.EffectiveEntryDate															AS SettleDate
							,ATI.FormattedAmount															AS FormattedAmount
							,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58Ref'		AS CheckNumber
							,'41000'																		AS MainGLSegment
				FROM  NACHA.Originations AS ATI
				WHERE UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE58%'
							AND UPPER(ATI.IdentificationNumber) LIKE '0-REFUND%'
							AND ATI.EffectiveEntryDate = @RevenueDate
							AND ATI.UsabilityIndex = 1
							AND ATI.SourceType = 4
							AND ATI.FormattedAmount < 0

				UNION ALL
				------------------------------------------------------
				--   Config 58 Monthly Fees DEPOSITS                           -- ADDED 23-Jun-2014
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET')													AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58F'				AS CheckNumber
						,'41000'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE58%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%RESUBMIT%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%ACHMODFEE%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0

				UNION ALL
				------------------------------------------------------
				--   Config 58 Monthly Fees  WITHDRAWALS
				--     There are no instances of this (negative Amounts), but did the same as Config 22   --  ADDED 23-Jun-2014
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET Debit')											AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58FD'				AS CheckNumber
						,'42020'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE58%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
  								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%ACHMODFEE%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0
												

				UNION ALL

				------------------------------------------------------
				--   Config 60 Monthly Fees  DEPOSITS
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET')													AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60F'				AS CheckNumber
						,'41000'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE60%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%RESUBMIT%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0

				UNION ALL
				------------------------------------------------------
				--   Config 60 Monthly Fees  WITHDRAWALS
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  OFFSET Debit')											AS Description
						,ATI.ConfigNumber																	AS Config
						,ATI.ReceivingCompanyName															AS ReceivingCompany
						,ATI.IdentificationNumber															AS IDNumber
						,CASE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,3)))
							WHEN 'NET'
								THEN UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,6,4)))
							ELSE UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))
						END																					AS ProcessingNbr
						,ATI.EffectiveEntryDate																AS SettleDate
						,ATI.FormattedAmount																AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60FD'				AS CheckNumber
						,'42020'																			AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE60%'
						AND (
								UPPER(ATI.IdentificationNumber) LIKE '%ACHMOFEE%'
								OR UPPER(ATI.IdentificationNumber) LIKE '%BTWEBPAY%'
								OR UPPER(ATI.IdentificationNumber) LIKE '0-NET%'
  							)
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				----------------------------------------------------------
				--
				----------------------------------------------------------
				------------------------------------------------------
				--   Config # 22 Transaction Revenue
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22N'	AS CheckNumber
						,'42000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET%'
						AND UPPER(ATI.ReceivingCompanyName) NOT LIKE 'EPP ACH FEE%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				------------------------------------------------------
				--   Config # 47 Transaction Revenue
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47N'	AS CheckNumber
						,'42000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE47%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET%'
						AND UPPER(ATI.ReceivingCompanyName) NOT LIKE 'EPPI ACH FEE%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				------------------------------------------------------
				--   Config # 57 Transaction Revenue
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57N'	AS CheckNumber
						,'42000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE57%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET%'
						AND UPPER(ATI.ReceivingCompanyName) NOT LIKE 'EPI ACH FEE%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				------------------------------------------------------
				--   Config # 58 Transaction Revenue                          --   ADDED 23-Jun-2014   ---
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58N'	AS CheckNumber
						,'42000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE58%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET%'
						AND UPPER(ATI.ReceivingCompanyName) NOT LIKE 'EPI ACH FEE%'  -- copied from 57, as SCSI 1st MID in 58 
						AND ATI.EffectiveEntryDate = @RevenueDate                    -- was procesing under 57
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4						

				UNION ALL
				------------------------------------------------------
				--   Config # 60 Transaction Revenue
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60N'	AS CheckNumber
						,'42000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE60%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET%'
						-- AND UPPER(ATI.ReceivingCompanyName) NOT LIKE 'EPP ACH FEE%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				-------------------------------------------------------------------------
				--   Looking for Billing Tree Settle (Config 22) with ACH Fees Involved WITHDRAWALS (Debit)
				------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,(ATI.FormattedAmount * -1)												AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22D'	AS CheckNumber
						,'42020'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET1003%'
						AND ATI.ConfigNumber = 22
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				-------------------------------------------------------------------------
				--   Looking for Billing Tree Settle (Config 47) with ACH Fees Involved WITHDRAWALS (Debit)
				------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName, '  NET1003')									AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,(ATI.FormattedAmount * -1)												AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47D'	AS CheckNumber
						,'42020'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET1003%'
						AND ATI.ConfigNumber = 47
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				-------------------------------------------------------------------------
				--   Looking for Billing Tree Settle (Config 57) with ACH Fees Involved WITHDRAWALS (Debit)
				------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,(ATI.FormattedAmount * -1)												AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57D'	AS CheckNumber
						,'42020'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET1003%'
						AND ATI.ConfigNumber = 57
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				-------------------------------------------------------------------------
				--   Looking for Billing Tree Settle (Config 58) with ACH Fees Involved WITHDRAWALS --   ADDED 23-Jun-2014   ---
				------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,(ATI.FormattedAmount * -1)												AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58D'	AS CheckNumber
						,'42020'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET1003%'
						AND ATI.ConfigNumber = 58
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						
				UNION ALL
				-------------------------------------------------------------------------
				--   Looking for Billing Tree Settle (Config 60) with ACH Fees Involved WITHDRAWALS
				------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  NET1003')										AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr
						,ATI.EffectiveEntryDate													AS SettleDate
						,(ATI.FormattedAmount * -1)												AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60D'	AS CheckNumber
						,'42020'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND UPPER(ATI.IdentificationNumber) LIKE 'NET1003%'
						AND ATI.ConfigNumber = 60
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4

				UNION ALL
				------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 22 Setup Fees; i.e. before they are a BT Customer
				--               These Fees only come in on Config 22 files
				--     We Need the Dummy Account Default created in CRM and Use its Processing #
				------------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  Dummy Acct')									AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1003'																	AS ProcessingNbr -- Need to use Default Processing #
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22F'	AS CheckNumber
						,'41000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%ACHMOFEE%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%BTWEBPAY%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%RESUBMIT%'
						--AND UPPER(ATI.IdentificationNumber) LIKE '%ACH/CC%' or '%SETUP%' or ......
						AND UPPER(ATI.IdentificationNumber) NOT LIKE 'NET%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-NET%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0
						
				UNION ALL
				------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 57 Setup Fees; i.e. before they are a BT Customer
				--               Oct 2014, UpFronts changed to come in on Config 57 
				--     We Need the Dummy Account Default created in CRM and Use its Processing #
				------------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'57 Dummy Acct')									AS Description
						,ATI.ConfigNumber														AS Config
						,ATI.ReceivingCompanyName												AS ReceivingCompany
						,ATI.IdentificationNumber												AS IDNumber
						,'1004'																	AS ProcessingNbr -- Need to use Default Processing #
						,ATI.EffectiveEntryDate													AS SettleDate
						,ATI.FormattedAmount													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57UF'	AS CheckNumber
						,'41000'																AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREE%'
				        AND ATI.ConfigNumber = '57'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%ACHMOFEE%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%BTWEBPAY%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%RESUBMIT%'
						--AND UPPER(ATI.IdentificationNumber) LIKE '%ACH/CC%' or '%SETUP%' or ......
						AND UPPER(ATI.IdentificationNumber) NOT LIKE 'NET%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-NET%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '%ACHMODFEE%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount > 0
						
				UNION ALL
				------------------------------------------------------------------------------------
				--   DUMMY ACCOUNTS - Config 22 REFUND of Setup Fees before they are a BT Customer
				--               These Refund Fees only come in on Config 22 files
				------------------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  Dummy Acct Refund')								AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,'1003'																		AS ProcessingNbr -- Need to use Default Processing #
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22Ref'	AS CheckNumber
						,'41000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND UPPER(ATI.IdentificationNumber) LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				-----------------------------------------------------------------------------------------------
				--    Get any Refund Transactions for Configs 22, 47, 57, 58 and 60
				-----------------------------------------------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  Refund')											AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '22Ref'	AS CheckNumber
						,'42000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE22%'
						AND UPPER(ATI.IdentificationNumber) LIKE '%REFUND%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				--  Refunds for Config 47
				SELECT	CONCAT(ATI.CompanyName,'  Refund')											AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '47Ref'	AS CheckNumber
						,'42000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE47%'
						AND UPPER(ATI.IdentificationNumber) LIKE '%REFUND%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				--  Refunds for Config 57
				SELECT	CONCAT(ATI.CompanyName,'  Refund')											AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '57Ref'	AS CheckNumber
						,'42000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE57%'
						AND UPPER(ATI.IdentificationNumber) LIKE '%REFUND%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0

				UNION ALL
				--  Refunds for Config 58           --   ADDED 23-Jun-2014   ---
				SELECT	CONCAT(ATI.CompanyName,'  Refund')											AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '58Ref'	AS CheckNumber
						,'42000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE58%'
						AND UPPER(ATI.IdentificationNumber) LIKE '%REFUND%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0
						
				UNION ALL
				--  Refunds for Config 60
				SELECT	CONCAT(ATI.CompanyName,'  Refund')											AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,3,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60Ref'	AS CheckNumber
						,'42000'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREEFEE60%'
						AND UPPER(ATI.IdentificationNumber) LIKE '%REFUND%'
						AND UPPER(ATI.IdentificationNumber) NOT LIKE '0-REFUND%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
						AND ATI.FormattedAmount < 0
				UNION ALL
				------------------------------------------------------
				--   Convenience Fee Credit for Config 60           --
				------------------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  CFeeCredit')										AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,'3065'																		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,ATI.FormattedAmount														AS FormattedAmount
						,LEFT(CONVERT(char, dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60CFC'	AS CheckNumber
						,'42010'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLNGTREE%'
						AND ATI.ConfigNumber = 60
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
				UNION ALL
				---------------------------------------------
				--  Convenience Fee Debits for Config 60   --
				---------------------------------------------
				SELECT	CONCAT(ATI.CompanyName,'  CFeeDebit')										AS Description
						,ATI.ConfigNumber															AS Config
						,ATI.ReceivingCompanyName													AS ReceivingCompany
						,ATI.IdentificationNumber													AS IDNumber
						,UPPER(CONVERT(nvarchar(8),SUBSTRING(ATI.IdentificationNumber,4,4)))		AS ProcessingNbr
						,ATI.EffectiveEntryDate														AS SettleDate
						,(ATI.FormattedAmount * -1)													AS FormattedAmount
						,LEFT(CONVERT(char,dbo.fnDateToJulian(EffectiveEntryDate)), 7) + '60CFD'	AS CheckNumber
						,'42010'																	AS MainGLSegment
				FROM	NACHA.Originations AS ATI
				WHERE	UPPER(ATI.CompanyName) LIKE 'BILLINGTREESETTL%'
						AND ATI.ConfigNumber = 60
						AND ATI.IdentificationNumber LIKE 'NET3065%'
						AND ATI.EffectiveEntryDate = @RevenueDate
						AND ATI.UsabilityIndex = 1
						AND ATI.SourceType = 4
			) AS ATIDailyRevenue
END







