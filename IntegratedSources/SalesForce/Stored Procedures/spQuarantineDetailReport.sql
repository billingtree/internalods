﻿













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spQuarantineDetailReport](
@Source        VARCHAR (7),
@CompanyName   VARCHAR (250),
@Config        INT, 
@MID           VARCHAR (20), 
@TypeRec       VARCHAR (1) 
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
    DECLARE @nConfig          INT
    DECLARE @vMID             VARCHAR (16)
    DECLARE @txtSource        VARCHAR (7)
    DECLARE @txtCompanyName   VARCHAR (250)
    DECLARE @txtTypeRec       VARCHAR (1)
    
    SET @txtSource = @Source
    SET @txtCompanyName = @CompanyName
    SET @txtTypeRec = @TypeRec
    SET @nConfig = @Config
    SET @vMID = @MID  
    
-------------------------------------------------------------------------------
--            Get ATI Originations and Returns from Quarantine               --
-------------------------------------------------------------------------------        
    BEGIN
         IF @txtSource = 'ATI'  
             IF @txtTypeRec = 'O'
                 SELECT 'ATI'                         AS "Source"
                       ,ATIOrig.ReferenceCode         AS "ReferenceCode"
                       ,ATIOrig.ConfigNumber          AS "ConfigSource"
                       ,ATIOrig.ProcessingNumber      AS "ProcessingMID"
                       ,ATIOrig.FileCreationDate      AS "TranDate"
                       ,ATIOrig.CompanyName           AS "Name"
                       ,ATIOrig.ReceivingCompanyName  AS "AcctHolder"
                       ,ATIOrig.FormattedAmount       AS "Amount"
                       ,ATIOrig.IdentificationNumber  AS "IDNumber"
                       ,ATIOrig.TransactionCode       AS "TranReturnCode"
                       ,'Origination'                 AS "TransType"               
                 FROM NACHA.Originations         ATIOrig
                 WHERE ATIOrig.UsabilityIndex = 0
                   AND ATIOrig.SourceType = 4
                   AND ATIOrig.CompanyName = @txtCompanyName
                   AND ATIOrig.ConfigNumber = @nConfig
             ELSE
                 SELECT 'ATI'                           AS "Source"
                       ,ATIReturn.ReferenceCode         AS "ReferenceCode"
                       ,ATIReturn.ConfigNumber          AS "ConfigSource"
                       ,ATIReturn.ProcessingNumber      AS "ProcessingMID"
                       ,ATIReturn.FileCreationDate      AS "TranDate"
                       ,ATIReturn.CompanyName           AS "Name"
                       ,ATIReturn.ReceivingCompanyName  AS "AcctHolder"
                       ,ATIReturn.FormattedAmount       AS "Amount"
                       ,ATIReturn.IdentificationNumber  AS "IDNumber" 
                       ,ATIReturn.ReturnCode            AS "TranReturnCode"                      
                       ,'Return'                        AS "TransType"               
                 FROM NACHA.Returns         ATIReturn
                 WHERE ATIReturn.UsabilityIndex = 0
                   AND ATIReturn.SourceType = 4
                   AND ATIReturn.CompanyName = @txtCompanyName
                   AND ATIReturn.ConfigNumber = @nConfig
    END                   

-------------------------------------------------------------------------------
--            Get ATI API Transactions and Returns from Quarantine           --
-------------------------------------------------------------------------------        
    BEGIN
         IF @txtSource = 'ATI API'  
             IF @txtTypeRec = 'T'
                 SELECT 'ATI API'                    AS "Source"
                       ,ATIAPI.Reference             AS "ReferenceCode"
                       ,ATIAPI.ConfigNumber          AS "ConfigSource"
                       ,ATIAPI.AccountID             AS "ProcessingMID"
                       ,ATIAPI.BatchDate             AS "TranDate"
                       ,'Name Not Provided'          AS "Name"
                       ,ATIAPI.AccountName           AS "AcctHolder"
                       ,ATIAPI.CheckAmount           AS "Amount"
                       ,ATIAPI.Reference             AS "IDNumber"
                       ,ATIAPI.CheckType             AS "TranReturnCode"
                       ,'Transaction'                AS "TransType"               
                 FROM Billing.ACHTransByBatchDate   ATIAPI
                 WHERE ATIAPI.UsabilityIndex = 0
                   AND ATIAPI.AccountID = @txtCompanyName
                   AND ATIAPI.ConfigNumber = @nConfig
             ELSE
                 SELECT 'ATI API'                    AS "Source"
                       ,ATIAPI.Reference             AS "ReferenceCode"
                       ,ATIAPI.ConfigNumber          AS "ConfigSource"
                       ,ATIAPI.AccountID             AS "ProcessingMID"
                       ,ATIAPI.ReturnDate            AS "TranDate"
                       ,'Name Not Provided'          AS "Name"
                       ,ATIAPI.ConsumerLastName      AS "AcctHolder"
                       ,ATIAPI.DebitAmount           AS "Amount"
                       ,ATIAPI.Reference             AS "IDNumber" 
                       ,ATIAPI.ReturnCode            AS "TranReturnCode"                      
                       ,'Return'                     AS "TransType"               
                 FROM Billing.ACHReturnsByReturnDate  ATIAPI
                 WHERE ATIAPI.UsabilityIndex = 0
                   AND ATIAPI.AccountID = @txtCompanyName
                   AND ATIAPI.ConfigNumber = @nConfig
    END      

-------------------------------------------------------------------------------
--         Get InStep Originations and Returns from Quarantine               --
-------------------------------------------------------------------------------    
    BEGIN
         IF @txtSource = 'InStep'  
             IF @txtTypeRec = 'O'
                 SELECT 'InStep'                         AS "Source"
                       ,InstepOrig.ReferenceCode         AS "ReferenceCode"
                       ,InstepOrig.ConfigNumber          AS "ConfigSource"
                       ,InstepOrig.ProcessingNumber      AS "ProcessingMID"
                       ,InstepOrig.FileCreationDate      AS "TranDate"
                       ,InstepOrig.CompanyName           AS "Name"
                       ,InstepOrig.ReceivingCompanyName  AS "AcctHolder"
                       ,InstepOrig.FormattedAmount       AS "Amount"
                       ,InstepOrig.IdentificationNumber  AS "IDNumber" 
                       ,InstepOrig.TransactionCode       AS "TranReturnCode"                      
                       ,'Origination'                    AS "TransType"               
                 FROM NACHA.Originations         InstepOrig
                 WHERE InstepOrig.UsabilityIndex = 0
                   AND InstepOrig.SourceType = 1
                   AND InstepOrig.CompanyName = @txtCompanyName
                   AND InstepOrig.ConfigNumber = @nConfig
             ELSE
                 SELECT 'InStep'                           AS "Source"
                       ,InStepReturn.ReferenceCode         AS "ReferenceCode"
                       ,InStepReturn.ConfigNumber          AS "ConfigSource"
                       ,InStepReturn.ProcessingNumber      AS "ProcessingMID"
                       ,InStepReturn.FileCreationDate      AS "TranDate"
                       ,InStepReturn.CompanyName           AS "Name"
                       ,InStepReturn.ReceivingCompanyName  AS "AcctHolder"
                       ,InStepReturn.FormattedAmount       AS "Amount"
                       ,InStepReturn.IdentificationNumber  AS "IDNumber" 
                       ,InStepReturn.ReturnCode            AS "TranReturnCode"                        
                       ,'Return'                           AS "TransType"               
                 FROM NACHA.Returns         InStepReturn
                 WHERE InStepReturn.UsabilityIndex = 0
                   AND InStepReturn.SourceType = 1
                   AND InStepReturn.CompanyName = @txtCompanyName
                   AND InStepReturn.ConfigNumber = @nConfig
    END 
    

-------------------------------------------------------------------------------
--            Get USAePay Transactions from Quarantine                       --
-------------------------------------------------------------------------------    
    BEGIN
         IF @txtSource = 'USAePay'  
              IF @txtTypeRec = 'T'
				 SELECT 'USAePay'                 AS "Source"
					   ,USA.FileName              AS "ReferenceCode"
					   ,USA.USAePayID             AS "ConfigSource"
					   ,USA.MerchantID            AS "ProcessingMID"
					   ,USA.TransDate             AS "TranDate"
					   ,USA.MerchantName          AS "Name"
					   ,USA.AccountHolder         AS "AcctHolder"
					   ,USA.Amount                AS "Amount"
					   ,CONCAT(USA.TransType, ' ', USA.Result)
												  AS "TransType"               
				 FROM USAePay.FileData        USA
				 WHERE USA.UsabilityIndex = 0
					   AND USA.MerchantName = @txtCompanyName
					   AND USA.MerchantID = @vMID
            ELSE
                SELECT 'USAePay'                  AS "Source"
					   ,USA.FileName              AS "ReferenceCode"
					   ,USA.SourceID              AS "ConfigSource"
					   ,USA.MerchantID            AS "ProcessingMID"
					   ,USA.TransDate             AS "TranDate"
					   ,USA.MerchantName          AS "Name"
					   ,'The Merchant'            AS "AcctHolder"
					   ,USA.Amount                AS "Amount"
					   ,'Daily Summary'           AS "TransType"               
				 FROM USAePay.DailySummary        USA
				 WHERE USA.UsabilityIndex = 0
					   AND USA.MerchantName = @txtCompanyName
					   AND ISNULL(USA.MerchantID, '') = ISNULL(@vMID, '')			   
    END     


-------------------------------------------------------------------------------
--         Get MeS Transactions or Chargebacks from Quarantine               --
-------------------------------------------------------------------------------    
    BEGIN
         IF @txtSource = 'MeS'  
             IF @txtTypeRec = 'T'
                 SELECT 'MeS'                     AS "Source"
                       ,Mes.ReportReferenceCode   AS "ReferenceCode"
                       ,NULL                      AS "ConfigSource"
                       ,Mes.MerchantID            AS "ProcessingMID"
                       ,Mes.TransactionDate       AS "TranDate"
                       ,Mes.DBAName               AS "Name"
                       ,Mes.CardNumber            AS "AcctHolder"
                       ,Mes.TransactionAmount     AS "Amount"
                       ,'Transaction'             AS "TransType"               
                 FROM MeS.SettlementSummary        Mes
                 WHERE Mes.UsabilityIndex = 0
                       AND Mes.DBAName = @txtCompanyName
                       AND Mes.MerchantID = @vMID
            ELSE
                 SELECT 'MeS'                     AS "Source"
                       ,Mes.ReportReferenceCode   AS "ReferenceCode"
                       ,NULL                      AS "ConfigSource"
                       ,Mes.MerchantID            AS "ProcessingMID"
                       ,Mes.TransactionDate       AS "TranDate"
                       ,Mes.DBAName               AS "Name"
                       ,Mes.CardNumber            AS "AcctHolder"
                       ,Mes.TransactionAmount     AS "Amount"
                       ,'Chargeback'              AS "TransType"               
                 FROM MeS.ChargebackAdjustments    Mes
                 WHERE Mes.UsabilityIndex = 0
                       AND Mes.DBAName = @txtCompanyName
                       AND Mes.MerchantID = @vMID                                   
    END     


-------------------------------------------------------------------------------
--                Get Global Transactions from Quarantine                    --
-------------------------------------------------------------------------------    
    BEGIN
         IF @txtSource = 'Global'  
             SELECT 'Global'                  AS "Source"
                   ,Global.FileName           AS "ReferenceCode"
                   ,NULL                      AS "ConfigSource"
                   ,Global.MerchantID         AS "ProcessingMID"
                   ,Global.TranDate           AS "TranDate"
                   ,Global.MerchantName       AS "Name"
                   ,Global.CardholderNumber   AS "AcctHolder"
                   ,Global.Amount             AS "Amount"
                   ,'Transaction'             AS "TransType"               
             FROM Global.FileDataPersisted   Global
             WHERE Global.UsabilityIndex = 0
                   AND Global.MerchantName = @txtCompanyName
                   AND Global.MerchantID LIKE CONCAT('%', @vMID)
    END     




END















