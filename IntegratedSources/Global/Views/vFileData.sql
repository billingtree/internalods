﻿








CREATE VIEW [Global].[vFileData] WITH SCHEMABINDING AS
SELECT	FileName
		,LineNumber
		,SUBSTRING(SourceRecord,1,3) AS SourceID
		--,CONVERT(date,SUBSTRING(SourceRecord,8,2)+SUBSTRING(SourceRecord,4,4)) AS FileDate		--		SUBSTRING(SourceRecord,4,6)
		,SUBSTRING(SourceRecord,4,6) AS FileDate
		--,CONVERT(time(0),SUBSTRING(SourceRecord,10,2)+':'+SUBSTRING(SourceRecord,12,2)+':'+SUBSTRING(SourceRecord,14,2)) AS FileTime		--SUBSTRING(SourceRecord,10,6)
		,SUBSTRING(SourceRecord,10,6) AS FileTime
		,SUBSTRING(SourceRecord,16,4) AS ScanCharge
		,SUBSTRING(SourceRecord,20,10) AS DepositID
		,SUBSTRING(SourceRecord,30,14) AS Hierarchy
		,SUBSTRING(SourceRecord,44,16) AS MerchantID
		,SUBSTRING(SourceRecord,60,11) AS CashLetterNumber
		,SUBSTRING(SourceRecord,71,15) AS CardAcceptorID
		,SUBSTRING(SourceRecord,86,2) AS TranCode
		,SUBSTRING(SourceRecord,88,19) AS CardholderNumber
		,SUBSTRING(SourceRecord,107,9) AS Amount
		,SUBSTRING(SourceRecord,116,6) AS AuthCode
		,SUBSTRING(SourceRecord,122,11) AS ReferenceNumber
		--,CONVERT(date,SUBSTRING(SourceRecord,137,2)+SUBSTRING(SourceRecord,133,4)) AS TranDate		--SUBSTRING(SourceRecord,133,6)
		,SUBSTRING(SourceRecord,133,6) AS TranDate
		--,CONVERT(time(0),SUBSTRING(SourceRecord,139,2)+':'+SUBSTRING(SourceRecord,141,2)+':'+SUBSTRING(SourceRecord,143,2)) AS TranTime		--SUBSTRING(SourceRecord,139,6)
		,SUBSTRING(SourceRecord,139,6) AS TranTime
		,SUBSTRING(SourceRecord,145,1) AS AuthSource
		,SUBSTRING(SourceRecord,146,1) AS TermCapability
		,SUBSTRING(SourceRecord,147,2) AS POSEntryMode
		,SUBSTRING(SourceRecord,149,4) AS AuthMCC
		,SUBSTRING(SourceRecord,153,1) AS CardActTermInd
		,SUBSTRING(SourceRecord,154,1) AS PrepaidIndicator
		,SUBSTRING(SourceRecord,155,1) AS MOTOIndicator
		,SUBSTRING(SourceRecord,156,1) AS ServiceDevelopment
		,SUBSTRING(SourceRecord,157,1) AS CardholderIDMethod
		,SUBSTRING(SourceRecord,158,15) AS VISATransID
		,SUBSTRING(SourceRecord,173,9) AS AuthAmount
		,SUBSTRING(SourceRecord,182,3) AS AuthCurrency
		,SUBSTRING(SourceRecord,185,2) AS AuthResponse
		,SUBSTRING(SourceRecord,187,4) AS ValidationCode
		,SUBSTRING(SourceRecord,191,1) AS AuthCharID
		,SUBSTRING(SourceRecord,192,1) AS ReqPayServiceValue
		,SUBSTRING(SourceRecord,193,1) AS MarketSpecificAuth
		,SUBSTRING(SourceRecord,194,4) AS AuthDate
		,SUBSTRING(SourceRecord,198,1) AS CentralTimeIndicator
		,SUBSTRING(SourceRecord,199,1) AS AVSResponse
		,SUBSTRING(SourceRecord,200,9) AS AuthorizationAmount
		,SUBSTRING(SourceRecord,209,1) AS PurchIDFormat
		,SUBSTRING(SourceRecord,210,25) AS PurchID
		,SUBSTRING(SourceRecord,235,1) AS LodgingNoShow
		,SUBSTRING(SourceRecord,236,6) AS LodgingExtraCharges
		,SUBSTRING(SourceRecord,242,6) AS LodgingCheckInDate
		,SUBSTRING(SourceRecord,248,1) AS CarRentalNoShow
		,SUBSTRING(SourceRecord,249,6) AS CarRentalExtraCharge
		,SUBSTRING(SourceRecord,255,6) AS CarRentalCheckOutDate
		,SUBSTRING(SourceRecord,261,2) AS CardType
		,SUBSTRING(SourceRecord,263,4) AS ChargeType
		,SUBSTRING(SourceRecord,267,1) AS MCInterchangeLevel
		,SUBSTRING(SourceRecord,268,1) AS VISAInterchangeLevel
		,SUBSTRING(SourceRecord,269,1) AS CPSQualTrans
		,SUBSTRING(SourceRecord,270,1) AS ErrorCode
		,SUBSTRING(SourceRecord,271,6) AS DowngradeCode
		,SUBSTRING(SourceRecord,277,24) AS MerchantName
		--,CONVERT(date,SUBSTRING(SourceRecord,301,2)+SUBSTRING(SourceRecord,303,4)) AS DepositDate		--SUBSTRING(SourceRecord,301,6)
		,SUBSTRING(SourceRecord,301,6) AS DepositDate
		,SUBSTRING(SourceRecord,307,3) AS VISAProductID
		,UsabilityIndex
		,MashupOutcome
FROM	Global.FileData


GO
CREATE UNIQUE CLUSTERED INDEX [IDX_Global]
    ON [Global].[vFileData]([FileName] ASC, [LineNumber] ASC) WITH (STATISTICS_NORECOMPUTE = ON);


GO
CREATE NONCLUSTERED INDEX [UIDX_Global]
    ON [Global].[vFileData]([MerchantID] ASC) WITH (STATISTICS_NORECOMPUTE = ON);

