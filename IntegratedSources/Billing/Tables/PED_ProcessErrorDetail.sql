﻿CREATE TABLE [Billing].[PED_ProcessErrorDetail] (
    [PED_ID]             INT      IDENTITY (1, 1) NOT NULL,
    [PED_PIL_ID]         INT      NOT NULL,
    [PED_CPP_ID]         INT      NULL,
    [PED_CPA_ID]         INT      NULL,
    [PED_CST_ID]         INT      NULL,
    [PED_CTF_ID]         INT      NULL,
    [PED_InsertDateTime] DATETIME CONSTRAINT [DF_PED_ProcessErrorDetail_InsertDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PED_ProcessErrorDetail] PRIMARY KEY CLUSTERED ([PED_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PED_ProcessErrorDetail_PIL_ProcessIssueList] FOREIGN KEY ([PED_PIL_ID]) REFERENCES [Billing].[PIL_ProcessIssueList] ([PIL_ID])
);

