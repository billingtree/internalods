﻿



CREATE VIEW [dbo].[vTrashReport]
AS
SELECT	'USAePay'                       AS "Source"
        ,FileName                       AS "FileName"
		,TransDate                      AS "TranDate"
		,MerchantName                   AS "MerchantName"
		,MerchantID                     AS "MerchantID"
		,Platform                       AS "Platform"
		,AccountHolder                  AS "AccountHolder"
		,Amount                         AS "Amount"
FROM	USAePay.FileData
WHERE   UsabilityIndex = -1

UNION ALL

SELECT	'MeS'                           AS "Source"
         ,CONVERT(NVARCHAR, ReportReferenceCode)            AS "FileName"
		,TransactionDate                AS "TranDate"
		,DBAName                        AS "MerchantName"
		,MerchantID                     AS "MerchantID"
		,'MeS'                          AS "Platform"
		,''                             AS "AccountHolder"
		,TransactionAmount              AS "Amount"
FROM	MeS.SettlementSummary
WHERE   UsabilityIndex = -1

UNION ALL

SELECT	'USAePay Daily Summary'         AS "Source"
        ,FileName                       AS "FileName"
		,TransDate                      AS "TranDate"
		,MerchantName                   AS "MerchantName"
		,MerchantID                     AS "MerchantID"
		,'USAePay Report Console'       AS "Platform"
		,'The Merchant'                 AS "AccountHolder"
		,Amount                         AS "Amount"
FROM	USAePay.DailySummary
WHERE   UsabilityIndex = -1


