﻿

CREATE VIEW [dbo].[vRptQuarantine]
AS
SELECT         Source          AS "Source",
               Name            AS "Name",
               SourceID        AS "SourceID",
               MerchantID	   AS "MerchantID",
               TransType       AS "TransType",
               Reason          AS "Reason",
               COUNT(*)        AS "Count",
               MAX(InsertDate) AS MostRecentInsertDate 
               
FROM           vRptQuarantinePrepView
GROUP BY Source, Name, SourceID, MerchantID, TransType, Reason


