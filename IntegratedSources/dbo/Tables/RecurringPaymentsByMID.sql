﻿CREATE TABLE [dbo].[RecurringPaymentsByMID] (
    [MID]                NVARCHAR (255)   NULL,
    [RecurringPaymentYN] CHAR (1)         NULL,
    [CRM_PPDID]          UNIQUEIDENTIFIER NULL,
    [CreatedDate]        SMALLDATETIME    CONSTRAINT [DF_RecurringPaymentsByMID_CreatedDate] DEFAULT (getdate()) NOT NULL
);

