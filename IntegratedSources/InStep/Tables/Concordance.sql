﻿CREATE TABLE [InStep].[Concordance] (
    [ID]                         INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileName]                   VARCHAR (50)   NULL,
    [ProcessorID]                NVARCHAR (500) NULL,
    [ProcessorName]              NVARCHAR (500) NULL,
    [ProcessorBankAccountNumber] NVARCHAR (500) NULL,
    [MerchantID]                 NVARCHAR (500) NULL,
    [MerchantName]               NVARCHAR (500) NULL,
    [EntryName]                  NVARCHAR (500) NULL,
    [MerchantStatus]             NVARCHAR (500) NULL,
    [InsertDate]                 SMALLDATETIME  CONSTRAINT [DF_Concordance_InsertDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]                  BIT            CONSTRAINT [DF_Concordance_IsDeleted_1] DEFAULT ((0)) NOT NULL
);

