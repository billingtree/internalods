﻿/******************************
** File:<SourceControlFileName>spMicrosoftCRMToSalesForceCRMDataSync</<SourceControlFileName>
** Desc:
** Auth:
** Date:
**************************
** Change History
**************************
** PR   Date	    Author  Description	
** --   --------   -------   ------------------------------------
** 
*******************************/
CREATE PROCEDURE [dbo].[spMicrosoftCRMToSalesForceCRMDataSync]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRANSACTION;
        UPDATE  [$(CRMShadow)].MS.bt_serviceorder
        SET     bt_ApplicationStageName = 'Complete'
        WHERE   bt_ServiceOrderId = 'A2C48907-A855-E311-8B0E-6C3BE5A8017C';
        COMMIT;

	--PRINT 'Account Start'
	--GO
        MERGE SalesForce.Account AS Target
        USING
            ( SELECT    ISNULL(parentaccountidname, name) AS Existing_Clients_Profile__c ,
                        CONVERT(NVARCHAR(36), accountid) AS Id ,
                        B.bt_name AS Industry ,
                        0 AS IsDeleted ,
                        name AS Name ,
                        numberofemployees AS NumberOfEmployees ,
                        CONVERT(NVARCHAR(36), parentaccountid) AS ParentId ,
                        bt_subtypename AS [Type] ,
                        accountnumber AS Account_Number__c
						--,new_sf_id AS MSCRMSFAccountID2Compare
              FROM      [$(CRMShadow)].MS.account AS A
                        LEFT JOIN [$(CRMShadow)].MS.bt_sic AS B ON A.bt_siccode = B.bt_sicid
            ) AS Source ( Existing_Clients_Profile__c, Id, Industry, IsDeleted,
                          Name, NumberOfEmployees, ParentId, Type,
                          Account_Number__c --,MSCRMSFAccountID2Compare
						)
        ON ( Target.Id = Source.Id )
        WHEN MATCHED THEN
            UPDATE SET
                    Target.Existing_Clients_Profile__c = Source.Existing_Clients_Profile__c ,
                    Target.Id = Source.Id ,
                    Target.Industry = Source.Industry ,
                    Target.IsDeleted = Source.IsDeleted ,
                    Target.Name = Source.Name ,
                    Target.NumberOfEmployees = Source.NumberOfEmployees ,
                    Target.ParentId = Source.ParentId ,
                    Target.Type = Source.Type ,
                    Target.Account_Number__c = Source.Account_Number__c ,
                    Target.UpdateDate = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT ( Existing_Clients_Profile__c ,
                     Id ,
                     Industry ,
                     IsDeleted ,
                     Name ,
                     NumberOfEmployees ,
                     ParentId ,
                     Type ,
                     Account_Number__c
				   )
            VALUES ( Source.Existing_Clients_Profile__c ,
                     Source.Id ,
                     Source.Industry ,
                     Source.IsDeleted ,
                     Source.Name ,
                     Source.NumberOfEmployees ,
                     Source.ParentId ,
                     Source.Type ,
                     Source.Account_Number__c
				   );
		--OUTPUT $action,deleted.Id,inserted.Id,deleted.*,'XXXXXXXXXXXXXXX',inserted.*; --INTO #TempTable;
	--GO
	--PRINT 'Account End'
	--GO

	--PRINT 'Opportunity Start'
	--GO
        MERGE SalesForce.Opportunity AS Target
        USING
            ( SELECT    CONVERT(NVARCHAR(36), opportunityid) AS Id ,
                        0 AS IsDeleted ,
                        name AS Name
						--,new_sf_id AS MSCRMSFOpportunitytID2Compare
              FROM      [$(CRMShadow)].MS.opportunity
            ) AS Source ( Id, IsDeleted, Name --,MSCRMSFOpportunitytID2Compare
						)
        ON ( Target.Id = Source.Id )
        WHEN MATCHED THEN
            UPDATE SET
                    Target.Id = Source.Id ,
                    Target.IsDeleted = Source.IsDeleted ,
                    Target.Name = Source.Name ,
                    Target.UpdateDate = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT ( Id, IsDeleted, Name )
            VALUES ( Source.Id ,
                     Source.IsDeleted ,
                     Source.Name
				   );
		--OUTPUT $action,deleted.Id,inserted.Id,deleted.*,'XXXXXXXXXXXXXXX',inserted.*; --INTO #TempTable;
	--GO
	--PRINT 'Opportunity End'
	--GO

	--PRINT 'OpportunityHistory Start'
	--GO
        MERGE SalesForce.OpportunityHistory AS Target
        USING
            ( SELECT    CONVERT(NVARCHAR(36), bt_opportunityhistoryid) AS Id ,
                        bt_stamp AS CloseDate ,
                        0 AS IsDeleted ,
                        CONVERT(NVARCHAR(36), bt_opportunity) AS OpportunityId ,
                        CASE WHEN bt_newlabel = 'Won' THEN 'Closed Won'
                             ELSE bt_newlabel
                        END AS StageName
              FROM      [$(CRMShadow)].MS.bt_opportunityhistory
            ) AS Source ( Id, CloseDate, IsDeleted, OpportunityId, StageName )
        ON ( Target.Id = Source.Id )
        WHEN MATCHED THEN
            UPDATE SET
                    Target.Id = Source.Id ,
                    Target.CloseDate = Source.CloseDate ,
                    Target.IsDeleted = Source.IsDeleted ,
                    Target.OpportunityId = Source.OpportunityId ,
                    Target.StageName = Source.StageName ,
                    Target.UpdateDate = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT ( Id ,
                     CloseDate ,
                     IsDeleted ,
                     OpportunityId ,
                     StageName
				   )
            VALUES ( Source.Id ,
                     Source.CloseDate ,
                     Source.IsDeleted ,
                     Source.OpportunityId ,
                     Source.StageName
				   );
		--OUTPUT $action,deleted.Id,inserted.Id,deleted.*,'XXXXXXXXXXXXXXX',inserted.*; --INTO #TempTable;
	--GO
	--PRINT 'Opportunity End'
	--GO

	--PRINT 'PaymentProcessing Start'
	--GO
        MERGE SalesForce.PaymentProcessing AS Target
        USING
            ( SELECT    0 AS IsDeleted ,
                        CONVERT(NVARCHAR(36), B.bt_processingprofileid) AS Id
						--,actualstart AS Fee_Collected__c
                        ,
                        CONVERT(NVARCHAR(36), C.bt_account) AS Account__c ,
                        C.bt_applicationstagename AS Application_Stage__c ,
                        C.bt_estimatedMonthlyDebitTransactions AS Estimated_Monthly_Transactions__c ,
                        C.bt_estimatedmonthlydebitvolume AS Estimated_Monthly_Volume__c ,
                        CONVERT(NVARCHAR(36), C.bt_opportunity) AS Opportunity__c ,
                        B.bt_config AS ACH_Config_Number__c ,
                        CASE B.bt_gatewayname
                          WHEN '.Net' THEN 'ACH'
                          ELSE B.bt_gatewayname
                        END AS ACH_Platform__c ,
                        CASE UPPER(LTRIM(RTRIM(B.bt_productlinename)))
                          WHEN 'ACH' THEN B.bt_bankmid
                          ELSE NULL
                        END AS ACH_Processing_Number__c ,
                        CASE UPPER(LTRIM(RTRIM(B.bt_productlinename)))
                          WHEN 'CREDIT CARD' THEN B.bt_bankmid
                          ELSE NULL
                        END AS CC_Merchant_Number__c ,
                        B.bt_sourceid AS CC_Source_ID__c ,
                        B.bt_startdate AS Complete_Stage_Date__c
						--,UPPER(LTRIM(RTRIM(B.new_sf_id))) AS MSCRMSFPaymentProcessingID2Compare
              FROM      [$(CRMShadow)].MS.bt_processingaccount AS A
                        INNER JOIN [$(CRMShadow)].MS.bt_processingprofile AS B ON A.bt_processingaccountid = B.bt_processingaccount
                        LEFT JOIN [$(CRMShadow)].MS.bt_serviceorder AS C --ON A.bt_processingaccountid = C.bt_processingaccount
                        ON A.bt_currentserviceorder = C.bt_serviceorderid
                        LEFT JOIN [$(CRMShadow)].MS.task AS D ON C.bt_account = D.activityid
              WHERE     B.bt_bankmid IS NOT NULL
                        AND B.bt_bankmid NOT LIKE '%000000'
                        AND B.statecodename = 'Active'
                        AND UPPER(LTRIM(RTRIM(C.bt_applicationstagename))) IN (
                        'COMPLETE', 'QA & TRAINING' )
            ) AS Source ( IsDeleted, Id--,Fee_Collected__c
						, Account__c, Application_Stage__c,
                          Estimated_Monthly_Transactions__c,
                          Estimated_Monthly_Volume__c, Opportunity__c,
                          ACH_Config_Number__c, ACH_Platform__c,
                          ACH_Processing_Number__c, CC_Merchant_Number__c,
                          CC_Source_ID__c, Complete_Stage_Date__c --,MSCRMSFPaymentProcessingID2Compare
						)
        ON ( Target.Id = Source.Id )
        WHEN MATCHED THEN
            UPDATE SET
                    Target.IsDeleted = Source.IsDeleted ,
                    Target.Id = Source.Id
				--,Target.Fee_Collected__c = Source.Fee_Collected__c
                    ,
                    Target.Account__c = Source.Account__c ,
                    Target.Application_Stage__c = Source.Application_Stage__c ,
                    Target.Estimated_Monthly_Transactions__c = Source.Estimated_Monthly_Transactions__c ,
                    Target.Estimated_Monthly_Volume__c = Source.Estimated_Monthly_Volume__c ,
                    Target.Opportunity__c = Source.Opportunity__c ,
                    Target.ACH_Config_Number__c = Source.ACH_Config_Number__c ,
                    Target.ACH_Platform__c = Source.ACH_Platform__c ,
                    Target.ACH_Processing_Number__c = Source.ACH_Processing_Number__c ,
                    Target.CC_Merchant_Number__c = Source.CC_Merchant_Number__c ,
                    Target.CC_Source_ID__c = Source.CC_Source_ID__c ,
                    Target.Complete_Stage_Date__c = Source.Complete_Stage_Date__c ,
                    Target.UpdateDate = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT ( IsDeleted ,
                     Id
				--,Fee_Collected__c
                     ,
                     Account__c ,
                     Application_Stage__c ,
                     Estimated_Monthly_Transactions__c ,
                     Estimated_Monthly_Volume__c ,
                     Opportunity__c ,
                     ACH_Config_Number__c ,
                     ACH_Platform__c ,
                     ACH_Processing_Number__c ,
                     CC_Merchant_Number__c ,
                     CC_Source_ID__c ,
                     Complete_Stage_Date__c
				   )
            VALUES ( Source.IsDeleted ,
                     Source.Id
				--,Source.Fee_Collected__c
                     ,
                     Source.Account__c ,
                     Source.Application_Stage__c ,
                     Source.Estimated_Monthly_Transactions__c ,
                     Source.Estimated_Monthly_Volume__c ,
                     Source.Opportunity__c ,
                     Source.ACH_Config_Number__c ,
                     Source.ACH_Platform__c ,
                     Source.ACH_Processing_Number__c ,
                     Source.CC_Merchant_Number__c ,
                     Source.CC_Source_ID__c ,
                     Source.Complete_Stage_Date__c
				   );
		--OUTPUT $action,deleted.Id,inserted.Id,deleted.*,'XXXXXXXXXXXXXXX',inserted.*; --INTO #TempTable;
	--GO
        BEGIN TRAN;
        UPDATE  SalesForce.PaymentProcessing
        SET     CC_Merchant_Number__c = NULL
        WHERE   Id IN ( '823D8729-F555-E311-8B0E-6C3BE5A8017C',
                        'C446C451-F655-E311-BA73-6C3BE5A872C4' );
        COMMIT;
	--GO
        BEGIN TRAN;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   Id IN ( '77E10B30-F555-E311-8B0E-6C3BE5A8017C',
                        '9D778054-F38C-E311-AC0D-6C3BE5A88B80',
                        '597B2805-F755-E311-BA73-6C3BE5A872C4',
                        '2ED7818F-F655-E311-BA73-6C3BE5A872C4' );
        COMMIT;
	--GO
        BEGIN TRAN;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   Id = '35ED73A1-F655-E311-BA73-6C3BE5A872C4'
                AND ACH_Processing_Number__c = '9999';
        COMMIT;  
	--GO
        BEGIN TRAN;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   Id = '587CCA64-F655-E311-BA73-6C3BE5A872C4'
                AND ACH_Processing_Number__c = '1387';
        COMMIT;   
	--GO

        BEGIN TRAN;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '3116'
                AND ID = '72A41EA8-F655-E311-BA73-6C3BE5A872C4';
        COMMIT;
	--GO

        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     Opportunity__c = '7237B43C-8A53-E311-8B0E-6C3BE5A8017C'
        WHERE   CC_Merchant_Number__c = '941000116434'
                AND Opportunity__c = '0067000000Y2AwsAAF';
        COMMIT TRANSACTION;

        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     CC_Merchant_Number__c = NULL
        WHERE   CC_Merchant_Number__c = '941000116434'
                AND Id = '262B8195-F655-E311-BA73-6C3BE5A872C4';
        COMMIT TRANSACTION;


        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     Opportunity__c = '9EE7DE1D-8A53-E311-8B0E-6C3BE5A8017C' ---- This needed to change
        WHERE   CC_Merchant_Number__c = '941000116483'
                AND Opportunity__c = '0067000000Y2Ax4AAF';
        COMMIT TRANSACTION;

        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     CC_Merchant_Number__c = NULL
        WHERE   CC_Merchant_Number__c = '941000116483'
                AND Id = '7FCFB776-F655-E311-BA73-6C3BE5A872C4';
        COMMIT TRANSACTION;

        BEGIN TRANSACTION;
	-- CRM had 2 records for MID = 3218 and Config 60, this updates the deleted one
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '3218'
                AND ACH_Config_Number__c = '60'
                AND Account__c = 'AB6BD745-4B53-E311-8B0E-6C3BE5A8017C';
        COMMIT;

        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     CC_Merchant_Number__c = NULL
        WHERE   CC_Merchant_Number__c = '941000118046'
                AND CC_Source_ID__c IS NULL;
        COMMIT;

        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     CC_Merchant_Number__c = NULL
        WHERE   CC_Merchant_Number__c = '941000117481'
                AND CC_Source_ID__c IS NULL;
        COMMIT;  


	--PRINT 'PaymentProcessing End'
	--GO



	--  By supplying an End Date, this will get these records out of the process, and stop the duplicate or multiple row update issue
        BEGIN TRANSACTION;
        UPDATE  [$(CRMShadow)].MS.bt_ProcessingProfile
        SET     bt_EndDate = GETDATE()
        WHERE   bt_BankMID IN ( '3205', '3207', '3208', '3209', '3210', '3211',
                                '3212', '3213', '3214', '3215', '3216', '3217' )
                AND bt_Config = '60';
        COMMIT;

	----------------------------------------------------------------------------------------------------
--   Right before the UpSert query for the Sales Force Processing Stage History table
--   Let's run this script to Update the End Date on the CRM Processing Profile for the 2 MeS MIDs
--   This will cause the UpSert query to NOT return this row
--   Thus permitting the MERGE statement to complete
----------------------------------------------------------------------------------------------------
        BEGIN TRANSACTION;
        UPDATE  CRMShadow.MS.bt_ProcessingProfile
        SET     bt_EndDate = GETDATE()
        WHERE   bt_ProcessingProfileId IN (
                SELECT  PP.bt_ProcessingProfileId
                FROM    CRMShadow.MS.bt_ProcessingProfile AS PP
                        INNER JOIN (      -- Right now, this Processing Account has 2 Active Processing Profiles
                                                                                                                           -- They are going to Delete the Moneris one (via a Hard or Soft Delete)
                                                                                                                           -- Once they do that, we will only get back 1 record
                                                                                                                           -- And, this logic will be rendered moot
                                                                                                                           -- But, we can certainly remove this script as well
                                     SELECT bt_ProcessingAccount AS PAGuid ,
                                            COUNT(*) AS PPCount
                                     FROM   CRMShadow.MS.bt_ProcessingProfile
                                     WHERE  StatusCodeName = 'Active'
                                     GROUP BY bt_ProcessingAccount
                                   ) AS TempFix ON PP.bt_ProcessingAccount = TempFix.PAGuid
                WHERE   PP.bt_BankMID IN ( '941000123607', '941000122705' )
                        AND PP.bt_EndDate IS NULL
                        AND TempFix.PPCount > 1 );
        COMMIT;


	--   Then run the Nightly Sync, which will allow the 11 new MIDs 1150 through 1160 to move over to Sales Force


	--PRINT 'ProcessingStageHistory Start'
	--GO
        MERGE SalesForce.ProcessingStageHistory AS Target
        USING
            ( SELECT    CONVERT(NVARCHAR(36), A.bt_serviceorderhistoryid) AS Id ,
                        bt_Stamp AS Date_Stage_Entered__c ,
                        0 AS IsDeleted ,
                        bt_NewLabel AS Name ,
                        CONVERT(NVARCHAR(36), C.bt_processingprofileid) AS Processing__c
              FROM      [$(CRMShadow)].MS.bt_serviceorderhistory AS A
                        INNER JOIN [$(CRMShadow)].MS.bt_serviceorder AS B ON A.bt_serviceorder = B.bt_serviceorderid
                        INNER JOIN ( SELECT MSPP.bt_processingaccount ,
                                            MSPP.bt_processingprofileid
                                     FROM   [$(CRMShadow)].MS.bt_processingprofile
                                            AS MSPP
                                     WHERE  MSPP.bt_enddate IS NULL
                                            AND MSPP.bt_BankMID IS NOT NULL
                                            AND MSPP.statecodename = 'Active'
                                            AND MSPP.bt_processingaccount IN (
                                            SELECT  B.bt_processingaccount AS Processing__c
                                            FROM    [$(CRMShadow)].MS.bt_serviceorderhistory
                                                    AS A
                                                    INNER JOIN [$(CRMShadow)].MS.bt_serviceorder
                                                    AS B ON A.bt_serviceorder = B.bt_serviceorderid
                                                            AND UPPER(LTRIM(RTRIM(B.bt_applicationstagename))) IN (
                                                            'COMPLETE',
                                                            'QA & TRAINING' ) )
                                   ) AS C ON B.bt_processingaccount = C.bt_processingaccount
            ) AS Source ( Id, Date_Stage_Entered__c, IsDeleted, Name,
                          Processing__c )
        ON ( Target.Id = Source.Id )
        WHEN MATCHED THEN
            UPDATE SET
                    Target.Id = Source.Id ,
                    Target.Date_Stage_Entered__c = Source.Date_Stage_Entered__c ,
                    Target.IsDeleted = Source.IsDeleted ,
                    Target.Name = Source.Name ,
                    Target.Processing__c = Source.Processing__c ,
                    Target.UpdateDate = GETDATE()
        WHEN NOT MATCHED THEN
            INSERT ( Id ,
                     Date_Stage_Entered__c ,
                     IsDeleted ,
                     Name ,
                     Processing__c
				   )
            VALUES ( Source.Id ,
                     Source.Date_Stage_Entered__c ,
                     Source.IsDeleted ,
                     Source.Name ,
                     Source.Processing__c
				   );
		--OUTPUT $action,deleted.Id,inserted.Id,deleted.*,'XXXXXXXXXXXXXXX',inserted.*; --INTO #TempTable;
	--GO
	--PRINT 'ProcessingStageHistory End'
	--GO
	--   Then, we will fix the records we just updated
        BEGIN TRANSACTION;
        UPDATE  [$(CRMShadow)].MS.bt_ProcessingProfile
        SET     bt_EndDate = NULL
        WHERE   bt_BankMID IN ( '3205', '3207', '3208', '3209', '3210', '3211',
                                '3212', '3213', '3214', '3215', '3216', '3217' )
                AND bt_Config = '60';
        COMMIT;



	-- GL Feed ATI monthly july issue
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '3114'
                AND ACH_Config_Number__c = '60';
        COMMIT;


	--GL Feed ATI daily Dec 2 balance issue
  
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c IN ( '1762', '1764', '1766', '1790',
                                              '1832' )
                AND ACH_Config_Number__c = '22'
                AND Trust_Name__c LIKE 'Asset Management%';
        COMMIT;

	--GL Feed ATI Daily Dec 3 Balance issue
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c IN ( '1762', '1764', '1766', '1790',
                                              '1832', '1763', '1791', '1984' )
                AND ACH_Config_Number__c = '22'
                AND Trust_Name__c LIKE 'Asset Management%';
        COMMIT;

	--##############################				TO CLEAR OUT Softdeleted CRM records from Salesforce tables				#################################################
        ALTER TABLE [SalesForce].[PaymentProcessing] NOCHECK CONSTRAINT [FK_PaymentProcessing_Opportunity];
	--GO

        UPDATE  SFCRM
        SET     CC_Merchant_Number__c = NULL ,
                CC_Source_ID__c = NULL ,
                ACH_Processing_Number__c = NULL ,
                ACH_Config_Number__c = NULL
        FROM    SalesForce.PaymentProcessing AS SFCRM
                INNER JOIN [$(CRMShadow)].MS.bt_processingprofile AS MSCRM ON SFCRM.Id = CONVERT(NVARCHAR(36), MSCRM.bt_processingprofileid)
                                                              AND MSCRM.statuscodename = 'Inactive'
                                                              AND UPPER(LTRIM(RTRIM(MSCRM.bt_productlinename))) IN (
                                                              'CREDIT CARD',
                                                              'ACH' );
	--GO
	--ALTER TABLE [SalesForce].[PaymentProcessing] CHECK CONSTRAINT [FK_PaymentProcessing_Opportunity]
	--GO
	--##############################

        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   Id = '704A8AEA-408F-E311-8CC7-6C3BE5A872C4';
        COMMIT;

	--ATI DailyweeklyGLFeed 2/25/2105 Out of balance issue.
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Config_Number__c = '57'
                AND ACH_Processing_Number__c = '1115'
                AND Id = 'DC46FC3B-F555-E311-8B0E-6C3BE5A8017C';
        COMMIT;  


	--ATI DailyweeklyGLFeed 2015-03-04 out of balance issue
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '3066'
                AND ACH_Config_Number__c = '60'
                AND Id = '4CD8961D-F555-E311-8B0E-6C3BE5A8017C';
        COMMIT;


	--ATI DailyweeklyGLFeed 2015-06-03 out of balance issue
        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = ''
        WHERE   ACH_Processing_Number__c = '2001'
                AND ACH_Config_Number__c = '22'
                AND Id = '87CFB776-F655-E311-BA73-6C3BE5A872C4';  
        COMMIT;

        BEGIN TRANSACTION; 
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = ''
        WHERE   ACH_Processing_Number__c = '1794'
                AND ACH_Config_Number__c = '47'
                AND Id = '70A41EA8-F655-E311-BA73-6C3BE5A872C4';
        COMMIT;

	--ATI DailyweeklyGLFeed 2015-06-15 out of balance issue
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '1018'
                AND ACH_Config_Number__c = '47'
                AND Id = '552B494B-F655-E311-BA73-6C3BE5A872C4'; 
        COMMIT;  

	--ATI DailyweeklyGLFeed 2015-08-03 out of balance issue
        BEGIN TRANSACTION;
        UPDATE  SalesForce.PaymentProcessing
        SET     ACH_Processing_Number__c = NULL
        WHERE   ACH_Processing_Number__c = '2068'
                AND ACH_Config_Number__c = '22'
                AND Id = '60E10B30-F555-E311-8B0E-6C3BE5A8017C';
        COMMIT;


    END;
