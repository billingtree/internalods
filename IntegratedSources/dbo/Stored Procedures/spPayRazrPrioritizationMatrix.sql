﻿











-- =============================================
-- Author:		Rick Stohle
-- Create date: 02-Jun-2014
-- Description:	Procedure to get Billing Tree Customers by with Revenue during a Year/Month parameters
-- =============================================
CREATE PROCEDURE [dbo].[spPayRazrPrioritizationMatrix] 
(
@FromDate  DATE,
@ToDate    DATE,
@Aggregate CHAR,
@CCOnly    CHAR
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    DECLARE @FromThisDate      DATE
    DECLARE @ToThisDate        DATE
    DECLARE @AggregateYN       CHAR (1)
    DECLARE @CCOnlyYN          CHAR (1)
    DECLARE @RunMode           CHAR (2)
    
 
    SET @FromThisDate = @FromDate
    SET @ToThisDate = @ToDate
    SET @AggregateYN = @Aggregate
    SET @CCOnlyYN = @CCOnly
    
    IF @AggregateYN = 'Y'
        IF @CCOnlyYN = 'Y'
            SET @RunMode = 'YY'
        ELSE
            SET @RunMode = 'YN'
    ELSE
        IF @CCOnlyYN = 'Y'
            SET @RunMode = 'NY'
        ELSE
            SET @RunMode = 'NN'                 
    
    
    -- Insert statements for procedure here
    

    IF @AggregateYN = 'Y' -- Sum Revenue across Month and Year

        IF @CCOnlyYN = 'Y'    -- Don't include those Child Accounts which also have ACH Revenue 
        
 			SELECT Acc.Name                                              AS ChildAccountName
				  ,Acc.bt_CompanyDBA                                     AS CompanyDBA
				  ,PP.bt_BankMID                                         AS MID
				  ,PP.bt_SourceId                                        AS ConfigSource         
				  ,Acc.AccountNumber                                     AS BillingTreeAccountNumber
				  ,SP.FullName                                           AS SalesPerson
				  ,PartProd.PartnerName                                  AS Partner
				  ,PartProd.PartnerType                                  AS PartnerType
				  ,PartProd.PartnerSubType                               AS PartnerSubType
				  ,PartProd.ProdName                                     AS Product
				  ,PartProd.ProdType                                     AS ProdType
				  ,PartProd.ProdVersion                                  AS Version
				  ,PP.bt_ProductLineName                                 AS ProductLine	
				  ,RPM.RecurringPaymentYN                                AS RecurringPaymentsYN	
				  ,CASE
				       WHEN BTWP.BTWebPayUserYN = 'Y' THEN
				           'Y'
				       ELSE
				           'N'
				   END                                                   AS BTWebPayUserYN	 	  
				  ,NULL                                                  AS MonYear
				  ,NULL                                                  AS SortDate
				  ,SUM(AllCCSources.GrossRevenue)                        AS GrossRevenue
		          
			FROM 
				(
				SELECT Customers.MID                        AS MID
				      ,Customers.SourceID                   AS SourceID
				      ,Customers.CRM_GUID                   AS CRM_ID
					  ,Customers.MonYear                    AS MonYear
					  ,SUM(Customers.GrossRevenue)          AS GrossRevenue             
				FROM (
					  --------------------------------------------------------------
					  --  MeS Monthly Revenue for the Month by Processing Profile --
					  --------------------------------------------------------------
					  SELECT MeS.MID                                                           AS MID
					        ,MeS.SourceID                                                      AS SourceID
					        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))
																							   AS MonYear              
							,SUM(MeS.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.MeSRevenueMonthlyGLFeed   AS MeS
					  WHERE MeS.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY MeS.MID
					          ,MeS.SourceID
					          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
							  ,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))           
		              
					  UNION ALL
		              
					  ------------------------------------------------------------------
					  --  USAePay Monthly Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------------
					  SELECT USA.MID                                                           AS MID
					        ,USA.SourceID                                                      AS SourceID
					        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
																							   AS MonYear              
							,SUM(USA.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.USAePayRevenueMonthlyGLFeed   AS USA
					  WHERE USA.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY USA.MID  
					          ,USA.SourceID 
					          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
							  ,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
		              
					  )   AS Customers 
				 GROUP BY Customers.MID
				         ,Customers.SourceID
				         ,Customers.CRM_GUID
						 ,Customers.MonYear 
						   
				 ) AllCCSources  
				 
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP  ON AllCCSources.CRM_ID = PP.bt_ProcessingProfileId 
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId  
			 LEFT JOIN [$(CRMShadow)].MS.Account                  AS Acc ON PA.bt_Account = Acc.AccountId  
			 LEFT JOIN dbo.MIDSalesPerson                    AS SP  ON AllCCSources.CRM_ID = SP.CRM_PPDID
		     
			 LEFT JOIN (
						SELECT DISTINCT SPF.bt_ProcessingAccount AS bt_ProcessingAccount, Acct.Name AS PartnerName
									   ,Acct.bt_TypeName AS PartnerType, Acct.bt_SubTypeName AS PartnerSubType
									   ,Prod.bt_TypeName AS ProdType, Prod.Name AS ProdName, Prod.bt_Version AS ProdVersion
						FROM [$(CRMShadow)].MS.bt_ServiceProductFee   AS SPF
						JOIN [$(CRMShadow)].MS.Product                AS Prod ON SPF.bt_Product = Prod.ProductId
																		AND Prod.bt_EndDate IS NULL
						JOIN [$(CRMShadow)].MS.Account                AS Acct ON Prod.bt_Provider = Acct.AccountId 
																		AND Acct.Name NOT LIKE 'BillingTree%'                                             
						WHERE SPF.bt_ProcessingAccount IS NOT NULL
						)  AS PartProd   ON PA.bt_ProcessingAccountId = PartProd.bt_ProcessingAccount
			LEFT JOIN dbo.RecurringPaymentsByMID            AS RPM  ON AllCCSources.CRM_ID = RPM.CRM_PPDID	
			LEFT JOIN dbo.BTWebPayUsersByMID                AS BTWP ON AllCCSources.MID = BTWP.MID
			                                                       AND AllCCSources.SourceID = BTWP.SourceId			
			WHERE Acc.AccountId  NOT IN   -- Eliminate any Child Account that has ACH Revenue during this Time
									   (
										SELECT DISTINCT ATI.[CRM ACCT LEVEL#-GP EXTENDER PARENT#]
										FROM dbo.vATIDailyRevenueGLFeed   AS ATI
										WHERE ATI.TranDate BETWEEN @FromThisDate AND @ToThisDate 	 
										)
			GROUP BY Acc.Name 
				  ,Acc.bt_CompanyDBA
				  ,PP.bt_BankMID 
				  ,PP.bt_SourceId   
				  ,Acc.AccountNumber
				  ,SP.FullName
				  ,PartProd.PartnerName
				  ,PartProd.PartnerType
				  ,PartProd.PartnerSubType
				  ,PartProd.ProdName
				  ,PartProd.ProdType
				  ,PartProd.ProdVersion
				  ,PP.bt_ProductLineName
				  ,RPM.RecurringPaymentYN 	  
				  ,BTWP.BTWebPayUserYN              
        
        ELSE   ------------------------------------------------------------
               -- Include Child Accounts which also have ACH Revenue     --
               ------------------------------------------------------------

			SELECT Acc.Name                                              AS ChildAccountName
				  ,Acc.bt_CompanyDBA                                     AS CompanyDBA
				  ,PP.bt_BankMID                                         AS MID
				  ,CASE 
					   WHEN PP.bt_Config > '' THEN  
						   PP.bt_Config
					   ELSE
						   PP.bt_SourceId   
				   END                                                   AS ConfigSource         
				  ,Acc.AccountNumber                                     AS BillingTreeAccountNumber
				  ,SP.FullName                                           AS SalesPerson
				  ,PartProd.PartnerName                                  AS Partner
				  ,PartProd.PartnerType                                  AS PartnerType
				  ,PartProd.PartnerSubType                               AS PartnerSubType
				  ,PartProd.ProdName                                     AS Product
				  ,PartProd.ProdType                                     AS ProdType
				  ,PartProd.ProdVersion                                  AS Version
				  ,PP.bt_ProductLineName                                 AS ProductLine	
				  ,RPM.RecurringPaymentYN                                AS RecurringPaymentsYN	
				  ,CASE
				       WHEN BTWP.BTWebPayUserYN = 'Y' THEN
				           'Y'
				       ELSE
				           'N'
				   END                                                   AS BTWebPayUserYN	 	  
				  ,NULL                                                  AS MonYear
				  ,NULL                                                  AS SortDate
				  ,SUM(AllSources.GrossRevenue)                          AS GrossRevenue
		          
			FROM 
				(
				SELECT Customers.MID                        AS MID
				      ,Customers.ConfigSource               AS ConfigSource
				      ,Customers.CRM_GUID                   AS CRM_ID
					  ,Customers.MonYear                    AS MonYear
					  ,SUM(Customers.GrossRevenue)          AS GrossRevenue             
				FROM (
					  ------------------------------------------------------------
					  --  ATI Daily Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------
					  SELECT ATI.ProcessingNbr                                                 AS MID
					        ,ATI.ConfigNbr                                                     AS ConfigSource   
					        ,ATI.[CRM PROCESSING RECORD#-GP extender field on Customer card]   AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, ATI.TranDate),3), '-', CONVERT(NVARCHAR, YEAR(ATI.TranDate)))
																							   AS MonYear
							,SUM(ATI.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.vATIDailyRevenueGLFeed   AS ATI
					  WHERE ATI.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY ATI.ProcessingNbr
					          ,ATI.ConfigNbr 
					          ,ATI.[CRM PROCESSING RECORD#-GP extender field on Customer card]
							  ,CONCAT(LEFT(DATENAME (MONTH, ATI.TranDate),3), '-', CONVERT(NVARCHAR, YEAR(ATI.TranDate)))
		              
					  UNION ALL
		              
					  --------------------------------------------------------------
					  --  MeS Monthly Revenue for the Month by Processing Profile --
					  --------------------------------------------------------------
					  SELECT MeS.MID                                                           AS MID
					        ,MeS.SourceID                                                      AS ConfigSource
					        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))
																							   AS MonYear              
							,SUM(MeS.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.MeSRevenueMonthlyGLFeed   AS MeS
					  WHERE MeS.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY MeS.MID 
					          ,MeS.SourceID
					          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))           
		              
					  UNION ALL
		              
					  ------------------------------------------------------------------
					  --  USAePay Monthly Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------------
					  SELECT USA.MID                                                           AS MID
					        ,CONVERT(BIGINT, USA.SourceID)                                     AS ConfigSource
					        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
																							   AS MonYear              
							,SUM(USA.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.USAePayRevenueMonthlyGLFeed   AS USA
					  WHERE USA.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY USA.MID 
					          ,USA.SourceID
					          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
		              
					  )  AS Customers
				 GROUP BY Customers.MID  
				         ,Customers.ConfigSource 
				         ,Customers.CRM_GUID
						 ,Customers.MonYear   
				 ) AllSources  
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP  ON AllSources.CRM_ID = PP.bt_ProcessingProfileId 
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId  
			 LEFT JOIN [$(CRMShadow)].MS.Account                  AS Acc ON PA.bt_Account = Acc.AccountId  
			 LEFT JOIN dbo.MIDSalesPerson                    AS SP  ON AllSources.CRM_ID = SP.CRM_PPDID
		     
			 LEFT JOIN (
						SELECT DISTINCT SPF.bt_ProcessingAccount AS bt_ProcessingAccount, Acct.Name AS PartnerName
									   ,Acct.bt_TypeName AS PartnerType, Acct.bt_SubTypeName AS PartnerSubType
									   ,Prod.bt_TypeName AS ProdType, Prod.Name AS ProdName, Prod.bt_Version AS ProdVersion
						FROM [$(CRMShadow)].MS.bt_ServiceProductFee   AS SPF
						JOIN [$(CRMShadow)].MS.Product                AS Prod ON SPF.bt_Product = Prod.ProductId
																		AND Prod.bt_EndDate IS NULL
						JOIN [$(CRMShadow)].MS.Account                AS Acct ON Prod.bt_Provider = Acct.AccountId 
																		AND Acct.Name NOT LIKE 'BillingTree%'                                             
						WHERE SPF.bt_ProcessingAccount IS NOT NULL
						)  AS PartProd   ON PA.bt_ProcessingAccountId = PartProd.bt_ProcessingAccount
		     LEFT JOIN dbo.RecurringPaymentsByMID                AS RPM  ON AllSources.CRM_ID = RPM.CRM_PPDID				
		     LEFT JOIN dbo.BTWebPayUsersByMID                    AS BTWP ON AllSources.MID = BTWP.MID
		                                                                AND AllSources.ConfigSource = BTWP.SourceId
			GROUP BY Acc.Name 
				  ,Acc.bt_CompanyDBA
				  ,PP.bt_BankMID 
				  ,CASE 
					   WHEN PP.bt_Config > '' THEN  
						   PP.bt_Config
					   ELSE
						   PP.bt_SourceId   
				   END
				  ,Acc.AccountNumber
				  ,SP.FullName
				  ,PartProd.PartnerName
				  ,PartProd.PartnerType
				  ,PartProd.PartnerSubType
				  ,PartProd.ProdName
				  ,PartProd.ProdType
				  ,PartProd.ProdVersion
				  ,PP.bt_ProductLineName
				  ,RPM.RecurringPaymentYN		
				  ,BTWP.BTWebPayUserYN   	  
    
    ELSE -------------------------------------------------------------
         -----   Don't Aggregate the Revenue across Month and Year  --
         -------------------------------------------------------------
    
        IF @CCOnlyYN = 'Y'    -- Don't include those Child Accounts which also have ACH Revenue 
        
 			SELECT Acc.Name                                              AS ChildAccountName
				  ,Acc.bt_CompanyDBA                                     AS CompanyDBA
				  ,PP.bt_BankMID                                         AS MID
				  ,''                                                    AS ConfigSource         
				  ,Acc.AccountNumber                                     AS BillingTreeAccountNumber
				  ,SP.FullName                                           AS SalesPerson
				  ,PartProd.PartnerName                                  AS Partner
				  ,PartProd.PartnerType                                  AS PartnerType
				  ,PartProd.PartnerSubType                               AS PartnerSubType
				  ,PartProd.ProdName                                     AS Product
				  ,PartProd.ProdType                                     AS ProdType
				  ,PartProd.ProdVersion                                  AS Version
				  ,PP.bt_ProductLineName                                 AS ProductLine	
				  ,RPM.RecurringPaymentYN                                AS RecurringPaymentsYN	
				  ,CASE
				       WHEN BTWP.BTWebPayUserYN = 'Y' THEN
				           'Y'
				       ELSE
				           'N'
				   END                                                   AS BTWebPayUserYN	 	  
				  ,AllCCSources.MonYear                                  AS MonYear
				  ,CONVERT(DATE, CONCAT('01-', AllCCSources.MonYear))    AS SortDate
				  ,SUM(AllCCSources.GrossRevenue)                        AS GrossRevenue
		          
			FROM 
				(
				SELECT Customers.MID                        AS MID
				      ,Customers.SourceID                   AS SourceID
				      ,Customers.CRM_GUID                   AS CRM_ID
					  ,Customers.MonYear                    AS MonYear
					  ,SUM(Customers.GrossRevenue)          AS GrossRevenue             
				FROM (
					  --------------------------------------------------------------
					  --  MeS Monthly Revenue for the Month by Processing Profile --
					  --------------------------------------------------------------
					  SELECT MeS.MID                                                           AS MID
					        ,MeS.SourceID                                                      AS SourceID
					        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))
																							   AS MonYear              
							,SUM(MeS.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.MeSRevenueMonthlyGLFeed   AS MeS
					  WHERE MeS.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY MeS.MID  
					          ,MeS.SourceID
					          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))           
		              
					  UNION ALL
		              
					  ------------------------------------------------------------------
					  --  USAePay Monthly Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------------
					  SELECT USA.MID                                                           AS MID
					        ,USA.SourceID                                                      AS SourceID
					        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
																							   AS MonYear              
							,SUM(USA.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.USAePayRevenueMonthlyGLFeed   AS USA
					  WHERE USA.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY USA.MID 
					          ,USA.SourceID
					          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
		              
					  )   AS Customers 
				 GROUP BY Customers.MID 
				         ,Customers.SourceID
				         ,Customers.CRM_GUID
						 ,Customers.MonYear   
				 ) AllCCSources  
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP  ON AllCCSources.CRM_ID = PP.bt_ProcessingProfileId 
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId  
			 LEFT JOIN [$(CRMShadow)].MS.Account                  AS Acc ON PA.bt_Account = Acc.AccountId  
			 LEFT JOIN dbo.MIDSalesPerson                    AS SP  ON AllCCSources.CRM_ID = SP.CRM_PPDID
		     
			 LEFT JOIN (
						SELECT DISTINCT SPF.bt_ProcessingAccount AS bt_ProcessingAccount, Acct.Name AS PartnerName
									   ,Acct.bt_TypeName AS PartnerType, Acct.bt_SubTypeName AS PartnerSubType
									   ,Prod.bt_TypeName AS ProdType, Prod.Name AS ProdName, Prod.bt_Version AS ProdVersion
						FROM [$(CRMShadow)].MS.bt_ServiceProductFee   AS SPF
						JOIN [$(CRMShadow)].MS.Product                AS Prod ON SPF.bt_Product = Prod.ProductId
																		AND Prod.bt_EndDate IS NULL
						JOIN [$(CRMShadow)].MS.Account                AS Acct ON Prod.bt_Provider = Acct.AccountId 
																		AND Acct.Name NOT LIKE 'BillingTree%'                                             
						WHERE SPF.bt_ProcessingAccount IS NOT NULL
						)  AS PartProd   ON PA.bt_ProcessingAccountId = PartProd.bt_ProcessingAccount
			LEFT JOIN dbo.RecurringPaymentsByMID                 AS RPM  ON AllCCSources.CRM_ID = RPM.CRM_PPDID		
			LEFT JOIN dbo.BTWebPayUsersByMID                     AS BTWP ON AllCCSources.MID = BTWP.MID		
			                                                            AND AllCCSources.SourceID = BTWP.SourceId
			WHERE Acc.AccountId  NOT IN   -- Eliminate any Child Account that has ACH Revenue during this Time
									   (
										SELECT DISTINCT ATI.[CRM ACCT LEVEL#-GP EXTENDER PARENT#]
										FROM dbo.vATIDailyRevenueGLFeed   AS ATI
										WHERE ATI.TranDate BETWEEN @FromThisDate AND @ToThisDate 	 
										)
			GROUP BY Acc.Name 
				  ,Acc.bt_CompanyDBA
				  ,PP.bt_BankMID 
				  ,PP.bt_SourceId   
				  ,Acc.AccountNumber
				  ,SP.FullName
				  ,PartProd.PartnerName
				  ,PartProd.PartnerType
				  ,PartProd.PartnerSubType
				  ,PartProd.ProdName
				  ,PartProd.ProdType
				  ,PartProd.ProdVersion
				  ,PP.bt_ProductLineName
				  ,RPM.RecurringPaymentYN
				  ,BTWP.BTWebPayUserYN 
				  ,AllCCSources.MonYear 
				  ,CONVERT(DATE, CONCAT('01-', AllCCSources.MonYear))	 
				  
				         
        
        ELSE   ------------------------------------------------------------
               -- Include Child Accounts which also have ACH Revenue     --
               ------------------------------------------------------------    

			SELECT Acc.Name                                              AS ChildAccountName
				  ,Acc.bt_CompanyDBA                                     AS CompanyDBA
				  ,PP.bt_BankMID                                         AS MID
				  ,CASE 
					   WHEN PP.bt_Config > '' THEN  
						   PP.bt_Config
					   ELSE
						   PP.bt_SourceId   
				   END                                                   AS ConfigSource         
				  ,Acc.AccountNumber                                     AS BillingTreeAccountNumber
				  ,SP.FullName                                           AS SalesPerson
				  ,PartProd.PartnerName                                  AS Partner
				  ,PartProd.PartnerType                                  AS PartnerType
				  ,PartProd.PartnerSubType                               AS PartnerSubType
				  ,PartProd.ProdName                                     AS Product
				  ,PartProd.ProdType                                     AS ProdType
				  ,PartProd.ProdVersion                                  AS Version
				  ,PP.bt_ProductLineName                                 AS ProductLine	
				  ,RPM.RecurringPaymentYN                                AS RecurringPaymentsYN	
				  ,CASE
				       WHEN BTWP.BTWebPayUserYN = 'Y' THEN
				           'Y'
				       ELSE
				           'N'
				   END                                                   AS BTWebPayUserYN	 	  
				  ,AllSources.MonYear                                    AS MonYear
				  ,CONVERT(DATE, CONCAT('01-', AllSources.MonYear))      AS SortDate
				  ,SUM(AllSources.GrossRevenue)                          AS GrossRevenue
		          
			FROM 
				(
				SELECT Customers.MID                        AS MID
				      ,Customers.ConfigSource               AS ConfigSource
				      ,Customers.CRM_GUID                   AS CRM_ID
					  ,Customers.MonYear                    AS MonYear
					  ,SUM(Customers.GrossRevenue)          AS GrossRevenue             
				FROM (
					  ------------------------------------------------------------
					  --  ATI Daily Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------
					  SELECT ATI.ProcessingNbr                                                 AS MID
					        ,ATI.ConfigNbr                                                     AS ConfigSource
					        ,ATI.[CRM PROCESSING RECORD#-GP extender field on Customer card]   AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, ATI.TranDate),3), '-', CONVERT(NVARCHAR, YEAR(ATI.TranDate)))
																							   AS MonYear
							,SUM(ATI.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.vATIDailyRevenueGLFeed   AS ATI
					  WHERE ATI.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY ATI.ProcessingNbr 
					          ,ATI.ConfigNbr
					          ,ATI.[CRM PROCESSING RECORD#-GP extender field on Customer card]
							  ,CONCAT(LEFT(DATENAME (MONTH, ATI.TranDate),3), '-', CONVERT(NVARCHAR, YEAR(ATI.TranDate)))
		              
					  UNION ALL
		              
					  --------------------------------------------------------------
					  --  MeS Monthly Revenue for the Month by Processing Profile --
					  --------------------------------------------------------------
					  SELECT MeS.MID                                                           AS MID
					        ,MeS.SourceID                                                      AS ConfigSource
					        ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))
																							   AS MonYear              
							,SUM(MeS.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.MeSRevenueMonthlyGLFeed   AS MeS
					  WHERE MeS.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY MeS.MID 
					          ,MeS.SourceID
					          ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, MeS.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(MeS.TranDate)))           
		              
					  UNION ALL
		              
					  ------------------------------------------------------------------
					  --  USAePay Monthly Revenue for the Month by Processing Profile --
					  ------------------------------------------------------------------
					  SELECT USA.MID                                                           AS MID
					        ,CONVERT(BIGINT, USA.SourceID)                                     AS ConfigSource
					        ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard          AS CRM_GUID
							,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
																							   AS MonYear              
							,SUM(USA.RevenueAmount)                                            AS GrossRevenue
					  FROM dbo.USAePayRevenueMonthlyGLFeed   AS USA
					  WHERE USA.TranDate BETWEEN @FromThisDate AND @ToThisDate
					  GROUP BY USA.MID  
					          ,USA.SourceID
					          ,USA.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
							  ,CONCAT(LEFT(DATENAME (MONTH, USA.TranDate), 3), '-', CONVERT(NVARCHAR, YEAR(USA.TranDate)))
		              
					  )  AS Customers
				 GROUP BY Customers.MID
				         ,Customers.ConfigSource
				         ,Customers.CRM_GUID
						 ,Customers.MonYear   
				 ) AllSources  
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP  ON AllSources.CRM_ID = PP.bt_ProcessingProfileId 
			 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA  ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId  
			 LEFT JOIN [$(CRMShadow)].MS.Account                  AS Acc ON PA.bt_Account = Acc.AccountId  
			 LEFT JOIN dbo.MIDSalesPerson                    AS SP  ON AllSources.CRM_ID = SP.CRM_PPDID
		     
			 LEFT JOIN (
						SELECT DISTINCT SPF.bt_ProcessingAccount AS bt_ProcessingAccount, Acct.Name AS PartnerName
									   ,Acct.bt_TypeName AS PartnerType, Acct.bt_SubTypeName AS PartnerSubType
									   ,Prod.bt_TypeName AS ProdType, Prod.Name AS ProdName, Prod.bt_Version AS ProdVersion
						FROM [$(CRMShadow)].MS.bt_ServiceProductFee   AS SPF
						JOIN [$(CRMShadow)].MS.Product                AS Prod ON SPF.bt_Product = Prod.ProductId
																		AND Prod.bt_EndDate IS NULL
						JOIN [$(CRMShadow)].MS.Account                AS Acct ON Prod.bt_Provider = Acct.AccountId 
																		AND Acct.Name NOT LIKE 'BillingTree%'                                             
						WHERE SPF.bt_ProcessingAccount IS NOT NULL
						)  AS PartProd   ON PA.bt_ProcessingAccountId = PartProd.bt_ProcessingAccount
		    LEFT JOIN dbo.RecurringPaymentsByMID                 AS RPM  ON AllSources.CRM_ID = RPM.CRM_PPDID	
		    LEFT JOIN dbo.BTWebPayUsersByMID                     AS BTWP ON AllSources.MID = BTWP.MID			 
		                                                                AND AllSources.ConfigSource = BTWP.SourceId
			GROUP BY Acc.Name 
				  ,Acc.bt_CompanyDBA
				  ,PP.bt_BankMID 
				  ,CASE 
					   WHEN PP.bt_Config > '' THEN  
						   PP.bt_Config
					   ELSE
						   PP.bt_SourceId   
				   END
				  ,Acc.AccountNumber
				  ,SP.FullName
				  ,PartProd.PartnerName
				  ,PartProd.PartnerType
				  ,PartProd.PartnerSubType
				  ,PartProd.ProdName
				  ,PartProd.ProdType
				  ,PartProd.ProdVersion
				  ,PP.bt_ProductLineName
				  ,RPM.RecurringPaymentYN 
				  ,BTWP.BTWebPayUserYN     			  
				  ,AllSources.MonYear
				  ,CONVERT(DATE, CONCAT('01-', AllSources.MonYear))
 
END









