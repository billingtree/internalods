﻿





-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spMIDsWithRevenuePartnersReport](
 @Year       INT
,@Month      INT
,@ToYear     INT
,@ToMonth    INT
,@PartnerName NVARCHAR (160)
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
SET NOCOUNT ON; 

    DECLARE @vYear            VARCHAR(4)
    DECLARE @vMonth           VARCHAR(2)
    DECLARE @vToYear          VARCHAR(4)
    DECLARE @vToMonth         VARCHAR(2)
    DECLARE @vFromDate        DATE
    DECLARE @vToDate          DATE
    
    SET @vYear = CONVERT(VARCHAR, @Year)
    SET @vMonth = CONVERT(VARCHAR, @Month)
    SET @vFromDate = CONVERT(DATE, CONCAT(@vYear, '-', @vMonth, '-', '01'))
    
    SET @vToYear = CONVERT(VARCHAR, @ToYear)
    SET @vToMonth = CONVERT(VARCHAR, @ToMonth) 
    SET @vToDate = CONVERT(DATE, CONCAT(@vToYear, '-', @vToMonth, '-', '01'))  
    SET @vToDate = EOMONTH(@vToDate)
     
    DECLARE @vPartnerName   NVARCHAR (160)
    SET @vPartnerName = @PartnerName

IF @vPartnerName = '*** All Customers'
	SELECT CASE 
	           WHEN A.ParentAccountIdName IS NULL THEN
	               A.Name
	           ELSE
	               A.ParentAccountIdName
	       END                             AS Parent
	      ,A.Name                          AS Name
		  ,A.bt_StatusName                 AS AccountStatus
		  ,A.StatusCodeName                AS AccountSoftDeleteStatus
		  ,PA.bt_ProductLineName           AS ProductLine
		  ,PA.StatusCodeName               AS ProceesingAcctSoftDeleteStatus
		  ,PP.bt_BankMID                   AS MID
		  ,CASE PA.bt_ProductLineName
			   WHEN 'Credit Card' THEN
				   PP.bt_SourceId
			   ELSE
				   PP.bt_Config
		   END                             AS SourceIdConfig            
		  ,PP.bt_StartDate                 AS MIDStartDate
		  ,PP.bt_EndDate                   AS MIDEndDate
		  ,PP.StatusCodeName               AS MIDSoftDeleteStatus
		  ,PartProd.PartnerName            AS PartnerName
		  ,PartProd.PartnerType            AS PartnerType
		  ,PartProd.PartnerSubType         AS PartnerSubType
		  ,PartProd.ProdName               AS ProductName
		  ,PartProd.ProdVersion            AS ProductVersion
		  ,PartProd.ServProdFeeStartDate   AS ServProdFeeStartDate
		  ,PartProd.ServProdFeeEndDate     AS ServProdFeeEndDate
		  ,PartProd.ServProdFeeSoftDelete  AS ServProdFeeSoftDelete
		  ,Opp.Name                        AS OpportunityName
		  ,Rev.Revenue                     AS Revenue
		  ,Rev.Cost                        AS Cost
		  ,Rev.Volume                      AS Volume
		  ,Rev.Count                       AS Count
	FROM 
		(
		 SELECT DISTINCT AllRevSources.CRM_Id             AS CRM_Id
		            ,SUM(AllRevSources.Revenue)           AS Revenue
		            ,SUM(AllRevSources.Cost)              AS Cost
		            ,SUM(AllRevSources.Volume)            AS Volume
		            ,SUM(AllRevSources.Count)             AS Count
		 FROM (
		         ------------------------------------------------------------
		         --    Get MeS Revenue, Costs, Volume and Count            --
		         ------------------------------------------------------------
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,SUM(Volume)                                           AS Volume
					   ,SUM(Count)                                            AS Count
				 FROM dbo.MeSRevenueMonthlyGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get ATI Revenue, Costs                              --
		         ------------------------------------------------------------
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,0                                                     AS Volume
					   ,0                                                     AS Count					   
				 FROM dbo.ATIDailyRevenueGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get ATI Volume and Count                            --
		         ------------------------------------------------------------
		         SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,0                                                     AS Revenue
					   ,0                                                     AS Cost
					   ,SUM(Volume)                                           AS Volume
					   ,SUM(Count)                                            AS Count					   
				 FROM dbo.ATIMonthlyVolumeGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get USAePay Revenue, Costs                          --
		         --    With Volume and Count if they are Gateway Only      --
		         ------------------------------------------------------------				 
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,CASE 
					        WHEN PP.bt_GatewayOnlyName = 'Yes' THEN
					            SUM(Volume)  
					        ELSE
					            0
					    END                                                   AS Volume
					   ,CASE 
					        WHEN PP.bt_GatewayOnlyName = 'Yes' THEN
					            SUM(Count)  
					        ELSE
					            0
					    END                                                   AS Count
				 FROM dbo.USAePayRevenueMonthlyGLFeed
				 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP         ON CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
				                                                                   = PP.bt_ProcessingProfileId  
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
				         ,PP.bt_GatewayOnlyName    
			   )	 AllRevSources
         GROUP BY AllRevSources.CRM_Id			   	            
		)                                         AS Rev   
	JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP         ON Rev.CRM_Id = PP.bt_ProcessingProfileId            
	LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount   AS PA         ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	LEFT JOIN CRM.vPartnerWithAllSubTypes         AS PartProd   ON PA.bt_CurrentServiceOrder = PartProd.ServiceOrderId
	LEFT JOIN [$(CRMShadow)].MS.Account                AS A          ON PA.bt_Account = A.AccountId
	LEFT JOIN [$(CRMShadow)].MS.Opportunity            AS Opp        ON PA.bt_Opportunity = Opp.OpportunityId

ELSE

	SELECT CASE 
	           WHEN A.ParentAccountIdName IS NULL THEN
	               A.Name
	           ELSE
	               A.ParentAccountIdName
	       END                             AS Parent
	      ,A.Name                          AS Name
		  ,A.bt_StatusName                 AS AccountStatus
		  ,A.StatusCodeName                AS AccountSoftDeleteStatus
		  ,PA.bt_ProductLineName           AS ProductLine
		  ,PA.StatusCodeName               AS ProceesingAcctSoftDeleteStatus
		  ,PP.bt_BankMID                   AS MID
		  ,CASE PA.bt_ProductLineName
			   WHEN 'Credit Card' THEN
				   PP.bt_SourceId
			   ELSE
				   PP.bt_Config
		   END                             AS SourceIdConfig            
		  ,PP.bt_StartDate                 AS MIDStartDate
		  ,PP.bt_EndDate                   AS MIDEndDate
		  ,PP.StatusCodeName               AS MIDSoftDeleteStatus
		  ,PartProd.PartnerName            AS PartnerName
		  ,PartProd.PartnerType            AS PartnerType
		  ,PartProd.PartnerSubType         AS PartnerSubType
		  ,PartProd.ProdName               AS ProductName
		  ,PartProd.ProdVersion            AS ProductVersion
		  ,PartProd.ServProdFeeStartDate   AS ServProdFeeStartDate
		  ,PartProd.ServProdFeeEndDate     AS ServProdFeeEndDate
		  ,PartProd.ServProdFeeSoftDelete  AS ServProdFeeSoftDelete
		  ,Opp.Name                        AS OpportunityName
		  ,Rev.Revenue                     AS Revenue
		  ,Rev.Cost                        AS Cost
		  ,Rev.Volume                      AS Volume
		  ,Rev.Count                       AS Count
	FROM 
		(
		 SELECT DISTINCT AllRevSources.CRM_Id             AS CRM_Id
		            ,SUM(AllRevSources.Revenue)           AS Revenue
		            ,SUM(AllRevSources.Cost)              AS Cost
		            ,SUM(AllRevSources.Volume)            AS Volume
		            ,SUM(AllRevSources.Count)             AS Count
		 FROM (
		         ------------------------------------------------------------
		         --    Get MeS Revenue, Costs, Volume and Count            --
		         ------------------------------------------------------------		           
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,SUM(Volume)                                           AS Volume
					   ,SUM(Count)                                            AS Count
				 FROM dbo.MeSRevenueMonthlyGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get ATI Revenue, Costs                              --
		         ------------------------------------------------------------				 
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,0                                                     AS Volume
					   ,0                                                     AS Count					   
				 FROM dbo.ATIDailyRevenueGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get ATI Volume and Count                            --
		         ------------------------------------------------------------				 
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,0                                                     AS Revenue
					   ,0                                                     AS Cost
					   ,SUM(Volume)                                           AS Volume
					   ,SUM(Count)                                            AS Count					   
				 FROM dbo.ATIMonthlyVolumeGLFeed
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
				 UNION ALL
		         ------------------------------------------------------------
		         --    Get USAePay Revenue, Costs                          --
		         --    With Volume and Count if they are Gateway Only      --
		         ------------------------------------------------------------					 
				 SELECT DISTINCT 
						CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  AS CRM_Id
					   ,SUM(RevenueAmount)                                    AS Revenue
					   ,SUM(Cost)                                             AS Cost
					   ,CASE 
					        WHEN PP.bt_GatewayOnlyName = 'Yes' THEN
					            SUM(Volume)  
					        ELSE
					            0
					    END                                                   AS Volume
					   ,CASE 
					        WHEN PP.bt_GatewayOnlyName = 'Yes' THEN
					            SUM(Count)  
					        ELSE
					            0
					    END                                                   AS Count				   
				 FROM dbo.USAePayRevenueMonthlyGLFeed
				 LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP         ON CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
				                                                                   = PP.bt_ProcessingProfileId  				 
				 WHERE TranDate BETWEEN @vFromDate AND @vToDate
				 GROUP BY CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard 
				         ,PP.bt_GatewayOnlyName  
			   )	 AllRevSources
         GROUP BY AllRevSources.CRM_Id			   	            
		)                                         AS Rev   
	JOIN [$(CRMShadow)].MS.bt_ProcessingProfile        AS PP         ON Rev.CRM_Id = PP.bt_ProcessingProfileId            
	LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount   AS PA         ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	LEFT JOIN CRM.vPartnerWithAllSubTypes         AS PartProd   ON PA.bt_CurrentServiceOrder = PartProd.ServiceOrderId
	LEFT JOIN [$(CRMShadow)].MS.Account                AS A          ON PA.bt_Account = A.AccountId
	LEFT JOIN [$(CRMShadow)].MS.Opportunity            AS Opp        ON PA.bt_Opportunity = Opp.OpportunityId

	
	WHERE PartProd.PartnerName = @vPartnerName

END













