-- =============================================
-- Author:        Rick Stohle
-- Create date:   Dec-2015
-- Modified:      <>
-- Description:   Report requested by Edz, to show a rolling 12 Months of Revenue, Volume, Count and Costs, by MID.
--                We use the Product Line parameter (ACH or Credit Card) to drive through the appropriate Revenue tables.
-- =============================================
CREATE PROCEDURE [dbo].[spMonthlyRevenueByProductLineReport]
    (
      @ProductLine CHAR(3) ,
      @YearMonth INT
    )

-- Insert statements for procedure here
AS
    BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @ProductLineName CHAR(3);
        DECLARE @RunYearMonth INT;
        SET @ProductLineName = @ProductLine;
        SET @RunYearMonth = @YearMonth;

        DECLARE @StartDate DATE;
        DECLARE @EndDate DATE;

        SET @EndDate = CONVERT(DATE, CONCAT(LEFT(@YearMonth, 4),
                                            RIGHT(@YearMonth, 2), '01'));  -- this is the 1st of the month
        SET @StartDate = DATEADD(MM, -11, @EndDate);                                            -- is the first day of the month; 11 months prior
        SET @EndDate = EOMONTH(@EndDate);                                                       -- change End Date to the last day of the month


	  -------------------------------------------------------------
	  --  Credit Card Revenue tables use the 1st of the Month
	  -------------------------------------------------------------
        DECLARE @CCDate1 DATE;
        DECLARE @CCDate2 DATE;
        DECLARE @CCDate3 DATE;
        DECLARE @CCDate4 DATE;
        DECLARE @CCDate5 DATE;
        DECLARE @CCDate6 DATE;
        DECLARE @CCDate7 DATE;
        DECLARE @CCDate8 DATE;
        DECLARE @CCDate9 DATE;
        DECLARE @CCDate10 DATE;
        DECLARE @CCDate11 DATE;
        DECLARE @CCDate12 DATE;
        SET @CCDate1 = @StartDate;
        SET @CCDate2 = DATEADD(MM, 1, @CCDate1);
        SET @CCDate3 = DATEADD(MM, 1, @CCDate2); 
        SET @CCDate4 = DATEADD(MM, 1, @CCDate3);
        SET @CCDate5 = DATEADD(MM, 1, @CCDate4);
        SET @CCDate6 = DATEADD(MM, 1, @CCDate5);
        SET @CCDate7 = DATEADD(MM, 1, @CCDate6);
        SET @CCDate8 = DATEADD(MM, 1, @CCDate7);
        SET @CCDate9 = DATEADD(MM, 1, @CCDate8);
        SET @CCDate10 = DATEADD(MM, 1, @CCDate9);
        SET @CCDate11 = DATEADD(MM, 1, @CCDate10);
        SET @CCDate12 = DATEADD(MM, 1, @CCDate11);

	  -------------------------------------------------------------
	  --  ACH Revenue tables use the last day of the Month
	  -------------------------------------------------------------
        DECLARE @ACHDate1 DATE;
        DECLARE @ACHDate2 DATE;
        DECLARE @ACHDate3 DATE;
        DECLARE @ACHDate4 DATE;
        DECLARE @ACHDate5 DATE;
        DECLARE @ACHDate6 DATE;
        DECLARE @ACHDate7 DATE;
        DECLARE @ACHDate8 DATE;
        DECLARE @ACHDate9 DATE;
        DECLARE @ACHDate10 DATE;
        DECLARE @ACHDate11 DATE;
        DECLARE @ACHDate12 DATE;
        SET @ACHDate1 = EOMONTH(@CCDate1); 
        SET @ACHDate2 = EOMONTH(@CCDate2);
        SET @ACHDate3 = EOMONTH(@CCDate3);
        SET @ACHDate4 = EOMONTH(@CCDate4);
        SET @ACHDate5 = EOMONTH(@CCDate5);
        SET @ACHDate6 = EOMONTH(@CCDate6);
        SET @ACHDate7 = EOMONTH(@CCDate7);
        SET @ACHDate8 = EOMONTH(@CCDate8);
        SET @ACHDate9 = EOMONTH(@CCDate9);
        SET @ACHDate10 = EOMONTH(@CCDate10);
        SET @ACHDate11 = EOMONTH(@CCDate11);
        SET @ACHDate12 = EOMONTH(@CCDate12);
	   
        DECLARE @DynamicSql NVARCHAR(4000); 

	        --------------------------------------------------------------------------
			--                      Declare Revenue Temp Table
			--                      Declare Volume Temp Table
			--                      Declare Counts Temp Table
			--                      Declare Costs Temp Table
			--------------------------------------------------------------------------
        DECLARE @Revenue TABLE
            (
              ParentName NVARCHAR(255) ,
              ChildName NVARCHAR(255) ,
              MerchantID NVARCHAR(255) ,
              FirstProcessingDate DATE ,
              RevDate1 MONEY ,
              RevDate2 MONEY ,
              RevDate3 MONEY ,
              RevDate4 MONEY ,
              RevDate5 MONEY ,
              RevDate6 MONEY ,
              RevDate7 MONEY ,
              RevDate8 MONEY ,
              RevDate9 MONEY ,
              RevDate10 MONEY ,
              RevDate11 MONEY ,
              RevDate12 MONEY
            );

        DECLARE @Volume TABLE
            (
              ParentName NVARCHAR(255) ,
              ChildName NVARCHAR(255) ,
              MerchantID NVARCHAR(255) ,
              FirstProcessingDate DATE ,
              GWVolDate1 MONEY ,
              GWVolDate2 MONEY ,
              GWVolDate3 MONEY ,
              GWVolDate4 MONEY ,
              GWVolDate5 MONEY ,
              GWVolDate6 MONEY ,
              GWVolDate7 MONEY ,
              GWVolDate8 MONEY ,
              GWVolDate9 MONEY ,
              GWVolDate10 MONEY ,
              GWVolDate11 MONEY ,
              GWVolDate12 MONEY
            );

        DECLARE @Counts TABLE
            (
              ParentName NVARCHAR(255) ,
              ChildName NVARCHAR(255) ,
              MerchantID NVARCHAR(255) ,
              FirstProcessingDate DATE ,
              GWCntDate1 MONEY ,
              GWCntDate2 MONEY ,
              GWCntDate3 MONEY ,
              GWCntDate4 MONEY ,
              GWCntDate5 MONEY ,
              GWCntDate6 MONEY ,
              GWCntDate7 MONEY ,
              GWCntDate8 MONEY ,
              GWCntDate9 MONEY ,
              GWCntDate10 MONEY ,
              GWCntDate11 MONEY ,
              GWCntDate12 MONEY
            );

        DECLARE @Costs TABLE
            (
              ParentName NVARCHAR(255) ,
              ChildName NVARCHAR(255) ,
              MerchantID NVARCHAR(255) ,
              FirstProcessingDate DATE ,
              CstDate1 MONEY ,
              CstDate2 MONEY ,
              CstDate3 MONEY ,
              CstDate4 MONEY ,
              CstDate5 MONEY ,
              CstDate6 MONEY ,
              CstDate7 MONEY ,
              CstDate8 MONEY ,
              CstDate9 MONEY ,
              CstDate10 MONEY ,
              CstDate11 MONEY ,
              CstDate12 MONEY
            );


        IF @ProductLineName = 'CrC'
            BEGIN
	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Revenue
			--             Insert the ResultSet into the Revenue Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
			        
					----------------------------------------------------------------
					--   Pivot Function for the Revenue Column - USAePay and MeS
					----------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '], 0)   AS RevDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '], 0)   AS RevDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '], 0)   AS RevDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '], 0)   AS RevDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '], 0)   AS RevDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '], 0)   AS RevDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '], 0)   AS RevDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '], 0)   AS RevDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '], 0)   AS RevDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate10, 106)
                    + '], 0)  AS RevDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate11, 106)
                    + '], 0)  AS RevDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '], 0)  AS RevDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(RevenueAmount, 0)               AS SourceRevenue
						  FROM dbo.USAePayRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''

						  UNION ALL
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(RevenueAmount, 0)               AS SourceRevenue
						  FROM dbo.MeSRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceRevenue)
					FOR SourceTranDate IN ([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '])
					)                                             AS PivotTable
                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Revenue
                        EXEC sp_executesql @DynamicSql;


	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Volume
			--             Insert the ResultSet into the Volume Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					-------------------------------------------------------------------
					--   Pivot Function for the GateWay Volume Column - USAePay only
					-------------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '], 0)   AS GWVolDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '], 0)   AS GWVolDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '], 0)   AS GWVolDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '], 0)   AS GWVolDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '], 0)   AS GWVolDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '], 0)   AS GWVolDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '], 0)   AS GWVolDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '], 0)   AS GWVolDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '], 0)   AS GWVolDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate10, 106)
                    + '], 0)  AS GWVolDat10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate11, 106)
                    + '], 0)  AS GWVolDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '], 0)  AS GWVolDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(Volume, 0)                      AS SourceVolume
						  FROM dbo.USAePayRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceVolume)
					FOR SourceTranDate IN ([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '])
					)                                             AS PivotTable		        

                   ';   -- Ends the Dynamic SQL 
       --PRINT (@DynamicSql)
                INSERT  INTO @Volume
                        EXEC sp_executesql @DynamicSql;



	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Counts
			--             Insert the ResultSet into the Counts Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					-------------------------------------------------------------------
					--   Pivot Function for the GateWay Count Column - USAePay only 
					-------------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '], 0)   AS GWCntDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '], 0)   AS GWCntDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '], 0)   AS GWCntDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '], 0)   AS GWCntDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '], 0)   AS GWCntDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '], 0)   AS GWCntDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '], 0)   AS GWCntDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '], 0)   AS GWCntDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '], 0)   AS GWCntDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate10, 106)
                    + '], 0)  AS GWCntDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate11, 106)
                    + '], 0)  AS GWCntDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '], 0)  AS GWCntDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(Count, 0)                       AS SourceCount
						  FROM dbo.USAePayRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceCount)
					FOR SourceTranDate IN ([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '])
					)                                             AS PivotTable
			                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Counts
                        EXEC sp_executesql @DynamicSql;



	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Costs
			--             Insert the ResultSet into the Costs Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					----------------------------------------------------------
					--   Pivot Function for the Cost Column - USAePay and MeS
					----------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '], 0)   AS CstDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '], 0)   AS CstDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '], 0)   AS CstDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '], 0)   AS CstDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '], 0)   AS CstDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '], 0)   AS CstDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '], 0)   AS CstDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '], 0)   AS CstDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '], 0)   AS CstDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate10, 106)
                    + '], 0)  AS CstDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate11, 106)
                    + '], 0)  AS CstDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '], 0)  AS CstDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(Cost, 0)                        AS SourceCost
						  FROM dbo.USAePayRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						  UNION ALL
								SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
								,CRMActLevelName_GPParentName           AS SourceChild
								,MID                                    AS SourceMerchantID
								,ISNULL(ProcessingDate, '' '')            AS SourceFirstProcessing
								,CONVERT(DATE, TranDate)                AS SourceTranDate
								,ISNULL(Cost, 0)                        AS SourceCost
						  FROM dbo.MeSRevenueMonthlyGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceCost)
					FOR SourceTranDate IN ([' + CONVERT(NVARCHAR(11), @CCDate1, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate2, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate3, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate4, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate5, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate6, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate7, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate8, 106)
                    + '],
                                           [' + CONVERT(NVARCHAR(11), @CCDate9, 106)
                    + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @CCDate12, 106)
                    + '])
					)                                             AS PivotTable

			                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Costs
                        EXEC sp_executesql @DynamicSql;





		-----------------------------------------------------------------------------
		--  Create the Driver of all USAePay and MeS MIDs for the 12 Month period
		--  Then join to the 4 Temp Tables created above
		-----------------------------------------------------------------------------
                SELECT DISTINCT
                        Driver.SourceParent AS ParentName ,
                        Driver.SourceChild AS ChildName ,
                        Driver.SourceMerchantID AS MerchantID ,
                        CASE WHEN Driver.SourceFirstProcessing = ' ' THEN NULL
                             ELSE Driver.SourceFirstProcessing
                        END AS FirstProcessingDate ,
                        RevPivot.RevDate1 ,
                        VolPivot.GWVolDate1 ,
                        CntPivot.GWCntDate1 ,
                        CstPivot.CstDate1 ,
                        RevPivot.RevDate2 ,
                        VolPivot.GWVolDate2 ,
                        CntPivot.GWCntDate2 ,
                        CstPivot.CstDate2 ,
                        RevPivot.RevDate3 ,
                        VolPivot.GWVolDate3 ,
                        CntPivot.GWCntDate3 ,
                        CstPivot.CstDate3 ,
                        RevPivot.RevDate4 ,
                        VolPivot.GWVolDate4 ,
                        CntPivot.GWCntDate4 ,
                        CstPivot.CstDate4 ,
                        RevPivot.RevDate5 ,
                        VolPivot.GWVolDate5 ,
                        CntPivot.GWCntDate5 ,
                        CstPivot.CstDate5 ,
                        RevPivot.RevDate6 ,
                        VolPivot.GWVolDate6 ,
                        CntPivot.GWCntDate6 ,
                        CstPivot.CstDate6 ,
                        RevPivot.RevDate7 ,
                        VolPivot.GWVolDate7 ,
                        CntPivot.GWCntDate7 ,
                        CstPivot.CstDate7 ,
                        RevPivot.RevDate8 ,
                        VolPivot.GWVolDate8 ,
                        CntPivot.GWCntDate8 ,
                        CstPivot.CstDate8 ,
                        RevPivot.RevDate9 ,
                        VolPivot.GWVolDate9 ,
                        CntPivot.GWCntDate9 ,
                        CstPivot.CstDate9 ,
                        RevPivot.RevDate10 ,
                        VolPivot.GWVolDate10 ,
                        CntPivot.GWCntDate10 ,
                        CstPivot.CstDate10 ,
                        RevPivot.RevDate11 ,
                        VolPivot.GWVolDate11 ,
                        CntPivot.GWCntDate11 ,
                        CstPivot.CstDate11 ,
                        RevPivot.RevDate12 ,
                        VolPivot.GWVolDate12 ,
                        CntPivot.GWCntDate12 ,
                        CstPivot.CstDate12 ,
                        ( RevPivot.RevDate1 + RevPivot.RevDate2
                          + RevPivot.RevDate3 + RevPivot.RevDate4
                          + RevPivot.RevDate5 + RevPivot.RevDate6
                          + RevPivot.RevDate7 + RevPivot.RevDate8
                          + RevPivot.RevDate9 + RevPivot.RevDate10
                          + RevPivot.RevDate11 + RevPivot.RevDate12 ) AS RevTotal ,
                        ( VolPivot.GWVolDate1 + VolPivot.GWVolDate2
                          + VolPivot.GWVolDate3 + VolPivot.GWVolDate4
                          + VolPivot.GWVolDate5 + VolPivot.GWVolDate6
                          + VolPivot.GWVolDate7 + VolPivot.GWVolDate8
                          + VolPivot.GWVolDate9 + VolPivot.GWVolDate10
                          + VolPivot.GWVolDate11 + VolPivot.GWVolDate12 ) AS GWVolTotal ,
                        ( CntPivot.GWCntDate1 + CntPivot.GWCntDate2
                          + CntPivot.GWCntDate3 + CntPivot.GWCntDate4
                          + CntPivot.GWCntDate5 + CntPivot.GWCntDate6
                          + CntPivot.GWCntDate7 + CntPivot.GWCntDate8
                          + CntPivot.GWCntDate9 + CntPivot.GWCntDate10
                          + CntPivot.GWCntDate11 + CntPivot.GWCntDate12 ) AS GWCntTotal ,
                        ( CstPivot.CstDate1 + CstPivot.CstDate2
                          + CstPivot.CstDate3 + CstPivot.CstDate4
                          + CstPivot.CstDate5 + CstPivot.CstDate6
                          + CstPivot.CstDate7 + CstPivot.CstDate8
                          + CstPivot.CstDate9 + CstPivot.CstDate10
                          + CstPivot.CstDate11 + CstPivot.CstDate12 ) AS CstTotal
                FROM    ( SELECT DISTINCT
                                    CRMParentName_GPExtenderNatlAcctName AS SourceParent ,
                                    CRMActLevelName_GPParentName AS SourceChild ,
                                    MID AS SourceMerchantID ,
                                    ISNULL(ProcessingDate, ' ') AS SourceFirstProcessing ,
                                    CONVERT(DATE, TranDate) AS SourceTranDate
                          FROM      dbo.USAePayRevenueMonthlyGLFeed
                          WHERE     TranDate BETWEEN @StartDate AND @EndDate
                          UNION ALL
                          SELECT DISTINCT
                                    CRMParentName_GPExtenderNatlAcctName AS SourceParent ,
                                    CRMActLevelName_GPParentName AS SourceChild ,
                                    MID AS SourceMerchantID ,
                                    ISNULL(ProcessingDate, ' ') AS SourceFirstProcessing ,
                                    CONVERT(DATE, TranDate) AS SourceTranDate
                          FROM      dbo.MeSRevenueMonthlyGLFeed
                          WHERE     TranDate BETWEEN @StartDate AND @EndDate
                        ) AS Driver
                        LEFT JOIN @Revenue AS RevPivot ON Driver.SourceParent = RevPivot.ParentName
                                                          AND Driver.SourceChild = RevPivot.ChildName
                                                          AND Driver.SourceMerchantID = RevPivot.MerchantID
                                                          AND Driver.SourceFirstProcessing = RevPivot.FirstProcessingDate
                        LEFT JOIN @Volume AS VolPivot ON Driver.SourceParent = VolPivot.ParentName
                                                         AND Driver.SourceChild = VolPivot.ChildName
                                                         AND Driver.SourceMerchantID = VolPivot.MerchantID
                                                         AND Driver.SourceFirstProcessing = VolPivot.FirstProcessingDate
                        LEFT JOIN @Counts AS CntPivot ON Driver.SourceParent = CntPivot.ParentName
                                                         AND Driver.SourceChild = CntPivot.ChildName
                                                         AND Driver.SourceMerchantID = CntPivot.MerchantID
                                                         AND Driver.SourceFirstProcessing = CntPivot.FirstProcessingDate
                        LEFT JOIN @Costs AS CstPivot ON Driver.SourceParent = CstPivot.ParentName
                                                        AND Driver.SourceChild = CstPivot.ChildName
                                                        AND Driver.SourceMerchantID = CstPivot.MerchantID
                                                        AND Driver.SourceFirstProcessing = CstPivot.FirstProcessingDate;
-----------------------------------------------------------
--                DROP the Temp Tables
-----------------------------------------------------------
        --DROP TABLE @Revenue
		--DROP TABLE @Volume
		--DROP TABLE @Counts
		--DROP TABLE @Costs
		
            END;


        ELSE                -----          ACH       --------
            BEGIN

	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Revenue
			--             Insert the ResultSet into the Revenue Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
			        
					----------------------------------------------------------------
					--   Pivot Function for the Revenue Column - Only ATI Daily
					----------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate1, 106)
                    + '], 0)   AS RevDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate2, 106)
                    + '], 0)   AS RevDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate3, 106)
                    + '], 0)   AS RevDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate4, 106)
                    + '], 0)   AS RevDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate5, 106)
                    + '], 0)   AS RevDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate6, 106)
                    + '], 0)   AS RevDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate7, 106)
                    + '], 0)   AS RevDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate8, 106)
                    + '], 0)   AS RevDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate9, 106)
                    + '], 0)   AS RevDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate10, 106)
                    + '], 0)  AS RevDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate11, 106)
                    + '], 0)  AS RevDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '], 0)  AS RevDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
						        ,CRMActLevelName_GPParentName           AS SourceChild
						        ,BTAssignedProcessingRecord_GPChildCustomerNum   AS SourceMerchantID
						        ,ISNULL(ProcessingDate, '' '')          AS SourceFirstProcessing
						        ,EOMONTH(CONVERT(DATE, TranDate))       AS SourceTranDate
					        	,ISNULL(RevenueAmount, 0)               AS SourceRevenue
				          FROM dbo.ATIDailyRevenueGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceRevenue)
					FOR SourceTranDate IN (['
                    + CONVERT(NVARCHAR(11), @ACHDate1, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate2, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate3, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate4, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate5, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate6, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate7, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate8, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate9, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '])
					)                                             AS PivotTable
                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Revenue
                        EXEC sp_executesql @DynamicSql;



	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Volume
			--             Insert the ResultSet into the Volume Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					-------------------------------------------------------------------
					--   Pivot Function for the GateWay Volume Column - Monthly Volume Only table
					-------------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate1, 106)
                    + '], 0)   AS GWVolDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate2, 106)
                    + '], 0)   AS GWVolDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate3, 106)
                    + '], 0)   AS GWVolDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate4, 106)
                    + '], 0)   AS GWVolDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate5, 106)
                    + '], 0)   AS GWVolDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate6, 106)
                    + '], 0)   AS GWVolDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate7, 106)
                    + '], 0)   AS GWVolDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate8, 106)
                    + '], 0)   AS GWVolDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate9, 106)
                    + '], 0)   AS GWVolDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate10, 106)
                    + '], 0)  AS GWVolDat10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate11, 106)
                    + '], 0)  AS GWVolDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '], 0)  AS GWVolDate12
					FROM (
						  SELECT DISTINCT 
						         CRMParentName_GPExtenderNatlAcctName   AS SourceParent
						        ,CRMActLevelName_GPParentName           AS SourceChild
						        ,BTAssignedProcessingRecord_GPChildCustomerNum   AS SourceMerchantID
						        ,ISNULL(ProcessingDate, '' '')          AS SourceFirstProcessing
						        ,EOMONTH(CONVERT(DATE, TranDate))       AS SourceTranDate
						        ,ISNULL(Volume, 0)                      AS SourceVolume
				          FROM dbo.ATIMonthlyVolumeGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceVolume)
					FOR SourceTranDate IN (['
                    + CONVERT(NVARCHAR(11), @ACHDate1, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate2, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate3, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate4, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate5, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate6, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate7, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate8, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate9, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '])
					)                                             AS PivotTable		        

                   ';   -- Ends the Dynamic SQL 
       --PRINT (@DynamicSql)
                INSERT  INTO @Volume
                        EXEC sp_executesql @DynamicSql;


	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Counts
			--             Insert the ResultSet into the Counts Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					-------------------------------------------------------------------
					--   Pivot Function for the GateWay Count Column - Monthly Volume Only table
					-------------------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate1, 106)
                    + '], 0)   AS GWCntDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate2, 106)
                    + '], 0)   AS GWCntDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate3, 106)
                    + '], 0)   AS GWCntDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate4, 106)
                    + '], 0)   AS GWCntDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate5, 106)
                    + '], 0)   AS GWCntDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate6, 106)
                    + '], 0)   AS GWCntDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate7, 106)
                    + '], 0)   AS GWCntDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate8, 106)
                    + '], 0)   AS GWCntDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate9, 106)
                    + '], 0)   AS GWCntDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate10, 106)
                    + '], 0)  AS GWCntDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate11, 106)
                    + '], 0)  AS GWCntDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '], 0)  AS GWCntDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
						,CRMActLevelName_GPParentName           AS SourceChild
						,BTAssignedProcessingRecord_GPChildCustomerNum   AS SourceMerchantID
						,ISNULL(ProcessingDate, '' '')          AS SourceFirstProcessing
						,EOMONTH(CONVERT(DATE, TranDate))       AS SourceTranDate
						,ISNULL(Count, 0)                       AS SourceCount
				  FROM dbo.ATIMonthlyVolumeGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceCount)
					FOR SourceTranDate IN (['
                    + CONVERT(NVARCHAR(11), @ACHDate1, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate2, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate3, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate4, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate5, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate6, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate7, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate8, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate9, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '])
					)                                             AS PivotTable
			                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Counts
                        EXEC sp_executesql @DynamicSql;



	        --------------------------------------------------------------------------
			--   Create Dynamic SQL to return a ResultSet for 12 Months of Costs
			--             Insert the ResultSet into the Costs Temp Table
			--------------------------------------------------------------------------
                SET @DynamicSql = '
					----------------------------------------------------------
					--   Pivot Function for the Cost Column - Both Daily and Monthly tables
					----------------------------------------------------------
					SELECT SourceParent          AS ParentName
						  ,SourceChild           AS ChildName
						  ,SourceMerchantID      AS MerchantID
						  ,ISNULL(SourceFirstProcessing, '' '') AS FirstProcessingDate
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate1, 106)
                    + '], 0)   AS CstDate1
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate2, 106)
                    + '], 0)   AS CstDate2
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate3, 106)
                    + '], 0)   AS CstDate3
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate4, 106)
                    + '], 0)   AS CstDate4
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate5, 106)
                    + '], 0)   AS CstDate5
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate6, 106)
                    + '], 0)   AS CstDate6
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate7, 106)
                    + '], 0)   AS CstDate7
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate8, 106)
                    + '], 0)   AS CstDate8
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate9, 106)
                    + '], 0)   AS CstDate9
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate10, 106)
                    + '], 0)  AS CstDate10
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate11, 106)
                    + '], 0)  AS CstDate11
						  ,ISNULL([' + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '], 0)  AS CstDate12
					FROM (
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
					         	,CRMActLevelName_GPParentName           AS SourceChild
					         	,BTAssignedProcessingRecord_GPChildCustomerNum   AS SourceMerchantID
						        ,ISNULL(ProcessingDate, '' '')          AS SourceFirstProcessing
						        ,EOMONTH(CONVERT(DATE, TranDate))       AS SourceTranDate
						        ,ISNULL(Cost, 0)                        AS SourceCost
				          FROM dbo.ATIDailyRevenueGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						  UNION ALL
						  SELECT CRMParentName_GPExtenderNatlAcctName   AS SourceParent
						        ,CRMActLevelName_GPParentName           AS SourceChild
					        	,BTAssignedProcessingRecord_GPChildCustomerNum   AS SourceMerchantID
						        ,ISNULL(ProcessingDate, '' '')          AS SourceFirstProcessing
						        ,EOMONTH(CONVERT(DATE, TranDate))       AS SourceTranDate
						        ,ISNULL(Cost, 0)                        AS SourceCost
				          FROM dbo.ATIMonthlyVolumeGLFeed
						  WHERE TranDate BETWEEN   '''
                    + CONVERT(NVARCHAR(11), @StartDate, 106) + '''  AND   '''
                    + CONVERT(NVARCHAR(11), @EndDate, 106)
                    + '''
						 )                                         AS SourceTable
					PIVOT
					(
					SUM(SourceCost)
					FOR SourceTranDate IN (['
                    + CONVERT(NVARCHAR(11), @ACHDate1, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate2, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate3, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate4, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate5, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate6, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate7, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate8, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate9, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate10, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate11, 106) + '],
                                           ['
                    + CONVERT(NVARCHAR(11), @ACHDate12, 106)
                    + '])
					)                                             AS PivotTable

			                   ';   -- Ends the Dynamic SQL

       --PRINT (@DynamicSql)
                INSERT  INTO @Costs
                        EXEC sp_executesql @DynamicSql;


		-----------------------------------------------------------------------------
		--  Create the Driver of all USAePay and MeS MIDs for the 12 Month period
		--  Then join to the 4 Temp Tables created above
		-----------------------------------------------------------------------------
                SELECT DISTINCT
                        Driver.SourceParent AS ParentName ,
                        Driver.SourceChild AS ChildName ,
                        RTRIM(Driver.SourceMerchantID) AS MerchantID ,
                        CASE WHEN Driver.SourceFirstProcessing = ' ' THEN NULL
                             ELSE Driver.SourceFirstProcessing
                        END AS FirstProcessingDate ,
                        RevPivot.RevDate1 ,
                        VolPivot.GWVolDate1 ,
                        CntPivot.GWCntDate1 ,
                        CstPivot.CstDate1 ,
                        RevPivot.RevDate2 ,
                        VolPivot.GWVolDate2 ,
                        CntPivot.GWCntDate2 ,
                        CstPivot.CstDate2 ,
                        RevPivot.RevDate3 ,
                        VolPivot.GWVolDate3 ,
                        CntPivot.GWCntDate3 ,
                        CstPivot.CstDate3 ,
                        RevPivot.RevDate4 ,
                        VolPivot.GWVolDate4 ,
                        CntPivot.GWCntDate4 ,
                        CstPivot.CstDate4 ,
                        RevPivot.RevDate5 ,
                        VolPivot.GWVolDate5 ,
                        CntPivot.GWCntDate5 ,
                        CstPivot.CstDate5 ,
                        RevPivot.RevDate6 ,
                        VolPivot.GWVolDate6 ,
                        CntPivot.GWCntDate6 ,
                        CstPivot.CstDate6 ,
                        RevPivot.RevDate7 ,
                        VolPivot.GWVolDate7 ,
                        CntPivot.GWCntDate7 ,
                        CstPivot.CstDate7 ,
                        RevPivot.RevDate8 ,
                        VolPivot.GWVolDate8 ,
                        CntPivot.GWCntDate8 ,
                        CstPivot.CstDate8 ,
                        RevPivot.RevDate9 ,
                        VolPivot.GWVolDate9 ,
                        CntPivot.GWCntDate9 ,
                        CstPivot.CstDate9 ,
                        RevPivot.RevDate10 ,
                        VolPivot.GWVolDate10 ,
                        CntPivot.GWCntDate10 ,
                        CstPivot.CstDate10 ,
                        RevPivot.RevDate11 ,
                        VolPivot.GWVolDate11 ,
                        CntPivot.GWCntDate11 ,
                        CstPivot.CstDate11 ,
                        RevPivot.RevDate12 ,
                        VolPivot.GWVolDate12 ,
                        CntPivot.GWCntDate12 ,
                        CstPivot.CstDate12 ,
                        ( RevPivot.RevDate1 + RevPivot.RevDate2
                          + RevPivot.RevDate3 + RevPivot.RevDate4
                          + RevPivot.RevDate5 + RevPivot.RevDate6
                          + RevPivot.RevDate7 + RevPivot.RevDate8
                          + RevPivot.RevDate9 + RevPivot.RevDate10
                          + RevPivot.RevDate11 + RevPivot.RevDate12 ) AS RevTotal ,
                        ( VolPivot.GWVolDate1 + VolPivot.GWVolDate2
                          + VolPivot.GWVolDate3 + VolPivot.GWVolDate4
                          + VolPivot.GWVolDate5 + VolPivot.GWVolDate6
                          + VolPivot.GWVolDate7 + VolPivot.GWVolDate8
                          + VolPivot.GWVolDate9 + VolPivot.GWVolDate10
                          + VolPivot.GWVolDate11 + VolPivot.GWVolDate12 ) AS GWVolTotal ,
                        ( CntPivot.GWCntDate1 + CntPivot.GWCntDate2
                          + CntPivot.GWCntDate3 + CntPivot.GWCntDate4
                          + CntPivot.GWCntDate5 + CntPivot.GWCntDate6
                          + CntPivot.GWCntDate7 + CntPivot.GWCntDate8
                          + CntPivot.GWCntDate9 + CntPivot.GWCntDate10
                          + CntPivot.GWCntDate11 + CntPivot.GWCntDate12 ) AS GWCntTotal ,
                        ( CstPivot.CstDate1 + CstPivot.CstDate2
                          + CstPivot.CstDate3 + CstPivot.CstDate4
                          + CstPivot.CstDate5 + CstPivot.CstDate6
                          + CstPivot.CstDate7 + CstPivot.CstDate8
                          + CstPivot.CstDate9 + CstPivot.CstDate10
                          + CstPivot.CstDate11 + CstPivot.CstDate12 ) AS CstTotal
                FROM    ( SELECT DISTINCT
                                    CRMParentName_GPExtenderNatlAcctName AS SourceParent ,
                                    CRMActLevelName_GPParentName AS SourceChild ,
                                    BTAssignedProcessingRecord_GPChildCustomerNum AS SourceMerchantID ,
                                    ISNULL(ProcessingDate, ' ') AS SourceFirstProcessing ,
                                    EOMONTH(CONVERT(DATE, TranDate)) AS SourceTranDate
                          FROM      dbo.ATIDailyRevenueGLFeed
                          WHERE     TranDate BETWEEN @StartDate AND @EndDate
                          UNION ALL
                          SELECT DISTINCT
                                    CRMParentName_GPExtenderNatlAcctName AS SourceParent ,
                                    CRMActLevelName_GPParentName AS SourceChild ,
                                    BTAssignedProcessingRecord_GPChildCustomerNum AS SourceMerchantID ,
                                    ISNULL(ProcessingDate, ' ') AS SourceFirstProcessing ,
                                    EOMONTH(CONVERT(DATE, TranDate)) AS SourceTranDate
                          FROM      dbo.ATIMonthlyVolumeGLFeed
                          WHERE     TranDate BETWEEN @StartDate AND @EndDate
                        ) AS Driver
                        LEFT JOIN @Revenue AS RevPivot ON Driver.SourceParent = RevPivot.ParentName
                                                          AND Driver.SourceChild = RevPivot.ChildName
                                                          AND Driver.SourceMerchantID = RevPivot.MerchantID
                                                          AND Driver.SourceFirstProcessing = RevPivot.FirstProcessingDate
                        LEFT JOIN @Volume AS VolPivot ON Driver.SourceParent = VolPivot.ParentName
                                                         AND Driver.SourceChild = VolPivot.ChildName
                                                         AND Driver.SourceMerchantID = VolPivot.MerchantID
                                                         AND Driver.SourceFirstProcessing = VolPivot.FirstProcessingDate
                        LEFT JOIN @Counts AS CntPivot ON Driver.SourceParent = CntPivot.ParentName
                                                         AND Driver.SourceChild = CntPivot.ChildName
                                                         AND Driver.SourceMerchantID = CntPivot.MerchantID
                                                         AND Driver.SourceFirstProcessing = CntPivot.FirstProcessingDate
                        LEFT JOIN @Costs AS CstPivot ON Driver.SourceParent = CstPivot.ParentName
                                                        AND Driver.SourceChild = CstPivot.ChildName
                                                        AND Driver.SourceMerchantID = CstPivot.MerchantID
                                                        AND Driver.SourceFirstProcessing = CstPivot.FirstProcessingDate;
            END;





    END;












