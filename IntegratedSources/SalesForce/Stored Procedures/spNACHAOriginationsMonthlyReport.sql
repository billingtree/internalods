﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spNACHAOriginationsMonthlyReport](
@Month1 INT,
@Year1  INT,
@ByMode CHAR
)

    -- Insert statements for procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month int, @Year int, @vByMode char
	SET @Month=@Month1
	SET @Year=@Year1
	SET @vByMode=@ByMode	
	
    -- Concatenate Year and Month in order to use the Index
    DECLARE @vYearMonth INT
    DECLARE @txtYear    VARCHAR (4)
    DECLARE @txtMonth   VARCHAR (2)
     
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))   	
	
----------------------------------------------------------------
--    ATI - get all Originations for the Month and Year
----------------------------------------------------------------  

                ----------------------------------------------------------
                --       Don't show Billing Tree Fees and Settlements   --
                --            Just Customer records                     --
                ----------------------------------------------------------
    IF @vByMode = 'C'   
		SELECT ATI.ReferenceCode               AS ReferenceCode
			   ,ATI.FileCreationDate           AS CreationDate
			   ,ATI.CompanyName                AS ACHName
			   ,ATI.BlockNumber                AS BatchNumber
			   ,ATI.StandardEntryClassCode     AS SEC
			   ,ATI.TransactionCode            AS TranCode
			   ,CASE ATI.TransactionCode
				   WHEN 27 THEN SUM(ATI.FormattedAmount)
				   WHEN 28 THEN SUM(ATI.FormattedAmount)
				   WHEN 29 THEN SUM(ATI.FormattedAmount)
				   WHEN 37 THEN SUM(ATI.FormattedAmount)
				   WHEN 38 THEN SUM(ATI.FormattedAmount)
				   WHEN 39 THEN SUM(ATI.FormattedAmount)
				END                            AS CreditTotal
			   ,CASE ATI.TransactionCode
				   WHEN 22 THEN SUM(ATI.FormattedAmount)
				   WHEN 23 THEN SUM(ATI.FormattedAmount)
				   WHEN 24 THEN SUM(ATI.FormattedAmount)
				   WHEN 32 THEN SUM(ATI.FormattedAmount)
				   WHEN 33 THEN SUM(ATI.FormattedAmount)
				   WHEN 34 THEN SUM(ATI.FormattedAmount)
				END                            AS DebitTotal        
		     
			   ,Count(*)                       AS CountTotal
			   ,'ATI'                          AS Source
		       
		FROM NACHA.Originations ATI  
		                                      
		WHERE ATI.FileCreationDateYYYYMM = @vYearMonth
		  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
		  AND ATI.UsabilityIndex = 1
		  AND ATI.SourceType = 4
		  AND UPPER(ATI.CompanyName) NOT LIKE 'BILLINGTREE%'
		  AND UPPER(ATI.CompanyName) NOT LIKE 'BT CONV FEE%'
		GROUP BY ATI.ReferenceCode
				 ,ATI.FileCreationDate
				 ,ATI.CompanyName
				 ,ATI.BlockNumber
				 ,ATI.StandardEntryClassCode
				 ,ATI.TransactionCode
		         
		UNION ALL

		----------------------------------------------------------------
		--    InStep - get all Originations for the Month and Year
		----------------------------------------------------------------    
		SELECT InStep.ReferenceCode               AS ReferenceCode
			   ,InStep.FileCreationDate           AS CreationDate
			   ,InStep.CompanyName                AS ACHName
			   ,InStep.BlockNumber                AS BatchNumber
			   ,InStep.StandardEntryClassCode     AS SEC
			   ,InStep.TransactionCode            AS TranCode
			   ,CASE InStep.TransactionCode
				   WHEN 27 THEN SUM(InStep.FormattedAmount)
				   WHEN 28 THEN SUM(InStep.FormattedAmount)
				   WHEN 29 THEN SUM(InStep.FormattedAmount)
				   WHEN 37 THEN SUM(InStep.FormattedAmount)
				   WHEN 38 THEN SUM(InStep.FormattedAmount)
				   WHEN 39 THEN SUM(InStep.FormattedAmount)
				END                              AS CreditTotal
			   ,CASE InStep.TransactionCode
				   WHEN 22 THEN SUM(InStep.FormattedAmount)
				   WHEN 23 THEN SUM(InStep.FormattedAmount)
				   WHEN 24 THEN SUM(InStep.FormattedAmount)
				   WHEN 32 THEN SUM(InStep.FormattedAmount)
				   WHEN 33 THEN SUM(InStep.FormattedAmount)
				   WHEN 34 THEN SUM(InStep.FormattedAmount)
				END                              AS DebitTotal        
		     
			   ,Count(*)                         AS CountTotal
			   ,'InStep'                         AS Source
		       
		FROM NACHA.Originations InStep  
		                                      
		WHERE InStep.FileCreationDateYYYYMM = @vYearMonth
		  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
		  AND InStep.UsabilityIndex = 1
		  AND InStep.SourceType = 1
		  AND UPPER(InStep.CompanyName) NOT LIKE 'BILLINGTREE%'
		  AND UPPER(InStep.CompanyName) NOT LIKE 'BT CONV FEE%'
		GROUP BY InStep.ReferenceCode
				 ,InStep.FileCreationDate
				 ,InStep.CompanyName
				 ,InStep.BlockNumber
				 ,InStep.StandardEntryClassCode
				 ,InStep.TransactionCode       
				   
ELSE
----------------------------------------------------------
--       Just show Billing Tree Fees and Settlements
----------------------------------------------------------
		SELECT ATI.ReferenceCode               AS ReferenceCode
			   ,ATI.FileCreationDate           AS CreationDate
			   ,ATI.CompanyName                AS ACHName
			   ,ATI.BlockNumber                AS BatchNumber
			   ,ATI.StandardEntryClassCode     AS SEC
			   ,ATI.TransactionCode            AS TranCode
			   ,CASE ATI.TransactionCode
				   WHEN 27 THEN SUM(ATI.FormattedAmount)
				   WHEN 28 THEN SUM(ATI.FormattedAmount)
				   WHEN 29 THEN SUM(ATI.FormattedAmount)
				   WHEN 37 THEN SUM(ATI.FormattedAmount)
				   WHEN 38 THEN SUM(ATI.FormattedAmount)
				   WHEN 39 THEN SUM(ATI.FormattedAmount)
				END                            AS CreditTotal
			   ,CASE ATI.TransactionCode
				   WHEN 22 THEN SUM(ATI.FormattedAmount)
				   WHEN 23 THEN SUM(ATI.FormattedAmount)
				   WHEN 24 THEN SUM(ATI.FormattedAmount)
				   WHEN 32 THEN SUM(ATI.FormattedAmount)
				   WHEN 33 THEN SUM(ATI.FormattedAmount)
				   WHEN 34 THEN SUM(ATI.FormattedAmount)
				END                            AS DebitTotal        
		     
			   ,Count(*)                       AS CountTotal
			   ,'ATI'                          AS Source
		       
		FROM NACHA.Originations ATI  
		                                      
		WHERE ATI.FileCreationDateYYYYMM = @vYearMonth
		  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
		  AND ATI.UsabilityIndex = 1
		  AND ATI.SourceType = 4
		  AND (UPPER(ATI.CompanyName) LIKE 'BILLINGTREE%'
		                       OR
		       UPPER(ATI.CompanyName) LIKE 'BT CONV FEE%')
		GROUP BY ATI.ReferenceCode
				 ,ATI.FileCreationDate
				 ,ATI.CompanyName
				 ,ATI.BlockNumber
				 ,ATI.StandardEntryClassCode
				 ,ATI.TransactionCode
		         
		UNION ALL

		----------------------------------------------------------------
		--    InStep - get all Originations for the Month and Year
		----------------------------------------------------------------    
		SELECT InStep.ReferenceCode               AS ReferenceCode
			   ,InStep.FileCreationDate           AS CreationDate
			   ,InStep.CompanyName                AS ACHName
			   ,InStep.BlockNumber                AS BatchNumber
			   ,InStep.StandardEntryClassCode     AS SEC
			   ,InStep.TransactionCode            AS TranCode
			   ,CASE InStep.TransactionCode
				   WHEN 27 THEN SUM(InStep.FormattedAmount)
				   WHEN 28 THEN SUM(InStep.FormattedAmount)
				   WHEN 29 THEN SUM(InStep.FormattedAmount)
				   WHEN 37 THEN SUM(InStep.FormattedAmount)
				   WHEN 38 THEN SUM(InStep.FormattedAmount)
				   WHEN 39 THEN SUM(InStep.FormattedAmount)
				END                              AS CreditTotal
			   ,CASE InStep.TransactionCode
				   WHEN 22 THEN SUM(InStep.FormattedAmount)
				   WHEN 23 THEN SUM(InStep.FormattedAmount)
				   WHEN 24 THEN SUM(InStep.FormattedAmount)
				   WHEN 32 THEN SUM(InStep.FormattedAmount)
				   WHEN 33 THEN SUM(InStep.FormattedAmount)
				   WHEN 34 THEN SUM(InStep.FormattedAmount)
				END                              AS DebitTotal        
		     
			   ,Count(*)                         AS CountTotal
			   ,'InStep'                         AS Source
		       
		FROM NACHA.Originations InStep  
		                                      
		WHERE InStep.FileCreationDateYYYYMM = @vYearMonth
		  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
		  AND InStep.UsabilityIndex = 1
		  AND InStep.SourceType = 1
		  AND (UPPER(InStep.CompanyName) LIKE 'BILLINGTREE%'
		                       OR
		       UPPER(InStep.CompanyName) LIKE 'BT CONV FEE%')		  
		GROUP BY InStep.ReferenceCode
				 ,InStep.FileCreationDate
				 ,InStep.CompanyName
				 ,InStep.BlockNumber
				 ,InStep.StandardEntryClassCode
				 ,InStep.TransactionCode       



		         
END
