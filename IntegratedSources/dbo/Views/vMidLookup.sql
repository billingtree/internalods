﻿


CREATE VIEW [dbo].[vMidLookup]
AS
      
SELECT DISTINCT
       [CRMParent]          AS CRMParent   
      ,StartYYYYMM          AS StartYYYYMM
      ,EndYYYYMM            AS EndYYYYMM
FROM dbo.MidLookupUMS 
UNION
SELECT DISTINCT
       [CRMParent]          AS CRMParent 
      ,StartYYYYMM          AS StartYYYYMM
      ,EndYYYYMM            AS EndYYYYMM
FROM dbo.MidLookupMeS




