﻿CREATE TABLE [SalesForce].[OpportunityHistory] (
    [Amount]           NUMERIC (18, 2) NULL,
    [CloseDate]        DATETIME        NULL,
    [CreatedById]      NVARCHAR (18)   NULL,
    [CreatedDate]      DATETIME        NULL,
    [ExpectedRevenue]  NUMERIC (18, 2) NULL,
    [ForecastCategory] NVARCHAR (40)   NULL,
    [Id]               NVARCHAR (36)   NOT NULL,
    [IsDeleted]        BIT             NULL,
    [OpportunityId]    NVARCHAR (36)   NULL,
    [Probability]      NUMERIC (3)     NULL,
    [StageName]        NVARCHAR (100)  NULL,
    [SystemModstamp]   DATETIME        NULL,
    [InsertDate]       DATETIME        CONSTRAINT [DF_OpportunityHistory_InsertDate] DEFAULT (getdate()) NOT NULL,
    [UpdateDate]       DATETIME        NULL,
    CONSTRAINT [PK_OpportunityHistory] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_OpportunityHistory_Opportunity] FOREIGN KEY ([OpportunityId]) REFERENCES [SalesForce].[Opportunity] ([Id])
);

