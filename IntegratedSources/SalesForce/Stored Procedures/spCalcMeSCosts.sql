﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spCalcMeSCosts]
(
@RunYear  INT,
@RunMonth INT,
@RunMode NUMERIC (17, 16) = NULL OUTPUT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @Year            INT
	       ,@Month           INT
	       ,@TranCount       NUMERIC (32, 16)
	       ,@HoldTranCount   NUMERIC (32, 16)
	       ,@MeSDate         DATE
	       ,@Level_1_Count   NUMERIC (32, 16)
	       ,@Level_2_Count   NUMERIC (32, 16)
	       ,@Level_3_Count   NUMERIC (32, 16)
	       ,@Level_4_Count   NUMERIC (32, 16)
	       ,@Level_5_Count   NUMERIC (32, 16)	
	              
	       ,@Level_1_Charge  NUMERIC (32, 16)
	       ,@Level_2_Charge  NUMERIC (32, 16)
	       ,@Level_3_Charge  NUMERIC (32, 16)
	       ,@Level_4_Charge  NUMERIC (32, 16)
	       ,@Level_5_Charge  NUMERIC (32, 16)
	       	       	       
	       ,@Total_Charge    NUMERIC (32, 16)
	       ,@PerTranRate     NUMERIC (17, 16)
	       
	SET @Year=@RunYear
	SET @Month=@RunMonth

    SET @Level_1_Count = 0
    SET @Level_2_Count = 0
    SET @Level_3_Count = 0
    SET @Level_4_Count = 0
    SET @Level_5_Count = 0
    SET @Level_1_Charge = 0
    SET @Level_2_Charge = 0
    SET @Level_3_Charge = 0
    SET @Level_4_Charge = 0
    SET @Level_5_Charge = 0

    SELECT @MeSDate = MeS.ACTIVE_DATE
          ,@TranCount = SUM(MeS.VMC_SALES_COUNT + MeS.VMC_CREDITS_COUNT)     
    FROM MeS.Revenue      AS MeS  
    WHERE YEAR(CONVERT (DATE, Active_Date)) = @Year
      AND MONTH(CONVERT (DATE, Active_Date)) = @Month
    GROUP BY MeS.ACTIVE_DATE 

--------------------------------
--          FOR TESTING
--    SET @TranCount = 6020123
--------------------------------


    SET @HoldTranCount = @TranCount
----------------------------------------------------------------------------
-- Level 1:  0 to 500,000           transactions are at the rate of 0.025
-- Level 2:  500,001 to 1,000,000   transactions are at the rate of 0.020
-- Level 3:  1,000,001 to 2,500,000 transactions are at the rate of 0.015
-- Level 4:  2,500,001 to 5,000,000 transactions are at the rate of 0.010
-- Level 5:  5,000,000 Plus         transactions are at the rate of 0.0085
----------------------------------------------------------------------------
--                 Calculate the Count of each Level 
----------------------------------------------------------------------------   
    IF @TranCount > 5000000
        BEGIN
            SET @Level_1_Count = 500000
            SET @Level_2_Count = 500000
            SET @Level_3_Count = 1500000
            SET @Level_4_Count = 2500000
            SET @Level_5_Count = @TranCount - 5000000
            SET @TranCount = 0
        END
    IF @TranCount > 2500000
        BEGIN    
            SET @Level_1_Count = 500000
            SET @Level_2_Count = 500000
            SET @Level_3_Count = 1500000
            SET @Level_4_Count = @TranCount - 2500000
            SET @Level_5_Count = 0
            SET @TranCount = 0
        END
    IF @TranCount > 1000000
        BEGIN    
            SET @Level_1_Count = 500000
            SET @Level_2_Count = 500000
            SET @Level_3_Count = @TranCount - 1000000
            SET @Level_4_Count = 0
            SET @Level_5_Count = 0
            SET @TranCount = 0
        END
    IF @TranCount > 500000
        BEGIN    
            SET @Level_1_Count = 500000
            SET @Level_2_Count = @TranCount - 500000
            SET @Level_3_Count = 0
            SET @Level_4_Count = 0
            SET @Level_5_Count = 0
            SET @TranCount = 0
        END
    IF @TranCount BETWEEN 1 and 500000
        BEGIN    
            SET @Level_1_Count = @TranCount
            SET @Level_2_Count = 0
            SET @Level_3_Count = 0
            SET @Level_4_Count = 0
            SET @Level_5_Count = 0
            SET @TranCount = 0        
        END

----------------------------------------------------------------------------
--                  Calculate the Charges for each Level
----------------------------------------------------------------------------         
    BEGIN
        SET @Level_1_Charge = @Level_1_Count * 0.025
        SET @Level_2_Charge = @Level_2_Count * 0.020
        SET @Level_3_Charge = @Level_3_Count * 0.015
        SET @Level_4_Charge = @Level_4_Count * 0.010
        SET @Level_5_Charge = @Level_5_Count * 0.0085
    END    


----------------------------------------------------------------------------
--                   Add up all of the Level Charges 
---------------------------------------------------------------------------- 
    SET @Total_Charge = @Level_1_Charge + @Level_2_Charge + @Level_3_Charge + @Level_4_Charge + @Level_5_Charge 

----------------------------------------------------------------------------
--                 Calculate the Per Transaction Rate
----------------------------------------------------------------------------     
    IF @HoldTranCount > 0
        BEGIN
            SET @PerTranRate = @Total_Charge / @HoldTranCount
        END
 
 
 
 
	IF @RunMode IS NULL
    SELECT @HoldTranCount            AS TranCount
          ,@Level_1_Count            AS Level_1_Count
          ,@Level_1_Charge           AS Level_1_Charge
          ,@Level_2_Count            AS Level_2_Count
          ,@Level_2_Charge           AS Level_2_Charge
          ,@Level_3_Count            AS Level_3_Count
          ,@Level_3_Charge           AS Level_3_Charge
          ,@Level_4_Count            AS Level_4_Count
          ,@Level_4_Charge           AS Level_4_Charge
          ,@Level_5_Count            AS Level_5_Count
          ,@Level_5_Charge           AS Level_5_Charge
          ,@Total_Charge             AS TotalCharges
          ,@PerTranRate              AS PerTranRate
	
	IF @RunMode IS NOT NULL
		--SELECT @PerTranRate
		SET @RunMode = @PerTranRate;

END

