﻿CREATE TABLE [Billing].[CPA_CustomerProcessingAccount] (
    [CPA_ID]                     INT              IDENTITY (1, 1) NOT NULL,
    [CPA_RunDateYYYYMM]          INT              NOT NULL,
    [CPA_CST_ID]                 INT              NOT NULL,
    [CPA_CRMAccountID]           UNIQUEIDENTIFIER NOT NULL,
    [CPA_CRMProcessingAccountID] UNIQUEIDENTIFIER NOT NULL,
    [CPA_ManualBillStartDate]    DATETIME         NULL,
    [CPA_ManualBillEndDate]      DATETIME         NULL,
    [CPA_ManualBillYN]           CHAR (1)         NULL,
    [CPA_TrustName]              NVARCHAR (100)   NULL,
    [CPA_ProductLine]            NVARCHAR (100)   NULL,
    [CPA_CRMRecordStatus]        NVARCHAR (255)   NULL,
    [CPA_BankAccountNumber]      NVARCHAR (20)    NULL,
    [CPA_ABANumber]              NVARCHAR (20)    NULL,
    [CPA_AccountNumberSuffix]    CHAR (5)         NULL,
    [CPA_DoNotBillStartDate]     DATETIME         NULL,
    [CPA_DoNotBillEndDate]       DATETIME         NULL,
    [CPA_DoNotBillYN]            CHAR (1)         NULL,
    [CPA_CreateDate]             SMALLDATETIME    CONSTRAINT [DF__CPA_Custo__CPA_C__124C06A0] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CPA_CustomerProcessingAccount] PRIMARY KEY CLUSTERED ([CPA_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CPA_CustomerProcessingAccount_CST_Customer] FOREIGN KEY ([CPA_CST_ID]) REFERENCES [Billing].[CST_Customer] ([CST_ID]) ON DELETE CASCADE
);

