﻿







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSalesACHMonthly]
(
@Name            NVARCHAR (250)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @vName          NVARCHAR (250)
	SET @vName = @Name

	        --------------------------------------------------------------------------
			-- Get a List of all Processing Profile GUIDs for the given Account Name
			--------------------------------------------------------------------------
			DECLARE @ListOfPPs TABLE
			(PP_Id uniqueidentifier NOT NULL,
			 TrustName VARCHAR (100),
			 MID CHAR (10) NOT NULL);

			INSERT INTO @ListOfPPs
			SELECT PP.bt_ProcessingProfileId   AS PP_Id
			      ,PA.bt_Name                  AS TrustName
			      ,CONCAT(PP.bt_BankMID,
	                      '-',
			              PP.bt_Config)        AS MID
			FROM [$(CRMShadow)].MS.bt_ProcessingAccount	  AS PA 
            JOIN [$(CRMShadow)].MS.bt_ProcessingProfile    AS PP  ON PA.bt_ProcessingAccountID = PP.bt_ProcessingAccount	
			WHERE PA.bt_AccountName = @vName
			  AND PA.bt_ProductLineName = 'ACH'
		

	        --------------------------------------------------------------------------
			-- Use the List of all Processing Profile GUIDs to populate Tran Activity
			--------------------------------------------------------------------------	
			DECLARE @TranActivity TABLE
			(YYYYMM INT NOT NULL,
			 YearMonth VARCHAR (50) NULL,
			 MID CHAR (10) NULL,
			 PP_Id uniqueidentifier NOT NULL,
			 TrustName VARCHAR (100),
			 Volume DECIMAL (11,2) NULL,
			 TranCount INT NULL);			
				
			INSERT INTO @TranActivity		
		    SELECT DateRange.YYYYMM                             AS YYYYMM
	              ,DateRange.YearMonth                          AS YearMonth
	              ,ListOfPPs.MID                                AS MID
				  ,ListOfPPs.PP_Id                              AS PP_Id
				  ,ListOfPPs.TrustName                          AS TrustName
	              ,SUM(ATITran.CheckAmount)                     AS Volume
	              ,CASE 
	                   WHEN SUM(ATITran.CheckAmount) IS NULL THEN
		                   0
		               ELSE
		                   COUNT(*)  
	               END		                                    AS TranCount	
	        FROM Billing.vYearMonthDateRangeFromToday    AS DateRange
			LEFT JOIN @ListOfPPs                         AS ListOfPPs  ON 1=1
            LEFT JOIN Billing.ACHTransByBatchDate        AS ATITran    ON ListOfPPs.PP_Id = ATITran.CRM_PPDID
                                                                      AND ATITran.BatchDateYYYYMM = DateRange.YYYYMM
											                          AND ISNULL(ATITran.Reference, ' ') NOT LIKE 'OFFSET%'


            GROUP BY DateRange.YYYYMM                                  
	                ,DateRange.YearMonth
                    ,ListOfPPs.MID
					,ListOfPPs.PP_Id
					,ListOfPPs.TrustName           


	        --------------------------------------------------------------------------
			-- Use the List of all Processing Profile GUIDs to populate Return Activity
			--------------------------------------------------------------------------	
			DECLARE @ReturnActivity TABLE
			(YYYYMM INT NOT NULL,
			 YearMonth VARCHAR (50) NULL,
			 MID CHAR (10) NULL,
			 PP_Id uniqueidentifier NOT NULL,
			 TrustName VARCHAR (100),
			 ReturnCount INT NULL);			
				
			INSERT INTO @ReturnActivity		
		    SELECT DateRange.YYYYMM                             AS YYYYMM
	              ,DateRange.YearMonth                          AS YearMonth
	              ,ListOfPPs.MID                                AS MID
				  ,ListOfPPs.PP_Id                              AS PP_Id
				  ,ListOfPPs.TrustName                          AS TrustName
	              ,CASE 
	                   WHEN SUM(ATIReturn.DebitAmount) IS NULL THEN
		                   0
		               ELSE
		                   COUNT(*) 
				   END                                          AS ReturnCount	
	        FROM Billing.vYearMonthDateRangeFromToday    AS DateRange
			LEFT JOIN @ListOfPPs                         AS ListOfPPs  ON 1=1
            LEFT JOIN Billing.ACHReturnsByReturnDate     AS ATIReturn  ON ListOfPPs.PP_Id = ATIReturn.CRM_PPDID
                                                                      AND ATIReturn.ReturnDateYYYYMM = DateRange.YYYYMM
											                          AND ATIReturn.ReturnCode IS NOT NULL
											                          AND ATIReturn.ReturnCode NOT LIKE 'C%'

            GROUP BY DateRange.YYYYMM                                  
	                ,DateRange.YearMonth
                    ,ListOfPPs.MID
					,ListOfPPs.PP_Id
					,ListOfPPs.TrustName           

	        --------------------------------------------------------------------------
			-- Use the List of all Processing Profile GUIDs to populate Batch Counts
			--------------------------------------------------------------------------	
			DECLARE @BatchActivity TABLE
			(YYYYMM INT NOT NULL,
			 YearMonth VARCHAR (50) NULL,
			 MID CHAR (10) NULL,
			 PP_Id uniqueidentifier NOT NULL,
			 TrustName VARCHAR (100),
			 BatchCount INT NULL);			
				
			INSERT INTO @BatchActivity		
		    SELECT DateRange.YYYYMM                             AS YYYYMM
	              ,DateRange.YearMonth                          AS YearMonth
	              ,ListOfPPs.MID                                AS MID
				  ,ListOfPPs.PP_Id                              AS PP_Id
				  ,ListOfPPs.TrustName                          AS TrustName
	              ,CASE 
	                   WHEN ATIBatch.CRM_PPDID IS NULL THEN
		                   0
		               ELSE
		                   COUNT(*) 
				   END                                          AS BatchCount	

	        FROM Billing.vYearMonthDateRangeFromToday    AS DateRange
			LEFT JOIN @ListOfPPs                         AS ListOfPPs  ON 1=1
            LEFT JOIN (
			           SELECT DISTINCT ATI.CRM_PPDID
					                  ,ATI.BatchDateYYYYMM
					                  ,ATI.BatchDate
									  ,ATI.BatchID
					   FROM Billing.ACHTransByBatchDate        AS ATI   
					   JOIN @ListOfPPs                         AS ListOfPPs ON ATI.CRM_PPDID = ListOfPPs.PP_Id 
					   WHERE ISNULL(ATI.Reference, ' ') NOT LIKE 'OFFSET%'
                      )                                  AS ATIBatch  ON ListOfPPs.PP_Id = ATIBatch.CRM_PPDID
                                                                      AND ATIBatch.BatchDateYYYYMM = DateRange.YYYYMM
            GROUP BY DateRange.YYYYMM                                  
	                ,DateRange.YearMonth
                    ,ListOfPPs.MID
					,ListOfPPs.PP_Id
					,ListOfPPs.TrustName          
					,ATIBatch.CRM_PPDID

	        ------------------------------------------------------------------------------
			-- Return the records; with Tran Counts/Volume, Batch Counts and Return Counts
			------------------------------------------------------------------------------	
			SELECT TA.YYYYMM
			      ,TA.YearMonth 
				  ,TA.MID
				  ,TA.TrustName
				  ,TA.Volume
				  ,TA.TranCount
				  ,BA.BatchCount
				  ,RA.ReturnCount
			FROM @TranActivity          AS TA 
			LEFT JOIN @BatchActivity    AS BA ON TA.YYYYMM = BA.YYYYMM
			                                   AND TA.PP_Id = BA.PP_Id
			LEFT JOIN @ReturnActivity   AS RA ON TA.YYYYMM = RA.YYYYMM
			                                   AND TA.PP_Id = RA.PP_Id



END

















