﻿




CREATE VIEW [dbo].[vRevenueUMSByYear]
AS

SELECT DISTINCT 
	   UMS.MID               AS MID
	  ,UMSLookup.SourceId    AS SourceId
	  ,UMS.Name              AS Name
	  ,UMSLookup.CRM_Id      AS CRM_ID
	  ,YEAR(UMS.TranDate)    AS TranDate
	  ,SUM(UMS.Revenue)      AS Revenue
	  ,'UMS'                 AS Source  
FROM dbo.RevenueUMS20082010      AS UMS
LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
WHERE UMS.Revenue <> 0  
GROUP BY UMS.MID
        ,UMSLookup.SourceId
        ,UMS.Name
        ,UMSLookup.CRM_Id
        ,YEAR(UMS.TranDate)
	  
UNION ALL

SELECT DISTINCT 
	   UMS.MID               AS MID
	  ,UMSLookup.SourceId    AS SourceId
	  ,UMS.Name              AS Name
	  ,UMSLookup.CRM_Id      AS CRM_ID
	  ,YEAR(UMS.TranDate)    AS TranDate
	  ,SUM(UMS.Revenue)      AS Revenue
	  ,'UMS'                 AS Source  
FROM dbo.RevenueUMS2011          AS UMS
LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
WHERE UMS.Revenue <> 0  
GROUP BY UMS.MID
        ,UMSLookup.SourceId
        ,UMS.Name
        ,UMSLookup.CRM_Id
        ,YEAR(UMS.TranDate)
	  
UNION ALL

SELECT DISTINCT 
	   UMS.MID               AS MID
	  ,UMSLookup.SourceId    AS SourceId
	  ,UMS.Name              AS Name
	  ,UMSLookup.CRM_Id      AS CRM_ID
	  ,YEAR(UMS.TranDate)    AS TranDate
	  ,SUM(UMS.Revenue)      AS Revenue
	  ,'UMS'                 AS Source  
FROM dbo.RevenueUMS2012          AS UMS
LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
WHERE UMS.Revenue <> 0  
GROUP BY UMS.MID
        ,UMSLookup.SourceId
        ,UMS.Name
        ,UMSLookup.CRM_Id
        ,YEAR(UMS.TranDate)

UNION ALL

SELECT DISTINCT 
	   UMS.MID               AS MID
	  ,UMSLookup.SourceId    AS SourceId
	  ,UMS.Name              AS Name
	  ,UMSLookup.CRM_Id      AS CRM_ID
	  ,YEAR(UMS.TranDate)    AS TranDate
	  ,SUM(UMS.Revenue)      AS Revenue
	  ,'UMS'                 AS Source  
FROM dbo.RevenueUMS2013          AS UMS
LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
WHERE UMS.Revenue <> 0  
GROUP BY UMS.MID
        ,UMSLookup.SourceId
        ,UMS.Name
        ,UMSLookup.CRM_Id
        ,YEAR(UMS.TranDate)




