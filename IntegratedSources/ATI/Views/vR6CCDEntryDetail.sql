﻿


CREATE VIEW [ATI].[vR6CCDEntryDetail] AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,ReturnCode
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,2) AS TransactionCode
		,SUBSTRING(SourceRecord,4,8) AS ReceivingDFIIdentification
		,SUBSTRING(SourceRecord,12,1) AS CheckDigit
		,SUBSTRING(SourceRecord,13,17) AS DFIAccountNumber
		,SUBSTRING(SourceRecord,30,10) AS Amount
		,SUBSTRING(SourceRecord,40,15) AS IdentificationNumber
		,SUBSTRING(SourceRecord,55,22) AS ReceivingCompanyName
		,SUBSTRING(SourceRecord,77,2) AS DiscretionaryData
		,SUBSTRING(SourceRecord,79,1) AS AddendaRecordIndicator
		,SUBSTRING(SourceRecord,80,15) AS TraceNumber
		,UsabilityIndex
		,MashupOutcome
FROM	ATI.ReturnFileData
WHERE	RecordType = 6
		AND EntryORAddendaType='CCD'





