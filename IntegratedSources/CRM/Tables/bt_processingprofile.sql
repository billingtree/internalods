﻿CREATE TABLE [CRM].[bt_processingprofile] (
    [bt_account]                 UNIQUEIDENTIFIER NULL,
    [bt_accountname]             NVARCHAR (160)   NULL,
    [bt_accountyominame]         NVARCHAR (160)   NULL,
    [bt_americanexpressmid]      NVARCHAR (16)    NULL,
    [bt_bank]                    INT              NULL,
    [bt_bankmid]                 NVARCHAR (50)    NULL,
    [bt_bankname]                NVARCHAR (255)   NULL,
    [bt_ccplatform]              INT              NULL,
    [bt_ccplatformname]          NVARCHAR (255)   NULL,
    [bt_config]                  NVARCHAR (100)   NULL,
    [bt_discovermid]             NVARCHAR (16)    NULL,
    [bt_enddate]                 DATETIME         NULL,
    [bt_gateway]                 INT              NULL,
    [bt_gatewaymid]              NVARCHAR (100)   NULL,
    [bt_gatewayname]             NVARCHAR (255)   NULL,
    [bt_gatewayonly]             BIT              NULL,
    [bt_gatewayonlyname]         NVARCHAR (255)   NULL,
    [bt_name]                    NVARCHAR (100)   NULL,
    [bt_processingaccount]       UNIQUEIDENTIFIER NULL,
    [bt_processingaccountname]   NVARCHAR (100)   NULL,
    [bt_processingprofileid]     UNIQUEIDENTIFIER NULL,
    [bt_productline]             UNIQUEIDENTIFIER NULL,
    [bt_productlinename]         NVARCHAR (100)   NULL,
    [bt_sourceid]                NVARCHAR (100)   NULL,
    [bt_startdate]               DATETIME         NULL,
    [bt_vcmcmerchant]            NVARCHAR (100)   NULL,
    [createdby]                  UNIQUEIDENTIFIER NULL,
    [createdbyname]              NVARCHAR (200)   NULL,
    [createdbyyominame]          NVARCHAR (200)   NULL,
    [createdon]                  DATETIME         NULL,
    [createdonbehalfby]          UNIQUEIDENTIFIER NULL,
    [createdonbehalfbyname]      NVARCHAR (200)   NULL,
    [createdonbehalfbyyominame]  NVARCHAR (200)   NULL,
    [importsequencenumber]       INT              NULL,
    [modifiedby]                 UNIQUEIDENTIFIER NULL,
    [modifiedbyname]             NVARCHAR (200)   NULL,
    [modifiedbyyominame]         NVARCHAR (200)   NULL,
    [modifiedon]                 DATETIME         NULL,
    [modifiedonbehalfby]         UNIQUEIDENTIFIER NULL,
    [modifiedonbehalfbyname]     NVARCHAR (200)   NULL,
    [modifiedonbehalfbyyominame] NVARCHAR (200)   NULL,
    [new_sf_id]                  NVARCHAR (18)    NULL,
    [overriddencreatedon]        DATETIME         NULL,
    [ownerid]                    UNIQUEIDENTIFIER NULL,
    [owneridname]                NVARCHAR (200)   NULL,
    [owneridtype]                NVARCHAR (64)    NULL,
    [owneridyominame]            NVARCHAR (200)   NULL,
    [owningbusinessunit]         UNIQUEIDENTIFIER NULL,
    [owningteam]                 UNIQUEIDENTIFIER NULL,
    [owninguser]                 UNIQUEIDENTIFIER NULL,
    [statecode]                  INT              NULL,
    [statecodename]              NVARCHAR (255)   NULL,
    [statuscode]                 INT              NULL,
    [statuscodename]             NVARCHAR (255)   NULL,
    [timezoneruleversionnumber]  INT              NULL,
    [utcconversiontimezonecode]  INT              NULL,
    [versionnumber]              BIGINT           NULL,
    [InsertDate]                 SMALLDATETIME    DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_bt_processingprofileid]
    ON [CRM].[bt_processingprofile]([bt_processingprofileid] ASC)
    INCLUDE([bt_bankmid], [bt_config], [bt_enddate], [bt_gatewayonly], [bt_processingaccount], [bt_productlinename], [bt_sourceid], [bt_startdate], [statuscodename]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_statuscodename_bt_processingaccount]
    ON [CRM].[bt_processingprofile]([statuscodename] ASC, [bt_processingaccount] ASC)
    INCLUDE([bt_bankmid], [bt_config], [bt_enddate], [bt_gatewayonly], [bt_processingprofileid], [bt_productlinename], [bt_sourceid], [bt_startdate]) WITH (FILLFACTOR = 80);

