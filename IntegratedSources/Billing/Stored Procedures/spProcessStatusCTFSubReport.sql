﻿












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessStatusCTFSubReport](
@PRLID          INT,
@IssueType      NVARCHAR (50)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vPRLID      INT
    DECLARE @vIssueType      NVARCHAR (50)
    SET @vPRLID = @PRLID   
    SET @vIssueType = RTRIM(@IssueType)

    -- Insert statements for procedure here

---------------------------------------------------------------------------------------
--            These 5 Issues are handled here
---------------------------------------------------------------------------------------
	--IF @vIssueType LIKE 'Fees Out of Range%'

		
		
		SELECT PSL.PSL_RunDateYYYYMM
			  ,PRT.PRT_Name
			  ,PSL.PSL_Status
			  ,PSL.PSL_InsertDateTime
			  ,PRL.PRL_StartDateTime
			  ,PRL.PRL_EndDateTime
			  ,PIT.PIT_Category
			  ,PIT.PIT_Description
			  ,PIT.PIT_Name
			  ,CPA.CPA_TrustName
			  ,CPP.CPP_MID
			  ,CPP.CPP_SourceIdConfig
			  ,CPP.CPP_SourceStep
			  ,CTF.CTF_ProductName
			  ,CTF.CTF_FeeType
			  ,CTF.CTF_FeeAmount
			  ,CTF.CTF_StartDate
			  ,CTF.CTF_EndDate
			  ,PIT.PIT_ID
			  ,CTF.CTF_CRMServiceProductFeeID
			  ,CASE 
			       WHEN PIA.PIA_ID IS NULL THEN 
			           'Set Issue Acceptance On'
			       ELSE
			           CONCAT(PIA.PIA_AcceptedValue
			                 ,' was Accepted by '
			                 ,PIA.PIA_UserName
			                 ,' On '
			                 ,CONVERT(DATE, PIA.PIA_InsertDateTime))
			   END                                                          AS IssueAcceptor        
		FROM Billing.PSL_ProcessStatusLog                AS PSL
		JOIN Billing.PRT_ProcessRunType                  AS PRT   ON PSL.PSL_PRT_ID = PRT.PRT_ID
		JOIN Billing.PRL_ProcessRunList                  AS PRL   ON PSL.PSL_ID = PRL.PRL_PSL_ID
		JOIN Billing.PIL_ProcessIssueList                AS PIL   ON PRL.PRL_ID = PIL.PIL_PRL_ID
		JOIN Billing.PIT_ProcessIssueType                AS PIT   ON PIL.PIL_PIT_ID = PIT.PIT_ID
		LEFT JOIN Billing.PED_ProcessErrorDetail         AS PED   ON PIL.PIL_ID = PED.PED_PIL_ID
		LEFT JOIN Billing.CTF_CustomerTranFee            AS CTF   ON PED.PED_CTF_ID = CTF.CTF_ID
		LEFT JOIN Billing.CPA_CustomerProcessingAccount  AS CPA   ON CTF.CTF_CPA_ID = CPA.CPA_ID
		LEFT JOIN Billing.CPP_CustomerProcessingProfile  AS CPP   ON CPA.CPA_ID = CPP.CPP_CPA_ID
		LEFT JOIN Billing.PIA_ProcessIssueAcceptance     AS PIA   ON PIT.PIT_ID = PIA.PIA_PIT_ID
		                                                         AND CTF.CTF_CRMServiceProductFeeID = PIA.PIA_CTF_CRMServiceProductFeeID 
		WHERE PRL.PRL_ID = @vPRLID 
		  AND PIT.PIT_Name = @vIssueType
  
    
    
    
		
END























