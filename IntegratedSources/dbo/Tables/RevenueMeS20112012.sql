﻿CREATE TABLE [dbo].[RevenueMeS20112012] (
    [TranDateYYYYMM] NVARCHAR (255) NULL,
    [MID]            NVARCHAR (255) NULL,
    [Name]           NVARCHAR (255) NULL,
    [Volume]         FLOAT (53)     NULL,
    [Revenue]        FLOAT (53)     NULL,
    [TranDate]       DATETIME       NULL,
    [RevenueBackup]  FLOAT (53)     NULL
);

