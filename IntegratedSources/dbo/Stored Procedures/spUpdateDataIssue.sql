﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateDataIssue]
(
 @IssueNumber    INT
,@DataIssue      NVARCHAR (2000)
,@IssueStatus    NVARCHAR (50)
,@UpdatedBy      NVARCHAR (255)
,@IssueComment   NVARCHAR (2000)
,@IssueOwner     NVARCHAR (50)
,@ReportYesNo    CHAR (1)
,@ReportName     NVARCHAR (2000)
,@IssuePriority  NVARCHAR (255)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

    DECLARE @DIL_Key    INT
    DECLARE @Issue      NVARCHAR (2000)
    DECLARE @Who        NVARCHAR (255)
    DECLARE @Status     NVARCHAR (50)
    DECLARE @Comment    NVARCHAR (2000)
    DECLARE @Owner      NVARCHAR (50)
    DECLARE @ReportYN   CHAR (1)
    DECLARE @ReportLocationName    NVARCHAR (2000)
    DECLARE @Priority   NVARCHAR (255)
	 
	SET @DIL_Key = @IssueNumber       
	SET @Issue = @DataIssue
	SET @Who = @UpdatedBy
    SET @Status = @IssueStatus
    SET @Comment = @IssueComment
    SET @Owner = @IssueOwner
    SET @ReportYN = @ReportYesNo
    SET @ReportLocationName = @ReportName
    SET @Priority = @IssuePriority

	--SELECT @Date = CreateDate
	--FROM dbo.RatesIgnoreAccount  
	--WHERE AccountNumber = @AcctNbr

    
		--IF @Date IS NULL  
		    BEGIN       
				BEGIN TRANSACTION
					UPDATE CRM.DIL_DataIssueLog  
					SET DIL_DataIssue = @Issue
					   ,DIL_ModifiedBy = @Who
					   ,DIL_Status = @Status
					   ,DIL_ModifiedOn = CONVERT(DATE, GetDate())
					   ,DIL_IssueOwner = @Owner
					   ,DIL_Comment = @Comment
					   ,DIL_ReportYN = @ReportYN
					   ,DIL_ReportLocationName = @ReportLocationName
					   ,DIL_Priority = @Priority
					WHERE DIL_ID = @DIL_Key  
				COMMIT  
            END
	   -- ELSE
	   --     BEGIN
		--		BEGIN TRANSACTION
		--			DELETE dbo.RatesIgnoreAccount 
		--			WHERE AccountNumber = @AcctNbr                   
		--		COMMIT  		  
		--		SET @AcctNbr = 'Removed'  
	    --    END		


		SELECT @DIL_Key                   AS DIL_ID
		      ,@Priority                  AS DIL_Priority
		      ,@Issue                     AS DIL_DataIssue
		      ,@ReportYN                  AS DIL_ReportYN
			  ,@Status                    AS DIL_Status
			  ,@Owner                     AS DIL_IssueOwner
			  ,NULL                       AS DIL_EstimatedClosedDate
			  ,@Comment                   AS DIL_Comment
			  ,NULL                       AS DIL_ClosedOn
			  ,''                         AS DIL_ClosedBy
			  ,NULL                       AS DIL_CreatedOn
			  ,''                         AS DIL_CreatedBy
			  ,CONVERT(DATE, GetDate())   AS DIL_ModifiedOn
			  ,@Who                       AS DIL_ModifiedBy
			  ,@ReportLocationName        AS DIL_ReportLocationName
			  
			  

END
















