﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spInStepATIMonthlyReport]	
(@Month1 int
,@Year1  int
,@Type VARCHAR(1)
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Month int, @Year int, @vType VARCHAR(1)
	SET @Month=@Month1
	SET @Year=@Year1
	SET @vType=@Type
	
    DECLARE @YearMonth                 INT
    DECLARE @txtYear                   VARCHAR (4)
    DECLARE @txtMonth                  VARCHAR (2)
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))  	
	
	
  BEGIN
    IF @vType = 'O'   --   Get Originations
	------------------------------------------------------------------------------
	--   This is ATI Monthly Originations, Excluding the OFFSET Records         --                                   --
	------------------------------------------------------------------------------
	SELECT	ATI.ConfigNumber                      AS ConfigNbr
			,ATI.ProcessingNumber                 AS ProcessingNbr
			,sfa.Id                               AS AcctID
			,sfa.Name                             AS Child
			,sfa.Existing_Clients_Profile__c      AS Parent
		    ,SUM(ATI.FormattedAmount)             AS TotalVolume
			,COUNT(*)                             AS TotalCount
			,'ATI'                                AS Source
			,CONCAT(@Month,'-',@Year)             AS 'MM-YYYY'
			,@Month                               AS MM
			,@Year                                AS YYYY
			,''                                   AS CorCount
	        ,''                                   AS OtherCount
	        ,''                                   AS UnAuthCount
	FROM	  NACHA.Originations           AS ATI  
	LEFT JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
	JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE ATI.FileCreationDateYYYYMM = @YearMonth
	  AND ATI.UsabilityIndex = 1
	  AND ATI.SourceType = 4
	  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	GROUP BY ATI.ConfigNumber
	    	,ATI.ProcessingNumber 
			,sfa.Id 
			,sfa.Name 
			,sfa.Existing_Clients_Profile__c
			
			
    UNION ALL
    
    
	------------------------------------------------------------------------------
	--   This is InStep Monthly Originations, Excluding the OFFSET Records      --                                   --
	------------------------------------------------------------------------------
	SELECT	InStep.ConfigNumber                   AS ConfigNbr
			,InStep.ProcessingNumber              AS ProcessingNbr
			,sfa.Id                               AS AcctID
			,sfa.Name                             AS Child
			,sfa.Existing_Clients_Profile__c      AS Parent
			,SUM(InStep.FormattedAmount)          AS TotalVolume
			,COUNT(*)                             AS TotalCount
			,'InStep'                             AS Source
			,CONCAT(@Month,'-',@Year)             AS 'MM-YYYY'
			,@Month                               AS MM
			,@Year                                AS YYYY
			,''                                   AS CorCount
	        ,''                                   AS OtherCount
	        ,''                                   AS UnAuthCount		
    FROM	  NACHA.Originations           AS InStep
	LEFT JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
	JOIN      SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	
	WHERE InStep.FileCreationDateYYYYMM = @YearMonth
	  AND InStep.UsabilityIndex = 1
	  AND InStep.SourceType = 1
      AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
	
	GROUP BY InStep.ConfigNumber
			 ,InStep.ProcessingNumber
			 ,sfa.Id 
			 ,sfa.Name 
			 ,sfa.Existing_Clients_Profile__c	
		
		
		
			
    ELSE  
    
    
    
---------------------------------------------------------------------------------       
--                                  Get the Returns                            --
---------------------------------------------------------------------------------
	                  --------------------------------------------------
	                  --   This is ATI Monthly Returns                --                                   
	                  --------------------------------------------------
	SELECT AllReturns.ConfigNbr                  AS ConfigNbr
		   ,AllReturns.ProcessingNbr             AS ProcessingNbr
		   ,AllReturns.AcctId                    AS AcctID
		   ,AllReturns.Child                     AS Child
		   ,AllReturns.Parent                    AS Parent
           ,(ISNULL(CorReturns.Volume,0)+ISNULL(OtherReturns.Volume,0)+
             ISNULL(UnAuthReturns.Volume,0))     AS TotalVolume
           ,(ISNULL(CorReturns.Count,0)+ISNULL(OtherReturns.Count,0)+
             ISNULL(UnAuthReturns.Count,0))      AS TotalCount
		   ,'ATI'                                AS Source
		   ,CONCAT(@Month,'-',@Year)             AS 'MM-YYYY'
		   ,@Month                               AS MM
		   ,@Year                                AS YYYY
	       ,CorReturns.Count                     AS CorCount
	       ,OtherReturns.Count                   AS OtherCount
	       ,UnAuthReturns.Count                  AS UnAuthCount
    FROM
                   ----------------------------------------------------------
                   --   Distinct Merchants who had Returns for the Month   --
                   ----------------------------------------------------------
    (	 
	    SELECT DISTINCT
	            ATIreturn.ConfigNumber               AS ConfigNbr
		    	,ATIreturn.ProcessingNumber          AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent

	    FROM	  NACHA.Returns                  AS ATIreturn   				
		LEFT JOIN SalesForce.PaymentProcessing   AS sfp
				                        ON ATIreturn.CRM_PPDID = sfp.Id
		JOIN      SalesForce.Account             AS sfa
					                    ON sfp.Account__c = sfa.Id
	    WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
	      AND ATIreturn.UsabilityIndex = 1
	      AND ATIreturn.SourceType = 4
     )                                           AllReturns          
			                                  
LEFT JOIN
                   -------------------------------------------
                   --   Just SEC Code = COR for the Month   --
                   -------------------------------------------
	  (  SELECT ATIreturn.ConfigNumber               AS ConfigNbr
		    	,ATIreturn.ProcessingNumber          AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(ATIreturn.FormattedAmount)      AS Volume
			    ,COUNT(*)                            AS Count

	    FROM	  NACHA.Returns                    AS ATIreturn
	    LEFT JOIN SalesForce.PaymentProcessing     AS sfp
				                     ON ATIreturn.CRM_PPDID = sfp.Id
		JOIN      SalesForce.Account               AS sfa
					                 ON sfp.Account__c = sfa.Id
	    WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
	      AND ATIreturn.UsabilityIndex = 1
		  AND ATIreturn.SourceType = 4
		  AND UPPER(ATIreturn.StandardEntryClassCode) = 'COR'
			
	    GROUP BY ATIreturn.ConfigNumber
			     ,ATIreturn.ProcessingNumber
			     ,sfa.Id 
			     ,sfa.Name 
			     ,sfa.Existing_Clients_Profile__c ) CorReturns    			                                  
			                                  ON AllReturns.AcctID = CorReturns.AcctID
			                                  AND AllReturns.ConfigNbr = CorReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = CorReturns.ProcessingNbr
LEFT JOIN
                   -------------------------------------------
                   --  UnAuthorized Returns for the Month   --
                   -------------------------------------------
	  (  SELECT ATIreturn.ConfigNumber               AS ConfigNbr
		    	,ATIreturn.ProcessingNumber          AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(ATIreturn.FormattedAmount)      AS Volume
			    ,COUNT(*)                            AS Count

	    FROM	  NACHA.Returns                 AS ATIreturn
		LEFT JOIN SalesForce.PaymentProcessing  AS sfp
				                     ON ATIreturn.CRM_PPDID = sfp.Id		
		JOIN      SalesForce.Account            AS sfa
				                     ON sfp.Account__c = sfa.Id
	    WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
	        AND ATIreturn.UsabilityIndex = 1
			AND ATIreturn.SourceType = 4
			AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
			AND ATIreturn.ReturnCode IN ('R05','R07','R10','R29','R51')
	    GROUP BY ATIreturn.ConfigNumber 
			     ,ATIreturn.ProcessingNumber
			     ,sfa.Id 
			     ,sfa.Name 
			     ,sfa.Existing_Clients_Profile__c ) UnAuthReturns    			                                  
			                                  ON AllReturns.AcctID = UnAuthReturns.AcctID
			                                  AND AllReturns.ConfigNbr = UnAuthReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = UnAuthReturns.ProcessingNbr

LEFT JOIN
                   -------------------------------------------
                   --    All Other Returns for the Month    --
                   -------------------------------------------
	  (  SELECT ATIreturn.ConfigNumber               AS ConfigNbr
		    	,ATIreturn.ProcessingNumber          AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(ATIreturn.FormattedAmount)      AS Volume
			    ,COUNT(*)                            AS Count
	     FROM	   NACHA.Returns                AS ATIreturn
         LEFT JOIN SalesForce.PaymentProcessing AS sfp
				                        ON ATIreturn.CRM_PPDID = sfp.Id			
         JOIN      SalesForce.Account           AS sfa
					                    ON sfp.Account__c = sfa.Id
	     WHERE ATIreturn.FileCreationDateYYYYMM = @YearMonth
	         AND ATIreturn.UsabilityIndex = 1
		  	 AND ATIreturn.SourceType = 4
			 AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
			 AND ATIreturn.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
	     GROUP BY ATIreturn.ConfigNumber 
			     ,ATIreturn.ProcessingNumber
			     ,sfa.Id 
			     ,sfa.Name 
			     ,sfa.Existing_Clients_Profile__c ) OtherReturns    			                                  
			                                  ON AllReturns.AcctID = OtherReturns.AcctID
			                                  AND AllReturns.ConfigNbr = OtherReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = OtherReturns.ProcessingNbr


UNION ALL


 	                  -----------------------------------------------------
	                  --   This is InStep Monthly Returns                --                                   
	                  -----------------------------------------------------
	SELECT AllReturns.ConfigNbr                  AS ConfigNbr
		   ,AllReturns.ProcessingNbr             AS ProcessingNbr
		   ,AllReturns.AcctId                    AS AcctID
		   ,AllReturns.Child                     AS Child
		   ,AllReturns.Parent                    AS Parent
           ,(ISNULL(CorReturns.Volume,0)+ISNULL(OtherReturns.Volume,0)+
             ISNULL(UnAuthReturns.Volume,0))     AS TotalVolume
           ,(ISNULL(CorReturns.Count,0)+ISNULL(OtherReturns.Count,0)+
             ISNULL(UnAuthReturns.Count,0))      AS TotalCount
		   ,'InStep'                             AS Source
		   ,CONCAT(@Month,'-',@Year)             AS 'MM-YYYY'
		   ,@Month                               AS MM
		   ,@Year                                AS YYYY
	       ,CorReturns.Count                     AS CorCount
	       ,OtherReturns.Count                   AS OtherCount
	       ,UnAuthReturns.Count                  AS UnAuthCount
    FROM
                   ----------------------------------------------------------
                   --   Distinct Merchants who had Returns for the Month   --
                   ----------------------------------------------------------
    (	 
	    SELECT DISTINCT
	            InStepreturn.ConfigNumber            AS ConfigNbr
		    	,InStepreturn.ProcessingNumber       AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent

	    FROM	  NACHA.Returns                 AS InStepreturn
		LEFT JOIN SalesForce.PaymentProcessing  AS sfp
				                      ON InStepreturn.CRM_PPDID = sfp.Id			
		JOIN      SalesForce.Account            AS sfa
					                  ON sfp.Account__c = sfa.Id
	    WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
	        AND InStepreturn.UsabilityIndex = 1
			AND InStepreturn.SourceType = 1			
			
     )                                           AllReturns          
			                                  
LEFT JOIN
                   -------------------------------------------
                   --   Just SEC Code = COR for the Month   --
                   -------------------------------------------
	  (  SELECT InStepreturn.ConfigNumber            AS ConfigNbr
		    	,InStepreturn.ProcessingNumber       AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(InStepreturn.FormattedAmount)   AS Volume
			    ,COUNT(*)                            AS Count

	    FROM      NACHA.Returns                 AS InStepreturn
		LEFT JOIN SalesForce.PaymentProcessing  AS sfp
				                        ON InStepreturn.CRM_PPDID = sfp.Id		
		JOIN      SalesForce.Account            AS sfa
					                   ON sfp.Account__c = sfa.Id
	    WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
	        AND InStepreturn.UsabilityIndex = 1
			AND InStepreturn.SourceType = 1	
			AND UPPER(InStepreturn.StandardEntryClassCode) = 'COR'
	    GROUP BY InStepreturn.ConfigNumber
			     ,InStepreturn.ProcessingNumber
			     ,sfa.Id 
			     ,sfa.Name 
			     ,sfa.Existing_Clients_Profile__c ) CorReturns    			                                  
			                                  ON AllReturns.AcctID = CorReturns.AcctID
			                                  AND AllReturns.ConfigNbr = CorReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = CorReturns.ProcessingNbr
LEFT JOIN
                   -------------------------------------------
                   --  UnAuthorized Returns for the Month   --
                   -------------------------------------------
	  (  SELECT InStepreturn.ConfigNumber            AS ConfigNbr
		    	,InStepreturn.ProcessingNumber       AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(InStepreturn.FormattedAmount)   AS Volume
			    ,COUNT(*)                            AS Count

	    FROM	  NACHA.Returns                   AS InStepreturn
		LEFT JOIN SalesForce.PaymentProcessing    AS sfp
				                       ON InStepreturn.CRM_PPDID = sfp.Id
		JOIN      SalesForce.Account              AS sfa
				                       ON sfp.Account__c = sfa.Id
	    WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
	        AND InStepreturn.UsabilityIndex = 1
			AND InStepreturn.SourceType = 1	
			AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
			AND InStepreturn.ReturnCode IN ('R05','R07','R10','R29','R51')
	    GROUP BY InStepreturn.ConfigNumber
			     ,InStepreturn.ProcessingNumber
			     ,sfa.Id
			     ,sfa.Name 
			     ,sfa.Existing_Clients_Profile__c ) UnAuthReturns    			                                  
			                                  ON AllReturns.AcctID = UnAuthReturns.AcctID
			                                  AND AllReturns.ConfigNbr = UnAuthReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = UnAuthReturns.ProcessingNbr

LEFT JOIN
                   -------------------------------------------
                   --    All Other Returns for the Month    --
                   -------------------------------------------
	  (  SELECT InStepreturn.ConfigNumber            AS ConfigNbr
		    	,InStepreturn.ProcessingNumber       AS ProcessingNbr
		    	,sfa.Id                              AS AcctID
			    ,sfa.Name                            AS Child
			    ,sfa.Existing_Clients_Profile__c     AS Parent
			    ,SUM(InStepreturn.FormattedAmount)   AS Volume
			    ,COUNT(*)                            AS Count

	    FROM	  NACHA.Returns                 AS InStepreturn
		LEFT JOIN SalesForce.PaymentProcessing  AS sfp
				                      ON InStepreturn.CRM_PPDID = sfp.Id
        JOIN      SalesForce.Account            AS sfa
				                      ON sfp.Account__c = sfa.Id
	    WHERE InStepreturn.FileCreationDateYYYYMM = @YearMonth
	        AND InStepreturn.UsabilityIndex = 1
			AND InStepreturn.SourceType = 1	
			AND UPPER(InStepreturn.StandardEntryClassCode) <> 'COR'
			AND InStepreturn.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
	    GROUP BY InStepreturn.ConfigNumber
			    ,InStepreturn.ProcessingNumber 
			    ,sfa.Id
			    ,sfa.Name
			    ,sfa.Existing_Clients_Profile__c ) OtherReturns    			                                  
			                                  ON AllReturns.AcctID = OtherReturns.AcctID
			                                  AND AllReturns.ConfigNbr = OtherReturns.ConfigNbr
			                                  AND AllReturns.ProcessingNbr = OtherReturns.ProcessingNbr
 

 
    
  END    			





END
