﻿CREATE TABLE [Global].[FileData] (
    [FileName]       NVARCHAR (50)  NOT NULL,
    [LineNumber]     INT            NOT NULL,
    [SourceRecord]   NVARCHAR (309) NULL,
    [UsabilityIndex] SMALLINT       NULL,
    [MashupOutcome]  TINYINT        NULL,
    [InsertDate]     DATETIME       CONSTRAINT [DF_FileData_InsertDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Analyze] PRIMARY KEY CLUSTERED ([FileName] ASC, [LineNumber] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [UIX_UsabilityIndex]
    ON [Global].[FileData]([UsabilityIndex] ASC) WITH (FILLFACTOR = 80);

