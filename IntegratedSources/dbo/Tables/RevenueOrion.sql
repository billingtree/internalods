﻿CREATE TABLE [dbo].[RevenueOrion] (
    [TranDate]       DATETIME       NULL,
    [TranDateYYYYMM] NVARCHAR (255) NULL,
    [MID]            NVARCHAR (255) NULL,
    [Name]           NVARCHAR (255) NULL,
    [Volume]         MONEY          NULL,
    [Revenue]        MONEY          NULL
);

