﻿CREATE TABLE [dbo].[ATIMonthlyVolumeCount] (
    [YearMonth]        INT           NULL,
    [ConfigNumber]     TINYINT       NULL,
    [ProcessingNumber] NVARCHAR (8)  NULL,
    [Volume]           MONEY         NULL,
    [Count]            INT           NULL,
    [InsertDate]       SMALLDATETIME CONSTRAINT [DF__ATIMonthl__Inser__01C9240F] DEFAULT (getdate()) NULL
);

