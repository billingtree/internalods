﻿CREATE TABLE [ATI].[OriginationFileData] (
    [ID]                  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileReferenceCodeID] INT           NOT NULL,
    [ConfigNumber]        TINYINT       NOT NULL,
    [LineNumber]          INT           NOT NULL,
    [BlockNumber]         SMALLINT      NOT NULL,
    [RecordType]          TINYINT       NOT NULL,
    [EntryORAddendaType]  NCHAR (3)     NULL,
    [IsCCDOffset]         BIT           NULL,
    [SourceRecord]        NVARCHAR (94) NOT NULL,
    [UsabilityIndex]      SMALLINT      NULL,
    [MashupOutcome]       TINYINT       NULL,
    [InsertDate]          DATETIME      CONSTRAINT [DF_OriginationFileData_InsertDate] DEFAULT (getdate()) NOT NULL,
    [CRM_PPDID]           NVARCHAR (36) NULL,
    [ProcessingNumber]    NVARCHAR (8)  NULL,
    CONSTRAINT [PK_FileData] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_FileData_FileDetails] FOREIGN KEY ([FileReferenceCodeID]) REFERENCES [ATI].[OriginationFileDetails] ([ReferenceCode]) ON DELETE CASCADE,
    CONSTRAINT [FK_OriginationFileData_PaymentProcessing] FOREIGN KEY ([CRM_PPDID]) REFERENCES [SalesForce].[PaymentProcessing] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordType]
    ON [ATI].[OriginationFileData]([RecordType] ASC)
    INCLUDE([FileReferenceCodeID], [BlockNumber], [SourceRecord]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordType_EntryORAddendaType_UsabilityIndex]
    ON [ATI].[OriginationFileData]([RecordType] ASC, [EntryORAddendaType] ASC, [UsabilityIndex] ASC)
    INCLUDE([FileReferenceCodeID], [BlockNumber], [SourceRecord], [MashupOutcome]) WITH (FILLFACTOR = 80);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI_DuplicateCheck]
    ON [ATI].[OriginationFileData]([FileReferenceCodeID] ASC, [LineNumber] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordTypeUsabilityIndexMashupOutcome]
    ON [ATI].[OriginationFileData]([RecordType] ASC, [UsabilityIndex] ASC, [MashupOutcome] ASC)
    INCLUDE([FileReferenceCodeID], [BlockNumber], [CRM_PPDID], [ProcessingNumber]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_FileReferenceCodeIDBlockNumber]
    ON [ATI].[OriginationFileData]([FileReferenceCodeID] ASC, [BlockNumber] ASC)
    INCLUDE([ID], [RecordType], [UsabilityIndex], [MashupOutcome], [CRM_PPDID], [ProcessingNumber]) WITH (FILLFACTOR = 80);

