﻿CREATE TABLE [MeS].[ChargebackAdjustments] (
    [ID]                    INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReportReferenceCode]   INT            NOT NULL,
    [MerchantID]            NVARCHAR (20)  NOT NULL,
    [DBAName]               NVARCHAR (35)  NULL,
    [ControlNumber]         VARCHAR (10)   NULL,
    [IncomingDate]          DATE           NULL,
    [CardNumber]            VARCHAR (16)   NULL,
    [ReferenceNumber]       VARCHAR (30)   NULL,
    [TransactionDate]       DATE           NOT NULL,
    [TransactionAmount]     MONEY          NOT NULL,
    [TridentTransactionID]  NVARCHAR (35)  NULL,
    [PurchaseID]            NVARCHAR (50)  NULL,
    [ClientReferenceNumber] VARCHAR (30)   NULL,
    [AuthorizationCode]     VARCHAR (6)    NULL,
    [AdjustmentDate]        VARCHAR (30)   NULL,
    [AdjustmentRefereneNum] NVARCHAR (15)  NULL,
    [Reason]                NVARCHAR (100) NULL,
    [FirstTime]             NVARCHAR (50)  NULL,
    [ReasonCode]            VARCHAR (5)    NULL,
    [CBReferenceNumber]     VARCHAR (15)   NULL,
    [TerminalID]            NVARCHAR (20)  NULL,
    [TransDateYYYYMM]       INT            NOT NULL,
    [CRM_PPDID]             NVARCHAR (36)  NULL,
    [UsabilityIndex]        SMALLINT       NULL,
    [MashupOutcome]         TINYINT        NULL,
    [InsertDate]            DATETIME       CONSTRAINT [DF_MeSChargebackAdjustments_InsertDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ChargebackAdjustments] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NCI_UsabilityIndex]
    ON [MeS].[ChargebackAdjustments]([UsabilityIndex] ASC)
    INCLUDE([ID], [MerchantID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_InsertDate_MeSChargeback]
    ON [MeS].[ChargebackAdjustments]([InsertDate] ASC) WITH (FILLFACTOR = 80);

