﻿

-- =============================================
-- Author:		Yaseen Jamaludeen
-- Create date: 2015-06-01
-- Description:	Procedure to carry out Consolidated Billing Workflow
-- WARNING : This is to be used in the SSIS main workflow. Do not execute independently.
-- =============================================
CREATE PROCEDURE [Billing].[spATI] --'2016-06-25'
(
	@RunDate date = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	IF @RunDate IS NOT NULL
		BEGIN
			DECLARE @RunDateYearMonth int = LEFT(REPLACE(CONVERT(char(10),@RunDate),'-',''),6)
			DECLARE @RunDateYear smallint = YEAR(@RunDate)
			DECLARE @RunDateMonth tinyint = MONTH(@RunDate)
			DECLARE @RunDateMonthBegin date = DATEADD(D,1,EOMONTH(@RunDate,-1))
			DECLARE @RunDateMonthEnd date = EOMONTH(@RunDate)

			DELETE Billing.CDT_CustomerDailyTran WHERE CDT_PRT_ID =3 AND CDT_RunDateYYYYMM = @RunDateYearMonth
			INSERT INTO Billing.CDT_CustomerDailyTran
					(CDT_CPP_ID
					,CDT_RunDateYYYYMM
					,CDT_TranDate
					,CDT_TranType
					,CDT_TranAmount
					,CDT_TranCount
					,CDT_PRT_ID)
			SELECT	CPP_ID		AS CDT_CPP_ID
					,@RunDateYearMonth		AS CDT_RunDateYYYYMM
					,TranDate	AS CDT_TranDate
					,TranType	AS CDT_TranType
					,TranAmount	AS CDT_TranAmount
					,TranCount	AS CDT_TranCount
					,3			AS CDT_PRT_ID
			FROM	(	------------------------------------------------
								   --Tran Type Origination 8070
						----------------------------------------------
						SELECT	O.ConfigNumber
								,O.AccountID			AS ProcessingNumber
								,O.AccountName
								,O.BatchDate			AS TranDate
								,'Origination'			AS TranType
								,SUM(O.CheckAmount)	AS TranAmount
								,COUNT(*)				AS TranCount
								,CPP.CPP_ID
						FROM	Billing.ACHTransByBatchDate AS O
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
										AND	ISNULL(UPPER(O.Reference),'') NOT LIKE '%OFFSET%'
										AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
										AND O.BatchDateYYYYMM = @RunDateYearMonth
						GROUP BY O.ConfigNumber
								,O.AccountID
								,O.AccountName
								,O.BatchDate
								,CPP_ID
						--SELECT	O.ConfigNumber
						--		,O.ProcessingNumber
						--		,O.CompanyName
						--		,O.FileCreationDate		AS TranDate
						--		,'Origination'			AS TranType
						--		,SUM(O.FormattedAmount)	AS TranAmount
						--		,COUNT(*)				AS TranCount
						--		,CPP.CPP_ID
						--FROM	NACHA.Originations AS O
						--			INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
						--				ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
						--				AND	UPPER(O.IdentificationNumber) NOT LIKE '%OFFSET%'
						--				AND O.SourceType = 4
						--				AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						--				AND O.FileCreationDateYYYYMM = @RunDateYearMonth
						--GROUP BY O.ConfigNumber
						--		,O.ProcessingNumber
						--		,O.CompanyName
						--		,O.FileCreationDate
						--		,CPP_ID
						--ORDER BY O.ConfigNumber
						--		,O.ProcessingNumber
						--		,O.CompanyName
						--		,O.FileCreationDate
					) AS O
			--ORDER BY CPP_ID
					--,CDT_TranDate


			UNION ALL

			SELECT	CPP_ID		AS CDT_CPP_ID
					,@RunDateYearMonth		AS CDT_RunDateYYYYMM
					,TranDate	AS CDT_TranDate
					,'Batch'	AS CDT_TranType
					,0			AS CDT_TranAmount
					,COUNT(*)	AS CDT_TranCount
					,3			AS CDT_PRT_ID
			FROM	(	------------------------------------------------
						--           Tran Type Batch
						------------------------------------------------
						SELECT	O.ConfigNumber
								,O.AccountID	AS ProcessingNumber-- , O.CompanyName
								,CPP.CPP_ID
								,O.FileName
								,O.BatchID
								,O.BatchDate	AS TranDate
						FROM	Billing.ACHTransByBatchDate AS O
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
										AND	O.BatchDateYYYYMM = @RunDateYearMonth
										AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
										AND ISNULL(UPPER(O.Reference),'') NOT LIKE '%OFFSET%'
						GROUP BY O.ConfigNumber
								,O.AccountID--, O.CompanyName
								,CPP.CPP_ID
								,O.FileName
								,O.BatchID
								,O.BatchDate
						--SELECT	O.ConfigNumber
						--		,O.ProcessingNumber-- , O.CompanyName
						--		,CPP.CPP_ID
						--		,O.FileReferenceCodeID
						--		,O.BatchNumber
						--		,O.FileCreationDate		AS TranDate
						--FROM	NACHA.Originations AS O
						--			INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
						--				ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
						--				AND	O.FileCreationDateYYYYMM = @RunDateYearMonth
						--				AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						--				AND UPPER(O.IdentificationNumber) NOT LIKE '%OFFSET%'
						--				AND O.SourceType = 4
						--				--AND O.CompanyName like '%collection Bur%'
						--GROUP BY O.ConfigNumber
						--		,O.ProcessingNumber--, O.CompanyName
						--		,CPP.CPP_ID
						--		,O.FileReferenceCodeID
						--		,O.BatchNumber
						--		,O.FileCreationDate
						--ORDER BY O.ConfigNumber
						--		,O.ProcessingNumber--, O.CompanyName
						--		--,O.CRM_PPDID
						--		,O.FileReferenceCodeID
						--		,O.BatchNumber
						--		,O.FileCreationDate
					) AS O
			GROUP BY CPP_ID
					,TranDate

			UNION ALL

			SELECT	CPP_ID		AS CDT_CPP_ID
					,@RunDateYearMonth		AS CDT_RunDateYYYYMM
					,TranDate	AS CDT_TranDate
					,TranType	AS CDT_TranType
					,TranAmount	AS CDT_TranAmount
					,TranCount	AS CDT_TranCount
					,3			AS CDT_PRT_ID
			FROM	(	------------------------------------------------
						--           Tran Type Return
						------------------------------------------------
						SELECT	R.ConfigNumber
								,R.AccountID		AS ProcessingNumber
								,R.ConsumerLastName	AS CompanyName
								,R.ReturnDate		AS TranDate
								,'Return'			AS TranType
								,SUM(R.DebitAmount)	AS TranAmount
								,COUNT(*)			AS TranCount
								,CPP_ID
						FROM	Billing.ACHReturnsByReturnDate AS R
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON R.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
										AND R.ReturnDateYYYYMM = @RunDateYearMonth
										AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
										AND R.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
										AND R.ReturnCode NOT LIKE 'C%'
						GROUP BY R.ConfigNumber
								,R.AccountID
								,R.ConsumerLastName
								,R.ReturnDate
								,CPP_ID
						--SELECT	R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate		AS TranDate
						--		,'Return'				AS TranType
						--		,SUM(R.FormattedAmount)	AS TranAmount
						--		,COUNT(*)				AS TranCount
						--		,CPP_ID
						--FROM	NACHA.Returns AS R
						--			INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
						--				ON R.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
						--				AND R.FileCreationDateYYYYMM = @RunDateYearMonth
						--				AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						--				AND UPPER(R.StandardEntryClassCode) <> 'COR'
						--				AND R.SourceType = 4
						--				AND R.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
						--GROUP BY R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate
						--		,CPP_ID
						--ORDER BY R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate
					) AS R

			UNION ALL

			SELECT	CPP_ID		AS CDT_CPP_ID
					,@RunDateYearMonth		AS CDT_RunDateYYYYMM
					,TranDate	AS CDT_TranDate
					,TranType	AS CDT_TranType
					,TranAmount	AS CDT_TranAmount
					,TranCount	AS CDT_TranCount
					,3			AS CDT_PRT_ID
			FROM	(	------------------------------------------------
						--           Tran Type UnAuth Return
						------------------------------------------------
						SELECT	R.ConfigNumber
								,R.AccountID		AS ProcessingNumber
								,R.ConsumerLastName
								,R.ReturnDate		AS TranDate
								,'UnAuth'			AS TranType
								,SUM(R.DebitAmount)	AS TranAmount
								,COUNT(*)			AS TranCount
								,CPP_ID
						FROM	Billing.ACHReturnsByReturnDate AS R
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON R.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
										AND	R.ReturnDateYYYYMM = @RunDateYearMonth
										AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
										AND R.ReturnCode IN ('R05','R07','R10','R29','R51')
						GROUP BY R.ConfigNumber
								,R.AccountID
								,R.ConsumerLastName
								,R.ReturnDate
								,CPP_ID
						--SELECT	R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate		AS TranDate
						--		,'UnAuth'				AS TranType
						--		,SUM(R.FormattedAmount)	AS TranAmount
						--		,COUNT(*)				AS TranCount
						--		,CPP_ID
						--FROM	NACHA.Returns  AS R
						--			INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
						--				ON R.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
						--				AND	R.FileCreationDateYYYYMM = @RunDateYearMonth
						--				AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						--				AND UPPER(R.StandardEntryClassCode) <> 'COR'
						--				AND R.SourceType = 4
						--				AND R.ReturnCode IN ('R05','R07','R10','R29','R51')
						--GROUP BY R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate
						--		,CPP_ID
						--ORDER BY R.ConfigNumber
						--		,R.ProcessingNumber
						--		,R.CompanyName
						--		,R.FileCreationDate
					) AS R
			-- Disabled to close "User Story 9892 Issue Log - Remove NACHA Returns from Batch Count"
			--UNION ALL

			--SELECT	CPP_ID		AS CDT_CPP_ID
			--		,@RunDateYearMonth		AS CDT_RunDateYYYYMM
			--		,TranDate	AS CDT_TranDate
			--		,'Batch'	AS CDT_TranType
			--		,0			AS CDT_TranAmount
			--		,COUNT(*)	AS CDT_TranCount
			--		,3			AS CDT_PRT_ID
			--FROM	(	------------------------------------------------
			--			--           Tran Type Batch Return
			--			------------------------------------------------
			--			SELECT	O.ConfigNumber
			--					,O.ProcessingNumber-- , O.CompanyName
			--					,CPP.CPP_ID
			--					,O.FileReferenceCodeID
			--					,O.BatchNumber
			--					,O.FileCreationDate		AS TranDate
			--			FROM	NACHA.Returns AS O
			--						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
			--							ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
			--							AND	O.FileCreationDateYYYYMM = @RunDateYearMonth
			--							AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
			--							AND UPPER(O.IdentificationNumber) NOT LIKE '%OFFSET%'
			--							AND O.SourceType = 4
			--							--AND O.CompanyName like '%collection Bur%'
			--			GROUP BY O.ConfigNumber
			--					,O.ProcessingNumber--, O.CompanyName
			--					,CPP.CPP_ID
			--					,O.FileReferenceCodeID
			--					,O.BatchNumber
			--					,O.FileCreationDate
			--			--ORDER BY O.ConfigNumber
			--			--		,O.ProcessingNumber--, O.CompanyName
			--			--		--,O.CRM_PPDID
			--			--		,O.FileReferenceCodeID
			--			--		,O.BatchNumber
			--			--		,O.FileCreationDate
			--		) AS R
			--GROUP BY CPP_ID
			--		,TranDate
			--ORDER BY CPP_ID
			--		,CDT_TranDate


			UPDATE	CDT
			SET		CDT.CDT_CTF_ID = CTF.CTF_ID
			--SELECT	CDT.CDT_CTF_ID 
			--		,CTF.CTF_ID
			FROM	Billing.CDT_CustomerDailyTran AS CDT
						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
							ON CDT.CDT_CPP_ID = CPP.CPP_ID
							AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						INNER JOIN Billing.CTF_CustomerTranFee AS CTF
							ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
							AND CDT.CDT_TranType = 'Origination'
							AND CTF.CTF_FeeType IN ('BillingTree Gateway Submission Fee')
							AND CDT.CDT_PRT_ID = 3
							AND CDT.CDT_CTF_ID IS NULL
							AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
							AND CDT.CDT_RunDateYYYYMM = @RunDateYearMonth
							AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth


			UPDATE	CDT
			SET		CDT.CDT_CTF_ID = CTF.CTF_ID
			--SELECT	CDT.CDT_CTF_ID 
			--		,CTF.CTF_ID
			FROM	Billing.CDT_CustomerDailyTran AS CDT
						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
							ON CDT.CDT_CPP_ID = CPP.CPP_ID
							AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						INNER JOIN Billing.CTF_CustomerTranFee AS CTF
							ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
							AND CDT.CDT_TranType = 'Batch'
							AND CTF.CTF_FeeType IN ('Per Batch Fee/Offset')
							AND CDT.CDT_PRT_ID = 3
							AND CDT.CDT_CTF_ID IS NULL
							AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
							AND CDT.CDT_RunDateYYYYMM = @RunDateYearMonth
							AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth

			UPDATE	CDT
			SET		CDT.CDT_CTF_ID = CTF.CTF_ID
			--SELECT	CDT.CDT_CTF_ID 
			--		,CTF.CTF_ID
			FROM	Billing.CDT_CustomerDailyTran AS CDT
						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
							ON CDT.CDT_CPP_ID = CPP.CPP_ID
							AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						INNER JOIN Billing.CTF_CustomerTranFee AS CTF
							ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
							AND CDT.CDT_TranType = 'Return'
							AND CTF.CTF_FeeType IN ('Return Fee')
							AND CDT.CDT_PRT_ID = 3
							AND CDT.CDT_CTF_ID IS NULL
							AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
							AND CDT.CDT_RunDateYYYYMM = @RunDateYearMonth
							AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth

			UPDATE	CDT
			SET		CDT.CDT_CTF_ID = CTF.CTF_ID
			--SELECT	CDT.CDT_CTF_ID 
			--		,CTF.CTF_ID
			FROM	Billing.CDT_CustomerDailyTran AS CDT
						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
							ON CDT.CDT_CPP_ID = CPP.CPP_ID
							AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
						INNER JOIN Billing.CTF_CustomerTranFee AS CTF
							ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
							AND CDT.CDT_TranType = 'UnAuth'
							AND CTF.CTF_FeeType IN ('Un-Authorized Return Fee')
							AND CDT.CDT_PRT_ID = 3
							AND CDT.CDT_CTF_ID IS NULL
							AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
							AND CDT.CDT_RunDateYYYYMM = @RunDateYearMonth
							AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth

			--SELECT distinct [CTF_ProductName]
			--      ,[CTF_FeeType]
			--  FROM [IntegratedSourcesGLTest].[Billing].[CTF_CustomerTranFee]
			--  where CTF_ProductName not like '%Credit%' and CTF_ProductName not like '%CC%'
			--  and CTF_FeeType='BillingTree Monthly Fee'
			--  order by 1,2

			--select * from Billing.CPA_CustomerProcessingAccount
			--SELECT *
			--  FROM [IntegratedSourcesGLTest].[Billing].[CTF_CustomerTranFee]
			--  where CTF_ProductName not like '%Credit%' and CTF_ProductName not like '%CC%'
			--  order by 1,2


			--UPDATE	CDT
			--SET		CDT.CDT_CTF_ID = CTF.CTF_ID
			----SELECT	CDT.CDT_CTF_ID 
			----		,CTF.CTF_ID
			--FROM	Billing.CDT_CustomerDailyTran AS CDT
			--			INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
			--				ON CDT.CDT_CPP_ID = CPP.CPP_ID
			--			INNER JOIN Billing.CTF_CustomerTranFee AS CTF
			--				ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
			--				AND CTF.CTF_FeeType IN ('BillingTree Gateway Submission Fee','Per Batch Fee/Offset')
			--				AND CTF.CTF_ProductName ='BillingTree ACH'
			--				AND CDT.CDT_PRT_ID = 3
			--				AND CDT.CDT_CTF_ID IS NULL
			--				AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
			--				AND CDT.CDT_RunDateYYYYMM = @RunDateYearMonth


			/*

			------------------------------------------------
			--           Tran Type Origination 8070
			------------------------------------------------
			SELECT	O.ConfigNumber
					,O.ProcessingNumber
					,O.CompanyName
					,O.FileCreationDate		AS TranDate
					,'Origination'			AS TranType
					,SUM(O.FormattedAmount)	AS TranAmount
					,COUNT(*)				AS TranCount
					,CPP.CPP_ID
			FROM	NACHA.Originations AS O
						INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
							ON O.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
			WHERE	UPPER(O.IdentificationNumber) NOT LIKE '%OFFSET%'
					AND O.SourceType = 4
					AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
					AND O.FileCreationDateYYYYMM = @RunDateYearMonth
			GROUP BY O.ConfigNumber
					,O.ProcessingNumber
					,O.CompanyName
					,O.FileCreationDate
					,CPP_ID
			ORDER BY O.ConfigNumber
					,O.ProcessingNumber
					,O.CompanyName
					,O.FileCreationDate

			------------------------------------------------
			--           Tran Type Batch
			------------------------------------------------
			SELECT	O.ConfigNumber
					,O.ProcessingNumber --, O.CompanyName
					,O.CRM_PPDID
					,O.FileReferenceCodeID
					,O.BatchNumber
					,O.FileCreationDate		AS TranDate
					,'Batch'				AS TranType
					,0						AS TranAmount
					,COUNT(*)				AS TranCount
			FROM	NACHA.Originations AS O
			WHERE	O.FileCreationDateYYYYMM = @RunDateYearMonth
					AND UPPER(O.IdentificationNumber) NOT LIKE '%OFFSET%'
					AND O.SourceType = 4
			GROUP BY O.ConfigNumber
					,O.ProcessingNumber
					,O.CRM_PPDID
					,O.FileReferenceCodeID
					,O.BatchNumber
					,O.FileCreationDate
			ORDER BY O.ConfigNumber
					,O.ProcessingNumber
					,O.CRM_PPDID
					,O.FileReferenceCodeID
					,O.BatchNumber
					,O.FileCreationDate

			*/

			UPDATE	Billing.PSL_ProcessStatusLog 
			SET		PSL_Status = 'Finished'
			WHERE	PSL_PRT_ID = 3
					AND PSL_RunDateYYYYMM = @RunDateYearMonth

			DECLARE @PRLID AS int
			SELECT	@PRLID = MAX(R.PRL_ID)
			FROM	Billing.PRL_ProcessRunList AS R
						INNER JOIN Billing.PSL_ProcessStatusLog AS S
							ON R.PRL_PSL_ID = S.PSL_ID
							AND S.PSL_PRT_ID = 3
							AND S.PSL_RunDateYYYYMM = @RunDateYearMonth

			UPDATE	Billing.PRL_ProcessRunList
			SET		PRL_EndDateTime = GETDATE()
			WHERE	PRL_ID = @PRLID
		END
END








