﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spPostSetupEstimateFulfillmentReport](
@Month INT,
@Year  INT
) WITH RECOMPILE

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @txtYear         VARCHAR (4)
    DECLARE @txtMonth        VARCHAR (2)
    DECLARE @InputDate       DATE
    DECLARE @InputDatePlus1  DATE
    DECLARE @InputDatePlus2  DATE
    DECLARE @InputDatePlus3  DATE
    DECLARE @InputDatePlus4  DATE
    DECLARE @InputDatePlus5  DATE
    
    DECLARE @InputDateYYYYMM       INT
    DECLARE @InputDatePlus1YYYYMM  INT
    DECLARE @InputDatePlus2YYYYMM  INT
    DECLARE @InputDatePlus3YYYYMM  INT
    DECLARE @InputDatePlus4YYYYMM  INT
    DECLARE @InputDatePlus5YYYYMM  INT    
        
    --  Convert the input parameters Year and Month to a Date  
    SET @txtYear = @Year 
    SET @txtMonth = @Month
    IF @Month < 10 
        SET @InputDate = CONVERT(DATE,
                         @txtYear + CONCAT('0', CONVERT(VARCHAR(1), @Month)) + '01')
    ELSE
        SET @InputDate = CONVERT(DATE,
                         @txtYear + @txtMonth + '01')
                         
    --  Add 1 month to the calculated Input Date from above  
    SET @InputDatePlus1 = DATEADD(MONTH, 1, @InputDate)
    
    --  Add 2 months to the calculated Input Date from above  
    SET @InputDatePlus2 = DATEADD(MONTH, 2, @InputDate)
    
    --  Add 3 month to the calculated Input Date from above  
    SET @InputDatePlus3 = DATEADD(MONTH, 3, @InputDate)
    
    --  Add 4 months to the calculated Input Date from above  
    SET @InputDatePlus4 = DATEADD(MONTH, 4, @InputDate)
    
    --  Add 5 month to the calculated Input Date from above  
    SET @InputDatePlus5 = DATEADD(MONTH, 5, @InputDate)
   
    --  Change Dates to YYYYMM Integer format
    SET @InputDateYYYYMM = dbo.fnDateToYYYYMM(@InputDate) 
    SET @InputDatePlus1YYYYMM = dbo.fnDateToYYYYMM(@InputDatePlus1)
    SET @InputDatePlus2YYYYMM = dbo.fnDateToYYYYMM(@InputDatePlus2)
    SET @InputDatePlus3YYYYMM = dbo.fnDateToYYYYMM(@InputDatePlus3)
    SET @InputDatePlus4YYYYMM = dbo.fnDateToYYYYMM(@InputDatePlus4)
    SET @InputDatePlus5YYYYMM = dbo.fnDateToYYYYMM(@InputDatePlus5) 


    
SELECT 'CC'                              AS "Source"
       ,M.MerchantID                     AS "MerchantID"
       ,M.SourceID                       AS "SourceConfig"
       ,M.SetupDate                      AS "SetupDate"
       ,M.MultipleSetupDates             AS "MultipleSetupdates"
       ,M.Parent                         AS "Parent"
       ,M.Child                          AS "Child"
       ,M.Stage                          AS "AppStage"
       ,M.EstMonthVolume                 AS "EstimatedVolume"
       ,M.EstMonthCount                  AS "EstimatedCount"
       ,Month1.Volume                    AS "1stMonthVolume"
       ,Month1.Count                     AS "1stMonthCount"
       ,Month2.Volume                    AS "2ndMonthVolume"
       ,Month2.Count                     AS "2ndMonthCount"
       ,Month3.Volume                    AS "3rdMonthVolume"
       ,Month3.Count                     AS "3rdMonthCount"
       ,Month4.Volume                    AS "4thMonthVolume"
       ,Month4.Count                     AS "4thMonthCount"    
       ,Month5.Volume                    AS "5thMonthVolume"
       ,Month5.Count                     AS "5thMonthCount"
       ,Month6.Volume                    AS "6thMonthVolume"
       ,Month6.Count                     AS "6thMonthCount"              
FROM vCRMSetupDateByMerchantID   M             WITH (NOLOCK)
LEFT JOIN
         (SELECT USA1.CRM_PPDID                        AS "CRM_ID"
                ,USA1.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA1.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA1           WITH (NOLOCK)
          WHERE USA1.TransType = 'S'
            AND USA1.Result = 'A'
            AND USA1.TransDateYYYYMM = @InputDateYYYYMM
            AND USA1.UsabilityIndex = 1
          GROUP BY USA1.CRM_PPDID
                  ,USA1.TransDateYYYYMM
         ) Month1                  ON Month1.CRM_ID = M.CRM_ID
LEFT JOIN
         (SELECT USA2.CRM_PPDID                        AS "CRM_ID"
                ,USA2.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA2.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA2           WITH (NOLOCK)
          WHERE USA2.TransType = 'S'
            AND USA2.Result = 'A'
            AND USA2.TransDateYYYYMM = @InputDatePlus1YYYYMM
            AND USA2.UsabilityIndex = 1
          GROUP BY USA2.CRM_PPDID
                  ,USA2.TransDateYYYYMM
         ) Month2                  ON Month2.CRM_ID = M.CRM_ID

LEFT JOIN
         (SELECT USA3.CRM_PPDID                        AS "CRM_ID"
                ,USA3.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA3.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA3           WITH (NOLOCK)
          WHERE USA3.TransType = 'S'
            AND USA3.Result = 'A'
            AND USA3.TransDateYYYYMM = @InputDatePlus2YYYYMM
            AND USA3.UsabilityIndex = 1
          GROUP BY USA3.CRM_PPDID
                  ,USA3.TransDateYYYYMM
         ) Month3                  ON Month3.CRM_ID = M.CRM_ID
LEFT JOIN
         (SELECT USA4.CRM_PPDID                        AS "CRM_ID"
                ,USA4.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA4.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA4           WITH (NOLOCK)
          WHERE USA4.TransType = 'S'
            AND USA4.Result = 'A'
            AND USA4.TransDateYYYYMM = @InputDatePlus3YYYYMM
            AND USA4.UsabilityIndex = 1
          GROUP BY USA4.CRM_PPDID
                  ,USA4.TransDateYYYYMM
         ) Month4                  ON Month4.CRM_ID = M.CRM_ID        
LEFT JOIN
         (SELECT USA5.CRM_PPDID                        AS "CRM_ID"
                ,USA5.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA5.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA5           WITH (NOLOCK)
          WHERE USA5.TransType = 'S'
            AND USA5.Result = 'A'
            AND USA5.TransDateYYYYMM = @InputDatePlus4YYYYMM
            AND USA5.UsabilityIndex = 1
          GROUP BY USA5.CRM_PPDID
                  ,USA5.TransDateYYYYMM
         ) Month5                  ON Month5.CRM_ID = M.CRM_ID    
LEFT JOIN
         (SELECT USA6.CRM_PPDID                        AS "CRM_ID"
                ,USA6.TransDateYYYYMM                  AS "TranYYYYMM"
                ,SUM(USA6.Amount)                      AS "Volume"
                ,COUNT(*)                              AS "Count"
          FROM USAePay.FileData USA6           WITH (NOLOCK)
          WHERE USA6.TransType = 'S'
            AND USA6.Result = 'A'
            AND USA6.TransDateYYYYMM = @InputDatePlus5YYYYMM
            AND USA6.UsabilityIndex = 1
          GROUP BY USA6.CRM_PPDID
                  ,USA6.TransDateYYYYMM
         ) Month6                  ON Month6.CRM_ID = M.CRM_ID  

WHERE YEAR(M.SetupDate) = YEAR(@InputDate)
  AND MONTH(M.SetupDate) = MONTH(@InputDate)

UNION ALL

SELECT 'ACH'                             AS "Source"
       ,ACH.ProcessingNbr                AS "MerchantID"
       ,ACH.ConfigNbr                    AS "SourceConfig"
       ,ACH.SetupDate                    AS "SetupDate"
       ,ACH.MultipleSetupDates           AS "MultipleSetupdates"
       ,ACH.Parent                       AS "Parent"
       ,ACH.Child                        AS "Child"
       ,ACH.Stage                        AS "AppStage"
       ,ACH.EstMonthVolume               AS "EstimatedVolume"
       ,ACH.EstMonthCount                AS "EstimatedCount"
       ,Month1.Volume                    AS "1stMonthVolume"
       ,Month1.Count                     AS "1stMonthCount"
       ,Month2.Volume                    AS "2ndMonthVolume"
       ,Month2.Count                     AS "2ndMonthCount"
       ,Month3.Volume                    AS "3rdMonthVolume"
       ,Month3.Count                     AS "3rdMonthCount"
       ,Month4.Volume                    AS "4thMonthVolume"
       ,Month4.Count                     AS "4thMonthCount"    
       ,Month5.Volume                    AS "5thMonthVolume"
       ,Month5.Count                     AS "5thMonthCount"
       ,Month6.Volume                    AS "6thMonthVolume"
       ,Month6.Count                     AS "6thMonthCount"              
FROM vCRMSetupDateByConfigProcessing   ACH           WITH (NOLOCK)
LEFT JOIN
         (SELECT ATI1.CRM_PPDID                       AS "CRM_ID"
                ,ATI1.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI1.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI1              WITH (NOLOCK)
          WHERE ATI1.FileCreationDateYYYYMM = @InputDateYYYYMM
            AND ATI1.UsabilityIndex = 1
            AND ATI1.SourceType = 4
            AND UPPER(ATI1.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI1.CRM_PPDID
                  ,ATI1.FileCreationDateYYYYMM
         ) Month1                  ON Month1.CRM_ID = ACH.CRM_ID
LEFT JOIN
         (SELECT ATI2.CRM_PPDID                       AS "CRM_ID"
                ,ATI2.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI2.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI2              WITH (NOLOCK)
          WHERE ATI2.FileCreationDateYYYYMM = @InputDatePlus1YYYYMM
            AND ATI2.UsabilityIndex = 1
            AND ATI2.SourceType = 4
            AND UPPER(ATI2.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI2.CRM_PPDID
                  ,ATI2.FileCreationDateYYYYMM
         ) Month2                  ON Month2.CRM_ID = ACH.CRM_ID

LEFT JOIN
         (SELECT ATI3.CRM_PPDID                       AS "CRM_ID"
                ,ATI3.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI3.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI3              WITH (NOLOCK)
          WHERE ATI3.FileCreationDateYYYYMM = @InputDatePlus2YYYYMM
            AND ATI3.UsabilityIndex = 1
            AND ATI3.SourceType = 4
            AND UPPER(ATI3.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI3.CRM_PPDID
                  ,ATI3.FileCreationDateYYYYMM
         ) Month3                  ON Month3.CRM_ID = ACH.CRM_ID
LEFT JOIN
         (SELECT ATI4.CRM_PPDID                       AS "CRM_ID"
                ,ATI4.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI4.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI4              WITH (NOLOCK)
          WHERE ATI4.FileCreationDateYYYYMM = @InputDatePlus3YYYYMM
            AND ATI4.UsabilityIndex = 1
            AND ATI4.SourceType = 4
            AND UPPER(ATI4.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI4.CRM_PPDID
                  ,ATI4.FileCreationDateYYYYMM
         ) Month4                  ON Month4.CRM_ID = ACH.CRM_ID        
LEFT JOIN
         (SELECT ATI5.CRM_PPDID                       AS "CRM_ID"
                ,ATI5.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI5.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI5              WITH (NOLOCK)
          WHERE ATI5.FileCreationDateYYYYMM = @InputDatePlus4YYYYMM
            AND ATI5.UsabilityIndex = 1
            AND ATI5.SourceType = 4
            AND UPPER(ATI5.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI5.CRM_PPDID
                  ,ATI5.FileCreationDateYYYYMM
         ) Month5                  ON Month5.CRM_ID = ACH.CRM_ID    
LEFT JOIN
         (SELECT ATI6.CRM_PPDID                       AS "CRM_ID"
                ,ATI6.FileCreationDateYYYYMM          AS "TranYYYYMM"
                ,SUM(ATI6.FormattedAmount)            AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   ATI6              WITH (NOLOCK)
          WHERE ATI6.FileCreationDateYYYYMM = @InputDatePlus5YYYYMM
            AND ATI6.UsabilityIndex = 1
            AND ATI6.SourceType = 4
            AND UPPER(ATI6.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI6.CRM_PPDID
                  ,ATI6.FileCreationDateYYYYMM
         ) Month6                  ON Month6.CRM_ID = ACH.CRM_ID  

WHERE YEAR(ACH.SetupDate) = YEAR(@InputDate)
  AND MONTH(ACH.SetupDate) = MONTH(@InputDate)   
  AND UPPER(ACH.Platform) like '%ACH%'

UNION ALL

SELECT 'ACH-InStep'                      AS "Source"
       ,ACH2.ProcessingNbr               AS "MerchantID"
       ,ACH2.ConfigNbr                   AS "SourceConfig"
       ,ACH2.SetupDate                   AS "SetupDate"
       ,ACH2.MultipleSetupDates          AS "MultipleSetupdates"
       ,ACH2.Parent                      AS "Parent"
       ,ACH2.Child                       AS "Child"
       ,ACH2.Stage                       AS "AppStage"
       ,ACH2.EstMonthVolume              AS "EstimatedVolume"
       ,ACH2.EstMonthCount               AS "EstimatedCount"
       ,Month1.Volume                    AS "1stMonthVolume"
       ,Month1.Count                     AS "1stMonthCount"
       ,Month2.Volume                    AS "2ndMonthVolume"
       ,Month2.Count                     AS "2ndMonthCount"
       ,Month3.Volume                    AS "3rdMonthVolume"
       ,Month3.Count                     AS "3rdMonthCount"
       ,Month4.Volume                    AS "4thMonthVolume"
       ,Month4.Count                     AS "4thMonthCount"    
       ,Month5.Volume                    AS "5thMonthVolume"
       ,Month5.Count                     AS "5thMonthCount"
       ,Month6.Volume                    AS "6thMonthVolume"
       ,Month6.Count                     AS "6thMonthCount"              
FROM vCRMSetupDateByConfigProcessing   ACH2          WITH (NOLOCK)
LEFT JOIN
         (SELECT InStep1.CRM_PPDID                    AS "CRM_ID"
                ,InStep1.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep1.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep1          WITH (NOLOCK)
          WHERE InStep1.FileCreationDateYYYYMM = @InputDateYYYYMM
            AND InStep1.UsabilityIndex = 1
            AND InStep1.SourceType = 1
            AND UPPER(InStep1.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep1.CRM_PPDID
                  ,InStep1.FileCreationDateYYYYMM
         ) Month1                  ON Month1.CRM_ID = ACH2.CRM_ID
LEFT JOIN
         (SELECT InStep2.CRM_PPDID                    AS "CRM_ID"
                ,InStep2.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep2.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep2          WITH (NOLOCK)
          WHERE InStep2.FileCreationDateYYYYMM = @InputDatePlus1YYYYMM
            AND InStep2.UsabilityIndex = 1
            AND InStep2.SourceType = 1
            AND UPPER(InStep2.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep2.CRM_PPDID
                  ,InStep2.FileCreationDateYYYYMM
         ) Month2                  ON Month2.CRM_ID = ACH2.CRM_ID

LEFT JOIN
         (SELECT InStep3.CRM_PPDID                    AS "CRM_ID"
                ,InStep3.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep3.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep3          WITH (NOLOCK)
          WHERE InStep3.FileCreationDateYYYYMM = @InputDatePlus2YYYYMM
            AND InStep3.UsabilityIndex = 1
            AND InStep3.SourceType = 1
            AND UPPER(InStep3.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep3.CRM_PPDID
                  ,InStep3.FileCreationDateYYYYMM
         ) Month3                  ON Month3.CRM_ID = ACH2.CRM_ID
LEFT JOIN
         (SELECT InStep4.CRM_PPDID                    AS "CRM_ID"
                ,InStep4.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep4.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep4          WITH (NOLOCK)
          WHERE InStep4.FileCreationDateYYYYMM = @InputDatePlus3YYYYMM
            AND InStep4.UsabilityIndex = 1
            AND InStep4.SourceType = 1
            AND UPPER(InStep4.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep4.CRM_PPDID
                  ,InStep4.FileCreationDateYYYYMM
         ) Month4                  ON Month4.CRM_ID = ACH2.CRM_ID        
LEFT JOIN
         (SELECT InStep5.CRM_PPDID                    AS "CRM_ID"
                ,InStep5.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep5.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep5          WITH (NOLOCK)
          WHERE InStep5.FileCreationDateYYYYMM = @InputDatePlus4YYYYMM
            AND InStep5.UsabilityIndex = 1
            AND InStep5.SourceType = 1
            AND UPPER(InStep5.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep5.CRM_PPDID
                  ,InStep5.FileCreationDateYYYYMM
         ) Month5                  ON Month5.CRM_ID = ACH2.CRM_ID    
LEFT JOIN
         (SELECT InStep6.CRM_PPDID                    AS "CRM_ID"
                ,InStep6.FileCreationDateYYYYMM       AS "TranYYYYMM"
                ,SUM(InStep6.FormattedAmount)         AS "Volume"
                ,COUNT(*)                             AS "Count"
          FROM NACHA.Originations   InStep6          WITH (NOLOCK)
          WHERE InStep6.FileCreationDateYYYYMM = @InputDatePlus5YYYYMM
            AND InStep6.UsabilityIndex = 1
            AND InStep6.SourceType = 1
            AND UPPER(InStep6.ReceivingCompanyName) <> 'BILLINGTREE'
          GROUP BY InStep6.CRM_PPDID
                  ,InStep6.FileCreationDateYYYYMM
         ) Month6                  ON Month6.CRM_ID = ACH2.CRM_ID  

WHERE YEAR(ACH2.SetupDate) = YEAR(@InputDate)
  AND MONTH(ACH2.SetupDate) = MONTH(@InputDate)  
  AND UPPER(ACH2.Platform) like '%INSTEP%'




END


