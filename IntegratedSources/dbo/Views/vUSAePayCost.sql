﻿


CREATE VIEW [dbo].[vUSAePayCost]
AS
SELECT C.MerchantNum                                            AS MerchantNum
      ,C.Company                                                AS Company
      ,C.MainGLSegment                                          AS MainGLSegment
      ,ROW_NUMBER() OVER(PARTITION BY C.MerchantNum
                                     ,C.ResellerCost
                                     ,C.Company
                         ORDER BY SFP.Application_Stage__c ASC) AS RecCounter
      ,SFP.CC_Merchant_Number__c                                AS CRM_PPDID   
      ,SFP.CC_Source_ID__c                                      AS SourceId
      ,CASE ROW_NUMBER() OVER(PARTITION BY C.MerchantNum
                                          ,C.ResellerCost
                                          ,C.Company
                         ORDER BY SFP.Application_Stage__c ASC) 
           WHEN 1 THEN 
               C.ResellerCost
           ELSE
               0
       END                                                      AS ResellerCost   
      ,SFP.Application_Stage__c                                 AS ApplicationStage   
      ,LEFT(C.YearMonth, 4)                                     AS TranYear
      ,RIGHT(C.YearMonth, 2)                                    AS TranMonth         
FROM USAePay.Cost                         AS C
LEFT JOIN SalesForce.PaymentProcessing    AS SFP ON (C.MerchantNum = SFP.CC_Merchant_Number__c
                                                               OR
                                                     RIGHT(C.MerchantNum, 8) = RIGHT(SFP.CC_Merchant_Number__c, 8)
                                                               OR
                                                     LEFT(C.MerchantNum, 13) = LEFT(SFP.CC_Merchant_Number__c, 13)) 
                                                     AND SFP.Application_Stage__c IN ('Complete'
                                                                                     ,'Training'
                                                                                     ,'QA Post-Assessment'
                                                                                     ,'Inactive'
                                                                                     ,'QA & Training')

                                                       AND SFP.IsDeleted = 0
LEFT JOIN SalesForce.Account             AS A    ON SFP.Account__c = A.Id
                                                 AND A.IsDeleted = 0   
                                                 AND A.Type = 'Customer'                                                
WHERE ISNUMERIC(C.MerchantNum) = 1
   AND C.ResellerCost > 0





