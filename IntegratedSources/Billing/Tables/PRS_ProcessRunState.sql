﻿CREATE TABLE [Billing].[PRS_ProcessRunState] (
    [PRS_ID]             INT           IDENTITY (1, 1) NOT NULL,
    [PRS_PSL_ID]         INT           NOT NULL,
    [PRS_Name]           NVARCHAR (50) NOT NULL,
    [PRS_InsertDateTime] DATETIME      CONSTRAINT [DF_PRS_ProcessRunState_PRS_InsertDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PRS_ProcessRunState] PRIMARY KEY CLUSTERED ([PRS_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PRS_ProcessRunState_PSL_ProcessStatusLog] FOREIGN KEY ([PRS_PSL_ID]) REFERENCES [Billing].[PSL_ProcessStatusLog] ([PSL_ID])
);

