﻿CREATE TABLE [Billing].[ACHTransByBatchDate] (
    [RowID]           INT            IDENTITY (1, 1) NOT NULL,
    [FileName]        NVARCHAR (255) NOT NULL,
    [ConfigNumber]    TINYINT        NOT NULL,
    [TranID]          BIGINT         NOT NULL,
    [TransactionType] VARCHAR (250)  NULL,
    [RoutingTransit]  VARCHAR (9)    NOT NULL,
    [AccountNumber]   VARCHAR (25)   NOT NULL,
    [CheckType]       VARCHAR (25)   NULL,
    [CheckAmount]     MONEY          NOT NULL,
    [EffectiveDate]   DATE           NOT NULL,
    [CheckNumber]     VARCHAR (22)   NULL,
    [Reference]       VARCHAR (50)   NULL,
    [AccountName]     VARCHAR (35)   NOT NULL,
    [Address1]        VARCHAR (35)   NULL,
    [Address2]        VARCHAR (30)   NULL,
    [City]            VARCHAR (30)   NULL,
    [State]           VARCHAR (4)    NULL,
    [Country]         VARCHAR (24)   NULL,
    [Zip]             VARCHAR (10)   NULL,
    [Phone]           VARCHAR (12)   NULL,
    [Email]           VARCHAR (80)   NULL,
    [ID]              VARCHAR (15)   NULL,
    [InHouseID]       VARCHAR (30)   NULL,
    [Payment]         VARCHAR (255)  NULL,
    [Status]          VARCHAR (50)   NULL,
    [SubmitCount]     TINYINT        NOT NULL,
    [Source]          VARCHAR (50)   NOT NULL,
    [SourceDate]      DATE           NOT NULL,
    [SubmitDate]      DATE           NULL,
    [SettleDate]      DATE           NULL,
    [ReturnCode]      VARCHAR (3)    NULL,
    [ReturnDate]      DATE           NULL,
    [ReturnReason]    VARCHAR (60)   NULL,
    [TraceNumber]     VARCHAR (15)   NULL,
    [BatchID]         INT            NOT NULL,
    [BatchDate]       DATE           NOT NULL,
    [AccountID]       INT            NOT NULL,
    [BatchDateYYYYMM] INT            NOT NULL,
    [CRM_PPDID]       NVARCHAR (36)  NULL,
    [UsabilityIndex]  SMALLINT       NULL,
    [MashupOutcome]   TINYINT        NULL,
    [InsertDateTime]  DATETIME       CONSTRAINT [DF__ACHTransB__Inser__73B270E7] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ACHTransByBatchDate] PRIMARY KEY CLUSTERED ([RowID] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE NONCLUSTERED INDEX [NCI_UsabilityIndex]
    ON [Billing].[ACHTransByBatchDate]([UsabilityIndex] ASC)
    INCLUDE([ConfigNumber], [BatchDate], [AccountID]) WITH (FILLFACTOR = 80);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECCodeDesc', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'TransactionType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BankABA', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'RoutingTransit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BankAcctNo', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'AccountNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CheckTypeDesc', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'CheckType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Amount', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'CheckAmount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EffEntryDate', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'EffectiveDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AccountIdent', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'CheckNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ReferenceData', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'Reference';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SSN', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConsumerInHouseID', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'InHouseID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TransStatusCodeDesc', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'Status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SourceName', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'Source';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AddedTime', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'SourceDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LegacyAccountBatchID', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHTransByBatchDate', @level2type = N'COLUMN', @level2name = N'BatchID';

