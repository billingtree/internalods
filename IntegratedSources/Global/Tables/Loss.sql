﻿CREATE TABLE [Global].[Loss] (
    [ID]             INT            IDENTITY (1, 1) NOT NULL,
    [Year]           SMALLINT       NOT NULL,
    [Month]          TINYINT        NULL,
    [MerchantNumber] NVARCHAR (50)  NULL,
    [DBA]            NVARCHAR (255) NULL,
    [OriginalAmount] MONEY          NULL,
    [ReturnType]     NVARCHAR (255) NULL,
    [CaseNumber]     NVARCHAR (50)  NULL,
    [ReserveBalance] MONEY          NULL,
    [JournalDR]      MONEY          NULL,
    CONSTRAINT [PK_Loss] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

