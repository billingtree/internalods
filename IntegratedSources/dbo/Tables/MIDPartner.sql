﻿CREATE TABLE [dbo].[MIDPartner] (
    [CRM_PPDID]        UNIQUEIDENTIFIER NULL,
    [Account]          NVARCHAR (255)   NULL,
    [MID]              NVARCHAR (255)   NULL,
    [MIDEndDate]       DATETIME         NULL,
    [ConfigSource]     NVARCHAR (255)   NULL,
    [PartnerName]      NVARCHAR (255)   NULL,
    [PartnerCode]      NVARCHAR (255)   NULL,
    [PartnerCodeToUse] NVARCHAR (255)   NULL,
    [PartnerCount]     INT              NULL,
    [CreatedDate]      SMALLDATETIME    CONSTRAINT [DF_MIDPartner_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [TypeName]         NVARCHAR (255)   NULL,
    [SubType]          NVARCHAR (255)   NULL,
    [Status]           NVARCHAR (255)   NULL
);

