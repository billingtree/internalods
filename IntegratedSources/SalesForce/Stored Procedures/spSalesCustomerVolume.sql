﻿


-- =============================================
-- Author:		Rick Stohle>
-- Create date: 01-Nov-2014
-- Description:	List Customer Volume/Counts per Month(s) by Account Owner categorizing their activity by $ Volume Buckets
-- =============================================
CREATE PROCEDURE [SalesForce].[spSalesCustomerVolume](
@RunYear     INT,
@RunMonth    INT,
@Owner       VARCHAR (200),
@Time        VARCHAR (3),
@ProductLine CHAR (3)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @YearMonthFrom  INT
    DECLARE @YearMonth      INT
    DECLARE @Year           INT
    DECLARE @Month          INT
    DECLARE @txtYear        VARCHAR (4)
    DECLARE @txtMonth       VARCHAR (2)
    DECLARE @txtOwner       VARCHAR (200)
    DECLARE @txtTime        VARCHAR (3)
    DECLARE @txtProductLine CHAR (3)
    
    SET @txtTime = @Time
    
    SET @Year = @RunYear
    SET @Month = @RunMonth

    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @RunMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))   
   
    IF @txtTime = 'YTD' -- Set Year Month From to January of this Year
         SET @YearMonthFrom = CONVERT(INT, CONCAT(@txtYear, '01'))  
    ELSE                -- Set Year Month From to be the same as the Selected Year and Month
         SET @YearMonthFrom = @YearMonth
   
    SET @txtOwner = @Owner
     
    SET @txtProductLine = @ProductLine 

    -- Insert statements for procedure here
       
IF @txtProductLine = 'CC '         
	SELECT Volume.Parent                 AS Parent
	      ,Volume.Child                  AS Child
		  ,Volume.AddrState              AS AddrState
	      ,Volume.AccountSubType         AS AccountSubType
	      ,Volume.AccountStatus          AS AccountStatus
	      ,Volume.ProductLine            AS ProductLine
	      ,Volume.MID                    AS MID
	      ,Volume.TranYYYYMM             AS TranYYYYMM
	      ,Volume.Amount                 AS Amount
	      ,SUM(Volume.Amount) OVER (PARTITION BY Volume.MID)
	                                     AS Total
          ,CASE 
               WHEN @txtTime  = 'YTD' THEN 
                   SUM(Volume.Amount) OVER (PARTITION BY Volume.MID) / @Month
               ELSE
                   Volume.Amount 
           END                           AS AverageMonthVolume            	                                     
	FROM
	    (       
		SELECT CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END                                              AS Parent
			  ,A.Name                                           AS Child
			  ,A.Address1_StateOrProvince                       AS AddrState
			  ,A.bt_StatusName                                  AS AccountStatus
			  ,A.bt_SubTypeName                                 AS AccountSubType
			  ,CONCAT(PA.bt_ProductLineName, ' - USAePay')      AS ProductLine
			  ,PP.bt_BankMID                                    AS MID 
			  ,USA.TransDateYYYYMM                              AS TranYYYYMM
			  ,SUM(CASE 
					   WHEN TransType = 'S' THEN
						   Amount
					   ELSE
						   Amount * -1
				   END)                                         AS Amount
			  --,COUNT(MeS.MerchantID)                            AS TranCount
		FROM CRMShadow.MS.Account                   AS A
		JOIN CRMShadow.MS.bt_ProcessingAccount      AS PA   ON A.AccountId = PA.bt_Account
		JOIN CRMShadow.MS.bt_ProcessingProfile      AS PP   ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
		JOIN USAePay.FileData                       AS USA  ON PP.bt_ProcessingProfileId = USA.CRM_PPDID
		WHERE A.OwnerIdName = @txtOwner
		  --AND A.StatusCodeName = 'Active'
		  --AND A.bt_StatusName IN ('Active', 'Prospect')
		  AND PP.bt_BankMID IS NOT NULL
		  AND PP.bt_ProductLineName = 'Credit Card'
		  AND USA.TransType IN ('S','C')
		  AND USA.Result = 'A'
		  AND USA.TransDateYYYYMM BETWEEN @YearMonthFrom AND @YearMonth
		  AND PP.bt_GatewayOnlyName = 'Yes'
		GROUP BY CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END 
			  ,A.Name
			  ,A.Address1_StateOrProvince  
			  ,A.bt_StatusName
			  ,A.bt_SubTypeName
			  ,CONCAT(PA.bt_ProductLineName, ' - USAePay')
			  ,PP.bt_BankMID 
			  ,USA.TransDateYYYYMM  
		   
		UNION ALL

		SELECT CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END                                              AS Parent
			  ,A.Name                                           AS Child
			  ,A.Address1_StateOrProvince                       AS AddrState
			  ,A.bt_StatusName                                  AS AccountStatus
			  ,A.bt_SubTypeName                                 AS AccountSubType
			  ,CONCAT(PA.bt_ProductLineName, ' - MeS')          AS ProductLine
			  ,PP.bt_BankMID                                    AS MID 
			  ,MeS.BatchDateYYYYMM                              AS TranYYYYMM
			  ,SUM(MeS.TransactionAmount)                       AS Amount
			  --,COUNT(MeS.MerchantID)                            AS TranCount
		FROM CRMShadow.MS.Account                   AS A
		JOIN CRMShadow.MS.bt_ProcessingAccount      AS PA   ON A.AccountId = PA.bt_Account
		JOIN CRMShadow.MS.bt_ProcessingProfile      AS PP   ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
		JOIN MeS.SettlementSummary                  AS MeS  ON PP.bt_ProcessingProfileId = MeS.CRM_PPDID
		WHERE A.OwnerIdName = @txtOwner
		  --AND A.StatusCodeName = 'Active'
		  --AND A.bt_StatusName IN ('Active', 'Prospect')
		  AND PP.bt_BankMID IS NOT NULL
		  AND PP.bt_ProductLineName = 'Credit Card'
		  AND MeS.BatchDateYYYYMM BETWEEN @YearMonthFrom AND @YearMonth
		  AND PP.bt_GatewayOnlyName = 'No'
		GROUP BY CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END 
			  ,A.Name
			  ,A.Address1_StateOrProvince
			  ,A.bt_StatusName
			  ,A.bt_SubTypeName
			  ,CONCAT(PA.bt_ProductLineName, ' - MeS')
			  ,PP.bt_BankMID 
			  ,MeS.BatchDateYYYYMM  
		)   AS Volume
	GROUP BY Volume.Parent    
	        ,Volume.Child    
			,Volume.AddrState
	        ,Volume.AccountSubType  
	        ,Volume.AccountStatus  
	        ,Volume.ProductLine  
	        ,Volume.MID  
	        ,Volume.TranYYYYMM  
	        ,Volume.Amount

ELSE


	SELECT Volume.Parent                 AS Parent
	      ,Volume.Child                  AS Child
		  ,Volume.AddrState              AS AddrState
	      ,Volume.AccountSubType         AS AccountSubType
	      ,Volume.AccountStatus          AS AccountStatus
	      ,Volume.ProductLine            AS ProductLine
	      ,Volume.MID                    AS MID
	      ,Volume.TranYYYYMM             AS TranYYYYMM
	      ,Volume.Amount                 AS Amount
	      ,SUM(Volume.Amount) OVER (PARTITION BY Volume.MID)
	                                     AS Total
          ,CASE 
               WHEN @txtTime  = 'YTD' THEN 
                   SUM(Volume.Amount) OVER (PARTITION BY Volume.MID) / @Month
               ELSE
                   Volume.Amount 
           END                           AS AverageMonthVolume            	                                     
	FROM
	    (       
		SELECT CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END                                              AS Parent
			  ,A.Name                                           AS Child
			  ,A.Address1_StateOrProvince                       AS AddrState
			  ,A.bt_StatusName                                  AS AccountStatus
			  ,A.bt_SubTypeName                                 AS AccountSubType
			  ,CONCAT(PA.bt_ProductLineName, ' - ATI')          AS ProductLine
			  ,PP.bt_BankMID                                    AS MID 
			  ,ATI.FileCreationDateYYYYMM                       AS TranYYYYMM
			  ,SUM(ATI.FormattedAmount)                         AS Amount
			  --,COUNT(MeS.MerchantID)                            AS TranCount
		FROM CRMShadow.MS.Account                   AS A
		JOIN CRMShadow.MS.bt_ProcessingAccount      AS PA   ON A.AccountId = PA.bt_Account
		JOIN CRMShadow.MS.bt_ProcessingProfile      AS PP   ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
		JOIN NACHA.Originations                     AS ATI  ON PP.bt_ProcessingProfileId = ATI.CRM_PPDID
		WHERE A.OwnerIdName = @txtOwner
		  AND PP.bt_BankMID IS NOT NULL
		  AND PP.bt_ProductLineName = 'ACH'
		  AND ATI.FileCreationDateYYYYMM BETWEEN @YearMonthFrom AND @YearMonth
		GROUP BY CASE 
				   WHEN A.ParentAccountIdName IS NULL THEN
					   A.Name 
				   ELSE 
					   A.ParentAccountIdName
			   END 
			  ,A.Name
			  ,A.Address1_StateOrProvince     
			  ,A.bt_StatusName
			  ,A.bt_SubTypeName
			  ,CONCAT(PA.bt_ProductLineName, ' - ATI')
			  ,PP.bt_BankMID 
			  ,ATI.FileCreationDateYYYYMM
         )  AS Volume 
         
         

END








