﻿


CREATE VIEW [dbo].[vMeSSettlementSummaryQuarantine]
AS
SELECT	ID
		,ReportReferenceCode
		,MerchantID
		,DBAName
		,TermNumber
		,BatchNumber
		,BatchDate
		,TransactionDate
		,CardType
		,CardNumber
		,Reference
		,PurchaseID
		,AuthorizationCode
		,EntryMode
		,TransactionAmount
		,TridentTransactionID
		,ClientReferenceNumber
		,MashupOutcome
		,CASE MashupOutcome
			WHEN 1 THEN 'Spurious'
			WHEN 2 THEN 'UnMatched'
			WHEN 3 THEN 'Matched'
			WHEN 4 THEN 'Duplicates'
		END AS MashupOutcomeDescription
		,InsertDate
FROM	MeS.SettlementSummary
WHERE	(UsabilityIndex = 0)



