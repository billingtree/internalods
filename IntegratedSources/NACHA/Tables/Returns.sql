﻿CREATE TABLE [NACHA].[Returns] (
    [SourceType]               INT             NOT NULL,
    [ReferenceCode]            NVARCHAR (8)    NULL,
    [ConfigNumber]             TINYINT         NULL,
    [FileCreationDate]         DATE            NULL,
    [FileCreationDateYYYYMM]   INT             NULL,
    [FileCreationTime]         TIME (0)        NULL,
    [ServiceClassCode]         NVARCHAR (3)    NULL,
    [CompanyName]              NVARCHAR (16)   NULL,
    [CompanyIdentification]    NVARCHAR (10)   NULL,
    [StandardEntryClassCode]   NVARCHAR (3)    NULL,
    [EffectiveEntryDate]       DATE            NULL,
    [CRM_PPDID]                NVARCHAR (36)   NULL,
    [ProcessingNumber]         NVARCHAR (8)    NULL,
    [BatchNumber]              NVARCHAR (7)    NULL,
    [FileReferenceCodeID]      INT             NOT NULL,
    [BlockNumber]              SMALLINT        NOT NULL,
    [ReturnCode]               NCHAR (3)       NULL,
    [TransactionCode]          NVARCHAR (2)    NULL,
    [Amount]                   NVARCHAR (10)   NULL,
    [IdentificationNumber]     NVARCHAR (15)   NULL,
    [ReceivingCompanyName]     NVARCHAR (22)   NULL,
    [TraceNumber]              NVARCHAR (15)   NULL,
    [UsabilityIndex]           SMALLINT        NULL,
    [MashupOutcome]            TINYINT         NULL,
    [FormattedAmount]          NUMERIC (22, 6) NULL,
    [MashupOutcomeDescription] VARCHAR (20)    NULL,
    [InsertDate]               SMALLDATETIME   CONSTRAINT [DF_Returns_InsertDate] DEFAULT (getdate()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [NCI_SourceType_UsabilityIndex_FileCreationDateYYYYMM]
    ON [NACHA].[Returns]([SourceType] ASC, [UsabilityIndex] ASC, [FileCreationDateYYYYMM] ASC);


GO
CREATE NONCLUSTERED INDEX [NCI_SourceType_FileCreationDateYYYYMM_UsabilityIndex_ReturnCode]
    ON [NACHA].[Returns]([SourceType] ASC, [FileCreationDateYYYYMM] ASC, [UsabilityIndex] ASC, [ReturnCode] ASC)
    INCLUDE([StandardEntryClassCode], [CRM_PPDID]);

