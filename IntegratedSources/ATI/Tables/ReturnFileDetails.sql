﻿CREATE TABLE [ATI].[ReturnFileDetails] (
    [ReferenceCode] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileName]      NVARCHAR (255) NOT NULL,
    [ModifiedDate]  DATETIME       NULL,
    [Size]          NCHAR (10)     NULL,
    [Checksum]      NCHAR (10)     NULL,
    [LineCount]     INT            NULL,
    [RecordCount]   INT            NULL,
    [UploadDate]    DATETIME       CONSTRAINT [DF_ReturnFileDetails_UploadDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ReturnFileDetails] PRIMARY KEY CLUSTERED ([ReferenceCode] ASC) WITH (FILLFACTOR = 80)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNI_FileName]
    ON [ATI].[ReturnFileDetails]([FileName] ASC) WITH (FILLFACTOR = 80);

