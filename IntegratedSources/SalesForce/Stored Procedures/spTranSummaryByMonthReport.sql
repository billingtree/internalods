﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spTranSummaryByMonthReport](
@Month1 INT,
@Year1  INT
)


    -- Insert statements for procedure here
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @vMonth int, @vYear int
	SET @vMonth=@Month1
	SET @vYear=@Year1	
	
	
    -- Concatenate Year and Month in order to use the Index
    DECLARE @vYearMonth INT
    DECLARE @txtYear    VARCHAR (4)
    DECLARE @txtMonth   VARCHAR (2)
     
    SET @txtYear = CONVERT(VARCHAR (4), @vYear)
    IF @vMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @vMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @vMonth) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))   	
	
	
select AllVendors.Parent                    AS Parent
      ,AllVendors.Child                     AS Child
      ,CCSales.Count                        AS CCSales
      ,CCCredits.Count                      AS CCCredits
      ,CCAuths.Count                        AS CCAuths
      ,CCDeclines.Count                     AS CCDeclines
      ,(ISNULL(CCSales.Count,0)+ISNULL(CCCredits.Count,0)+
       ISNULL(CCAuths.Count,0)+ISNULL(CCDeclines.Count,0)) 
                                            AS CCTotal
      ,CBfromMeS.Count                      AS CCChargeBacks
      ,(ISNULL(OrigFromATI.Count,0) + ISNULL(OrigFromInStep.Count,0))
                                            AS ACHOriginations
      ,(ISNULL(CCSales.Count,0)+ISNULL(CCCredits.Count,0)+
       ISNULL(CCAuths.Count,0)+ISNULL(CCDeclines.Count,0)+
       ISNULL(OrigFromATI.Count,0)+ISNULL(OrigFromInStep.Count,0))         
                                            AS ACHCCTotal
      ,(ISNULL(AuthReturnFromATI.Count,0) + ISNULL(AuthReturnFromInStep.Count,0))
                                            AS ACHAuthReturns
      ,(ISNULL(UnAuthReturnFromATI.Count,0) + ISNULL(UnauthReturnFromInStep.Count,0))
                                            AS ACHUnauthReturns
      ,(ISNULL(AuthReturnFromATI.Count,0) + ISNULL(UnAuthReturnFromATI.Count,0) +
        ISNULL(AuthReturnFromInStep.Count,0) + ISNULL(UnAuthReturnFromInStep.Count,0))
                                            AS ACHTotalReturns 
FROM 
-------------------------------------------------------------------------
--    Get all Accounts from the CRM                                    --
-------------------------------------------------------------------------
(select DISTINCT 
       sfa.Id                          "AcctID", 
       sfa.Name                        "Child", 
       sfa.Existing_Clients_Profile__c "Parent"
from SalesForce.Account sfa

) AllVendors
  
LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Sales                      --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
from USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account sfa            on sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDateYYYYMM = @vYearMonth
  and usa.UsabilityIndex = 1
group by sfa.Id) CCSales           ON AllVendors.AcctID = CCSales.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Credits                    --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
from USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account sfa            on sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'C'
  and usa.TransDateYYYYMM = @vYearMonth
  and usa.UsabilityIndex = 1
group by sfa.Id) CCCredits          ON AllVendors.AcctID = CCCredits.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Authorizations             --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
from USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account sfa            on sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'A'
  and usa.TransDateYYYYMM = @vYearMonth
  and usa.UsabilityIndex = 1
group by sfa.Id) CCAuths           ON AllVendors.AcctID = CCAuths.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Declines                   --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
from USAePay.FileData usa
 join SalesForce.PaymentProcessing sfp  on usa.CRM_PPDID = sfp.Id  
 join SalesForce.Account sfa            on sfp.Account__c = sfa.Id
where usa.TransType IN ('A','C','S')
  and usa.Result = 'D'
  and usa.TransDateYYYYMM = @vYearMonth
  and usa.UsabilityIndex = 1
group by sfa.Id) CCDeclines        ON AllVendors.AcctID = CCDeclines.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's ChargeBacks from MeS                  --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
from MeS.ChargebackAdjustments   mesCB
 join SalesForce.PaymentProcessing sfp  on mesCB.CRM_PPDID = sfp.Id  
 join SalesForce.Account sfa            on sfp.Account__c = sfa.Id
where YEAR(convert(DATE, mesCB.IncomingDate)) = @vYear
  and MONTH(convert(DATE, mesCB.IncomingDate)) = @vMonth
  and mesCB.UsabilityIndex = 1
group by sfa.Id) CBfromMeS         ON AllVendors.AcctID = CBfromMeS.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Originations from ATI                  --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Originations                AS ATI
JOIN SalesForce.PaymentProcessing      AS sfp ON ATI.CRM_PPDID = sfp.Id
JOIN SalesForce.Account                AS sfa ON sfp.Account__c = sfa.Id
WHERE ATI.FileCreationDateYYYYMM = @vYearMonth
  AND ATI.UsabilityIndex = 1
  AND ATI.SourceType = 4 
  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'  
GROUP BY sfa.Id) OrigFromATI         ON AllVendors.AcctID = OrigFromATI.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Authorized Returns from ATI                  --
-------------------------------------------------------------------------
(SELECT sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Returns                  AS ATIreturn
JOIN SalesForce.PaymentProcessing   AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
JOIN SalesForce.Account             AS sfa ON sfp.Account__c = sfa.Id
WHERE ATIreturn.FileCreationDateYYYYMM = @vYearMonth
  AND ATIreturn.UsabilityIndex = 1
  AND ATIreturn.SourceType = 4
  AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
  AND ATIreturn.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
GROUP BY sfa.Id) AuthReturnFromATI         ON AllVendors.AcctID = AuthReturnFromATI.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's UnAuthorized Returns from ATI                  --
-------------------------------------------------------------------------
(SELECT sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Returns                  AS ATIreturn
JOIN SalesForce.PaymentProcessing   AS sfp ON ATIreturn.CRM_PPDID = sfp.Id
JOIN SalesForce.Account             AS sfa ON sfp.Account__c = sfa.Id
WHERE ATIreturn.FileCreationDateYYYYMM = @vYearMonth
  AND ATIreturn.UsabilityIndex = 1
  AND ATIreturn.SourceType = 4
  AND UPPER(ATIreturn.StandardEntryClassCode) <> 'COR'
  AND ATIreturn.ReturnCode IN ('R05','R07','R10','R29','R51')
GROUP BY sfa.Id) UnauthReturnFromATI         ON AllVendors.AcctID = UnauthReturnFromATI.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Originations from InStep                  --
-------------------------------------------------------------------------
(select sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Originations           AS InStep
JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
WHERE InStep.FileCreationDateYYYYMM = @vYearMonth
  AND InStep.UsabilityIndex = 1
  AND InStep.SourceType = 1
  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record 
GROUP BY sfa.Id) OrigFromInStep         ON AllVendors.AcctID = OrigFromInStep.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's Authorized Returns from InStep                  --
-------------------------------------------------------------------------
(SELECT sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Returns                AS InStepReturn
JOIN SalesForce.PaymentProcessing AS sfp ON InStepReturn.CRM_PPDID = sfp.Id
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
WHERE InStepReturn.FileCreationDateYYYYMM = @vYearMonth
  AND InStepReturn.UsabilityIndex = 1
  AND InStepReturn.SourceType = 1
  AND UPPER(InStepReturn.StandardEntryClassCode) <> 'COR'
  AND InStepReturn.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
GROUP BY sfa.Id) AuthReturnFromInStep         ON AllVendors.AcctID = AuthReturnFromInStep.AcctID

LEFT JOIN
-------------------------------------------------------------------------
--    Get those identified above Merchant's UnAuthorized Returns from InStep                  --
-------------------------------------------------------------------------
(SELECT sfa.Id "AcctID", count(*) "Count" 
FROM NACHA.Returns                AS InStepReturn
JOIN SalesForce.PaymentProcessing AS sfp ON InStepReturn.CRM_PPDID = sfp.Id
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
WHERE InStepReturn.FileCreationDateYYYYMM = @vYearMonth
  AND InStepReturn.UsabilityIndex = 1
  AND InStepReturn.SourceType = 1
  AND UPPER(InStepReturn.StandardEntryClassCode) <> 'COR'
  AND InStepReturn.ReturnCode IN ('R05','R07','R10','R29','R51')
GROUP BY sfa.Id) UnauthReturnFromInStep         ON AllVendors.AcctID = UnauthReturnFromInStep.AcctID


-------------------------------------------
--    Group By everthing above           --
-------------------------------------------
GROUP BY AllVendors.Parent                 
        ,AllVendors.Child 
        ,CCSales.Count                       
        ,CCCredits.Count                     
        ,CCAuths.Count                      
        ,CCDeclines.Count                    
        ,(ISNULL(CCSales.Count,0)+ISNULL(CCCredits.Count,0)+
          ISNULL(CCAuths.Count,0)+ISNULL(CCDeclines.Count,0)) 
        ,CBfromMeS.Count                      
        ,(ISNULL(OrigFromATI.Count,0) + ISNULL(OrigFromInStep.Count,0))
        ,(ISNULL(CCSales.Count,0)+ISNULL(CCCredits.Count,0)+
          ISNULL(CCAuths.Count,0)+ISNULL(CCDeclines.Count,0)+
          ISNULL(OrigFromATI.Count,0)+ISNULL(OrigFromInStep.Count,0))         
        ,(ISNULL(AuthReturnFromATI.Count,0) + ISNULL(AuthReturnFromInStep.Count,0))
        ,(ISNULL(UnAuthReturnFromATI.Count,0) + ISNULL(UnauthReturnFromInStep.Count,0))
        ,(ISNULL(AuthReturnFromATI.Count,0) + ISNULL(UnAuthReturnFromATI.Count,0) +
          ISNULL(AuthReturnFromInStep.Count,0) + ISNULL(UnAuthReturnFromInStep.Count,0))
---------------------------------------------------------------
--    Accounts must have activity on 1 of the 3 areas below  --
--        - ACH or CC totals                                 --
--        - ACH Returns                                      --
--        - MeS ChargeBacks                                  --
---------------------------------------------------------------
HAVING ((ISNULL(CCSales.Count,0)+ISNULL(CCCredits.Count,0)+
         ISNULL(CCAuths.Count,0)+ISNULL(CCDeclines.Count,0)+
         ISNULL(OrigFromATI.Count,0)+ISNULL(OrigFromInStep.Count,0))  > 0
                   OR
        (ISNULL(AuthReturnFromATI.Count,0) + ISNULL(UnAuthReturnFromATI.Count,0) +
         ISNULL(AuthReturnFromInStep.Count,0) + ISNULL(UnAuthReturnFromInStep.Count,0)) > 0
                   OR
         CBfromMeS.Count > 0)


END
