﻿CREATE TABLE [dbo].[STG_CardRequestsDigitals] (
    [ID_STG_CardRequestsDigitals] UNIQUEIDENTIFIER CONSTRAINT [DF_STG_CardRequestsDigitals_ID_STG_CardRequestsDigitals] DEFAULT (newid()) NOT NULL,
    [MatchInput]                  NVARCHAR (255)   NULL,
    [FileName]                    NVARCHAR (100)   NULL,
    [LinkToPdf]                   NVARCHAR (1024)  NULL,
    [Source]                      NVARCHAR (50)    NULL,
    [fileCreatedOn]               DATETIME         NULL,
    [STG_loadedOn]                DATETIME         NULL,
    [CRM_contactId]               UNIQUEIDENTIFIER NULL,
    [CRM_loadedOn]                DATETIME         NULL,
    [ERROR_code]                  INT              NULL,
    [ERROR_description]           NVARCHAR (1024)  NULL,
    [ERROR_occuredOn]             DATETIME         NULL,
    CONSTRAINT [PK_STG_CardRequestsDigitals] PRIMARY KEY CLUSTERED ([ID_STG_CardRequestsDigitals] ASC) WITH (FILLFACTOR = 80)
);

