﻿
CREATE VIEW [ATI].[vO7TELAddenda] AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,2) AS AddendaTypeCode
		,SUBSTRING(SourceRecord,4,80) AS PaymentRelatedInformation
		,SUBSTRING(SourceRecord,84,4) AS AddendaSequenceNumber
		,SUBSTRING(SourceRecord,88,7) AS EntryDetailSequenceNumber
FROM	ATI.OriginationFileData
WHERE	RecordType = 7
		AND EntryORAddendaType='TEL'




