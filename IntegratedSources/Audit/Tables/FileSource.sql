﻿CREATE TABLE [Audit].[FileSource] (
    [ID]           TINYINT      NOT NULL,
    [VendorName]   VARCHAR (15) NOT NULL,
    [UploadStatus] BIT          NULL
);

