﻿


--drop view [dbo].[vCRMSetupDateByConfigProcessing]

CREATE VIEW [dbo].[vCRMSetupDateByConfigProcessing]
AS
SELECT DISTINCT 
       SFP.Id                            AS "CRM_ID"
      ,SFP.ACH_Config_Number__c          AS "ConfigNbr"
      ,SFP.ACH_Processing_Number__c      AS "ProcessingNbr"
      ,SFA.Existing_Clients_Profile__c   AS "Parent"
      ,SFA.Name                          AS "Child"
      ,CONVERT(DATE, PSH.SetupDate)      AS "SetupDate"
      ,SFP.Application_Stage__c          AS "Stage"
      ,SFP.Estimated_Monthly_Transactions__c
                                         AS "EstMonthCount"
      ,SFP.Estimated_Monthly_Volume__c   AS "EstMonthVolume"  
      ,' '                               AS "MultipleSetupDates" 
      ,SFP.ACH_Platform__c               AS "Platform"                                                                          
FROM  dbo.SetupDateByMID                PSH 
JOIN  SalesForce.PaymentProcessing      SFP   ON SFP.Id = CONVERT(NVARCHAR(36), PSH.CRM_PPDID)
                                          AND SFP.IsDeleted = 0
                                          AND SFP.CC_Merchant_Number__c IS NULL
                                          AND SFP.Application_Stage__c <> 'Inactive'
JOIN SalesForce.Account                 SFA   ON SFA.Id = SFP.Account__c

WHERE YEAR(PSH.SetupDate) > 2012



