﻿CREATE TABLE [dbo].[RevenueCCPDL] (
    [MIDPartial] FLOAT (53)     NULL,
    [MID]        FLOAT (53)     NULL,
    [Name]       NVARCHAR (255) NULL,
    [CCGNumber]  NVARCHAR (255) NULL
);

