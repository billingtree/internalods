﻿CREATE TABLE [Interim].[MIDManuallyBilled] (
    [StartDate]   DATE             NULL,
    [EndDate]     DATE             NULL,
    [CRM_ID]      UNIQUEIDENTIFIER NULL,
    [CreatedDate] DATETIME         NULL
);

