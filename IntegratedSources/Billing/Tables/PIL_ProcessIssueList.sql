﻿CREATE TABLE [Billing].[PIL_ProcessIssueList] (
    [PIL_ID]             INT      NOT NULL,
    [PIL_PIT_ID]         INT      NOT NULL,
    [PIL_PRL_ID]         INT      NOT NULL,
    [PIL_Count]          INT      NOT NULL,
    [PIL_InsertDateTime] DATETIME CONSTRAINT [DF_PIL_ProcessIssueList_InsertDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PIL_ProcessIssueList] PRIMARY KEY CLUSTERED ([PIL_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PIL_ProcessIssueList_PIT_ProcessIssueType] FOREIGN KEY ([PIL_PIT_ID]) REFERENCES [Billing].[PIT_ProcessIssueType] ([PIT_ID]),
    CONSTRAINT [FK_PIL_ProcessIssueList_PRL_ProcessRunList] FOREIGN KEY ([PIL_PRL_ID]) REFERENCES [Billing].[PRL_ProcessRunList] ([PRL_ID])
);

