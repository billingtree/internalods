﻿



CREATE VIEW [Billing].[vYearMonthDateRangeFromToday]
AS

	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, GetDate())))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, GetDate()))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, GetDate())))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, GetDate()))))
                  END
				 )                                                      
					                                                              AS YYYYMM
          ,CONCAT(YEAR(CONVERT(DATE, GetDate()))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, GetDate())))                      AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -1, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -1, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -1, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -1, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -1, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -1, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -2, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -2, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -2, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -2, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -2, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -2, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -3, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -3, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -3, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -3, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -3, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -3, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -4, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -4, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -4, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -4, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -4, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -4, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -5, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -5, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -5, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -5, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -5, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -5, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -6, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -6, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -6, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -6, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -6, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -6, GetDate()))))  AS YearMonth
	UNION ALL 
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -7, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -7, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -7, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -7, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -7, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -7, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -8, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -8, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -8, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -8, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -8, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -8, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -9, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -9, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -9, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -9, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -9, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -9, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -10, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -10, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -10, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -10, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -10, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -10, GetDate()))))  AS YearMonth
	UNION ALL
	SELECT CONCAT(CONVERT(NVARCHAR, YEAR(CONVERT(DATE, DATEADD(Month, -11, GetDate()))))
		         ,CASE 
				      WHEN CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -11, GetDate())))) > 9 THEN
					      CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -11, GetDate()))))
                      ELSE
					      CONCAT(CONVERT(NVARCHAR, '0'),
						         CONVERT(NVARCHAR, MONTH(CONVERT(DATE, DATEADD(Month, -11, GetDate())))))
                  END
				 )                                                      
					                                                              AS YYYYMM
		  ,CONCAT(YEAR(CONVERT(DATE, DATEADD(Month, -11, GetDate())))
		         ,' '
				 ,DateName(Month,CONVERT(DATE, DATEADD(Month, -11, GetDate()))))  AS YearMonth




