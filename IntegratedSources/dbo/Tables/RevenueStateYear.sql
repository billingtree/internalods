﻿CREATE TABLE [dbo].[RevenueStateYear] (
    [State]        NVARCHAR (255) NULL,
    [Revenue2008]  MONEY          NULL,
    [YearRank2008] FLOAT (53)     NULL,
    [Revenue2009]  MONEY          NULL,
    [YearRank2009] FLOAT (53)     NULL,
    [Revenue2010]  MONEY          NULL,
    [YearRank2010] FLOAT (53)     NULL,
    [Revenue2011]  MONEY          NULL,
    [YearRank2011] FLOAT (53)     NULL,
    [Revenue2012]  MONEY          NULL,
    [YearRank2012] FLOAT (53)     NULL,
    [Revenue2013]  MONEY          NULL,
    [YearRank2013] FLOAT (53)     NULL,
    [Revenue2014]  MONEY          NULL,
    [YearRank2014] FLOAT (53)     NULL,
    [RevenueTotal] MONEY          NULL,
    [OverallRank]  FLOAT (53)     NULL
);

