﻿

-- =============================================
-- Author:		Yaseen Jamaludeen
-- Create date: 2014-03-05
-- Description:	Procedure to get ATI Daily Revenue data for GL Feed.
-- =============================================
CREATE PROCEDURE [dbo].[spATIDailyRevenueGLFeedWeeklyCost] --'2014-01-01'
(
	@RevenueDate date = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	IF @RevenueDate IS NULL
		SET @RevenueDate = getdate()
	INSERT INTO	dbo.vATIDailyRevenueGLFeed
				(Type
				,[CRM PARENT ID-GP EXTENDER NATL ACCT ID]
				,[CRM PARENT NAME-GP EXTENDER NATL ACCT NAME]
				,[BT ASSIGNED ACCT#-GP PARENT ACCT#]
				,[CRM ACCT LEVEL NAME-GP PARENT NAME]
				,[BT ASSIGNED PROCESSING RECORD#-GP CHILD CUSTOMER#]
				,[CRM PROCESSING RECORD NAME-GP CHILD CUSTOMER NAME]
				,[CRM ACCT LEVEL#-GP EXTENDER PARENT#]
				,[CRM PROCESSING RECORD#-GP extender field on Customer card]
				,Main
				,BillingStreet
				,BillingStreet2
				,BillingStreet3
				,BillingCity
				,BillingState
				,BillingZip
				,BillingCountry
				,Phone
				,Fax
				,Email
				,GPMain
				,GPBillingStreet
				,GPBillingStreet2
				,GPBillingStreet3
				,GPBillingCity
				,GPBillingState
				,GPBillingZip
				,GPBillingCountry
				,GPPhone
				,GPFax
				,GPEmail
				,[CRM ACCT LEVEL EE COUNT-GP PARENT EE COUNT]
				,[CRM PROCESSING ACCT EE COUNT-GP CHILD EE COUNT]
				,MID
				,ConfigNbr
				,ProcessingNbr
				,SourceID
				,Industry
				,TranDate
				,Volume
				,Count
				,[CRM ACCT LEVEL PARTNER-GP PARENT PARTNER]
				,[CRM PROCESSING ACCT PARTNER-GP CHILD PARTNER]
				,Product
				,RevenueCategory
				,SetupDate
				,ProcessingDate
				,CancelDate
				,SalesPersonID
				,SalesPersonFirstName
				,SalesPersonMiddleName
				,SalesPersonLastName
				,RevenueAmount
				,Quantity
				,[Main GL Segment]
				,BatchID
				,AmountReceived
				,EFTFlag
				,CheckNumber
				,PaymentType
				,LineDescription
				,Cost)
	SELECT	'10'																									AS Type
			,ISNULL(SFA.ParentId,SFA.Id)																			AS 'CRM PARENT ID-GP EXTENDER NATL ACCT ID'
			,SFA.Existing_Clients_Profile__c																		AS 'CRM PARENT NAME-GP EXTENDER NATL ACCT NAME'
			,SFA.Account_Number__c																					AS 'BT ASSIGNED ACCT#-GP PARENT ACCT#'
			,SFA.Name																								AS 'CRM ACCT LEVEL NAME-GP PARENT NAME'
			,LEFT(CONVERT(char, Costs.ConfigNumber),2) + CONVERT(CHAR, Costs.ProcessingNumber)						AS 'BT ASSIGNED PROCESSING RECORD#-GP CHILD CUSTOMER#'
			,ISNULL(SFP.Trust_Name__c,SFA.Name)																		AS 'CRM PROCESSING RECORD NAME-GP CHILD CUSTOMER NAME'
			,SFA.Id																									AS 'CRM ACCT LEVEL#-GP EXTENDER PARENT#'
			,SFP.Id																									AS 'CRM PROCESSING RECORD#-GP extender field on Customer card'
			,'Main'																									AS Main
			,SFA.BillingStreet																						AS BillingStreet
			,''																										AS BillingStreet2
			,''																										AS BillingStreet3
			,SFA.BillingCity																						AS BillingCity
			,SFA.BillingState																						AS BillingState
			,SFA.BillingPostalCode																					AS BillingZip
			,SFA.BillingCountry																						AS BillingCountry
			,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SFA.Phone, '(', ''), ')', ''), '-', ''), ' ', ''), 'x', '')	AS Phone
			,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SFA.Fax, '(', ''), ')', ''), '-', ''), ' ', ''), 'x', '')		AS Fax
			,'                            '																			AS Email
			,'Main'																									AS GPMain
			,SFA.BillingStreet																						AS GPBillingStreet
			,''																										AS GPBillingStreet2
			,''																										AS GPBillingStreet3
			,SFA.BillingCity																						AS GPBillingCity
			,SFA.BillingState																						AS GPBillingState
			,SFA.BillingPostalCode																					AS GPBillingZip
			,SFA.BillingCountry																						AS GPBillingCountry
			,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SFA.Phone, '(', ''), ')', ''), '-', ''), ' ', ''), 'x', '')	AS GPPhone
			,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SFA.Fax, '(', ''), ')', ''), '-', ''), ' ', ''), 'x', '')		AS GPFax
			,''																										AS GPEmail
			,CASE
				WHEN SFA.ParentId > ''
					THEN dbo.fn_GetParentEmployeeCount(SFA.ParentId)
				ELSE dbo.fn_GetParentEmployeeCount(SFA.Id)
			END																										AS 'CRM ACCT LEVEL EE COUNT-GP PARENT EE COUNT'
			,SFA.NumberOfEmployees																					AS 'CRM PROCESSING ACCT EE COUNT-GP CHILD EE COUNT'
			,''																										AS MID
			,Costs.ConfigNumber																						AS ConfigNbr
			,Costs.ProcessingNumber																					AS ProcessingNbr
			,''																										AS SourceID
			,CONVERT(varchar, ISNULL(IC.GP_CODE, '89'))																AS Industry
			,Costs.EndDate																							AS TranDate
			,0																										AS Volume
			,0																										AS Count
			,'0000'																									AS 'CRM ACCT LEVEL PARTNER-GP PARENT PARTNER'
			,ISNULL(MIDs.PartnerCodeToUse,'0000')																	AS 'CRM PROCESSING ACCT PARTNER-GP CHILD PARTNER'
			,''																										AS Product
			,ISNULL(RC.RevenueCategory,1)																			AS RevenueCategory
			,SetupDates.SetupDate																					AS SetupDate
			,ProcessingDates.InitialProcessingDate																	AS ProcessingDate
			,CancelDates.EndDate																					AS CancelDate
			,SalesPerson.FullName																					AS SalesPersonID
			,SalesPerson.FirstName																					AS SalesPersonFirstName
			,''																										AS SalesPersonMiddleName
			,SalesPerson.LastName																					AS SalesPersonLastName
			,0																										AS RevenueAmount
			,1																										AS Quantity
			,Costs.MainGLSegment																					AS 'Main GL Segment'
			,'ATI_' + LEFT(CONVERT(char, dbo.fnDateToJulian(Costs.EndDate)), 7)										AS BatchID
			,0																										AS AmountReceived
			,'0'																									AS EFTFlag
			,''																										AS CheckNumber
			,''																										AS PaymentType
			,''																										AS LineDescription
			,TotalCost																								AS Cost
FROM	dbo.vATIWeeklyCosts AS Costs
			LEFT JOIN SalesForce.PaymentProcessing AS SFP
				ON Costs.ConfigNumber = SFP.ACH_Config_Number__c
				AND Costs.ProcessingNumber = SFP.ACH_Processing_Number__c
			LEFT JOIN SalesForce.Account AS SFA
				ON SFP.Account__c = SFA.Id
			LEFT JOIN [$(CRMShadow)].MS.account AS CRMAccount
				ON SFA.Id = CRMAccount.accountid
			LEFT JOIN dbo.IndustryCodes AS IC
				ON CRMAccount.bt_siccodename = IC.IndustryName
			LEFT JOIN dbo.RevenueCategoryByMID AS RC
				ON SFP.Id = RC.CRM_PPDID
			LEFT JOIN dbo.CancellationMID AS CancelDates
				ON CancelDates.btProcessingProfileId = SFP.Id
			LEFT JOIN dbo.SetupDateByMID AS SetupDates
				ON SetupDates.CRM_PPDID = SFP.Id
			LEFT JOIN dbo.MIDSalesPerson AS SalesPerson
				ON SalesPerson.CRM_PPDID = SFP.Id
			LEFT JOIN dbo.InitialProcessingDateByMID AS ProcessingDates
				ON ProcessingDates.CRM_PPDID = SFP.Id
			LEFT JOIN dbo.MIDPartner AS MIDs
				ON CONVERT(nvarchar(36),MIDs.CRM_PPDID) = SFP.Id
WHERE	StartDate = @RevenueDate
END



