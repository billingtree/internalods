﻿CREATE TABLE [Billing].[PRL_ProcessRunList] (
    [PRL_ID]             INT      IDENTITY (1, 1) NOT NULL,
    [PRL_PSL_ID]         INT      NOT NULL,
    [PRL_StartDateTime]  DATETIME NOT NULL,
    [PRL_EndDateTime]    DATETIME NOT NULL,
    [PRL_InsertDateTime] DATETIME CONSTRAINT [DF_PRL_ProcessRunList_InsertDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PRL_ProcessRunList] PRIMARY KEY CLUSTERED ([PRL_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PRL_ProcessRunList_PSL_ProcessStatusLog] FOREIGN KEY ([PRL_PSL_ID]) REFERENCES [Billing].[PSL_ProcessStatusLog] ([PSL_ID])
);

