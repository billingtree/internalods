﻿



CREATE VIEW [dbo].[vRevenueInStepByYear]
AS


  SELECT DISTINCT 
         CONVERT(NVARCHAR, InStep.MID)  AS MID
        ,InStep.Config                  AS Config
        ,InStep.Name                    AS Name
        ,InStepLookup.CRM_Id            AS CRM_ID
        ,YEAR(InStep.TranDate)          AS TranDate
        ,SUM(Instep.Revenue)            AS Revenue
        ,'InStep'                       AS Source  
  FROM dbo.RevenueInStep              AS InStep
  LEFT JOIN dbo.RevenueInStepLookup   AS InStepLookup ON InStep.MID = InStepLookup.MID
  WHERE InStep.Revenue <> 0
  GROUP BY CONVERT(NVARCHAR, InStep.MID)
          ,InStep.Config
          ,InStep.Name
          ,InStepLookup.CRM_Id
          ,YEAR(InStep.TranDate)
    






