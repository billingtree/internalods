﻿CREATE TABLE [MS].[systemuser] (
    [accessmode]                        INT              NULL,
    [accessmodename]                    NVARCHAR (255)   NULL,
    [address1_addressid]                UNIQUEIDENTIFIER NULL,
    [address1_addresstypecode]          INT              NULL,
    [address1_addresstypecodename]      NVARCHAR (255)   NULL,
    [address1_city]                     NVARCHAR (128)   NULL,
    [address1_composite]                NVARCHAR (MAX)   NULL,
    [address1_country]                  NVARCHAR (128)   NULL,
    [address1_county]                   NVARCHAR (128)   NULL,
    [address1_fax]                      NVARCHAR (64)    NULL,
    [address1_latitude]                 FLOAT (53)       NULL,
    [address1_line1]                    NVARCHAR (1024)  NULL,
    [address1_line2]                    NVARCHAR (1024)  NULL,
    [address1_line3]                    NVARCHAR (1024)  NULL,
    [address1_longitude]                FLOAT (53)       NULL,
    [address1_name]                     NVARCHAR (100)   NULL,
    [address1_postalcode]               NVARCHAR (40)    NULL,
    [address1_postofficebox]            NVARCHAR (40)    NULL,
    [address1_shippingmethodcode]       INT              NULL,
    [address1_shippingmethodcodename]   NVARCHAR (255)   NULL,
    [address1_stateorprovince]          NVARCHAR (128)   NULL,
    [address1_telephone1]               NVARCHAR (64)    NULL,
    [address1_telephone2]               NVARCHAR (50)    NULL,
    [address1_telephone3]               NVARCHAR (50)    NULL,
    [address1_upszone]                  NVARCHAR (4)     NULL,
    [address1_utcoffset]                INT              NULL,
    [address2_addressid]                UNIQUEIDENTIFIER NULL,
    [address2_addresstypecode]          INT              NULL,
    [address2_addresstypecodename]      NVARCHAR (255)   NULL,
    [address2_city]                     NVARCHAR (128)   NULL,
    [address2_composite]                NVARCHAR (MAX)   NULL,
    [address2_country]                  NVARCHAR (128)   NULL,
    [address2_county]                   NVARCHAR (128)   NULL,
    [address2_fax]                      NVARCHAR (50)    NULL,
    [address2_latitude]                 FLOAT (53)       NULL,
    [address2_line1]                    NVARCHAR (1024)  NULL,
    [address2_line2]                    NVARCHAR (1024)  NULL,
    [address2_line3]                    NVARCHAR (1024)  NULL,
    [address2_longitude]                FLOAT (53)       NULL,
    [address2_name]                     NVARCHAR (100)   NULL,
    [address2_postalcode]               NVARCHAR (40)    NULL,
    [address2_postofficebox]            NVARCHAR (40)    NULL,
    [address2_shippingmethodcode]       INT              NULL,
    [address2_shippingmethodcodename]   NVARCHAR (255)   NULL,
    [address2_stateorprovince]          NVARCHAR (128)   NULL,
    [address2_telephone1]               NVARCHAR (50)    NULL,
    [address2_telephone2]               NVARCHAR (50)    NULL,
    [address2_telephone3]               NVARCHAR (50)    NULL,
    [address2_upszone]                  NVARCHAR (4)     NULL,
    [address2_utcoffset]                INT              NULL,
    [businessunitid]                    UNIQUEIDENTIFIER NULL,
    [businessunitidname]                NVARCHAR (160)   NULL,
    [calendarid]                        UNIQUEIDENTIFIER NULL,
    [caltype]                           INT              NULL,
    [caltypename]                       NVARCHAR (255)   NULL,
    [createdby]                         UNIQUEIDENTIFIER NULL,
    [createdbyname]                     NVARCHAR (200)   NULL,
    [createdbyyominame]                 NVARCHAR (200)   NULL,
    [createdon]                         DATETIME         NULL,
    [createdonbehalfby]                 UNIQUEIDENTIFIER NULL,
    [createdonbehalfbyname]             NVARCHAR (200)   NULL,
    [createdonbehalfbyyominame]         NVARCHAR (200)   NULL,
    [defaultfilterspopulated]           BIT              NULL,
    [defaultmailbox]                    UNIQUEIDENTIFIER NULL,
    [defaultmailboxname]                NVARCHAR (200)   NULL,
    [disabledreason]                    NVARCHAR (500)   NULL,
    [displayinserviceviews]             BIT              NULL,
    [displayinserviceviewsname]         NVARCHAR (255)   NULL,
    [domainname]                        NVARCHAR (1024)  NULL,
    [emailrouteraccessapproval]         INT              NULL,
    [emailrouteraccessapprovalname]     NVARCHAR (255)   NULL,
    [employeeid]                        NVARCHAR (100)   NULL,
    [entityimage_timestamp]             BIGINT           NULL,
    [entityimage_url]                   NVARCHAR (200)   NULL,
    [entityimageid]                     UNIQUEIDENTIFIER NULL,
    [exchangerate]                      DECIMAL (28)     NULL,
    [firstname]                         NVARCHAR (64)    NULL,
    [fullname]                          NVARCHAR (200)   NULL,
    [governmentid]                      NVARCHAR (100)   NULL,
    [homephone]                         NVARCHAR (50)    NULL,
    [importsequencenumber]              INT              NULL,
    [incomingemaildeliverymethod]       INT              NULL,
    [incomingemaildeliverymethodname]   NVARCHAR (255)   NULL,
    [internalemailaddress]              NVARCHAR (100)   NULL,
    [invitestatuscode]                  INT              NULL,
    [invitestatuscodename]              NVARCHAR (255)   NULL,
    [isdisabled]                        BIT              NULL,
    [isdisabledname]                    NVARCHAR (255)   NULL,
    [isemailaddressapprovedbyo365admin] BIT              NULL,
    [isintegrationuser]                 BIT              NULL,
    [isintegrationusername]             NVARCHAR (255)   NULL,
    [islicensed]                        BIT              NULL,
    [issyncwithdirectory]               BIT              NULL,
    [jobtitle]                          NVARCHAR (100)   NULL,
    [lastname]                          NVARCHAR (64)    NULL,
    [middlename]                        NVARCHAR (50)    NULL,
    [mobilealertemail]                  NVARCHAR (100)   NULL,
    [mobilephone]                       NVARCHAR (64)    NULL,
    [modifiedby]                        UNIQUEIDENTIFIER NULL,
    [modifiedbyname]                    NVARCHAR (200)   NULL,
    [modifiedbyyominame]                NVARCHAR (200)   NULL,
    [modifiedon]                        DATETIME         NULL,
    [modifiedonbehalfby]                UNIQUEIDENTIFIER NULL,
    [modifiedonbehalfbyname]            NVARCHAR (200)   NULL,
    [modifiedonbehalfbyyominame]        NVARCHAR (200)   NULL,
    [nickname]                          NVARCHAR (50)    NULL,
    [organizationid]                    UNIQUEIDENTIFIER NULL,
    [organizationidname]                NVARCHAR (100)   NULL,
    [outgoingemaildeliverymethod]       INT              NULL,
    [outgoingemaildeliverymethodname]   NVARCHAR (255)   NULL,
    [overriddencreatedon]               DATETIME         NULL,
    [parentsystemuserid]                UNIQUEIDENTIFIER NULL,
    [parentsystemuseridname]            NVARCHAR (200)   NULL,
    [parentsystemuseridyominame]        NVARCHAR (200)   NULL,
    [passporthi]                        INT              NULL,
    [passportlo]                        INT              NULL,
    [personalemailaddress]              NVARCHAR (100)   NULL,
    [photourl]                          NVARCHAR (200)   NULL,
    [preferredaddresscode]              INT              NULL,
    [preferredaddresscodename]          NVARCHAR (255)   NULL,
    [preferredemailcode]                INT              NULL,
    [preferredemailcodename]            NVARCHAR (255)   NULL,
    [preferredphonecode]                INT              NULL,
    [preferredphonecodename]            NVARCHAR (255)   NULL,
    [processid]                         UNIQUEIDENTIFIER NULL,
    [queueid]                           UNIQUEIDENTIFIER NULL,
    [queueidname]                       NVARCHAR (400)   NULL,
    [salutation]                        NVARCHAR (20)    NULL,
    [setupuser]                         BIT              NULL,
    [setupusername]                     NVARCHAR (255)   NULL,
    [siteid]                            UNIQUEIDENTIFIER NULL,
    [siteidname]                        NVARCHAR (160)   NULL,
    [skills]                            NVARCHAR (100)   NULL,
    [stageid]                           UNIQUEIDENTIFIER NULL,
    [systemuserid]                      UNIQUEIDENTIFIER NULL,
    [territoryid]                       UNIQUEIDENTIFIER NULL,
    [territoryidname]                   NVARCHAR (200)   NULL,
    [timezoneruleversionnumber]         INT              NULL,
    [title]                             NVARCHAR (128)   NULL,
    [transactioncurrencyid]             UNIQUEIDENTIFIER NULL,
    [transactioncurrencyidname]         NVARCHAR (100)   NULL,
    [userlicensetype]                   INT              NULL,
    [utcconversiontimezonecode]         INT              NULL,
    [versionnumber]                     BIGINT           NULL,
    [windowsliveid]                     NVARCHAR (1024)  NULL,
    [yammeremailaddress]                NVARCHAR (200)   NULL,
    [yammeruserid]                      NVARCHAR (128)   NULL,
    [yomifirstname]                     NVARCHAR (64)    NULL,
    [yomifullname]                      NVARCHAR (200)   NULL,
    [yomilastname]                      NVARCHAR (64)    NULL,
    [yomimiddlename]                    NVARCHAR (50)    NULL,
    [InsertDate]                        SMALLDATETIME    CONSTRAINT [DF_systemuser_InsertDate] DEFAULT (getdate()) NULL
);

