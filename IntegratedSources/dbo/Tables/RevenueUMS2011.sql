﻿CREATE TABLE [dbo].[RevenueUMS2011] (
    [TranDate]           DATETIME       NULL,
    [TranDateYYYYMM]     NVARCHAR (255) NULL,
    [MID]                NVARCHAR (255) NULL,
    [Name]               NVARCHAR (255) NULL,
    [Status]             NVARCHAR (255) NULL,
    [Volume]             FLOAT (53)     NULL,
    [Disc Inc]           FLOAT (53)     NULL,
    [Surchg Inc]         FLOAT (53)     NULL,
    [20% Surchg Exp]     FLOAT (53)     NULL,
    [IC]                 FLOAT (53)     NULL,
    [Discount Income]    FLOAT (53)     NULL,
    [Trans Nbr]          FLOAT (53)     NULL,
    [Trans Income]       FLOAT (53)     NULL,
    [DurbinVolume]       NVARCHAR (255) NULL,
    [DurbinNbr]          NVARCHAR (255) NULL,
    [Durbin Disc Inc]    NVARCHAR (255) NULL,
    [Durbin IC Exp]      NVARCHAR (255) NULL,
    [Durbin Disc Income] NVARCHAR (255) NULL,
    [Batch Income]       FLOAT (53)     NULL,
    [Total Income]       FLOAT (53)     NULL,
    [Revenue]            FLOAT (53)     NULL
);

