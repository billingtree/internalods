﻿



CREATE VIEW [CRM].[vOpportunityHistoryStagePrior]
AS

----------------------------------------------------------
--  Get the Prior Opportunity Stage before Won or Lost  --
----------------------------------------------------------
SELECT DISTINCT 
       OH.bt_Opportunity          AS OpportunityId
      ,OH.bt_NewLabel             AS CurrentStage
      ,OH.bt_OldLabel             AS PriorStage
FROM [$(CRMShadow)].MS.bt_OpportunityHistory   AS OH
WHERE OH.bt_Opportunity IS NOT NULL
  AND OH.StatusCodeName <> 'Inactive'
  AND OH.bt_NewLabel IN ('Closed Won', 'Closed Lost')










