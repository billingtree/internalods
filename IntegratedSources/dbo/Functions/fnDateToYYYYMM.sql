﻿





CREATE FUNCTION  [dbo].[fnDateToYYYYMM] 
(@DateIn DATE)
 RETURNS INT
AS
BEGIN
    DECLARE @txtYear         VARCHAR (4)
    DECLARE @txtMonth        VARCHAR (2)
    DECLARE @txtYYYYMM       VARCHAR (6)
    DECLARE @nYear           INT
    DECLARE @nMonth          INT
    DECLARE @InputDate       DATE
    DECLARE @ReturnResult    INT 
    
    SET @InputDate = @DateIn
    SET @nYear = YEAR(@DateIn) 
    SET @nMonth = MONTH(@DateIn)
    
    SET @txtYear = CONVERT(VARCHAR(4), @nYear) 
    
    IF @nMonth < 10
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @nMonth))
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @nMonth)   
            
    SET @txtYYYYMM = CONCAT(@txtYear, @txtMonth)
    
    SET @ReturnResult = CONVERT(INT, @txtYYYYMM)

    RETURN @ReturnResult
   
END    
 


