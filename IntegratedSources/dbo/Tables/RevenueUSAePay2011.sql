﻿CREATE TABLE [dbo].[RevenueUSAePay2011] (
    [TranDate]       DATETIME       NULL,
    [TranDateYYYYMM] FLOAT (53)     NULL,
    [CCGNumber]      NVARCHAR (255) NULL,
    [Name]           NVARCHAR (255) NULL,
    [SaleCount]      FLOAT (53)     NULL,
    [CreditCount]    FLOAT (53)     NULL,
    [DeclineCount]   FLOAT (53)     NULL,
    [Fee]            FLOAT (53)     NULL,
    [TranCount]      FLOAT (53)     NULL,
    [TranRevenue]    FLOAT (53)     NULL,
    [MonthlyFee]     FLOAT (53)     NULL,
    [Revenue]        FLOAT (53)     NULL
);

