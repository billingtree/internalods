﻿CREATE TABLE [Billing].[CPP_CustomerProcessingProfile] (
    [CPP_ID]                         INT              IDENTITY (1, 1) NOT NULL,
    [CPP_RunDateYYYYMM]              INT              NOT NULL,
    [CPP_CPA_ID]                     INT              NOT NULL,
    [CPP_CRMProcessingAccountID]     UNIQUEIDENTIFIER NOT NULL,
    [CPP_CRMProcessingProfileID]     UNIQUEIDENTIFIER NOT NULL,
    [CPP_MID]                        NVARCHAR (50)    NULL,
    [CPP_SourceIdConfig]             NVARCHAR (100)   NULL,
    [CPP_StartDate]                  DATETIME         NULL,
    [CPP_EndDate]                    DATETIME         NULL,
    [CPP_GatewayOnly]                BIT              NULL,
    [CPP_CRMRecordStatus]            NVARCHAR (255)   NULL,
    [CPP_SourceStep]                 NVARCHAR (255)   NULL,
    [CPP_InitialTranDateForMonth]    DATE             NULL,
    [CPP_CurrentAppStage]            NVARCHAR (255)   NULL,
    [CPP_QATrainingStageDate]        DATETIME         NULL,
    [CPP_CompleteStageDate]          DATETIME         NULL,
    [CPP_ProcessingProfileStartDate] DATETIME         NULL,
    [CPP_CreateDate]                 SMALLDATETIME    CONSTRAINT [DF__CPP_Custo__CPP_C__1528734B] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CPP_CustomerProcessingProfile] PRIMARY KEY CLUSTERED ([CPP_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CPP_CustomerProcessingProfile_CPA_CustomerProcessingAccount] FOREIGN KEY ([CPP_CPA_ID]) REFERENCES [Billing].[CPA_CustomerProcessingAccount] ([CPA_ID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [NCI_CPP_CPA_ID]
    ON [Billing].[CPP_CustomerProcessingProfile]([CPP_CPA_ID] ASC) WITH (FILLFACTOR = 80);

