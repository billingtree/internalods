﻿CREATE TABLE [CRM].[DIL_DataIssueLog] (
    [DIL_ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [DIL_Priority]            NVARCHAR (255)  NULL,
    [DIL_DataIssue]           NVARCHAR (2000) NULL,
    [DIL_ReportYN]            CHAR (1)        NULL,
    [DIL_Status]              NVARCHAR (50)   NULL,
    [DIL_IssueOwner]          NVARCHAR (50)   NULL,
    [DIL_EstimatedClosedDate] DATE            NULL,
    [DIL_Comment]             NVARCHAR (2000) NULL,
    [DIL_ClosedOn]            DATE            NULL,
    [DIL_ClosedBy]            NVARCHAR (255)  NULL,
    [DIL_CreatedOn]           DATE            NULL,
    [DIL_CreatedBy]           NVARCHAR (255)  NULL,
    [DIL_ModifiedOn]          DATE            NULL,
    [DIL_ModifiedBy]          NVARCHAR (255)  NULL,
    [DIL_ReportLocationName]  NVARCHAR (2000) NULL
);

