﻿CREATE TABLE [dbo].[SetupDateByMID] (
    [CRM_PPDID]   UNIQUEIDENTIFIER NULL,
    [SetupDate]   DATE             NULL,
    [CreatedDate] SMALLDATETIME    CONSTRAINT [DF_SetupDateByMID_CreatedDate] DEFAULT (getdate()) NOT NULL
);

