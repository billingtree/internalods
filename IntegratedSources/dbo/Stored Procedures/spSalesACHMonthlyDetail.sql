﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSalesACHMonthlyDetail]
(
@MID            VARCHAR (10),
@YYYYMM         INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

	DECLARE @vMID        VARCHAR (10)
	DECLARE @vConfig     VARCHAR (4)
	DECLARE @vYYYYMM     INT
	SET @vMID = LEFT(@MID, 4)
	SET @vConfig = RIGHT(RTRIM(@MID),2)
	SET @vYYYYMM = @YYYYMM

	        --------------------------------------------------------------------------
			-- Get a List of all unique dates the MID processed Transactions on 
			--------------------------------------------------------------------------
			DECLARE @ListOfDates TABLE
			(Dates DATE NOT NULL);

			INSERT INTO @ListOfDates
			SELECT DISTINCT AllDates.Dates
			FROM (
				SELECT DISTINCT ATI.BatchDate   AS Dates
				FROM Billing.ACHTransByBatchDate  ATI
				WHERE ATI.AccountId = @vMID
				  AND ATI.ConfigNumber = @vConfig
				  AND ATI.BatchDateYYYYMM = @vYYYYMM
				  AND ISNULL(ATI.Reference, '') NOT LIKE 'OFFSET%'
				UNION ALL
				SELECT DISTINCT ATI.ReturnDate  AS Dates
				FROM Billing.ACHReturnsByReturnDate  ATI
				WHERE ATI.AccountId = @vMID
				  AND ATI.ConfigNumber = @vConfig
				  AND ATI.ReturnDateYYYYMM = @vYYYYMM
				  AND ATI.ReturnCode IS NOT NULL
				  AND ATI.ReturnCode NOT LIKE 'C%'
				 )              AS AllDates
		

	        --------------------------------------------------------------------------
			--           Count Transactions (sum Amount) per Day
			--------------------------------------------------------------------------	
			DECLARE @TranActivity TABLE
			(TranDate DATE NOT NULL,
			 TranAmount DECIMAL (11,2) NULL,
			 TranCount INT NULL);			
				
			INSERT INTO @TranActivity		
			SELECT ATI.BatchDate         AS BatchDate
			      ,SUM(ATI.CheckAmount)  AS TranAmount
				  ,COUNT(*)              AS TranCount
			FROM Billing.ACHTransByBatchDate     AS ATI
			WHERE ATI.AccountId = @vMID
			  AND ATI.ConfigNumber = @vConfig
			  AND ATI.BatchDateYYYYMM = @vYYYYMM
			  AND ISNULL(ATI.Reference, '') NOT LIKE 'OFFSET%'
            GROUP BY ATI.BatchDate


	        --------------------------------------------------------------------------
			--           Count Batches per Day
			--------------------------------------------------------------------------	
			DECLARE @BatchActivity TABLE
			(TranDate DATE NOT NULL,
			 TranCount INT NULL);			
				
			INSERT INTO @BatchActivity	
			SELECT BatchRecs.BatchDate   AS BatchDate
			      ,COUNT(*)              AS TranCount
			FROM (	
					SELECT ATI.BatchDate         AS BatchDate
						  ,ATI.BatchId           AS BatchId
						  ,COUNT(*)              AS TranCount
					FROM Billing.ACHTransByBatchDate     AS ATI
					WHERE ATI.AccountId = @vMID
					  AND ATI.ConfigNumber = @vConfig
					  AND ATI.BatchDateYYYYMM = @vYYYYMM
					  AND ISNULL(ATI.Reference, '') NOT LIKE 'OFFSET%'
					GROUP BY ATI.BatchDate
							,ATI.BatchId
                 )                      AS BatchRecs
            GROUP BY BatchRecs.BatchDate 



	        --------------------------------------------------------------------------
			--           Count Returns (sum Amount) per Day
			--------------------------------------------------------------------------	
			DECLARE @ReturnActivity TABLE
			(TranDate DATE NOT NULL,
			 TranAmount DECIMAL (11,2) NULL,
			 TranCount INT NULL);			
				
			INSERT INTO @ReturnActivity		
			SELECT ATI.ReturnDate         AS ReturnDate
			      ,SUM(ATI.DebitAmount)   AS TranAmount
				  ,COUNT(*)               AS TranCount
			FROM Billing.ACHReturnsByReturnDate  ATI
			WHERE ATI.AccountId = @vMID
			  AND ATI.ConfigNumber = @vConfig
			  AND ATI.ReturnDateYYYYMM = @vYYYYMM
			  AND ATI.ReturnCode IS NOT NULL
			  AND ATI.ReturnCode NOT LIKE 'C%'
            GROUP BY ATI.ReturnDate


	        ----------------------------------------------------------------------------------------
			-- Return the records; with Tran Counts/Volume, Batch Counts and Return Counts/Volume
			----------------------------------------------------------------------------------------
			SELECT AllDates.Dates    AS Dates
			      ,TA.TranAmount     AS TranAmount
			      ,TA.TranCount      AS TranCount
				  ,BA.TranCount      AS BatchCount
                  ,RA.TranAmount     AS ReturnAmount
				  ,RA.TranCount      AS ReturnCount
			FROM @ListOfDates           AS AllDates   
			LEFT JOIN @TranActivity     AS TA       ON AllDates.Dates = TA.TranDate
			LEFT JOIN @BatchActivity    AS BA       ON AllDates.Dates = BA.TranDate
			LEFT JOIN @ReturnActivity   AS RA       ON AllDates.Dates = RA.TranDate



END








