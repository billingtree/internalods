﻿CREATE TABLE [USAePay].[CostStage] (
    [MerchantNum]    NVARCHAR (255) NULL,
    [Company]        NVARCHAR (255) NULL,
    [TotalCollected] INT            NULL,
    [ResellerCost]   INT            NULL,
    [CollectionFee]  INT            NULL,
    [ResidualPayout] INT            NULL,
    [DateActivated]  DATE           NULL
);

