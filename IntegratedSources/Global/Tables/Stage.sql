﻿CREATE TABLE [Global].[Stage] (
    [LineNumber]   INT            IDENTITY (1, 1) NOT NULL,
    [FileName]     NVARCHAR (50)  NULL,
    [SourceRecord] NVARCHAR (309) NULL,
    CONSTRAINT [PK_Global] PRIMARY KEY CLUSTERED ([LineNumber] ASC) WITH (FILLFACTOR = 80)
);

