﻿CREATE TABLE [USAePay].[DailySummary] (
    [FileName]        VARCHAR (255) NOT NULL,
    [TransDate]       DATE          NOT NULL,
    [SourceID]        VARCHAR (8)   NOT NULL,
    [MerchantID]      VARCHAR (16)  NULL,
    [PaymentTechID]   VARCHAR (16)  NULL,
    [MerchantName]    VARCHAR (255) NOT NULL,
    [Sales]           INT           NOT NULL,
    [Credits]         INT           NOT NULL,
    [Voids]           INT           NOT NULL,
    [Declines]        INT           NOT NULL,
    [Amount]          MONEY         NOT NULL,
    [TransDateYYYYMM] INT           NOT NULL,
    [CRM_PPDID]       NVARCHAR (36) NULL,
    [UsabilityIndex]  SMALLINT      NULL,
    [MashupOutcome]   TINYINT       NULL,
    [InsertDate]      SMALLDATETIME CONSTRAINT [DF__DailySumm__Inser__45CA13F8] DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_FileName]
    ON [USAePay].[DailySummary]([FileName] ASC) WITH (FILLFACTOR = 80);

