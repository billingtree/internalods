﻿CREATE TABLE [dbo].[Moneris_BillingTreeTransactionFileTransfer](
	Moneris_BillingTreeTransactionFileTransfer  INT IDENTITY NOT NULL PRIMARY KEY,
	[RecordType] [varchar](1) NOT NULL,
	[MerchantNumber] [nvarchar](20) NOT NULL,
	[MerchantName] [varchar](100) NOT NULL,
	[TerminalNumber] [varchar](6) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [varchar](15) NULL,
	[CardType] [varchar](15) NULL,
	[MaskeCardNumber] [varchar](20) NULL,
	[AuthCode] [varchar](10) NULL,
	[AuthEntryCode] [varchar](10) NULL,
	[TransactionAmount] [numeric](15, 2) NULL,
	[ReferenceNumber] [numeric](18, 0) NULL
) ON [PRIMARY]



