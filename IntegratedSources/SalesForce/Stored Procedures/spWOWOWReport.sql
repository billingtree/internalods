﻿





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spWOWOWReport](
@FromStartDay     DATE,
@FromEndDay       DATE,
@ToStartDay       DATE,
@ToEndDay         DATE,
@UntilStartDay    DATE,
@UntilEndDay      DATE,
@ProductLine      CHAR (3)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vFromStartDay   DATE
    DECLARE @vFromEndDay     DATE
    DECLARE @vToStartDay     DATE
    DECLARE @vToEndDay       DATE
    DECLARE @vUntilStartDay  DATE
    DECLARE @vUntilEndDay    DATE
    DECLARE @vProductLine    CHAR (3)
    
    SET @vFromStartDay = @FromStartDay   
    SET @vFromEndDay = @FromEndDay
    SET @vToStartDay = @ToStartDay 
    SET @vToEndDay = @ToEndDay 
    SET @vUntilStartDay = @UntilStartDay 
    SET @vUntilEndDay = @UntilEndDay 
    SET @vProductLine = @ProductLine     

    -- Insert statements for procedure here
    
    
IF @vProductLine = 'CC '
----------------------------------------------------------------
--               Get Credit Card Transactions 
----------------------------------------------------------------    
		SELECT Both.Type                                     AS "Type"
			  ,Both.Parent                                   AS "Parent"
			  ,Both.Child                                    AS "Child"
			  ,Both.MIDProcessingNbr                         AS "MIDProcessingNbr"
			  ,Both.SourceConfigNbr                          AS "SourceConfigNbr"
              ,SD.SetupDate                                  AS SetupDate
              ,IPD.InitialProcessingDate                     AS InitialProcessingDate
              ,O.OwnerIdName                                 AS OppOwner
			  ,(ISNULL(Month1.Volume1,0))                    AS "Volume1"
			  ,(ISNULL(Month1.Count1,0))                     AS "Count1"
			  ,(ISNULL(Month2.Volume2,0))                    AS "Volume2"
			  ,(ISNULL(Month2.Count2,0))                     AS "Count2"
			  ,(ISNULL(Month3.Volume3,0))                    AS "Volume3"
			  ,(ISNULL(Month3.Count3,0))                     AS "Count3"
		FROM
			 -----------------------------------------------------------------------------
			 --    Get All CRM Processing Accounts with Numeric Merchant IDs            --
			 -----------------------------------------------------------------------------
			(  SELECT sfp.Id                          AS CRM_ID
				 ,sfa.Name                            AS Child
				 ,sfa.Existing_Clients_Profile__c     AS Parent
				 ,SFP.CC_Merchant_Number__c           AS MIDProcessingNbr
				 ,SFP.CC_Source_ID__c                 AS SourceConfigNbr
				 ,'CC'                                AS Type
			 FROM SalesForce.PaymentProcessing AS sfp
			 JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
			 WHERE ISNUMERIC(SFP.CC_Merchant_Number__c) = 1
					 
		  ) 	Both 

		LEFT JOIN
		-------------------------------------------------
		--    Get Time Period 1                        --
		-------------------------------------------------
				(select sfp.Id                                    "CRM_ID"                                
					   ,sfa.Name                                  "Child1" 
					   ,sfa.Existing_Clients_Profile__c           "Parent1"
					   ,SUM(usa.amount)                           "Volume1" 
					   ,count(*)                                  "Count1" 
					   ,'CC'                                      "Type"
				from USAePay.FileData             AS usa
				JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
				JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
				where usa.Result = 'A'
				  and usa.TransType = 'S'
				  and usa.TransDate BETWEEN @vFromStartDay AND @vFromEndDay
				  and usa.UsabilityIndex = 1
				group by sfp.Id
						,sfa.Name
						,sfa.Existing_Clients_Profile__c) Month1  ON Both.CRM_ID = Month1.CRM_ID

		LEFT JOIN
		-------------------------------------------------
		--    Get Time Period 2                        --
		-------------------------------------------------
				(select sfp.Id                                      "CRM_ID"
					   ,sfa.Name                                    "Child2" 
					   ,sfa.Existing_Clients_Profile__c             "Parent2"
					   ,SUM(usa.amount)                             "Volume2"
					   ,count(*)                                    "Count2"
					   ,'CC'                                        "Type"
				from USAePay.FileData             AS usa
				JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
				JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
				where usa.Result = 'A'
				  and usa.TransType = 'S'
				  and usa.TransDate BETWEEN @vToStartDay AND @vToEndDay
				  and usa.UsabilityIndex = 1
				group by sfp.Id
						,sfa.Name
						,sfa.Existing_Clients_Profile__c) Month2  ON Both.CRM_ID = Month2.CRM_ID
		
		LEFT JOIN
		-------------------------------------------------
		--    Get Time Period 3                        --
		-------------------------------------------------
				(select sfp.Id                                      "CRM_ID"
					   ,sfa.Name                                    "Child3" 
					   ,sfa.Existing_Clients_Profile__c             "Parent3"
					   ,SUM(usa.amount)                             "Volume3"
					   ,count(*)                                    "Count3"
					   ,'CC'                                        "Type"
				from USAePay.FileData             AS usa
				JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
				JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
				where usa.Result = 'A'
				  and usa.TransType = 'S'
				  and usa.TransDate BETWEEN @vUntilStartDay AND @vUntilEndDay
				  and usa.UsabilityIndex = 1
				group by sfp.Id
						,sfa.Name
						,sfa.Existing_Clients_Profile__c) Month3  ON Both.CRM_ID = Month3.CRM_ID						
						
        LEFT JOIN dbo.SetupDateByMID                AS SD   ON Both.CRM_ID = SD.CRM_PPDID						
        LEFT JOIN dbo.InitialProcessingDateByMID    AS IPD  ON Both.CRM_ID = IPD.CRM_PPDID
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile AS PP   ON Both.CRM_ID = PP.bt_ProcessingProfileId
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
        LEFT JOIN [$(CRMShadow)].MS.Opportunity          AS O    ON PA.bt_Opportunity = O.OpportunityId
		-----------------------------------------------------------------------
		--  Do a Group By so Having can weed out Accounts with no Activity   --
		------------------------------------------------------------------------
		GROUP BY Both.Type
			  ,Both.Parent
			  ,Both.Child
			  ,Both.MIDProcessingNbr
			  ,Both.SourceConfigNbr  
			  ,SD.SetupDate
              ,IPD.InitialProcessingDate 
              ,O.OwnerIdName 
			  ,(ISNULL(Month1.Volume1,0))   
			  ,(ISNULL(Month1.Count1,0)) 
			  ,(ISNULL(Month2.Volume2,0))  
			  ,(ISNULL(Month2.Count2,0))   
			  ,(ISNULL(Month3.Volume3,0))  
			  ,(ISNULL(Month3.Count3,0)) 
		----------------------------------------------------------------------
		--  Only show those Accounts that have a Total Count > 0            --
		----------------------------------------------------------------------  
		  HAVING (ISNULL(Month1.Count1,0)) + 
				 (ISNULL(Month2.Count2,0)) +
				 (ISNULL(Month3.Count3,0))> 0


				  
ELSE
----------------------------------------------------------------
--               Get ACH Transactions 
----------------------------------------------------------------

		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		--                         ACH Data   (ATI and InStep)                         --
		---------------------------------------------------------------------------------
		---------------------------------------------------------------------------------
		  SELECT Both.Type                                    AS "Type"
				,Both.Parent                                  AS "Parent"
				,Both.Child                                   AS "Child"
				,Both.MIDProcessingNbr                        AS MIDProcessingNbr
				,Both.SourceConfigNbr                         AS SourceConfigNbr        
                ,SD.SetupDate                                 AS SetupDate
                ,IPD.InitialProcessingDate                    AS InitialProcessingDate
                ,O.OwnerIdName                                AS OppOwner
				,(ISNULL(ATIMonth1.Volume1,0)+
				  ISNULL(InStepMonth1.Volume1,0))             AS "Volume1"
				,(ISNULL(ATIMonth1.Count1,0)+
				  ISNULL(InStepMonth1.Count1,0))              AS "Count1"
				,(ISNULL(ATIMonth2.Volume2,0)+
				  ISNULL(InStepMonth2.Volume2,0))             AS "Volume2"
				,(ISNULL(ATIMonth2.Count2,0)+
				  ISNULL(InStepMonth2.Count2,0))              AS "Count2"
			    ,(ISNULL(ATIMonth3.Volume3,0)+
				  ISNULL(InStepMonth3.Volume3,0))             AS "Volume3"
				,(ISNULL(ATIMonth3.Count3,0)+
				  ISNULL(InStepMonth3.Count3,0))              AS "Count3"

		  FROM
			 -----------------------------------------------------------------------------
			 --    Get All CRM Processing Accounts with Numeric Config & Processing #s  --
			 -----------------------------------------------------------------------------
			(  SELECT sfp.Id                          AS CRM_ID
				 ,sfa.Name                            AS Child
				 ,sfa.Existing_Clients_Profile__c     AS Parent
				 ,SFP.ACH_Processing_Number__c        AS MIDProcessingNbr
				 ,SFP.ACH_Config_Number__c            AS SourceConfigNbr	
				 ,'ACH'                               AS Type
			 FROM SalesForce.PaymentProcessing AS sfp
			 JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
			 WHERE ISNUMERIC(SFP.ACH_Processing_Number__c) = 1
			   AND ISNUMERIC(SFP.ACH_Config_Number__c) = 1
					 
		  ) 	Both 



		LEFT JOIN
		---------------------------------------------------------
		--    Get ATI for Time Period 1                        --
		---------------------------------------------------------
				( SELECT sfp.Id                               AS CRM_ID
						,sfa.Name                             AS Child1
						,sfa.Existing_Clients_Profile__c      AS Parent1
						,SUM(ATI.FormattedAmount)             AS Volume1
						,COUNT(*)                             AS Count1
						,'ACH'                                AS Type
					FROM NACHA.Originations           AS ATI
					JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE ATI.FileCreationDate BETWEEN @vFromStartDay AND @vFromEndDay
					  AND ATI.UsabilityIndex = 1
					  AND ATI.SourceType = 4
					  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
					GROUP BY sfp.Id
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  ATIMonth1  ON Both.CRM_ID = ATIMonth1.CRM_ID


		LEFT JOIN
		-------------------------------------------------
		--    Get ATI for Time Period 2                --
		-------------------------------------------------
				( SELECT sfp.Id                               AS CRM_ID
						,sfa.Name                             AS Child2
						,sfa.Existing_Clients_Profile__c      AS Parent2
						,SUM(ATI.FormattedAmount)             AS Volume2
						,COUNT(*)                             AS Count2
						,'ACH'                                AS Type
					FROM NACHA.Originations           AS ATI
					JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE ATI.FileCreationDate BETWEEN @vToStartDay AND @vToEndDay
					  AND ATI.UsabilityIndex = 1
					  AND ATI.SourceType = 4
					  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
					GROUP BY sfp.Id
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  ATIMonth2  ON Both.CRM_ID = ATIMonth2.CRM_ID

		LEFT JOIN
		-------------------------------------------------
		--    Get ATI for Time Period 3                --
		-------------------------------------------------
				( SELECT sfp.Id                               AS CRM_ID
						,sfa.Name                             AS Child3
						,sfa.Existing_Clients_Profile__c      AS Parent3
						,SUM(ATI.FormattedAmount)             AS Volume3
						,COUNT(*)                             AS Count3
						,'ACH'                                AS Type
					FROM NACHA.Originations           AS ATI
					JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE ATI.FileCreationDate BETWEEN @vUntilStartDay AND @vUntilEndDay
					  AND ATI.UsabilityIndex = 1
					  AND ATI.SourceType = 4
					  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
					GROUP BY sfp.Id
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  ATIMonth3  ON Both.CRM_ID = ATIMonth3.CRM_ID
							
		LEFT JOIN
		---------------------------------------------------------
		--    Get InStep for Time Period 1                     --
		---------------------------------------------------------
				( SELECT sfp.Id                               AS CRM_ID
						,sfa.Name                             AS Child1
						,sfa.Existing_Clients_Profile__c      AS Parent1
						,SUM(InStep.FormattedAmount)          AS Volume1
						,COUNT(*)                             AS Count1
						,'ACH'                                AS Type
					FROM NACHA.Originations           AS InStep
					JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE InStep.FileCreationDate BETWEEN @vFromStartDay AND @vFromEndDay
					  AND InStep.UsabilityIndex = 1
					  AND InStep.SourceType = 1
					  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record	
					GROUP BY sfp.Id
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  InStepMonth1  ON Both.CRM_ID = InStepMonth1.CRM_ID

		LEFT JOIN
		---------------------------------------------------------
		--    Get InStep for Time Period 2                     --
		---------------------------------------------------------
				(  SELECT sfp.Id                              AS CRM_ID
						 ,sfa.Name                            AS Child2
						 ,sfa.Existing_Clients_Profile__c     AS Parent2
						 ,SUM(InStep.FormattedAmount)         AS Volume2
						 ,COUNT(*)                            AS Count2
						 ,'ACH'                               AS Type
					FROM NACHA.Originations           AS InStep
					JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE InStep.FileCreationDate BETWEEN @vToStartDay AND @vToEndDay
					  AND InStep.UsabilityIndex = 1
					  AND InStep.SourceType = 1
					  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
					GROUP BY sfp.Id 
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  InStepMonth2  ON Both.CRM_ID = InStepMonth2.CRM_ID

		LEFT JOIN
		---------------------------------------------------------
		--    Get InStep for Time Period 3                     --
		---------------------------------------------------------
				(  SELECT sfp.Id                              AS CRM_ID
						 ,sfa.Name                            AS Child3
						 ,sfa.Existing_Clients_Profile__c     AS Parent3
						 ,SUM(InStep.FormattedAmount)         AS Volume3
						 ,COUNT(*)                            AS Count3
						 ,'ACH'                               AS Type
					FROM NACHA.Originations           AS InStep
					JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
					JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
					WHERE InStep.FileCreationDate BETWEEN @vUntilStartDay AND @vUntilEndDay
					  AND InStep.UsabilityIndex = 1
					  AND InStep.SourceType = 1
					  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
					GROUP BY sfp.Id 
							,sfa.Name 
							,sfa.Existing_Clients_Profile__c)  InStepMonth3  ON Both.CRM_ID = InStepMonth3.CRM_ID		
							
        LEFT JOIN dbo.SetupDateByMID                AS SD   ON Both.CRM_ID = SD.CRM_PPDID						
        LEFT JOIN dbo.InitialProcessingDateByMID    AS IPD  ON Both.CRM_ID = IPD.CRM_PPDID
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile AS PP   ON Both.CRM_ID = PP.bt_ProcessingProfileId
        LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
        LEFT JOIN [$(CRMShadow)].MS.Opportunity          AS O    ON PA.bt_Opportunity = O.OpportunityId
		-----------------------------------------------------------------------
		--  Do a Group By so Having can weed out Accounts with no Activity   --
		------------------------------------------------------------------------
		GROUP BY Both.Type
				,Both.Parent
				,Both.Child
				,Both.MIDProcessingNbr   
				,Both.SourceConfigNbr    
			    ,SD.SetupDate
                ,IPD.InitialProcessingDate
                ,O.OwnerIdName
				,(ISNULL(ATIMonth1.Volume1,0)+
				  ISNULL(InStepMonth1.Volume1,0))             
				,(ISNULL(ATIMonth1.Count1,0)+
				  ISNULL(InStepMonth1.Count1,0))             
				,(ISNULL(ATIMonth2.Volume2,0)+
				  ISNULL(InStepMonth2.Volume2,0))                     
				,(ISNULL(ATIMonth2.Count2,0)+
				  ISNULL(InStepMonth2.Count2,0)) 
				,(ISNULL(ATIMonth3.Volume3,0)+
				  ISNULL(InStepMonth3.Volume3,0))                     
				,(ISNULL(ATIMonth3.Count3,0)+
				  ISNULL(InStepMonth3.Count3,0))     
		          
		----------------------------------------------------------------------
		--  Only show those Accounts that have a Total Count > 0            --
		----------------------------------------------------------------------          
		HAVING ((ISNULL(ATIMonth1.Count1,0)+
				  ISNULL(InStepMonth1.Count1,0)) +
			    (ISNULL(ATIMonth2.Count2,0)+
				  ISNULL(InStepMonth2.Count2,0)) +
				(ISNULL(ATIMonth3.Count3,0)+
				  ISNULL(InStepMonth3.Count3,0))) > 0   				     

END














