﻿CREATE TABLE [dbo].[ATIDailyRevenue] (
    [Description]      NVARCHAR (40)  NULL,
    [Config]           TINYINT        NULL,
    [ReceivingCompany] NVARCHAR (255) NULL,
    [IDNumber]         NVARCHAR (255) NULL,
    [ProcessingNbr]    NVARCHAR (60)  NULL,
    [SettleDate]       DATE           NULL,
    [FormattedAmount]  MONEY          NULL,
    [CheckNumber]      NVARCHAR (15)  NULL,
    [MainGLSegment]    NVARCHAR (10)  NULL,
    [InsertDate]       SMALLDATETIME  CONSTRAINT [DF__ATIDailyR__Inser__387A3A7B] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_SettleDate__WithIncludes]
    ON [dbo].[ATIDailyRevenue]([SettleDate] ASC)
    INCLUDE([Description], [Config], [ProcessingNbr], [FormattedAmount], [CheckNumber], [MainGLSegment]) WITH (FILLFACTOR = 80);

