﻿CREATE TABLE [dbo].[RevenueCCGLookup] (
    [CCGNumber]        NVARCHAR (255) NULL,
    [PartialCCGNumber] FLOAT (53)     NULL,
    [SF_MID]           NVARCHAR (255) NULL,
    [CRM_MID]          FLOAT (53)     NULL,
    [CRM_Id]           NVARCHAR (255) NULL,
    [CRM_PA_Name]      NVARCHAR (255) NULL
);

