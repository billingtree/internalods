﻿


CREATE VIEW [dbo].[vRptQuarantineCount]
AS
SELECT USA.USACount + 
       UDS.SummaryCount +
       MeSSettlement.MeSCount + 
       MeSCB.MeSCBCount +
       ATIOrig.ATIOrigCount +
       ATIReturn.ATIReturnCount +
       InStepOrig.InStepOrigCount +       
       InStepReturn.InStepReturnCount + 
       --Global.GlobalCount      
	   ATIAPITran.ATIAPITranCount +
	   ATIAPIReturn.ATIAPIReturnCount    AS "Count"
FROM (
      SELECT         COUNT(*)               AS "USACount"
      FROM           USAePay.FileData 
      WHERE          UsabilityIndex = 0) USA
JOIN
     (
      SELECT         COUNT(*)               AS "SummaryCount"
      FROM           USAePay.DailySummary
      WHERE          UsabilityIndex = 0) UDS
      ON 1=1      
JOIN
     (
      SELECT         COUNT(*)               AS "MeSCount"
      FROM           MeS.SettlementSummary
      WHERE          UsabilityIndex = 0) MeSSettlement
      ON 1=1
JOIN
     (
      SELECT         COUNT(*)               AS "MeSCBCount"
      FROM           MeS.ChargebackAdjustments
      WHERE          UsabilityIndex = 0) MeSCB
      ON 1=1
/*****************
JOIN
     (
      SELECT         COUNT(*)               AS "GlobalCount"
      FROM           Global.FileDataPersisted
      WHERE          UsabilityIndex = 0) Global
      ON 1=1
*****************/
JOIN
     (
      SELECT         COUNT(*)               AS "ATIOrigCount"
      FROM           NACHA.Originations
      WHERE          SourceType = 4
        AND          UsabilityIndex = 0) ATIOrig
      ON 1=1
JOIN
     (
      SELECT         COUNT(*)               AS "ATIReturnCount"
      FROM           NACHA.Returns
      WHERE          SourceType = 4
        AND          UsabilityIndex = 0) ATIReturn
      ON 1=1
JOIN
     (
      SELECT         COUNT(*)               AS "InStepOrigCount"
      FROM           NACHA.Originations      
      WHERE          SourceType = 1
        AND          UsabilityIndex = 0) InStepOrig
      ON 1=1
   
JOIN
     (
      SELECT         COUNT(*)               AS "InStepReturnCount"
      FROM           NACHA.Returns
      WHERE          SourceType = 1
        AND          UsabilityIndex = 0) InStepReturn
      ON 1=1
JOIN
     (
      SELECT         COUNT(*)               AS "ATIAPITranCount"
      FROM           Billing.ACHTransByBatchDate
      WHERE          UsabilityIndex = 0)  ATIAPITran
      ON 1=1
JOIN
     (
      SELECT         COUNT(*)               AS "ATIAPIReturnCount"
      FROM           Billing.ACHReturnsByReturnDate
      WHERE          UsabilityIndex = 0)  ATIAPIReturn
      ON 1=1


