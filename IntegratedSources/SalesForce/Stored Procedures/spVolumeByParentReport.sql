﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spVolumeByParentReport](
@Month INT,
@Year  INT
)
 WITH RECOMPILE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @txtYear                VARCHAR (4)
    DECLARE @txtMonth               VARCHAR (2)
    DECLARE @InputDate              DATE
    DECLARE @InputDateMinus1        DATE
    DECLARE @InputDateMinus2        DATE
    DECLARE @InputDateMinus3        DATE
    DECLARE @InputDateMinus4        DATE
    DECLARE @InputDateMinus5        DATE
    
    DECLARE @InputDateYYYYMM        INT
    DECLARE @InputDateMinus1YYYYMM  INT
    DECLARE @InputDateMinus2YYYYMM  INT
    DECLARE @InputDateMinus3YYYYMM  INT
    DECLARE @InputDateMinus4YYYYMM  INT
    DECLARE @InputDateMinus5YYYYMM  INT    
        
    --  Convert the input parameters Year and Month to a Date  
    SET @txtYear = @Year 
    SET @txtMonth = @Month
    IF @Month < 10 
        SET @InputDate = CONVERT(DATE,
                         @txtYear + CONCAT('0', CONVERT(VARCHAR(1), @Month)) + '01')
    ELSE
        SET @InputDate = CONVERT(DATE,
                         @txtYear + @txtMonth + '01')
                         
    --  Subtract 1 month to the calculated Input Date from above  
    SET @InputDateMinus1 = DATEADD(MONTH, -1, @InputDate)
    
    --  Subtract 2 months to the calculated Input Date from above  
    SET @InputDateMinus2 = DATEADD(MONTH, -2, @InputDate)
    
    --  Subtract 3 month to the calculated Input Date from above  
    SET @InputDateMinus3 = DATEADD(MONTH, -3, @InputDate)
    
    --  Subtract 4 months to the calculated Input Date from above  
    SET @InputDateMinus4 = DATEADD(MONTH, -4, @InputDate)
    
    --  Subtract 5 month to the calculated Input Date from above  
    SET @InputDateMinus5 = DATEADD(MONTH, -5, @InputDate)
   
    --  Change Dates to YYYYMM Integer format
    SET @InputDateYYYYMM = dbo.fnDateToYYYYMM(@InputDate) 
    SET @InputDateMinus1YYYYMM = dbo.fnDateToYYYYMM(@InputDateMinus1)
    SET @InputDateMinus2YYYYMM = dbo.fnDateToYYYYMM(@InputDateMinus2)
    SET @InputDateMinus3YYYYMM = dbo.fnDateToYYYYMM(@InputDateMinus3)
    SET @InputDateMinus4YYYYMM = dbo.fnDateToYYYYMM(@InputDateMinus4)
    SET @InputDateMinus5YYYYMM = dbo.fnDateToYYYYMM(@InputDateMinus5) 



SELECT USAePayAndATI.Parent                     AS "Parent"
      ,SUM(USAePayAndATI.LastMonthVolume)       AS "LastMonthVolume"   
      ,SUM(USAePayAndATI.TwoMonthsAgoVolume)    AS "TwoMonthsAgoVolume"   
      ,SUM(USAePayAndATI.ThreeMonthsAgoVolume)  AS "ThreeMonthsAgoVolume"   
      ,SUM(USAePayAndATI.FourMonthsAgoVolume)   AS "FourMonthsAgoVolume"   
      ,SUM(USAePayAndATI.FiveMonthsAgoVolume)   AS "FiveMonthsAgoVolume"  
      
FROM (      
---------------------------------------------------------------------
--   Get USAePay transaction volume for the last 5 Months 
---------------------------------------------------------------------        
SELECT SFA.Existing_Clients_Profile__c       AS "Parent"
      ,SUM(LMV.Volume)                       AS "LastMonthVolume"   
      ,SUM(L2MV.Volume)                      AS "TwoMonthsAgoVolume"   
      ,SUM(L3MV.Volume)                      AS "ThreeMonthsAgoVolume"   
      ,SUM(L4MV.Volume)                      AS "FourMonthsAgoVolume"   
      ,SUM(L5MV.Volume)                      AS "FiveMonthsAgoVolume"   
FROM SalesForce.PaymentProcessing    SFP
LEFT JOIN SalesForce.Account         SFA   ON SFA.Id = SFP.Account__c
LEFT JOIN 
         (SELECT USA1.CRM_PPDID              AS "ID"
                ,USA1.TransDateYYYYMM        AS "TransDate"
                ,SUM(USA1.Amount)            AS "Volume"
          FROM USAePay.FileData USA1  
          WHERE USA1.TransType = 'S'
            AND USA1.Result = 'A'
            AND USA1.TransDateYYYYMM = @InputDateMinus1YYYYMM  
            AND USA1.UsabilityIndex = 1
          GROUP BY USA1.CRM_PPDID
                  ,USA1.TransDateYYYYMM   
          )  LMV                      ON LMV.ID = SFP.Id

LEFT JOIN 
         (SELECT USA2.CRM_PPDID               AS "ID"
                ,USA2.TransDateYYYYMM         AS "TransDate"
                ,SUM(USA2.Amount)             AS "Volume"
          FROM USAePay.FileData USA2  
          WHERE USA2.TransType = 'S'
            AND USA2.Result = 'A'
            AND USA2.TransDateYYYYMM = @InputDateMinus2YYYYMM   
            AND USA2.UsabilityIndex = 1
          GROUP BY USA2.CRM_PPDID
                  ,USA2.TransDateYYYYMM   
          )  L2MV                    ON L2MV.ID = SFP.Id

LEFT JOIN 
         (SELECT USA3.CRM_PPDID               AS "ID"
                ,USA3.TransDateYYYYMM         AS "TransDate"
                ,SUM(USA3.Amount)             AS "Volume"
          FROM USAePay.FileData USA3  
          WHERE USA3.TransType = 'S'
            AND USA3.Result = 'A'
            AND USA3.TransDateYYYYMM = @InputDateMinus3YYYYMM  
            AND USA3.UsabilityIndex = 1
          GROUP BY USA3.CRM_PPDID
                  ,USA3.TransDateYYYYMM   
          )  L3MV                    ON L3MV.ID = SFP.Id

LEFT JOIN 
        (SELECT USA4.CRM_PPDID                AS "ID"
               ,USA4.TransDateYYYYMM          AS "TransDate"
               ,SUM(USA4.Amount)              AS "Volume"
         FROM USAePay.FileData USA4  
         WHERE USA4.TransType = 'S'
           AND USA4.Result = 'A'
           AND USA4.TransDateYYYYMM = @InputDateMinus4YYYYMM   
           AND USA4.UsabilityIndex = 1
         GROUP BY USA4.CRM_PPDID
                 ,USA4.TransDateYYYYMM   
         )  L4MV                    ON L4MV.ID = SFP.Id

LEFT JOIN 
        (SELECT USA5.CRM_PPDID               AS "ID"
               ,USA5.TransDateYYYYMM         AS "TransDate"
               ,SUM(USA5.Amount)             AS "Volume"
         FROM USAePay.FileData USA5  
         WHERE USA5.TransType = 'S'
           AND USA5.Result = 'A'
           AND USA5.TransDateYYYYMM = @InputDateMinus5YYYYMM  
           AND USA5.UsabilityIndex = 1
         GROUP BY USA5.CRM_PPDID
                 ,USA5.TransDateYYYYMM   
         )  L5MV                    ON L5MV.ID = SFP.Id

WHERE SFP.CC_Merchant_Number__c IS NOT NULL
  AND SFA.Name IS NOT NULL
  AND ISNUMERIC(SFP.CC_Source_ID__c) = 1


GROUP BY SFA.Existing_Clients_Profile__c
HAVING SUM(LMV.Volume) > 0 
    OR SUM(L2MV.Volume) > 0 
    OR SUM(L3MV.Volume) > 0 
    OR SUM(L4MV.Volume) > 0 
    OR SUM(L5MV.Volume) > 0

  
UNION ALL

 
---------------------------------------------------------------------
--   Get ATI NACHA transaction volume for the last 5 Months 
---------------------------------------------------------------------    
SELECT SFA.Existing_Clients_Profile__c       AS "Parent"
      ,SUM(LMV.Volume)                       AS "LastMonthVolume"   
      ,SUM(L2MV.Volume)                      AS "TwoMonthsAgoVolume"   
      ,SUM(L3MV.Volume)                      AS "ThreeMonthsAgoVolume"   
      ,SUM(L4MV.Volume)                      AS "FourMonthsAgoVolume"   
      ,SUM(L5MV.Volume)                      AS "FiveMonthsAgoVolume"   
FROM SalesForce.PaymentProcessing    SFP
LEFT JOIN SalesForce.Account         SFA   ON SFA.Id = SFP.Account__c
LEFT JOIN 
         (SELECT ATI1.CRM_PPDID              AS "ID"
                ,ATI1.FileCreationDateYYYYMM AS "TransDate"
                ,SUM(ATI1.FormattedAmount)   AS "Volume"
          FROM NACHA.Originations ATI1  
          WHERE ATI1.FileCreationDateYYYYMM = @InputDateMinus1YYYYMM  
            AND ATI1.UsabilityIndex = 1
            AND ATI1.SourceType = 4
            AND UPPER(ATI1.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI1.CRM_PPDID
                  ,ATI1.FileCreationDateYYYYMM   
          )  LMV                      ON LMV.ID = SFP.Id

LEFT JOIN 
         (SELECT ATI2.CRM_PPDID              AS "ID"
                ,ATI2.FileCreationDateYYYYMM AS "TransDate"
                ,SUM(ATI2.FormattedAmount)   AS "Volume"
          FROM NACHA.Originations ATI2  
          WHERE ATI2.FileCreationDateYYYYMM = @InputDateMinus2YYYYMM  
            AND ATI2.UsabilityIndex = 1
            AND ATI2.SourceType = 4
            AND UPPER(ATI2.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI2.CRM_PPDID
                  ,ATI2.FileCreationDateYYYYMM   
          )  L2MV                    ON L2MV.ID = SFP.Id

LEFT JOIN 
         (SELECT ATI3.CRM_PPDID              AS "ID"
                ,ATI3.FileCreationDateYYYYMM AS "TransDate"
                ,SUM(ATI3.FormattedAmount)   AS "Volume"
          FROM NACHA.Originations ATI3  
          WHERE ATI3.FileCreationDateYYYYMM = @InputDateMinus3YYYYMM  
            AND ATI3.UsabilityIndex = 1
            AND ATI3.SourceType = 4
            AND UPPER(ATI3.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI3.CRM_PPDID
                  ,ATI3.FileCreationDateYYYYMM   
          )  L3MV                    ON L3MV.ID = SFP.Id

LEFT JOIN 
         (SELECT ATI4.CRM_PPDID              AS "ID"
                ,ATI4.FileCreationDateYYYYMM AS "TransDate"
                ,SUM(ATI4.FormattedAmount)   AS "Volume"
          FROM NACHA.Originations ATI4  
          WHERE ATI4.FileCreationDateYYYYMM = @InputDateMinus4YYYYMM  
            AND ATI4.UsabilityIndex = 1
            AND ATI4.SourceType = 4
            AND UPPER(ATI4.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI4.CRM_PPDID
                  ,ATI4.FileCreationDateYYYYMM   
         )  L4MV                    ON L4MV.ID = SFP.Id

LEFT JOIN 
         (SELECT ATI5.CRM_PPDID              AS "ID"
                ,ATI5.FileCreationDateYYYYMM AS "TransDate"
                ,SUM(ATI5.FormattedAmount)   AS "Volume"
          FROM NACHA.Originations ATI5  
          WHERE ATI5.FileCreationDateYYYYMM = @InputDateMinus5YYYYMM  
            AND ATI5.UsabilityIndex = 1
            AND ATI5.SourceType = 4
            AND UPPER(ATI5.IdentificationNumber) NOT LIKE '%OFFSET%'
          GROUP BY ATI5.CRM_PPDID
                  ,ATI5.FileCreationDateYYYYMM   
         )  L5MV                    ON L5MV.ID = SFP.Id

WHERE ISNUMERIC(SFP.ACH_Processing_Number__c) = 1
  AND ISNUMERIC(SFP.ACH_Config_Number__c) = 1
  AND SFA.Name IS NOT NULL


GROUP BY SFA.Existing_Clients_Profile__c
HAVING SUM(LMV.Volume) > 0 
    OR SUM(L2MV.Volume) > 0 
    OR SUM(L3MV.Volume) > 0 
    OR SUM(L4MV.Volume) > 0 
    OR SUM(L5MV.Volume) > 0
    
     
)      AS USAePayAndATI

GROUP BY USAePayAndATI.Parent




END