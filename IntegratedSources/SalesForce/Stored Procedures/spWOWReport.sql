﻿



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spWOWReport](
@FromStartDay  DATE,
@FromEndDay    DATE,
@ToStartDay    DATE,
@ToEndDay      DATE
) WITH RECOMPILE

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vFromStartDay  DATE
    DECLARE @vFromEndDay    DATE
    DECLARE @vToStartDay    DATE
    DECLARE @vToEndDay      DATE

    SET @vFromStartDay = @FromStartDay   
    SET @vFromEndDay = @FromEndDay
    SET @vToStartDay = @ToStartDay 
    SET @vToEndDay = @ToEndDay   

    -- Insert statements for procedure here
    
SELECT Both.Type                                     AS "Type"
      ,Both.Parent                                   AS "Parent"
      ,Both.Child                                    AS "Child"
	  ,Both.Industry                                 AS Industry
	  ,Both.NumberOfEmployees                        AS NumberOfEmployees      
	  ,Both.MIDProcessingNbr                         AS "MIDProcessingNbr"
	  ,Both.SourceConfigNbr                          AS "SourceConfigNbr"
      ,(ISNULL(Month1.Volume1,0)+
        ISNULL(Month2.Volume2,0))                    AS "TotalVolume"
      ,(ISNULL(Month1.Count1,0)+
        ISNULL(Month2.Count2,0))                     AS "TotalCount"
      ,(ISNULL(Month1.Volume1,0))                    AS "Volume1"
      ,(ISNULL(Month1.Count1,0))                     AS "Count1"
      ,(ISNULL(Month2.Volume2,0))                    AS "Volume2"
      ,(ISNULL(Month2.Count2,0))                     AS "Count2"
FROM
     -----------------------------------------------------------------------------
     --    Get All CRM Processing Accounts with Numeric Merchant IDs            --
     -----------------------------------------------------------------------------
    (  SELECT sfp.Id                          AS CRM_ID
         ,sfa.Name                            AS Child
	     ,sfa.Existing_Clients_Profile__c     AS Parent
	     ,SFP.CC_Merchant_Number__c           AS MIDProcessingNbr
	     ,SFP.CC_Source_ID__c                 AS SourceConfigNbr
	     ,SFA.Industry                        AS Industry
	     ,SFA.NumberOfEmployees               AS NumberOfEmployees
	     ,'CC'                                AS Type
     FROM SalesForce.PaymentProcessing AS sfp
     JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
     WHERE ISNUMERIC(SFP.CC_Merchant_Number__c) = 1
			 
  ) 	Both 

LEFT JOIN
-------------------------------------------------
--    Get Time Period 1                        --
-------------------------------------------------
(select sfp.Id                                    "CRM_ID"                                
       ,sfa.Name                                  "Child1" 
       ,sfa.Existing_Clients_Profile__c           "Parent1"
       ,SUM(usa.amount)                           "Volume1" 
       ,count(*)                                  "Count1" 
       ,'CC'                                      "Type"
from USAePay.FileData             AS usa
JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDate BETWEEN @vFromStartDay AND @vFromEndDay
  and usa.UsabilityIndex = 1
group by sfp.Id
        ,sfa.Name
        ,sfa.Existing_Clients_Profile__c) Month1  ON Both.CRM_ID = Month1.CRM_ID

LEFT JOIN
-------------------------------------------------
--    Get Time Period 2                        --
-------------------------------------------------
(select sfp.Id                                      "CRM_ID"
       ,sfa.Name                                    "Child2" 
       ,sfa.Existing_Clients_Profile__c             "Parent2"
       ,SUM(usa.amount)                             "Volume2"
       ,count(*)                                    "Count2"
       ,'CC'                                        "Type"
from USAePay.FileData             AS usa
JOIN SalesForce.PaymentProcessing AS sfp ON usa.CRM_PPDID = sfp.Id 
JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
where usa.Result = 'A'
  and usa.TransType = 'S'
  and usa.TransDate BETWEEN @vToStartDay AND @vToEndDay
  and usa.UsabilityIndex = 1
group by sfp.Id
        ,sfa.Name
        ,sfa.Existing_Clients_Profile__c) Month2  ON Both.CRM_ID = Month2.CRM_ID
-----------------------------------------------------------------------
--  Do a Group By so Having can weed out Accounts with no Activity   --
------------------------------------------------------------------------
GROUP BY Both.Type
      ,Both.Parent
      ,Both.Child
	  ,Both.Industry 
	  ,Both.NumberOfEmployees  
	  ,Both.MIDProcessingNbr
	  ,Both.SourceConfigNbr      
      ,(ISNULL(Month1.Volume1,0)+
        ISNULL(Month2.Volume2,0)) 
      ,(ISNULL(Month1.Count1,0)+
        ISNULL(Month2.Count2,0))  
      ,(ISNULL(Month1.Volume1,0))   
      ,(ISNULL(Month1.Count1,0)) 
      ,(ISNULL(Month2.Volume2,0))  
      ,(ISNULL(Month2.Count2,0))   
----------------------------------------------------------------------
--  Only show those Accounts that have a Total Count > 0            --
----------------------------------------------------------------------  
  HAVING (ISNULL(Month1.Count1,0)) + 
         (ISNULL(Month2.Count2,0)) > 0




UNION ALL
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
--                         ACH Data   (ATI and InStep)                         --
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
  SELECT Both.Type                                    AS "Type"
        ,Both.Parent                                  AS "Parent"
        ,Both.Child                                   AS "Child"
	    ,Both.Industry                                AS Industry
	    ,Both.NumberOfEmployees                       AS NumberOfEmployees           
	    ,Both.MIDProcessingNbr                        AS MIDProcessingNbr
	    ,Both.SourceConfigNbr                         AS SourceConfigNbr        
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0)+
          ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             AS "TotalVolume"
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              AS "TotalCount"
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0))             AS "Volume1"
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0))              AS "Count1"
        ,(ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             AS "Volume2"
        ,(ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              AS "Count2"

  FROM
     -----------------------------------------------------------------------------
     --    Get All CRM Processing Accounts with Numeric Config & Processing #s  --
     -----------------------------------------------------------------------------
    (  SELECT sfp.Id                          AS CRM_ID
         ,sfa.Name                            AS Child
	     ,sfa.Existing_Clients_Profile__c     AS Parent
	     ,SFP.ACH_Processing_Number__c        AS MIDProcessingNbr
	     ,SFP.ACH_Config_Number__c            AS SourceConfigNbr	
	     ,SFA.Industry                        AS Industry
	     ,SFA.NumberOfEmployees               AS NumberOfEmployees   	          
	     ,'ACH'                               AS Type
     FROM SalesForce.PaymentProcessing AS sfp
     JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
     WHERE ISNUMERIC(SFP.ACH_Processing_Number__c) = 1
       AND ISNUMERIC(SFP.ACH_Config_Number__c) = 1
			 
  ) 	Both 



LEFT JOIN
---------------------------------------------------------
--    Get ATI for Time Period 1                        --
---------------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child1
	    ,sfa.Existing_Clients_Profile__c      AS Parent1
	    ,SUM(ATI.FormattedAmount)             AS Volume1
	    ,COUNT(*)                             AS Count1
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS ATI
	JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE ATI.FileCreationDate BETWEEN @vFromStartDay AND @vFromEndDay
	  AND ATI.UsabilityIndex = 1
      AND ATI.SourceType = 4
	  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	GROUP BY sfp.Id
		    ,sfa.Name 
		    ,sfa.Existing_Clients_Profile__c)  ATIMonth1  ON Both.CRM_ID = ATIMonth1.CRM_ID


LEFT JOIN
-------------------------------------------------
--    Get ATI for Time Period 2                --
-------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child2
	    ,sfa.Existing_Clients_Profile__c      AS Parent2
	    ,SUM(ATI.FormattedAmount)             AS Volume2
	    ,COUNT(*)                             AS Count2
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS ATI
	JOIN SalesForce.PaymentProcessing AS sfp ON ATI.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE ATI.FileCreationDate BETWEEN @vToStartDay AND @vToEndDay
	  AND ATI.UsabilityIndex = 1
      AND ATI.SourceType = 4
	  AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	GROUP BY sfp.Id
		    ,sfa.Name 
			,sfa.Existing_Clients_Profile__c)  ATIMonth2  ON Both.CRM_ID = ATIMonth2.CRM_ID

LEFT JOIN
---------------------------------------------------------
--    Get InStep for Time Period 1                     --
---------------------------------------------------------
( SELECT sfp.Id                               AS CRM_ID
	    ,sfa.Name                             AS Child1
	    ,sfa.Existing_Clients_Profile__c      AS Parent1
	    ,SUM(InStep.FormattedAmount)          AS Volume1
	    ,COUNT(*)                             AS Count1
	    ,'ACH'                                AS Type
	FROM NACHA.Originations           AS InStep
	JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE InStep.FileCreationDate BETWEEN @vFromStartDay AND @vFromEndDay
	  AND InStep.UsabilityIndex = 1
      AND InStep.SourceType = 1
	  AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record	
	GROUP BY sfp.Id
		    ,sfa.Name 
		    ,sfa.Existing_Clients_Profile__c)  InStepMonth1  ON Both.CRM_ID = InStepMonth1.CRM_ID

LEFT JOIN
---------------------------------------------------------
--    Get InStep for Time Period 2                     --
---------------------------------------------------------
(  SELECT sfp.Id                              AS CRM_ID
         ,sfa.Name                            AS Child2
	     ,sfa.Existing_Clients_Profile__c     AS Parent2
	     ,SUM(InStep.FormattedAmount)         AS Volume2
	     ,COUNT(*)                            AS Count2
	     ,'ACH'                               AS Type
	FROM NACHA.Originations           AS InStep
	JOIN SalesForce.PaymentProcessing AS sfp ON InStep.CRM_PPDID = sfp.Id
	JOIN SalesForce.Account           AS sfa ON sfp.Account__c = sfa.Id
	WHERE InStep.FileCreationDate BETWEEN @vToStartDay AND @vToEndDay
	  AND InStep.UsabilityIndex = 1
      AND InStep.SourceType = 1
      AND InStep.ReceivingCompanyName <> 'BILLINGTREE'  --  Exclude the OFFSET Record
	GROUP BY sfp.Id 
		    ,sfa.Name 
			,sfa.Existing_Clients_Profile__c)  InStepMonth2  ON Both.CRM_ID = InStepMonth2.CRM_ID
-----------------------------------------------------------------------
--  Do a Group By so Having can weed out Accounts with no Activity   --
------------------------------------------------------------------------
GROUP BY Both.Type
        ,Both.Parent
        ,Both.Child
	    ,Both.Industry   
	    ,Both.NumberOfEmployees
        ,Both.MIDProcessingNbr   
	    ,Both.SourceConfigNbr    
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0)+
          ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))             
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))              
        ,(ISNULL(ATIMonth1.Volume1,0)+
          ISNULL(InStepMonth1.Volume1,0))             
        ,(ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0))             
        ,(ISNULL(ATIMonth2.Volume2,0)+
          ISNULL(InStepMonth2.Volume2,0))                     
        ,(ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0))  
          
----------------------------------------------------------------------
--  Only show those Accounts that have a Total Count > 0            --
----------------------------------------------------------------------          
HAVING (ISNULL(ATIMonth1.Count1,0)+
          ISNULL(InStepMonth1.Count1,0)+
          ISNULL(ATIMonth2.Count2,0)+
          ISNULL(InStepMonth2.Count2,0)) > 0       

END












