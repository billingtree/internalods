﻿



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessStatusCPASubReport](
@PRLID          INT,
@IssueType      NVARCHAR (50)
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vPRLID          INT
    DECLARE @vIssueType      NVARCHAR (50)
    SET @vPRLID = @PRLID   
    SET @vIssueType = RTRIM(@IssueType)

    -- Insert statements for procedure here

------------------------------------------------------------------------
--            These 4 Issue Types are handled by this Procedure
------------------------------------------------------------------------
    --IF @vIssueType = 'Multiple PPs per PA'
    --IF @vIssueType = 'PA with no Account'
    --IF @vIssueType = 'Manual Bill to Start in the Future'
    --IF @vIssueType = 'Manual Bill to End in the Future'
    
    
		SELECT PSL.PSL_RunDateYYYYMM
			  ,PRT.PRT_Name
			  ,PSL.PSL_Status
			  ,PSL.PSL_InsertDateTime
			  ,PRL.PRL_StartDateTime
			  ,PRL.PRL_EndDateTime
			  ,PIT.PIT_Category
			  ,PIT.PIT_Description
			  ,PIT.PIT_Name
			  ,CST.CST_AccountName
			  ,CPA.CPA_TrustName
			  ,CPA.CPA_ManualBillStartDate
			  ,CPA.CPA_ManualBillEndDate
			  ,CPP.CPP_MID
			  ,CPP.CPP_SourceIdConfig
			  ,CPP.CPP_SourceStep
			  ,CPP.CPP_StartDate
			  ,CPP.CPP_EndDate
			  ,CPP.CPP_CRMProcessingProfileID
			  ,CPA.CPA_CRMProcessingAccountID
		FROM Billing.PSL_ProcessStatusLog                AS PSL
		JOIN Billing.PRT_ProcessRunType                  AS PRT   ON PSL.PSL_PRT_ID = PRT.PRT_ID
		JOIN Billing.PRL_ProcessRunList                  AS PRL   ON PSL.PSL_ID = PRL.PRL_PSL_ID
		JOIN Billing.PIL_ProcessIssueList                AS PIL   ON PRL.PRL_ID = PIL.PIL_PRL_ID
		JOIN Billing.PIT_ProcessIssueType                AS PIT   ON PIL.PIL_PIT_ID = PIT.PIT_ID
		LEFT JOIN Billing.PED_ProcessErrorDetail         AS PED   ON PIL.PIL_ID = PED.PED_PIL_ID
		LEFT JOIN Billing.CPA_CustomerProcessingAccount  AS CPA   ON PED.PED_CPA_ID = CPA.CPA_ID
		LEFT JOIN Billing.CPP_CustomerProcessingProfile  AS CPP   ON CPA.CPA_ID = CPP.CPP_CPA_ID
		LEFT JOIN Billing.CST_Customer                   AS CST   ON CPA.CPA_CST_ID = CST.CST_ID
		WHERE PRL.PRL_ID = @vPRLID 
		  AND PIT.PIT_Name = @vIssueType
	OPTION (OPTIMIZE FOR UNKNOWN)--(RECOMPILE)
	



		
END










