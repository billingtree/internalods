﻿<?xml version="1.0" encoding="utf-8"?>
<ComponentItem xmlns:rdl="http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition" xmlns:rd="http://schemas.microsoft.com/SQLServer/reporting/reportdesigner" Name="Source" xmlns="http://schemas.microsoft.com/sqlserver/reporting/2010/01/componentdefinition">
  <Properties>
    <Property Name="Type">ReportParameter</Property>
    <Property Name="ThumbnailSource">iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxEAAAsRAX9kX5EAAAPySURBVEhLtVZNTFxVFP7e/EAhSAp2B5iRYkgMFLSSaQLFxIZqZhgd2RhjIKymTVw4w8afGhqM/CSTGBA3oAsCDZEEGS1TQIs2upNpl0AoIWXRFmhCjK0OA8Ob6znnvkcordYae2a+d8/fPefec899M4ZSCk+SjPq3O//3DD+PfmRYrE6Qf+QZLRgGHE4nHA6GAy6XCy53lsiG04HsQ4eEd5LNID+2OwmGQTLNZZr7MXZfAoc1WmTreVPWxgwlnLbo50H5n+iJlwgdn/bQOasUwSRkWCBinnUPEDsIMvTkr4wZGRlWPNiwS5RNYN7OzHw2eexVSwaRNbTaeto6Hg/Q/jPYsh30BOb21fjgZJEtHbmpDEA70PI+kgQU8A/yz4ldnMTEdxfx7WSc15VWyNCHnjSbk1LhYFrgYKZpw4TJngcXQaQTSH6F4OuNCAYCeKPRz85uvUidhgNa6Uhl6oRsk6nkyKuXCfeTLpFCvmQX+8Oc2M2q8b4gTroLbpeb7gJD34lG32tsChFKmZEEsjoBbdTeOvGy9V0Tu+Yu7dEKTivmS8VYXV1FW1ubXEpOxmiLvId4PP48hQ0QjttnIKCGswLxiemAXBK6a+wlBeLA7Nvd3Y2mpiaUlZUhnU7voaWlBa2trW92dXU9t5dAr0whPjWDyelpwgwXBFM/XEZ8+nuRuVXohUCvEhfGx8extLSE4ZELOFn/MsLhCNxuNwYGBpCbm4sTJ7xXBwcHT25sbOTKRdveTv1GUCkbKcFuMpVUyaSWk8kk+6n19XVVVVWltne21c7Ojvqko0PktbU14Xm54XD4y87Ozi94tEqEw4S7XBoqvXQLncUWDWTU50MbEpqdnQVNlrLNz8+j/fx5DA0N4enCQpQelXNFe3v7rZqaml83NzdzXKww0xTVgfyZy7PiYFGeNe6Rt+Yl/HTlCkJnzop87uNz8Pl9yM7JETmRuIrqF15EQUGByEySgFfM39OnXhHl31FWlhtP5eWh+lgltlIpTF2awsjICEo9HpH7+vrw/gcfsuvtRCLhrauru6MP+V8S94JNKysrMnq9XuosYHh4WOTKymM8pEZHR6v9fv+yy+lw/hL97PN67uFH0aunT+HuvXuYjE9TzQ+L7uat25ibS2BsbAwez7MoLi6+U1FREWpoaFgsKir6U3r6MfBOf3//19wdxEfLy8uXKIeKRqNfcdcwz7DsLc3NzRce90f/OMPj8bzb29s7EwwGt7T6QYrFYr8vLi4e/S//KkILCwtHfD7fWyUlJWs9PT3Xamtr05ZNKBKJFC0vXy+Mxy8NPawMj0IBIcTgUlBL3qCYUhqG3+/7ZmJiIkL2gFIq8BfEdtLDidSsMAAAAABJRU5ErkJggg==</Property>
    <Property Name="ThumbnailMimeType">image/png</Property>
  </Properties>
  <RdlFragment>
    <rdl:Report>
      <rdl:AutoRefresh>0</rdl:AutoRefresh>
      <rdl:ReportSections>
        <rdl:ReportSection>
          <rdl:Body>
            <rdl:Height>0in</rdl:Height>
            <rdl:Style />
          </rdl:Body>
          <rdl:Width>0in</rdl:Width>
          <rdl:Page>
            <rdl:Style />
          </rdl:Page>
        </rdl:ReportSection>
      </rdl:ReportSections>
      <rdl:ReportParameters>
        <rdl:ReportParameter Name="Source">
          <rdl:DataType>String</rdl:DataType>
          <rdl:AllowBlank>true</rdl:AllowBlank>
          <rdl:Prompt>Source</rdl:Prompt>
          <ComponentMetadata>
            <ComponentId>316671cb-350f-4a83-b4d7-91edaa8f6265</ComponentId>
          </ComponentMetadata>
        </rdl:ReportParameter>
      </rdl:ReportParameters>
      <rd:ReportUnitType>Invalid</rd:ReportUnitType>
      <rd:ReportID>e6ce8841-24a2-422d-b927-829d9d70af2e</rd:ReportID>
    </rdl:Report>
  </RdlFragment>
</ComponentItem>