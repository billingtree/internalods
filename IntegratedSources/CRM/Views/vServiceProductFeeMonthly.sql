﻿
CREATE VIEW [CRM].[vServiceProductFeeMonthly]
AS

------------------------------------------
--  CC and ACH BillingTree Monthly Fee  --
------------------------------------------ 
SELECT SPF.bt_Account             AS AccountId
      ,SPF.bt_Opportunity         AS OpportunityId
      ,SPF.bt_ProcessingAccount   AS ProcessingAccountId
      ,SPF.bt_ServiceOrder        AS ServiceOrderId
      ,SPF.bt_Product             AS ProductId
      ,SPF.bt_ServiceProductFeeId AS ServiceProductFeeId
      ,PR.Name                    AS ProductName          
      ,SPF.bt_FeeTypeName         AS FeeType
      ,SPF.bt_Amount              AS SellingPrice
      ,SPF.bt_IntegrationLevel    AS ProductFeeIntegrationLevel
      ,SPF.bt_StartDate           AS FeeStartDate
      ,SPF.bt_EndDate             AS FeeEndDate
      ,SPF.StatusCodeName         AS StatusCodeName
      ,SPF.CreatedOn              AS CreatedOn
      ,SPF.ModifiedOn             AS ModifiedOn
FROM [$(CRMShadow)].MS.bt_ServiceProductFee    AS SPF
JOIN [$(CRMShadow)].MS.Product                 AS PR   ON SPF.bt_Product = Pr.ProductId
WHERE SPF.bt_FeeTypeName = 'BillingTree Monthly Fee'
  AND SPF.StatusCodeName = 'Active'
