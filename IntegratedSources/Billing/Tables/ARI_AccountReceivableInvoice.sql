﻿CREATE TABLE [Billing].[ARI_AccountReceivableInvoice] (
    [ARI_RunDateYYYYMM]         INT              NOT NULL,
    [ARI_Type]                  TINYINT          NULL,
    [ARI_ParentID]              UNIQUEIDENTIFIER NULL,
    [ARI_ParentName]            NVARCHAR (255)   NULL,
    [ARI_ParentAccountNumber]   UNIQUEIDENTIFIER NULL,
    [ARI_AccountName]           NVARCHAR (255)   NULL,
    [ARI_AccountNumber]         NVARCHAR (10)    NULL,
    [ARI_CustomerID]            NVARCHAR (150)   NULL,
    [ARI_TrustName]             NVARCHAR (150)   NULL,
    [ARI_AccountID]             NVARCHAR (150)   NULL,
    [ARI_ProcessingAccountID]   UNIQUEIDENTIFIER NULL,
    [ARI_ProcessingProfileID]   UNIQUEIDENTIFIER NULL,
    [ARI_CRMMID]                NVARCHAR (16)    NULL,
    [ARI_BankAccountNumber]     NVARCHAR (20)    NULL,
    [ARI_ABANumber]             NVARCHAR (20)    NULL,
    [ARI_GPMain]                NVARCHAR (255)   NULL,
    [ARI_GPBillingStreet]       NVARCHAR (255)   NULL,
    [ARI_GPBillingStreet2]      NVARCHAR (255)   NULL,
    [ARI_GPBillingStreet3]      NVARCHAR (255)   NULL,
    [ARI_GPBillingCity]         NVARCHAR (255)   NULL,
    [ARI_GPBillingState]        NVARCHAR (255)   NULL,
    [ARI_GPBillingZip]          NVARCHAR (255)   NULL,
    [ARI_GPBillingCountry]      NVARCHAR (255)   NULL,
    [ARI_GPPhone]               NVARCHAR (255)   NULL,
    [ARI_GPFax]                 NVARCHAR (255)   NULL,
    [ARI_GPEmail]               NVARCHAR (255)   NULL,
    [ARI_MID]                   NVARCHAR (255)   NULL,
    [ARI_SourceID]              NVARCHAR (255)   NULL,
    [ARI_ProcessingNbr]         NVARCHAR (8)     NULL,
    [ARI_ConfigNbr]             TINYINT          NULL,
    [ARI_TranDate]              DATE             NULL,
    [ARI_Volume]                MONEY            NULL,
    [ARI_Count]                 INT              NULL,
    [ARI_SalesPersonID]         NVARCHAR (255)   NULL,
    [ARI_SalesPersonFirstName]  NVARCHAR (255)   NULL,
    [ARI_SalesPersonMiddleName] NVARCHAR (255)   NULL,
    [ARI_SalesPersonLastName]   NVARCHAR (255)   NULL,
    [ARI_ItemNumber]            NVARCHAR (50)    NULL,
    [ARI_LineDescription]       NVARCHAR (50)    NULL,
    [ARI_Quantity]              INT              NULL,
    [ARI_UnitPrice]             MONEY            NULL,
    [ARI_ExtPrice]              MONEY            NULL,
    [ARI_MainGLSegment]         NVARCHAR (10)    NULL,
    [ARI_BatchID]               NVARCHAR (15)    NULL,
    [ARI_CheckBookID]           NCHAR (10)       NULL,
    [ARI_AmountReceived]        MONEY            NULL,
    [ARI_EFTFlag]               BIT              NULL,
    [ARI_CPA_ID]                INT              NULL,
    [ARI_CreateDate]            SMALLDATETIME    CONSTRAINT [DF__ARI_Accou__ARI_C__7C7CC112] DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_ARI_RunDateYYYYMM__ARI_LineDescription]
    ON [Billing].[ARI_AccountReceivableInvoice]([ARI_RunDateYYYYMM] ASC, [ARI_LineDescription] ASC)
    INCLUDE([ARI_ParentName], [ARI_MID], [ARI_SourceID], [ARI_TranDate], [ARI_Volume], [ARI_Count], [ARI_ExtPrice]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_ARI_RunDateYYYYMM__ARI_LineDescription_INCLUDED]
    ON [Billing].[ARI_AccountReceivableInvoice]([ARI_RunDateYYYYMM] ASC, [ARI_LineDescription] ASC)
    INCLUDE([ARI_ParentName], [ARI_MID], [ARI_SourceID], [ARI_TranDate], [ARI_Volume], [ARI_Count], [ARI_UnitPrice]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_ARI_RunDateYYYYMM_ARI_LineDescription__WithIncludes]
    ON [Billing].[ARI_AccountReceivableInvoice]([ARI_RunDateYYYYMM] ASC, [ARI_LineDescription] ASC)
    INCLUDE([ARI_ParentName], [ARI_CustomerID], [ARI_ProcessingNbr], [ARI_ConfigNbr], [ARI_TranDate], [ARI_ExtPrice]) WITH (FILLFACTOR = 80);

