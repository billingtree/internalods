﻿-- =============================================
-- Author:		Yaseen Jamaludeen
-- Create date: 2015-06-01
-- Description:	Procedure to carry out Consolidated Billing Workflow
-- WARNING : This is to be used in the SSIS main workflow. Do not execute independently.
-- =============================================
CREATE PROCEDURE [Billing].[spCRMPreBillEditErrorANDMonthly] --'2016-06-25'
    (
      @RunDate DATE = NULL ,
      @RunType INT = 1		-- Defaulted to 1 for PreBill Edit/Error and 2 will be passed when run for CRM Monthly Billing
    )
AS
    BEGIN
        SET NOCOUNT ON;
        IF @RunDate IS NOT NULL
            BEGIN
                DECLARE @RunDateYearMonth INT = LEFT(REPLACE(CONVERT(CHAR(10), @RunDate),
                                                             '-', ''), 6);
                DECLARE @RunDateYear SMALLINT = YEAR(@RunDate);
                DECLARE @RunDateMonth TINYINT = MONTH(@RunDate);
                DECLARE @RunDateMonthBegin DATE = DATEADD(D, 1,
                                                          EOMONTH(@RunDate, -1));
                DECLARE @RunDateMonthEnd DATE = EOMONTH(@RunDate);

			--SELECT	@RunDate			AS RunDate
			--		,@RunDateYearMonth	AS RunDateYearMonth
			--		,@RunDateYear		AS RunDateYear
			--		,@RunDateMonth		AS RunDateMonth
			--		,@RunDateMonthBegin	AS RunDateMonthBegin
			--		,@RunDateMonthEnd	AS RunDateMonthEnd

			/*
			DELETE	Billing.CST_Customer
			--WHERE	CST_RunDateYYYYMM >= @RunDateYearMonth

			DELETE	Billing.CCL_CustomerContactList
			--WHERE	CST_RunDateYYYYMM >= @RunDateYearMonth

			DELETE	Billing.CPA_CustomerProcessingAccount
			--WHERE	CST_RunDateYYYYMM >= @RunDateYearMonth

			DELETE	Billing.CPP_CustomerProcessingProfile
			--WHERE	CST_RunDateYYYYMM >= @RunDateYearMonth

			DELETE	Billing.CTF_CustomerTranFee
			--WHERE	CST_RunDateYYYYMM >= @RunDateYearMonth

			SELECT * FROM IntegratedSourcesGLTest.Billing.CST_Customer
			SELECT * FROM IntegratedSourcesGLTest.Billing.CPA_CustomerProcessingAccount
			SELECT * FROM IntegratedSourcesGLTest.Billing.CPP_CustomerProcessingProfile
			*/



                DELETE  FROM Billing.CST_Customer
                WHERE   CST_RunDateYYYYMM = @RunDateYearMonth;

                DECLARE @MaxID AS INT = NULL;
                SELECT  @MaxID = ISNULL(MAX(CST_ID), 0)
                FROM    Billing.CST_Customer;
                DBCC CHECKIDENT ('Billing.CST_Customer', RESEED, @MaxID);

                SELECT  @MaxID = ISNULL(MAX(CCL_ID), 0)
                FROM    Billing.CCL_CustomerContactList;
                DBCC CHECKIDENT ('Billing.CCL_CustomerContactList', RESEED, @MaxID);

                SELECT  @MaxID = ISNULL(MAX(CPA_ID), 0)
                FROM    Billing.CPA_CustomerProcessingAccount;
                DBCC CHECKIDENT ('Billing.CPA_CustomerProcessingAccount', RESEED, @MaxID);

                SELECT  @MaxID = ISNULL(MAX(CPP_ID), 0)
                FROM    Billing.CPP_CustomerProcessingProfile;
                DBCC CHECKIDENT ('Billing.CPP_CustomerProcessingProfile', RESEED, @MaxID);

                SELECT  @MaxID = ISNULL(MAX(CTF_ID), 0)
                FROM    Billing.CTF_CustomerTranFee;
                DBCC CHECKIDENT ('Billing.CTF_CustomerTranFee', RESEED, @MaxID);

                SELECT  @MaxID = ISNULL(MAX(CDT_ID), 0)
                FROM    Billing.CDT_CustomerDailyTran;
                DBCC CHECKIDENT ('Billing.CDT_CustomerDailyTran', RESEED, @MaxID);



			-- Get Unique ProcessingProfileIDs and the month's first transaction date for all actitivities in ATI, USAePay and MeS
                DECLARE @ODSActivity TABLE
                    (
                      PP_Id UNIQUEIDENTIFIER NOT NULL ,
                      InitialTranDate DATE NOT NULL
                    );
                INSERT  INTO @ODSActivity
                        SELECT  PP_Id ,
                                MIN(InitialTranDate) AS InitialTranDate
                        FROM    ( SELECT    CRM_PPDID AS PP_Id ,
                                            MIN(TransDate) AS InitialTranDate
                                  FROM      USAePay.DailySummary
                                  WHERE     TransDateYYYYMM = @RunDateYearMonth
                                            AND UsabilityIndex = 1
                                            AND ( Sales > 0
                                                  OR Credits > 0
                                                  OR Declines > 0
                                                )
                                  GROUP BY  CRM_PPDID
                                  UNION
                                  SELECT    CRM_PPDID AS PP_Id ,
                                            MIN(BatchDate) AS InitialTranDate
                                  FROM      Billing.ACHTransByBatchDate
                                  WHERE     BatchDateYYYYMM = @RunDateYearMonth
                                            AND UsabilityIndex = 1
                                  GROUP BY  CRM_PPDID
						--UNION
						--SELECT	CRM_PPDID				AS PP_Id
						--		,MIN(TransactionDate)	AS InitialTranDate
						--FROM	MeS.SettlementSummary
						--WHERE	TransDateYYYYMM = @RunDateYearMonth
						--		AND UsabilityIndex = 1
						--GROUP BY CRM_PPDID
						--UNION
						--SELECT	CRM_PPDID				AS PP_Id
						--		,MIN(FileCreationDate)	AS InitialTranDate
						--FROM	NACHA.Originations
						--WHERE	FileCreationDateYYYYMM = @RunDateYearMonth
						--		AND UsabilityIndex = 1
						--GROUP BY CRM_PPDID
                                ) AS ActivityDates
                        GROUP BY PP_Id;

                DECLARE @CPP_for_CPA_CRMActivePP TABLE
                    (
                      CRMProcessingAccountID UNIQUEIDENTIFIER NOT NULL ,
                      CRMProcessingProfileID UNIQUEIDENTIFIER NOT NULL ,
                      MID NVARCHAR(50) NULL ,
                      SourceIdConfig NVARCHAR(100) NULL ,
                      StartDate DATETIME NULL ,
                      EndDate DATETIME NULL ,
                      GatewayOnly BIT NULL ,
                      CRMRecordStatus NVARCHAR(255) NULL ,
                      SourceStep NVARCHAR(255) NULL ,
                      InitialTranDateForMonth DATE NULL ,
                      CurrentAppStage NVARCHAR(255) NULL ,
                      QATrainingStageDate DATETIME NULL ,
                      CompleteStageDate DATETIME NULL ,
                      ProcessingProfileStartDate DATETIME NULL
                    );

                INSERT  INTO @CPP_for_CPA_CRMActivePP
                        ( CRMProcessingAccountID ,
                          CRMProcessingProfileID ,
                          MID ,
                          SourceIdConfig ,
                          StartDate ,
                          EndDate ,
                          GatewayOnly ,
                          CRMRecordStatus ,
                          SourceStep
				        )
			--------------------------------------------------------------------
			--    Find additional Processing Profile which are "Active"
			--    and are not in the previously identified CPP records
			--------------------------------------------------------------------
                        SELECT  ISNULL(bt_processingaccount,
                                       '00000000-0000-0000-0000-000000000000') AS CRMProcessingAccountID ,
                                bt_processingprofileid AS CRMProcessingProfileID ,
                                bt_bankmid AS MID ,
                                CASE WHEN bt_productlinename = 'ACH'
                                     THEN bt_config
                                     ELSE bt_sourceid
                                END AS SourceIdConfig ,
                                bt_startdate AS StartDate ,
                                bt_enddate AS EndDate ,
                                bt_gatewayonly AS GatewayOnly ,
                                statuscodename AS CRMRecordStatus ,
                                'CRM Active PP' AS SourceStep
                        FROM    CRM.bt_processingprofile
                        WHERE   ISNUMERIC(bt_bankmid) = 1
                                AND bt_processingaccount IS NOT NULL
                                AND statuscodename = 'Active'
                                AND ( bt_enddate IS NULL
                                      OR ( YEAR(bt_enddate) > @RunDateYear
                                           OR ( YEAR(bt_enddate) = @RunDateYear
                                                AND MONTH(bt_enddate) >= @RunDateMonth
                                              )
                                         )
                                    )
                                AND bt_processingprofileid NOT IN ( SELECT
                                                              PP_Id
                                                              FROM
                                                              @ODSActivity )
                                AND owneridname NOT IN ( 'CRM Dev' );


                DECLARE @CPP_for_CPA TABLE
                    (
                      CPP_CRMProcessingAccountID UNIQUEIDENTIFIER NOT NULL ,
                      CPP_CRMProcessingProfileID UNIQUEIDENTIFIER NOT NULL ,
                      CPP_MID NVARCHAR(50) NULL ,
                      CPP_SourceIdConfig NVARCHAR(100) NULL ,
                      CPP_StartDate DATETIME NULL ,
                      CPP_EndDate DATETIME NULL ,
                      CPP_GatewayOnly BIT NULL ,
                      CPP_CRMRecordStatus NVARCHAR(255) NULL ,
                      CPP_SourceStep NVARCHAR(255) NULL ,
                      CPP_InitialTranDateForMonth DATE NULL ,
                      CPP_CurrentAppStage NVARCHAR(255) NULL ,
                      CPP_QATrainingStageDate DATETIME NULL ,
                      CPP_CompleteStageDate DATETIME NULL ,
                      CPP_ProcessingProfileStartDate DATETIME NULL
                    );

                INSERT  INTO @CPP_for_CPA
			---------------------------------------------------------------------------
			--        Using the ODS Transactions as the driver 
			--    Then, use this to drive CPA and ultimately CST
			----------------------------------------------------------------------------
                        SELECT  ISNULL(PP.bt_processingaccount,
                                       '00000000-0000-0000-0000-000000000000') AS CPP_CRMProcessingAccountID ,
                                PP.bt_processingprofileid AS CPP_CRMProcessingProfileID ,
                                PP.bt_bankmid AS CPP_MID ,
                                CASE WHEN PP.bt_productlinename = 'ACH'
                                     THEN PP.bt_config
                                     ELSE PP.bt_sourceid
                                END AS CPP_SourceIdConfig ,
                                PP.bt_startdate AS CPP_StartDate ,
                                PP.bt_enddate AS CPP_EndDate ,
                                PP.bt_gatewayonly AS CPP_GatewayOnly ,
                                PP.statuscodename AS CPP_CRMRecordStatus ,
                                'ODS Activity' AS CPP_SourceStep ,
                                Activity.InitialTranDate AS CPP_InitialTranDateForMonth ,
                                NULL AS CPP_CurrentAppStage ,
                                NULL AS CPP_QATrainingStageDate ,
                                NULL AS CPP_CompleteStageDate ,
                                NULL AS CPP_ProcessingProfileStartDate
                        FROM    @ODSActivity AS Activity
                                LEFT JOIN CRM.bt_processingprofile AS PP ON Activity.PP_Id = PP.bt_processingprofileid
                        WHERE   PP.owneridname NOT IN ( 'CRM Dev' )
                        UNION ALL

			--------------------------------------------------------------------
			--    Find additional Processing Profile which are "Active"
			--    and are not in the previously identified CPP records
			--------------------------------------------------------------------
                        SELECT  CRMProcessingAccountID AS CPP_CRMProcessingAccountID ,
                                CRMProcessingProfileID AS CPP_CRMProcessingProfileID ,
                                MID AS CPP_MID ,
                                SourceIdConfig AS CPP_SourceIdConfig ,
                                ISNULL(SH.QATrainingStageDate,
                                       ISNULL(SH.CompleteStageDate,
                                              CPP.StartDate)) AS CPP_StartDate ,
                                EndDate AS CPP_EndDate ,
                                GatewayOnly AS CPP_GatewayOnly ,
                                CRMRecordStatus AS CPP_CRMRecordStatus ,
                                SourceStep AS CPP_SourceStep ,
                                InitialTranDateForMonth AS CPP_InitialTranDateForMonth ,
                                SO.bt_applicationstagename AS CPP_CurrentAppStage ,
                                SH.QATrainingStageDate AS CPP_QATrainingStageDate ,
                                SH.CompleteStageDate AS CPP_CompleteStageDate ,
                                CPP.StartDate AS CPP_ProcessingProfileStartDate
                        FROM    @CPP_for_CPA_CRMActivePP AS CPP
                                LEFT JOIN CRM.bt_processingaccount AS PA ON CPP.CRMProcessingAccountID = PA.bt_processingaccountid
                                LEFT JOIN CRM.bt_serviceorder AS SO ON PA.bt_currentserviceorder = SO.bt_serviceorderid
                                LEFT JOIN ( SELECT  ISNULL(C.bt_serviceorder,
                                                           Q.bt_serviceorder) AS bt_serviceorder ,
                                                    Q.QATrainingStageDate ,
                                                    C.CompleteStageDate
                                            FROM    ( SELECT  bt_serviceorder ,
                                                              MAX(bt_stamp) AS QATrainingStageDate
                                                      FROM    CRM.bt_serviceorderhistory
                                                      WHERE   bt_newlabel = 'QA & Training'
                                                              AND bt_serviceorder IS NOT NULL
                                                      GROUP BY bt_serviceorder
                                                    ) AS Q
                                                    FULL OUTER JOIN ( SELECT
                                                              bt_serviceorder ,
                                                              MAX(bt_stamp) AS CompleteStageDate
                                                              FROM
                                                              CRM.bt_serviceorderhistory
                                                              WHERE
                                                              bt_newlabel = 'Complete'
                                                              AND bt_serviceorder IS NOT NULL
                                                              GROUP BY bt_serviceorder
                                                              ) AS C ON Q.bt_serviceorder = C.bt_serviceorder
                                          ) AS SH ON SO.bt_serviceorderid = SH.bt_serviceorder
                        WHERE   CONVERT(DATE, ISNULL(SH.QATrainingStageDate,
                                                     ISNULL(SH.CompleteStageDate,
                                                            StartDate))) !> @RunDateMonthEnd;




			--select count(*) '@CPP_for_CPA' from @CPP_for_CPA

			------------------------------------------------------------------------
			--       Using CPP created first, as the driver to POPULATE CPA
			-------------------------------------------------------------------------
                DECLARE @CPA_for_CST TABLE
                    (
                      CPA_CRMAccountID UNIQUEIDENTIFIER NOT NULL ,
                      CPA_CRMProcessingAccountID UNIQUEIDENTIFIER NOT NULL ,
                      CPA_ManualBillStartDate DATETIME NULL ,
                      CPA_ManualBillEndDate DATETIME NULL ,
                      CPA_ManualBillYN CHAR(1) NULL ,
                      CPA_TrustName NVARCHAR(100) NULL ,
                      CPA_ProductLine NVARCHAR(100) NULL ,
                      CPA_CRMRecordStatus NVARCHAR(255) NULL ,
                      CPA_BankAccountNumber NVARCHAR(20) NULL ,
                      CPA_ABANumber NVARCHAR(20) NULL ,
                      CPA_AccountNumberSuffix CHAR(5) NULL ,
                      CPA_DoNotBillStartDate DATETIME NULL ,
                      CPA_DoNotBillEndDate DATETIME NULL ,
                      CPA_DoNotBillYN CHAR(1) NULL
                    );
                INSERT  INTO @CPA_for_CST
                        SELECT	DISTINCT
                                ISNULL(PA.bt_account,
                                       '00000000-0000-0000-0000-000000000000') --  There is 1 Processing Account without an Account in the CRM
																			AS CRMAccountID ,
                                PA.bt_processingaccountid AS CPA_CRMProcessingAccountID ,
                                PA.bt_manualbillingstartdate AS CPA_ManualBillStartDate ,
                                PA.bt_manualbillingenddate AS CPA_ManualBillStartDate ,
                                CASE WHEN ( YEAR(PA.bt_manualbillingstartdate) < @RunDateYear
                                            OR ( YEAR(PA.bt_manualbillingstartdate) = @RunDateYear
                                                 AND MONTH(PA.bt_manualbillingstartdate) <= @RunDateMonth
                                               )
                                          )
                                          AND ( PA.bt_manualbillingenddate IS NULL
                                                OR ( YEAR(PA.bt_manualbillingenddate) > @RunDateYear
                                                     OR ( YEAR(PA.bt_manualbillingenddate) = @RunDateYear
                                                          AND MONTH(PA.bt_manualbillingenddate) >= @RunDateMonth
                                                        )
                                                   )
                                              ) THEN 'Y'
                                     ELSE 'N'
                                END AS CPA_ManualBillYN ,
                                PA.bt_name AS CPA_TrustName ,
                                PA.bt_productlinename AS CPA_ProductLine ,
                                PA.statuscodename AS CPA_CRMRecordStatus ,
                                PA.bt_accountnumber AS CPA_BankAccountNumber ,
                                PA.bt_abanumber AS CPA_ABANumber ,
                                CONCAT('-', RIGHT(PA.bt_accountnumber, 4)) AS CPA_AccountNumberSuffix ,
                                PA.bt_donotbillstartdate AS CPA_DoNotBillStartDate ,
                                PA.bt_donotbillenddate AS CPA_DoNotBillEndDate ,
                                CASE WHEN ( YEAR(PA.bt_donotbillstartdate) < @RunDateYear
                                            OR ( YEAR(PA.bt_donotbillstartdate) = @RunDateYear
                                                 AND MONTH(PA.bt_donotbillstartdate) <= @RunDateMonth
                                               )
                                          )
                                          AND ( PA.bt_donotbillenddate IS NULL
                                                OR ( YEAR(PA.bt_donotbillenddate) > @RunDateYear
                                                     OR ( YEAR(PA.bt_donotbillenddate) = @RunDateYear
                                                          AND MONTH(PA.bt_donotbillenddate) >= @RunDateMonth
                                                        )
                                                   )
                                              ) THEN 'Y'
                                     ELSE 'N'
                                END AS CPA_DoNotBillYN
                        FROM    @CPP_for_CPA AS CPP
                                LEFT JOIN CRM.bt_processingaccount AS PA ON CPP.CPP_CRMProcessingAccountID = PA.bt_processingaccountid
                        WHERE   PA.owneridname NOT IN ( 'CRM Dev' );
			--SELECT	COUNT(*) '@CPA_for_CST' FROM @CPA_for_CST
			----------------------------------------------------------------------------------------------
			--           Using CPA as the driver populate and INSERT Customers
			----------------------------------------------------------------------------------------------
                INSERT  INTO Billing.CST_Customer
                        ( CST_RunDateYYYYMM ,
                          CST_CRMAccountID ,
                          CST_ParentAccountNumber ,
                          CST_AccountNumber ,
                          CST_AccountName ,
                          CST_SubType ,
                          CST_Status ,
                          CST_SalesRepFullName ,
                          CST_SalesRepFirstName ,
                          CST_SalesRepLastName ,
                          CST_ContactAddressString
                        )
                        SELECT  @RunDateYearMonth AS CST_RunDateYYYYMM ,
                                ThePA.CPA_CRMAccountID AS CST_CRMAccountID ,
                                NULL AS CST_ParentAccountNumber ,
                                Acc.accountnumber AS CST_AccountNumber ,
                                Acc.name AS CST_AccountName ,
                                Acc.bt_subtypename AS CST_SubType ,
                                Acc.bt_statusname AS CST_Status ,
                                SU.fullname AS CST_SalesRepFullName ,
                                SU.firstname AS CST_SalesRepFirstName ,
                                SU.lastname AS CST_SalesRepLastName ,
                                NULL AS CST_ContactAddressString
                        FROM    ( SELECT	DISTINCT
                                            CPA_CRMAccountID
                                  FROM      @CPA_for_CST
                                  WHERE     CPA_CRMAccountID <> '00000000-0000-0000-0000-000000000000'
                                ) AS ThePA
                                LEFT JOIN CRM.account AS Acc ON ThePA.CPA_CRMAccountID = Acc.accountid   -- 1 Account is not found
                                LEFT JOIN CRM.systemuser AS SU ON Acc.ownerid = SU.systemuserid
                        WHERE   Acc.owneridname NOT IN ( 'CRM Dev' );
			--select count(*) as CST_Customer from Billing.CST_Customer

			----------------------------------------------------------------------------------------------
			--           INSERT the already populated CPA records with CST references
			----------------------------------------------------------------------------------------------
                INSERT  INTO Billing.CPA_CustomerProcessingAccount
                        ( CPA_RunDateYYYYMM ,
                          CPA_CST_ID ,
                          CPA_CRMAccountID ,
                          CPA_CRMProcessingAccountID ,
                          CPA_ManualBillStartDate ,
                          CPA_ManualBillEndDate ,
                          CPA_ManualBillYN ,
                          CPA_TrustName ,
                          CPA_ProductLine ,
                          CPA_CRMRecordStatus ,
                          CPA_BankAccountNumber ,
                          CPA_ABANumber ,
                          CPA_AccountNumberSuffix ,
                          CPA_DoNotBillStartDate ,
                          CPA_DoNotBillEndDate ,
                          CPA_DoNotBillYN
                        )
                        SELECT  CST.CST_RunDateYYYYMM ,
                                CST_ID ,
                                CPA.CPA_CRMAccountID ,
                                CPA.CPA_CRMProcessingAccountID ,
                                CPA.CPA_ManualBillStartDate ,
                                CPA.CPA_ManualBillEndDate ,
                                CPA.CPA_ManualBillYN ,
                                CPA.CPA_TrustName ,
                                CPA.CPA_ProductLine ,
                                CPA.CPA_CRMRecordStatus ,
                                CPA.CPA_BankAccountNumber ,
                                CPA.CPA_ABANumber ,
                                CPA.CPA_AccountNumberSuffix ,
                                CPA.CPA_DoNotBillStartDate ,
                                CPA.CPA_DoNotBillEndDate ,
                                CPA.CPA_DoNotBillYN
                        FROM    @CPA_for_CST AS CPA
                                LEFT JOIN Billing.CST_Customer AS CST ON CPA.CPA_CRMAccountID = CST.CST_CRMAccountID
                                                              AND CST.CST_RunDateYYYYMM = @RunDateYearMonth;
			--select count(*) as CPA_CustomerProcessingAccount from Billing.CPA_CustomerProcessingAccount

			----------------------------------------------------------------------------------------------
			--           INSERT the already populated CPP records with CPA references
			----------------------------------------------------------------------------------------------
                INSERT  INTO Billing.CPP_CustomerProcessingProfile
                        ( CPP_RunDateYYYYMM ,
                          CPP_CPA_ID ,
                          CPP_CRMProcessingAccountID ,
                          CPP_CRMProcessingProfileID ,
                          CPP_MID ,
                          CPP_SourceIdConfig ,
                          CPP_StartDate ,
                          CPP_EndDate ,
                          CPP_GatewayOnly ,
                          CPP_CRMRecordStatus ,
                          CPP_SourceStep ,
                          CPP_InitialTranDateForMonth ,
                          CPP_CurrentAppStage ,
                          CPP_QATrainingStageDate ,
                          CPP_CompleteStageDate ,
                          CPP_ProcessingProfileStartDate
                        )
                        SELECT  CPA.CPA_RunDateYYYYMM ,
                                CPA.CPA_ID ,
                                CPP.CPP_CRMProcessingAccountID ,
                                CPP.CPP_CRMProcessingProfileID ,
                                CPP.CPP_MID ,
                                CPP.CPP_SourceIdConfig ,
                                CPP.CPP_StartDate ,
                                CPP.CPP_EndDate ,
                                CPP.CPP_GatewayOnly ,
                                CPP.CPP_CRMRecordStatus ,
                                CPP.CPP_SourceStep ,
                                CPP.CPP_InitialTranDateForMonth ,
                                CPP.CPP_CurrentAppStage ,
                                CPP.CPP_QATrainingStageDate ,
                                CPP.CPP_CompleteStageDate ,
                                CPP.CPP_ProcessingProfileStartDate
                        FROM    @CPP_for_CPA AS CPP
                                LEFT JOIN Billing.CPA_CustomerProcessingAccount
                                AS CPA ON CPP.CPP_CRMProcessingAccountID = CPA.CPA_CRMProcessingAccountID
                                          AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth;
			--select count(*) as CPP_CustomerProcessingProfile from Billing.CPP_CustomerProcessingProfile



			-------------------------------------------------------------------------
			--       Using CPA as the driver to populate CTF with all relevant fees
			-------------------------------------------------------------------------
                INSERT  INTO Billing.CTF_CustomerTranFee
                        ( CTF_RunDateYYYYMM ,
                          CTF_CPA_ID ,
                          CTF_CRMServiceProductFeeID ,
                          CTF_ProductName ,
                          CTF_FeeType ,
                          CTF_FeeAmount ,
                          CTF_StartDate ,
                          CTF_EndDate ,
                          CTF_EffectiveStartDateForMonth ,
                          CTF_EffectiveEndDateForMonth ,
                          CTF_DaysInEffectForMonth
                        )
                        SELECT  @RunDateYearMonth AS CTF_RunDateYYYYMM ,
                                CPA.CPA_ID AS CTF_CPA_ID ,
                                SPF.bt_serviceproductfeeid AS CTF_CRMServiceProductFeeID ,
                                PR.name AS CTF_ProductName ,
                                SPF.bt_feetypename AS CTF_FeeType ,
                                SPF.bt_amount AS CTF_FeeAmount ,
                                SPF.bt_startdate AS CTF_StartDate ,
                                SPF.bt_enddate AS CTF_EndDate ,
                                CASE WHEN SPF.bt_startdate < @RunDateMonthBegin
                                     THEN @RunDateMonthBegin
                                     WHEN SPF.bt_startdate >= @RunDateMonthBegin
                                     THEN CONVERT(DATE, SPF.bt_startdate)
                                END AS CTF_EffectiveStartDateForMonth ,
                                CASE WHEN SPF.bt_enddate IS NULL
                                     THEN @RunDateMonthEnd
                                     WHEN SPF.bt_enddate < @RunDateMonthBegin
                                     THEN CONVERT(DATE, SPF.bt_enddate)
                                     WHEN SPF.bt_enddate BETWEEN @RunDateMonthBegin
                                                         AND  @RunDateMonthEnd
                                     THEN CONVERT(DATE, SPF.bt_enddate)
                                     WHEN SPF.bt_enddate > @RunDateMonthEnd
                                     THEN @RunDateMonthEnd
                                END AS CTF_EffectiveEndDateForMonth ,
                                DATEDIFF(DAY,
                                         CASE WHEN SPF.bt_startdate < @RunDateMonthBegin
                                              THEN @RunDateMonthBegin
                                              WHEN SPF.bt_startdate >= @RunDateMonthBegin
                                              THEN CONVERT(DATE, SPF.bt_startdate)
                                         END--CTF_EffectiveStartDateForMonth
									,
                                         CASE WHEN SPF.bt_enddate IS NULL
                                              THEN @RunDateMonthEnd
                                              WHEN SPF.bt_enddate < @RunDateMonthBegin
                                              THEN CONVERT(DATE, SPF.bt_enddate)
                                              WHEN SPF.bt_enddate BETWEEN @RunDateMonthBegin
                                                              AND
                                                              @RunDateMonthEnd
                                              THEN CONVERT(DATE, SPF.bt_enddate)
                                              WHEN SPF.bt_enddate > @RunDateMonthEnd
                                              THEN @RunDateMonthEnd
                                         END--CTF_EffectiveEndDateForMonth
								) + 1 AS CTF_DaysInEffectForMonth
                        FROM    Billing.CPA_CustomerProcessingAccount AS CPA
                                INNER JOIN CRM.bt_serviceproductfee AS SPF ON CPA.CPA_CRMProcessingAccountID = SPF.bt_processingaccount
                                INNER JOIN CRM.product AS PR ON SPF.bt_product = PR.productid
                                                              AND SPF.statuscodename = 'Active'
                                                              AND SPF.bt_feetypename IN (
                                                              'BillingTree Monthly Fee'-- Monthly Fees
														,
                                                              'BillingTree Gateway Submission Fee'-- Gateway Fees
														, 'Return Fee'-- Return Fees
														,
                                                              'Un-Authorized Return Fee'-- Un-Authorized Return Fees
														,
                                                              'Per Batch Fee/Offset' )				-- Batch Fees
                                                              AND ( PR.name NOT LIKE '%Credit%'
                                                              OR SPF.bt_feetypename != 'Un-Authorized Return Fee'
                                                              )
                                                              AND SPF.owneridname NOT IN (
                                                              'CRM Dev' )
                                                              AND ISNULL(SPF.bt_productfeename,
                                                              '') NOT LIKE '%GCS FastPay%'
                                                              AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                              AND SPF.bt_startdate <= @RunDateMonthEnd
                                                              AND ISNULL(SPF.bt_enddate,
                                                              @RunDateMonthBegin) >= @RunDateMonthBegin;

			-------------------------------------------------------------------------------------------------------------------------
			--           Using CST as the driver here, Get each Accounts collection of Contacts by the Role of Billing Contact
			-------------------------------------------------------------------------------------------------------------------------
                INSERT  INTO Billing.CCL_CustomerContactList
                        ( CCL_RunDateYYYYMM ,
                          CCL_CST_ID ,
                          CCL_CRMContactID ,
                          CCL_eMailAddress
                        )
                        SELECT  @RunDateYearMonth AS CCL_RunDateYYYYMM ,
                                CST.CST_ID AS CCL_CST_ID ,
                                C.contactid AS CCL_CRMContactID ,
                                C.emailaddress1 AS CCL_eMailAddress
                        FROM    Billing.CST_Customer AS CST
                                INNER JOIN CRM.contact AS C ON CST.CST_CRMAccountID = C.parentcustomerid
                                INNER JOIN CRM.bt_contactrole_contact AS CRC ON C.contactid = CRC.contactid
                                INNER JOIN CRM.bt_contactrole AS CR ON CRC.bt_contactroleid = CR.bt_contactroleid
                                                              AND CR.bt_name = 'billing contact'
                                                              AND C.owneridname NOT IN (
                                                              'CRM Dev' );
			----------------------------------------------------------------------------
			--          Update the CST with this Address String based on CCL
			----------------------------------------------------------------------------
                UPDATE  CST
                SET     CST_ContactAddressString = SUBSTRING(( SELECT
                                                              ';'
                                                              + CCL1.CCL_eMailAddress AS [text()]
                                                              FROM
                                                              Billing.CCL_CustomerContactList
                                                              AS CCL1
                                                              WHERE
                                                              CCL1.CCL_CST_ID = CCL2.CCL_CST_ID
                                                              AND CCL1.CCL_RunDateYYYYMM = CCL2.CCL_RunDateYYYYMM
                                                              ORDER BY CCL1.CCL_CST_ID
                                                             FOR
                                                              XML
                                                              PATH('')
                                                             ), 2, 1000)
                FROM    Billing.CCL_CustomerContactList AS CCL2
                        INNER JOIN Billing.CST_Customer AS CST ON CCL2.CCL_CST_ID = CST.CST_ID
                                                              AND CST.CST_RunDateYYYYMM = @RunDateYearMonth
                                                              AND CCL2.CCL_RunDateYYYYMM = @RunDateYearMonth;
























			/*


			SELECT * FROM IntegratedSourcesGLTest.Billing.PSL_ProcessStatusLog
			SELECT * FROM IntegratedSourcesGLTest.Billing.PRL_ProcessRunList
			SELECT * FROM IntegratedSourcesGLTest.Billing.PIL_ProcessIssueList
			SELECT * FROM IntegratedSourcesGLTest.Billing.PED_ProcessErrorDetail

			--TRUNCATE TABLE IntegratedSourcesGLTest.Billing.PRL_ProcessRunList
			TRUNCATE TABLE IntegratedSourcesGLTest.Billing.PED_ProcessErrorDetail
			DELETE IntegratedSourcesGLTest.Billing.PIL_ProcessIssueList
			DELETE IntegratedSourcesGLTest.Billing.PRL_ProcessRunList
			DELETE IntegratedSourcesGLTest.Billing.PSL_ProcessStatusLog

			--DBCC CHECKIDENT ('Billing.PIL_ProcessIssueList', RESEED, 0)
			DBCC CHECKIDENT ('Billing.PED_ProcessErrorDetail', RESEED, 1)
			DBCC CHECKIDENT ('Billing.PRL_ProcessRunList', RESEED, 0)
			DBCC CHECKIDENT ('Billing.PSL_ProcessStatusLog', RESEED, 0)

			*/


			/*

			SELECT	PED_ID
			FROM		Billing.PED_ProcessErrorDetail AS PED
						INNER JOIN Billing.PIL_ProcessIssueList AS PIL
							ON PED.PED_PIL_ID = PIL.PIL_ID
						INNER JOIN Billing.PRL_ProcessRunList AS PRL
							ON PIL.PIL_PRL_ID = PRL.PRL_ID
						INNER JOIN Billing.PSL_ProcessStatusLog AS PSL
							ON PRL.PRL_PSL_ID = PSL.PSL_ID
							AND PSL.PSL_RunDateYYYYMM = 201504


			DELETE	PED
			FROM	Billing.PED_ProcessErrorDetail AS PED
						INNER JOIN Billing.PIL_ProcessIssueList AS PIL
							ON PED.PED_PIL_ID = PIL.PIL_ID
						INNER JOIN Billing.PRL_ProcessRunList AS PRL
							ON PIL.PIL_PRL_ID = PRL.PRL_ID
						INNER JOIN Billing.PSL_ProcessStatusLog AS PSL
							ON PRL.PRL_PSL_ID = PSL.PSL_ID
							AND PSL.PSL_RunDateYYYYMM = 201505

			DECLARE @MaxPED AS int
			SELECT	@MaxPED=ISNULL(MAX(PED_ID),1)
			FROM	Billing.PED_ProcessErrorDetail
			DBCC CHECKIDENT ('Billing.PED_ProcessErrorDetail', RESEED, @MaxPED)

			DELETE	PIL
			FROM	Billing.PIL_ProcessIssueList AS PIL
						INNER JOIN Billing.PRL_ProcessRunList AS PRL
							ON PIL.PIL_PRL_ID = PRL.PRL_ID
						INNER JOIN Billing.PSL_ProcessStatusLog AS PSL
							ON PRL.PRL_PSL_ID = PSL.PSL_ID
							AND PSL.PSL_RunDateYYYYMM = 201505

			DELETE	PRL
			FROM	Billing.PRL_ProcessRunList AS PRL
						INNER JOIN Billing.PSL_ProcessStatusLog AS PSL
							ON PRL.PRL_PSL_ID = PSL.PSL_ID
							AND PSL.PSL_RunDateYYYYMM = 201505

			DECLARE @MaxPRL AS int
			SELECT	@MaxPRL=ISNULL(MAX(PRL_ID),1)
			FROM	Billing.PRL_ProcessRunList
			DBCC CHECKIDENT ('Billing.PRL_ProcessRunList', RESEED, @MaxPRL)


			DELETE	PSL
			FROM	Billing.PSL_ProcessStatusLog AS PSL
					WHERE PSL.PSL_RunDateYYYYMM = 201505

			DECLARE @MaxPSL AS int
			SELECT	@MaxPSL=ISNULL(MAX(PSL_ID),1)
			FROM	Billing.PSL_ProcessStatusLog
			DBCC CHECKIDENT ('Billing.PSL_ProcessStatusLog', RESEED, @MaxPSL)


			*/
			--DECLARE @MaxPSL AS int
			--SELECT	@MaxPSL=ISNULL(MAX(PSL_ID),0)
			--FROM	Billing.PSL_ProcessStatusLog
			--DBCC CHECKIDENT ('Billing.PSL_ProcessStatusLog', RESEED, @MaxPSL)


			--DECLARE @RunDateYearMonth AS int = 201501

                DELETE  PED
                FROM    Billing.PED_ProcessErrorDetail AS PED
                        INNER JOIN Billing.PIL_ProcessIssueList AS PIL ON PED.PED_PIL_ID = PIL.PIL_ID
                        INNER JOIN Billing.PRL_ProcessRunList AS PRL ON PIL.PIL_PRL_ID = PRL.PRL_ID
                        INNER JOIN Billing.PSL_ProcessStatusLog AS PSL ON PRL.PRL_PSL_ID = PSL.PSL_ID
                                                              AND PSL_PRT_ID = @RunType
                                                              AND PSL.PSL_RunDateYYYYMM = @RunDateYearMonth;

                DECLARE @MaxPED AS INT;
                SELECT  @MaxPED = ISNULL(MAX(PED_ID), 1)
                FROM    Billing.PED_ProcessErrorDetail;
                DBCC CHECKIDENT ('Billing.PED_ProcessErrorDetail', RESEED, @MaxPED);


                DECLARE @MaxPIL AS INT;
                SELECT  @MaxPIL = ISNULL(MAX(PIL_ID), 0)
                FROM    Billing.PIL_ProcessIssueList;

                IF OBJECT_ID('TempDB..#PED_ProcessErrorDetail') IS NOT NULL
                    DROP TABLE #PED_ProcessErrorDetail;
                CREATE TABLE #PED_ProcessErrorDetail
                    (
                      PIL_PIT_ID INT NULL ,
                      PED_PIL_ID INT NULL ,
                      PED_CPP_ID INT NULL ,
                      PED_CPA_ID INT NULL ,
                      PED_CST_ID INT NULL ,
                      PED_CTF_ID INT NULL
                    );

                INSERT  INTO #PED_ProcessErrorDetail
                        ( PIL_PIT_ID ,
                          PED_PIL_ID ,
                          PED_CPP_ID ,
                          PED_CPA_ID ,
                          PED_CST_ID ,
                          PED_CTF_ID
                        )
                        SELECT  PIT_ID ,
                                @MaxPIL
                                + ( DENSE_RANK() OVER ( ORDER BY PIT_ID ) ) AS PIL_ID ,
                                CPP_ID ,
                                CPA_ID ,
                                CST_ID ,
                                CTF_ID
                        FROM    (
						--**********************LEAVE ALONE FOR issue 9
						-- Records in ODS Quarantine
                                  SELECT    101 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  WHERE     EXISTS ( SELECT	DISTINCT
                                                            1
                                                     FROM   USAePay.DailySummary
                                                     WHERE  TransDateYYYYMM = @RunDateYearMonth
                                                            AND UsabilityIndex = 0
                                                            AND ( Sales > 0
                                                              OR Credits > 0
                                                              OR Declines > 0
                                                              )--Voids > 0)
                                                     UNION
                                                     SELECT	DISTINCT
                                                            1
                                                     FROM   MeS.SettlementSummary
                                                     WHERE  TransDateYYYYMM = @RunDateYearMonth
                                                            AND UsabilityIndex = 0
                                                     UNION
                                                     SELECT	DISTINCT
                                                            1
                                                     FROM   Billing.ACHTransByBatchDate
                                                     WHERE  BatchDateYYYYMM = @RunDateYearMonth
                                                            AND UsabilityIndex = 0
                                                     UNION
                                                     SELECT	DISTINCT
                                                            1
                                                     FROM   Billing.ACHReturnsByReturnDate
                                                     WHERE  ReturnDateYYYYMM = @RunDateYearMonth
                                                            AND UsabilityIndex = 0 )
											--UNION
											--SELECT	DISTINCT 1
											--FROM	NACHA.Originations
											--WHERE	FileCreationDateYYYYMM = @RunDateYearMonth
											--		AND UsabilityIndex = 0
											--UNION
											--SELECT	DISTINCT 1
											--FROM	NACHA.Returns
											--WHERE	FileCreationDateYYYYMM = @RunDateYearMonth
											--		AND UsabilityIndex = 0)
                                  UNION ALL
                                  SELECT	--,'Missing Bank Account #s'                                                             AS TFR_IssueName
                                            102 AS PIT_ID    -- don’t need this for PED, rather we need the PIL, which you don’t have until you know you have this Issue ocurring
                                            ,
                                            NULL AS CPP_ID ,
                                            CPA.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPA_CustomerProcessingAccount
                                            AS CPA
                                  WHERE     ( ISNULL(CPA.CPA_BankAccountNumber,
                                                     '') = ''
                                              OR ISNULL(CPA.CPA_ABANumber, '') = ''
                                            )
                                            AND ISNULL(CPA.CPA_ManualBillYN,
                                                       'N') = 'N'
                                            AND ISNULL(CPA.CPA_DoNotBillYN,
                                                       'N') = 'N'
                                            AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'PA with no Account'    AS TFR_IssueName
                                            103 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            CPA.CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPA_CustomerProcessingAccount
                                            AS CPA
                                  WHERE     CPA.CPA_CRMAccountID = '00000000-0000-0000-0000-000000000000'
                                            AND ISNULL(CPA.CPA_ManualBillYN,
                                                       'N') = 'N'
                                            AND ISNULL(CPA.CPA_DoNotBillYN,
                                                       'N') = 'N'
                                            AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
						--**********************LEAVE ALONE FOR issue 9
                                  SELECT	--,'PP with no PA'					AS TFR_IssueName
                                            104 AS PIT_ID ,
                                            CPP.CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPP_CustomerProcessingProfile
                                            AS CPP
                                            LEFT JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                                  WHERE     CPP.CPP_CRMProcessingAccountID = '00000000-0000-0000-0000-000000000000'
								--CPA.CPA_ID IS NULL
                                            AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	DISTINCT --,'Multiple Active PPs per PA'          AS TFR_IssueName  -- COULD BE SIMPLIFIED/REFACTORED
                                            105 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            A.CPP_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPP_CPA_ID	-- PPs of DupePAs
                                                        ,
                                                        CPP_ID ,
                                                        CPP_StartDate ,
                                                        ISNULL(CPP_EndDate,
                                                              GETDATE()) AS CPP_EndDate
                                              FROM      Billing.CPP_CustomerProcessingProfile
                                              WHERE     CPP_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPP_CPA_ID IN (	--DupePAs
                                                        SELECT
                                                              PP.CPP_CPA_ID AS PA_ID
                                                        FROM  Billing.CPP_CustomerProcessingProfile
                                                              AS PP
                                                              LEFT JOIN Billing.CPA_CustomerProcessingAccount
                                                              AS CPA ON PP.CPP_CPA_ID = CPA.CPA_ID
                                                        WHERE ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                              AND PP.CPP_RunDateYYYYMM = @RunDateYearMonth
																			--AND (PP.CPP_EndDate IS NULL
																			--	OR LEFT(REPLACE(CONVERT(char(10),CONVERT(date,PP.CPP_EndDate)),'-',''),6) = @RunDateYearMonth
																			--	)
                                                        GROUP BY CPP_CPA_ID
                                                        HAVING
                                                              COUNT(*) > 1 )
                                            ) A
                                            INNER JOIN ( SELECT
                                                              CPP_CPA_ID	-- PPs of DupePAs
                                                              ,
                                                              CPP_ID ,
                                                              CPP_StartDate ,
                                                              ISNULL(CPP_EndDate,
                                                              GETDATE()) AS CPP_EndDate
                                                         FROM Billing.CPP_CustomerProcessingProfile
                                                         WHERE
                                                              CPP_RunDateYYYYMM = @RunDateYearMonth
                                                              AND CPP_CPA_ID IN (	--DupePAs
                                                              SELECT
                                                              PP.CPP_CPA_ID AS PA_ID
                                                              FROM
                                                              Billing.CPP_CustomerProcessingProfile
                                                              AS PP
                                                              LEFT JOIN Billing.CPA_CustomerProcessingAccount
                                                              AS CPA ON PP.CPP_CPA_ID = CPA.CPA_ID
                                                              WHERE
                                                              ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                              AND PP.CPP_RunDateYYYYMM = @RunDateYearMonth
																							--AND (PP.CPP_EndDate IS NULL
																							--	OR LEFT(REPLACE(CONVERT(char(10),CONVERT(date,PP.CPP_EndDate)),'-',''),6) = @RunDateYearMonth
																							--	)
                                                              GROUP BY CPP_CPA_ID
                                                              HAVING
                                                              COUNT(*) > 1 )
                                                       ) B ON A.CPP_CPA_ID = B.CPP_CPA_ID	-- A Self Join of DupePA's PP details
                                                              AND A.CPP_ID <> B.CPP_ID
                                                              AND ( B.CPP_StartDate <= A.CPP_EndDate )
                                                              AND ( B.CPP_EndDate >= A.CPP_StartDate )
						-- After fixing Bug 9950 - Multiple PPs Per PA shows Duplicates but before 9949 New Item Log - Multiple PPs per PA, consider Dates

						--SELECT	DISTINCT --,'Multiple Active PPs per PA'          AS TFR_IssueName
						--		105					AS PIT_ID
						--		,NULL				AS CPP_ID
						--		,DupePPsPerPA.PA_ID	AS CPA_ID
						--		,NULL				AS CST_ID
						--		,NULL				AS CTF_ID
						--FROM	Billing.CPP_CustomerProcessingProfile AS CPP
						--			INNER JOIN (	SELECT	PP.CPP_CPA_ID AS PA_ID
						--							FROM	Billing.CPP_CustomerProcessingProfile AS PP
						--										LEFT JOIN Billing.CPA_CustomerProcessingAccount AS CPA
						--											ON PP.CPP_CPA_ID = CPA.CPA_ID
						--							WHERE	ISNULL(CPA.CPA_ManualBillYN,'N') = 'N'
						--									AND PP.CPP_RunDateYYYYMM = @RunDateYearMonth
						--									AND (PP.CPP_EndDate IS NULL
						--										OR LEFT(REPLACE(CONVERT(char(10),CONVERT(date,PP.CPP_EndDate)),'-',''),6) = @RunDateYearMonth
						--										)
						--							GROUP BY CPP_CPA_ID
						--							HAVING COUNT(*) > 1
						--						) AS DupePPsPerPA
						--				ON CPP.CPP_CPA_ID = DupePPsPerPA.PA_ID
                                  UNION ALL
                                  SELECT	--Missing Monthly Fee
                                            111 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Identifier.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPA.CPA_ID ,
                                                        CST_AccountName AS AccountName ,
                                                        CPA.CPA_TrustName AS TrustName ,
                                                        CPP.CPP_MID AS MID ,
                                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                        CASE WHEN CTF.CTF_FeeType IS NULL
                                                             THEN 0
                                                             ELSE CTF.CTF_DaysInEffectForMonth
                                                        END AS FeeDaysInEffect ,
                                                        ISNULL(DATEDIFF(d,
                                                              CPP.CPP_InitialTranDateForMonth,
                                                              @RunDateMonthEnd)
                                                              + 1, 1) AS RequiredDaysInEffect
                                              FROM      Billing.CPA_CustomerProcessingAccount
                                                        AS CPA
                                                        INNER JOIN Billing.CST_Customer
                                                        AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                                                        INNER JOIN Billing.CPP_CustomerProcessingProfile
                                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
						--AND CPP.CPP_SourceStep = 'ODS Activity'    
                                                        LEFT JOIN Billing.CTF_CustomerTranFee
                                                        AS CTF ON CPA.CPA_ID = CTF.CTF_CPA_ID
                                                              AND CTF.CTF_FeeType = 'BillingTree Monthly Fee'
						--AND ISNULL(CTF.CTF_FeeAmount, 0) > 0
                                              WHERE     CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPA.CPA_ProductLine IN (
                                                        'ACH', 'Credit Card' )
                                                        AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                        AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                            ) AS Identifier
                                  GROUP BY  Identifier.CPA_ID ,
                                            Identifier.AccountName ,
                                            Identifier.TrustName ,
                                            Identifier.MID ,
                                            Identifier.SourceIdConfig ,
                                            Identifier.RequiredDaysInEffect
                                  HAVING    SUM(Identifier.FeeDaysInEffect) < Identifier.RequiredDaysInEffect
                                  UNION ALL
                                  SELECT	--Missing Per Batch Fee
                                            112 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Identifier.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPA.CPA_ID ,
                                                        CST_AccountName AS AccountName ,
                                                        CPA.CPA_TrustName AS TrustName ,
                                                        CPP.CPP_MID AS MID ,
                                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                        CASE WHEN CTF.CTF_FeeType IS NULL
                                                             THEN 0
                                                             ELSE CTF.CTF_DaysInEffectForMonth
                                                        END AS FeeDaysInEffect ,
                                                        DATEDIFF(d,
                                                              CPP.CPP_InitialTranDateForMonth,
                                                              @RunDateMonthEnd)
                                                        + 1 AS RequiredDaysInEffect
                                              FROM      Billing.CPA_CustomerProcessingAccount
                                                        AS CPA
                                                        INNER JOIN Billing.CST_Customer
                                                        AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                                                        INNER JOIN Billing.CPP_CustomerProcessingProfile
                                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                              AND CPP.CPP_SourceStep = 'ODS Activity'
                                                        LEFT JOIN Billing.CTF_CustomerTranFee
                                                        AS CTF ON CPA.CPA_ID = CTF.CTF_CPA_ID
                                                              AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
                                                              AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) > 0
                                              WHERE     CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPA.CPA_ProductLine = 'ACH'
                                                        AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                        AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                            ) AS Identifier
                                  GROUP BY  Identifier.CPA_ID ,
                                            Identifier.AccountName ,
                                            Identifier.TrustName ,
                                            Identifier.MID ,
                                            Identifier.SourceIdConfig ,
                                            Identifier.RequiredDaysInEffect
                                  HAVING    SUM(Identifier.FeeDaysInEffect) < Identifier.RequiredDaysInEffect
                                  UNION ALL
                                  SELECT	--Missing Return Fee
                                            113 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Identifier.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPA.CPA_ID ,
                                                        CST_AccountName AS AccountName ,
                                                        CPA.CPA_TrustName AS TrustName ,
                                                        CPP.CPP_MID AS MID ,
                                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                        CASE WHEN CTF.CTF_FeeType IS NULL
                                                             THEN 0
                                                             ELSE CTF.CTF_DaysInEffectForMonth
                                                        END AS FeeDaysInEffect ,
                                                        DATEDIFF(d,
                                                              CPP.CPP_InitialTranDateForMonth,
                                                              @RunDateMonthEnd)
                                                        + 1 AS RequiredDaysInEffect
                                              FROM      Billing.CPA_CustomerProcessingAccount
                                                        AS CPA
                                                        INNER JOIN Billing.CST_Customer
                                                        AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                                                        INNER JOIN Billing.CPP_CustomerProcessingProfile
                                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                              AND CPP.CPP_SourceStep = 'ODS Activity'
                                                        LEFT JOIN Billing.CTF_CustomerTranFee
                                                        AS CTF ON CPA.CPA_ID = CTF.CTF_CPA_ID
                                                              AND CTF.CTF_FeeType = 'Return Fee'
                                                              AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) > 0
                                              WHERE     CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPA.CPA_ProductLine = 'ACH'
                                                        AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                        AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                            ) AS Identifier
                                  GROUP BY  Identifier.CPA_ID ,
                                            Identifier.AccountName ,
                                            Identifier.TrustName ,
                                            Identifier.MID ,
                                            Identifier.SourceIdConfig ,
                                            Identifier.RequiredDaysInEffect
                                  HAVING    SUM(Identifier.FeeDaysInEffect) < Identifier.RequiredDaysInEffect
                                  UNION ALL
                                  SELECT	--Missing Submission Fee
                                            114 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Identifier.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPA.CPA_ID ,
                                                        CST_AccountName AS AccountName ,
                                                        CPA.CPA_TrustName AS TrustName ,
                                                        CPP.CPP_MID AS MID ,
                                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                        CASE WHEN CTF.CTF_FeeType IS NULL
                                                             THEN 0
                                                             ELSE CTF.CTF_DaysInEffectForMonth
                                                        END AS FeeDaysInEffect ,
                                                        DATEDIFF(d,
                                                              CPP.CPP_InitialTranDateForMonth,
                                                              @RunDateMonthEnd)
                                                        + 1 AS RequiredDaysInEffect
                                              FROM      Billing.CPA_CustomerProcessingAccount
                                                        AS CPA
                                                        INNER JOIN Billing.CST_Customer
                                                        AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                                                        INNER JOIN Billing.CPP_CustomerProcessingProfile
                                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                              AND CPP.CPP_SourceStep = 'ODS Activity'
                                                        LEFT JOIN Billing.CTF_CustomerTranFee
                                                        AS CTF ON CPA.CPA_ID = CTF.CTF_CPA_ID
                                                              AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
                                                              AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) > 0
                                              WHERE     CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPA.CPA_ProductLine IN (
                                                        'ACH', 'Credit Card' )
                                                        AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                        AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                            ) AS Identifier
                                  GROUP BY  Identifier.CPA_ID ,
                                            Identifier.AccountName ,
                                            Identifier.TrustName ,
                                            Identifier.MID ,
                                            Identifier.SourceIdConfig ,
                                            Identifier.RequiredDaysInEffect
                                  HAVING    SUM(Identifier.FeeDaysInEffect) < Identifier.RequiredDaysInEffect
                                  UNION ALL
                                  SELECT	--Missing Un-Authorized Return Fee
                                            115 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Identifier.CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CPA.CPA_ID ,
                                                        CST_AccountName AS AccountName ,
                                                        CPA.CPA_TrustName AS TrustName ,
                                                        CPP.CPP_MID AS MID ,
                                                        CPP.CPP_SourceIdConfig AS SourceIdConfig ,
                                                        CASE WHEN CTF.CTF_FeeType IS NULL
                                                             THEN 0
                                                             ELSE CTF.CTF_DaysInEffectForMonth
                                                        END AS FeeDaysInEffect ,
                                                        DATEDIFF(d,
                                                              CPP.CPP_InitialTranDateForMonth,
                                                              @RunDateMonthEnd)
                                                        + 1 AS RequiredDaysInEffect
                                              FROM      Billing.CPA_CustomerProcessingAccount
                                                        AS CPA
                                                        INNER JOIN Billing.CST_Customer
                                                        AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                                                        INNER JOIN Billing.CPP_CustomerProcessingProfile
                                                        AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                                              AND CPP.CPP_SourceStep = 'ODS Activity'
                                                        LEFT JOIN Billing.CTF_CustomerTranFee
                                                        AS CTF ON CPA.CPA_ID = CTF.CTF_CPA_ID
                                                              AND CTF.CTF_FeeType = 'Un-Authorized Return Fee'
                                                              AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) > 0
                                              WHERE     CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CPA.CPA_ProductLine = 'ACH'
                                                        AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                        AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                            ) AS Identifier
                                  GROUP BY  Identifier.CPA_ID ,
                                            Identifier.AccountName ,
                                            Identifier.TrustName ,
                                            Identifier.MID ,
                                            Identifier.SourceIdConfig ,
                                            Identifier.RequiredDaysInEffect
                                  HAVING    SUM(Identifier.FeeDaysInEffect) < Identifier.RequiredDaysInEffect

/*			
						--Monthly Fee
						------------------------------------------------------------------------------
						--   Missing Monthly Fee - Type 1: No Record Exists and With ODS Activity
						------------------------------------------------------------------------------
						SELECT	--Missing Monthly Fee
								111						AS PIT_ID
								,NULL					AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL					AS CST_ID
								,NULL					AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'BillingTree Monthly Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine IN ('ACH','Credit Card')
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NULL

						UNION ALL
						------------------------------------------------------------------------------
						--   Missing Per Batch Fee - Type 1: No Record Exists and With ODS Activity
						------------------------------------------------------------------------------
						SELECT	--Missing Per Batch Fee
								112						AS PIT_ID
								,NULL					AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL					AS CST_ID
								,NULL					AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NULL

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Per Batch Fee - Type 2: Record Exists, With ODS Activity, 
						--   but Initial Tran Date For Month comes before the Fee's Effective Start Date 
						------------------------------------------------------------------------------
						SELECT	--Missing Per Batch Fee
								112						AS PIT_ID
								,NULL					AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL					AS CST_ID
								,NULL					AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NOT NULL
								AND CPP.CPP_InitialTranDateForMonth < CTF.CTF_EffectiveStartDateForMonth

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Return Fee - Type 1: No Record Exists and With ODS Activity
						------------------------------------------------------------------------------
						SELECT	--Missing Return Fee
								113			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Return Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NULL

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Return Fee - Type 2: Record Exists, With ODS Activity, 
						--   but Initial Tran Date For Month comes before the Fee's Effective Start Date 
						------------------------------------------------------------------------------
						SELECT	--Missing Return Fee
								113			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Return Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NOT NULL
								AND CPP.CPP_InitialTranDateForMonth < CTF.CTF_EffectiveStartDateForMonth

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Submission Fee - Type 1: No Record Exists and With ODS Activity
						------------------------------------------------------------------------------
						SELECT	--Missing Submission Fee
								114			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine IN ('ACH','Credit Card')
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NULL

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Submission Fee - Type 2: Record Exists, With ODS Activity, 
						--   but Initial Tran Date For Month comes before the Fee's Effective Start Date 
						------------------------------------------------------------------------------
						SELECT	--Missing Submission Fee
								114			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine IN ('ACH','Credit Card')
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NOT NULL
								AND CPP.CPP_InitialTranDateForMonth < CTF.CTF_EffectiveStartDateForMonth


						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Un-Authorized Return Fee - Type 1: No Record Exists and With ODS Activity
						------------------------------------------------------------------------------
						SELECT	--Missing Un-Authorized Return Fee
								115			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Un-Authorized Return Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NULL

						UNION ALL

						------------------------------------------------------------------------------
						--   Missing Un-Authorized Return Fee - Type 2: Record Exists, With ODS Activity, 
						--   but Initial Tran Date For Month comes before the Fee's Effective Start Date 
						------------------------------------------------------------------------------
						SELECT	--Missing Un-Authorized Return Fee
								115			AS PIT_ID
								,NULL		AS CPP_ID
								,CPA.CPA_ID	AS CPA_ID
								,NULL		AS CST_ID
								,NULL		AS CTF_ID
						FROM	Billing.CPA_CustomerProcessingAccount AS CPA
									INNER JOIN Billing.CST_Customer AS CST
										ON CPA.CPA_CST_ID = CST.CST_ID
									INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
										ON CPA.CPA_ID = CPP.CPP_CPA_ID
									LEFT JOIN Billing.CTF_CustomerTranFee AS CTF
										ON CPA.CPA_ID = CTF.CTF_CPA_ID
										AND CTF.CTF_FeeType = 'Un-Authorized Return Fee'
						WHERE	CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
								AND CPA.CPA_ProductLine = 'ACH'
								AND ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
								AND ISNULL(CPA.CPA_DoNotBillYN, 'N') = 'N'
								AND CTF.CTF_FeeType IS NOT NULL
								AND CPP.CPP_InitialTranDateForMonth < CTF.CTF_EffectiveStartDateForMonth
*/
                                  UNION ALL
                                  SELECT	--Duplicated Monthly Fee
                                            121 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Potentials.CTF_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CTF.CTF_CPA_ID ,
                                                        CTF.CTF_ProductName
                                              FROM      Billing.CTF_CustomerTranFee
                                                        AS CTF              --  multiple CTFs
                                              WHERE     CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CTF.CTF_FeeType = 'BillingTree Monthly Fee'
                                              GROUP BY  CTF.CTF_CPA_ID ,
                                                        CTF.CTF_ProductName
                                              HAVING    COUNT(*) > 1
                                            ) AS Potentials
                                            INNER JOIN Billing.CTF_CustomerTranFee
                                            AS CTF ON Potentials.CTF_CPA_ID = CTF.CTF_CPA_ID
                                                      AND CTF.CTF_FeeType = 'BillingTree Monthly Fee'
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                            INNER JOIN Billing.CPP_CustomerProcessingProfile
                                            AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                  GROUP BY  Potentials.CTF_CPA_ID ,
                                            CPP.CPP_MID ,
                                            CPP.CPP_SourceIdConfig
                                  HAVING    SUM(CTF.CTF_DaysInEffectForMonth) > DATEDIFF(DAY,
                                                              MIN(CTF.CTF_EffectiveStartDateForMonth),
                                                              MAX(CTF.CTF_EffectiveEndDateForMonth))
                                            + 1
                                  UNION ALL
                                  SELECT	--Duplicated Per Batch Fee
                                            122 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Potentials.CTF_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CTF.CTF_CPA_ID
                                              FROM      Billing.CTF_CustomerTranFee
                                                        AS CTF              --  multiple CTFs
                                              WHERE     CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
                                              GROUP BY  CTF.CTF_CPA_ID ,
                                                        CTF.CTF_FeeType
                                              HAVING    COUNT(*) > 1
                                            ) AS Potentials
                                            INNER JOIN Billing.CTF_CustomerTranFee
                                            AS CTF ON Potentials.CTF_CPA_ID = CTF.CTF_CPA_ID
                                                      AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND CPA.CPA_ProductLine = 'ACH'
                                            INNER JOIN Billing.CPP_CustomerProcessingProfile
                                            AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                  GROUP BY  Potentials.CTF_CPA_ID ,
                                            CPP.CPP_MID ,
                                            CPP.CPP_SourceIdConfig
                                  HAVING    SUM(CTF.CTF_DaysInEffectForMonth) > DATEDIFF(DAY,
                                                              MIN(CTF.CTF_EffectiveStartDateForMonth),
                                                              MAX(CTF.CTF_EffectiveEndDateForMonth))
                                            + 1
                                  UNION ALL
                                  SELECT	--Duplicated Return Fee
                                            123 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Potentials.CTF_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CTF.CTF_CPA_ID
                                              FROM      Billing.CTF_CustomerTranFee
                                                        AS CTF              --  multiple CTFs
                                              WHERE     CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CTF.CTF_FeeType = 'Return Fee'
                                              GROUP BY  CTF.CTF_CPA_ID ,
                                                        CTF.CTF_FeeType
                                              HAVING    COUNT(*) > 1
                                            ) AS Potentials
                                            INNER JOIN Billing.CTF_CustomerTranFee
                                            AS CTF ON Potentials.CTF_CPA_ID = CTF.CTF_CPA_ID
                                                      AND CTF.CTF_FeeType = 'Return Fee'
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND CPA.CPA_ProductLine = 'ACH'
                                            INNER JOIN Billing.CPP_CustomerProcessingProfile
                                            AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                  GROUP BY  Potentials.CTF_CPA_ID ,
                                            CPP.CPP_MID ,
                                            CPP.CPP_SourceIdConfig
                                  HAVING    SUM(CTF.CTF_DaysInEffectForMonth) > DATEDIFF(DAY,
                                                              MIN(CTF.CTF_EffectiveStartDateForMonth),
                                                              MAX(CTF.CTF_EffectiveEndDateForMonth))
                                            + 1
                                  UNION ALL
                                  SELECT	--Duplicated Submission Fee
                                            124 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Potentials.CTF_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CTF.CTF_CPA_ID
                                              FROM      Billing.CTF_CustomerTranFee
                                                        AS CTF              --  multiple CTFs
                                              WHERE     CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
                                              GROUP BY  CTF.CTF_CPA_ID ,
                                                        CTF.CTF_FeeType
                                              HAVING    COUNT(*) > 1
                                            ) AS Potentials
                                            INNER JOIN Billing.CTF_CustomerTranFee
                                            AS CTF ON Potentials.CTF_CPA_ID = CTF.CTF_CPA_ID
                                                      AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                            INNER JOIN Billing.CPP_CustomerProcessingProfile
                                            AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                  GROUP BY  Potentials.CTF_CPA_ID ,
                                            CPP.CPP_MID ,
                                            CPP.CPP_SourceIdConfig
                                  HAVING    SUM(CTF.CTF_DaysInEffectForMonth) > DATEDIFF(DAY,
                                                              MIN(CTF.CTF_EffectiveStartDateForMonth),
                                                              MAX(CTF.CTF_EffectiveEndDateForMonth))
                                            + 1
                                  UNION ALL
                                  SELECT	--Duplicated Un-Authorized Return Fee
                                            125 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            Potentials.CTF_CPA_ID AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      ( SELECT    CTF.CTF_CPA_ID
                                              FROM      Billing.CTF_CustomerTranFee
                                                        AS CTF              --  multiple CTFs
                                              WHERE     CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                                        AND CTF.CTF_FeeType = 'Return Fee'
                                              GROUP BY  CTF.CTF_CPA_ID ,
                                                        CTF.CTF_FeeType
                                              HAVING    COUNT(*) > 1
                                            ) AS Potentials
                                            INNER JOIN Billing.CTF_CustomerTranFee
                                            AS CTF ON Potentials.CTF_CPA_ID = CTF.CTF_CPA_ID
                                                      AND CTF.CTF_FeeType = 'Return Fee'
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND CPA.CPA_ProductLine = 'ACH'
                                            INNER JOIN Billing.CPP_CustomerProcessingProfile
                                            AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                                  GROUP BY  Potentials.CTF_CPA_ID ,
                                            CPP.CPP_MID ,
                                            CPP.CPP_SourceIdConfig
                                  HAVING    SUM(CTF.CTF_DaysInEffectForMonth) > DATEDIFF(DAY,
                                                              MIN(CTF.CTF_EffectiveStartDateForMonth),
                                                              MAX(CTF.CTF_EffectiveEndDateForMonth))
                                            + 1
                                  UNION ALL
                                  SELECT	--,'Active PPs not Identified (will be billed a Fee)' AS TFR_IssueName
                                            1001 AS PIT_ID ,
                                            CPP.CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPP_CustomerProcessingProfile
                                            AS CPP
                                            LEFT JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CPP.CPP_CPA_ID = CPA.CPA_ID
                                  WHERE     ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
                                            AND ISNULL(CPA.CPA_DoNotBillYN,
                                                       'N') = 'N'
                                            AND CPP.CPP_SourceStep = 'CRM Active PP'
                                            AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
						--**********************LEAVE ALONE FOR issue 9
                                  SELECT	--,'Missing Customer eMail Contacts' AS TFR_IssueName
                                            1002 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            CST_ID AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CST_Customer
                                  WHERE     ISNULL(CST_ContactAddressString,
                                                   '') = ''
                                            AND CST_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'PP Activity before Fee Start Date'	AS TFR_IssueName
                                            1003 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            CPA.CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPA_CustomerProcessingAccount
                                            AS CPA
                                  WHERE     ISNULL(CPA.CPA_ManualBillYN, 'N') = 'N'
                                            AND ISNULL(CPA.CPA_DoNotBillYN,
                                                       'N') = 'N'
                                            AND YEAR(CPA.CPA_ManualBillStartDate) >= LEFT(CPA.CPA_RunDateYYYYMM,
                                                              4)
                                            AND MONTH(CPA.CPA_ManualBillStartDate) > RIGHT(CPA.CPA_RunDateYYYYMM,
                                                              2)
                                            AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
						--**********************LEAVE ALONE FOR issue 9
                                  SELECT	--,'PP Activity After Fee expiry Date'	AS TFR_IssueName
                                            1004 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            CPA.CPA_ID ,
                                            NULL AS CST_ID ,
                                            NULL AS CTF_ID
                                  FROM      Billing.CPA_CustomerProcessingAccount
                                            AS CPA
                                  WHERE     CPA.CPA_ManualBillYN = 'Y'
                                            AND YEAR(CPA.CPA_ManualBillEndDate) >= LEFT(CPA.CPA_RunDateYYYYMM,
                                                              4)
                                            AND MONTH(CPA.CPA_ManualBillEndDate) > RIGHT(CPA.CPA_RunDateYYYYMM,
                                                              2)
                                            AND CPA.CPA_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'Fees out of Range (Monthly)'							AS TFR_IssueName
                                            1011 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            CTF.CTF_ID
                                  FROM      Billing.CTF_CustomerTranFee AS CTF
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                      AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                                      AND CTF.CTF_FeeType LIKE '%Monthly Fee'
									--AND CTF.CTF_FeeAmount <> 0
                                                      AND ( CTF.CTF_FeeAmount NOT BETWEEN 10.00 AND 55.00
                                                            AND CPA.CPA_ProductLine = 'ACH'
                                                            OR CTF.CTF_FeeAmount NOT BETWEEN 10.00 AND 55.00
                                                            AND CPA.CPA_ProductLine = 'Credit Card'
                                                          )
                                                      AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT --,'Fees out of Range (Batch)'                                                    AS TFR_IssueName
                                            1012 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            CTF.CTF_ID
                                  FROM      Billing.CTF_CustomerTranFee AS CTF
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                      AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                                      AND CTF.CTF_FeeType = 'Per Batch Fee/Offset'
                                                      AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) NOT BETWEEN 0.09 AND 0.50
                                                      AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'Fees out of Range (Return)'                                                   AS TFR_IssueName
                                            1013 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            CTF.CTF_ID
                                  FROM      Billing.CTF_CustomerTranFee AS CTF
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                      AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                                      AND CTF.CTF_FeeType = 'Return Fee'
                                                      AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) NOT BETWEEN 1.09 AND 5.00
                                                      AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'Fees out of Range (Submission)'                                               AS TFR_IssueName
                                            1014 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            CTF.CTF_ID
                                  FROM      Billing.CTF_CustomerTranFee AS CTF
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                      AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                                      AND CTF.CTF_FeeType LIKE '%Submission Fee'
                                                      AND ( ISNULL(CTF.CTF_FeeAmount,
                                                              0) NOT BETWEEN 0.15 AND 0.50
                                                            AND CPA.CPA_ProductLine = 'ACH'
                                                            OR ISNULL(CTF.CTF_FeeAmount,
                                                              0) NOT BETWEEN 0.10 AND 0.33
                                                            AND CPA.CPA_ProductLine = 'Credit Card'
                                                          )
                                                      AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                  UNION ALL
                                  SELECT	--,'Fees out of Range (UnAuth)'                                                   AS TFR_IssueName
                                            1015 AS PIT_ID ,
                                            NULL AS CPP_ID ,
                                            NULL AS CPA_ID ,
                                            NULL AS CST_ID ,
                                            CTF.CTF_ID
                                  FROM      Billing.CTF_CustomerTranFee AS CTF
                                            INNER JOIN Billing.CPA_CustomerProcessingAccount
                                            AS CPA ON CTF.CTF_CPA_ID = CPA.CPA_ID
                                                      AND ISNULL(CPA.CPA_ManualBillYN,
                                                              'N') = 'N'
                                                      AND ISNULL(CPA.CPA_DoNotBillYN,
                                                              'N') = 'N'
                                                      AND CTF.CTF_FeeType = 'Un-Authorized Return Fee'
                                                      AND ISNULL(CTF.CTF_FeeAmount,
                                                              0) NOT BETWEEN 7.50 AND 25.00
                                                      AND CTF.CTF_RunDateYYYYMM = @RunDateYearMonth
                                ) AS PED
                        ORDER BY PIT_ID;

			/* Separate process
			DECLARE @LoopDate date = @RunDateMonthBegin -- Begining of month
			WHILE @LoopDate <= @RunDateMonthEnd  -- End of month
				BEGIN
					--PRINT @LoopDate
					INSERT INTO Billing.CDT_CustomerDailyTran
							(CDT_CPP_ID
							,CDT_RunDateYYYYMM
							,CDT_TranDate
							,CDT_TranType
							,CDT_TranAmount
							,CDT_TranCount
							,CDT_PRT_ID)
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Debit'				AS CDT_TranType
							,U.Amount			AS CDT_TranAmount
							,U.Sales			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Sales > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate
					UNION ALL
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Credit'			AS CDT_TranType
							,0					AS CDT_TranAmount
							,U.Credits			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Credits > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate
					UNION ALL
					SELECT	CPP.CPP_ID			AS CDT_CPP_ID
							,U.TransDateYYYYMM	AS CDT_RunDateYYYYMM
							,U.TransDate		AS CDT_TranDate
							,'Decline'			AS CDT_TranType
							,0					AS CDT_TranAmount
							,U.Declines			AS CDT_TranCount
							,4					AS CDT_PRT_ID
					FROM	USAePay.DailySummary AS U
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON U.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
									AND U.Declines > 0
									AND U.UsabilityIndex = 1
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
									AND U.TransDate = @LoopDate

	
					UPDATE	CDT
					SET		CDT.CDT_CTF_ID = CTF.CTF_ID
					FROM	Billing.CDT_CustomerDailyTran AS CDT
								INNER JOIN Billing.CPP_CustomerProcessingProfile AS CPP
									ON CDT.CDT_CPP_ID = CPP.CPP_ID
									AND CPP.CPP_RunDateYYYYMM = @RunDateYearMonth
								INNER JOIN Billing.CTF_CustomerTranFee AS CTF
									ON CPP.CPP_CPA_ID = CTF.CTF_CPA_ID
									AND CTF.CTF_FeeType = 'BillingTree Gateway Submission Fee'
									AND CDT.CDT_CTF_ID IS NULL
									AND CDT.CDT_TranDate BETWEEN CTF.CTF_EffectiveStartDateForMonth AND CTF.CTF_EffectiveEndDateForMonth
									AND CDT.CDT_TranDate = @LoopDate

					SET @LoopDate=DATEADD(d,1,@LoopDate)
				END


			--DECLARE @RunDateYearMonth AS int = 201504

			--DECLARE @MaxPSL AS int
			--SELECT	@MaxPSL = MAX(PSL_ID)
			--FROM	Billing.PSL_ProcessStatusLog
			--WHERE	PSL_RunDateYYYYMM = @RunDateYearMonth
			--IF @MaxPSL IS NULL
			--INSERT INTO Billing.PSL_ProcessStatusLog
			--(PSL_PRT_ID
			--,PSL_RunDateYYYYMM
			--,PSL_Status)
			--VALUES	(1
			--		,@RunDateYearMonth
			--		,'Hard Errors')

			--DECLARE @PSLID AS int
			--SELECT	@PSLID = PSL_ID
			--FROM	Billing.PSL_ProcessStatusLog
			--WHERE	PSL_RunDateYYYYMM = @RunDateYearMonth


			--DECLARE @NextPRL AS int
			--SELECT	@NextPRL = ISNULL(MAX(PRL_ID),0)+1
			--FROM	Billing.PRL_ProcessRunList
			----SELECT @MaxPSL
			--INSERT INTO Billing.PRL_ProcessRunList
			--(PRL_PSL_ID
			--,PRL_StartDateTime
			--,PRL_EndDateTime)
			--VALUES	(@PSLID
			--		,GETDATE()
			--		,GETDATE())
			*/ --Separate Process
                DECLARE @PRLID AS INT;
                SELECT  @PRLID = MAX(R.PRL_ID)
                FROM    Billing.PRL_ProcessRunList AS R
                        INNER JOIN Billing.PSL_ProcessStatusLog AS S ON R.PRL_PSL_ID = S.PSL_ID
                                                              AND S.PSL_PRT_ID = @RunType
                                                              AND S.PSL_RunDateYYYYMM = @RunDateYearMonth;
			
			--SELECT * FROM	#PED_ProcessErrorDetail order by 1
                INSERT  INTO Billing.PIL_ProcessIssueList
                        ( PIL_ID ,
                          PIL_PIT_ID ,
                          PIL_PRL_ID ,
                          PIL_Count
                        )
                        SELECT  PED_PIL_ID ,
                                PIL_PIT_ID ,
                                @PRLID AS PIL_PRL_ID ,
                                COUNT(*) AS PIL_Count
                        FROM    #PED_ProcessErrorDetail
                        GROUP BY PED_PIL_ID ,
                                PIL_PIT_ID
                        ORDER BY 1;

                DECLARE @Count INT;
                SELECT  @Count = SUM(All4.RecCount)
                FROM    ( SELECT    COUNT(*) AS RecCount
                          FROM      USAePay.DailySummary
                          WHERE     TransDateYYYYMM = @RunDateYearMonth
                                    AND UsabilityIndex = 0
                                    AND ( Sales > 0
                                          OR Credits > 0
                                          OR Declines > 0
                                        )
                          UNION ALL
                          SELECT    COUNT(*) AS RecCount
                          FROM      MeS.SettlementSummary
                          WHERE     TransDateYYYYMM = @RunDateYearMonth
                                    AND UsabilityIndex = 0
                          UNION ALL
                          SELECT    COUNT(*) AS RecCount
                          FROM      Billing.ACHTransByBatchDate
                          WHERE     BatchDateYYYYMM = @RunDateYearMonth
                                    AND UsabilityIndex = 0
                          UNION ALL
                          SELECT    COUNT(*) AS RecCount
                          FROM      Billing.ACHReturnsByReturnDate
                          WHERE     ReturnDateYYYYMM = @RunDateYearMonth
                                    AND UsabilityIndex = 0

						--UNION ALL

						--SELECT	COUNT(*) AS RecCount
						--FROM	NACHA.Originations
						--WHERE	FileCreationDateYYYYMM = @RunDateYearMonth 
						--		AND UsabilityIndex = 0

						--UNION ALL

						--SELECT	COUNT(*) AS RecCount
						--FROM	NACHA.Returns
						--WHERE	FileCreationDateYYYYMM = @RunDateYearMonth 
						--		AND UsabilityIndex = 0
                        ) AS All4;
			--SELECT @Count

                UPDATE  PIL
                SET     PIL_Count = @Count
                FROM    Billing.PIL_ProcessIssueList AS PIL
                        INNER JOIN Billing.PRL_ProcessRunList AS PRL ON PIL_PRL_ID = @PRLID
                                                              AND PIL_PIT_ID = 101;			-- For correct count of records in quarantine


                INSERT  INTO Billing.PED_ProcessErrorDetail
                        ( PED_PIL_ID ,
                          PED_CPP_ID ,
                          PED_CPA_ID ,
                          PED_CST_ID ,
                          PED_CTF_ID
                        )
                        SELECT  PED_PIL_ID ,
                                PED_CPP_ID ,
                                PED_CPA_ID ,
                                PED_CST_ID ,
                                PED_CTF_ID
                        FROM    #PED_ProcessErrorDetail
                        ORDER BY 1;

                UPDATE  Billing.PSL_ProcessStatusLog
                SET     PSL_Status = ISNULL(( SELECT TOP 1
                                                        PIT_Category
                                                        + CASE
                                                              WHEN COUNT(*) = 1
                                                              THEN ''
                                                              ELSE 's'
                                                          END
                                              FROM      Billing.PIL_ProcessIssueList
                                                        AS IL
                                                        INNER JOIN Billing.PRL_ProcessRunList
                                                        AS RL ON IL.PIL_PRL_ID = RL.PRL_ID
                                                        INNER JOIN Billing.PIT_ProcessIssueType
                                                        AS IT ON IL.PIL_PIT_ID = IT.PIT_ID
                                                              AND RL.PRL_ID = @PRLID
                                              GROUP BY  PIT_Category
                                              ORDER BY  PIT_Category
                                            ), 'No Errors')
                WHERE   PSL_PRT_ID = @RunType
                        AND PSL_RunDateYYYYMM = @RunDateYearMonth;


                UPDATE  Billing.PRL_ProcessRunList
                SET     PRL_EndDateTime = GETDATE()
                WHERE   PRL_ID = @PRLID;

                SELECT  ISNULL(( SELECT TOP 1
                                        LEFT(PIT_Category, 4)
                                 FROM   Billing.PIL_ProcessIssueList AS IL
                                        INNER JOIN Billing.PRL_ProcessRunList
                                        AS RL ON IL.PIL_PRL_ID = RL.PRL_ID
                                        INNER JOIN Billing.PIT_ProcessIssueType
                                        AS IT ON IL.PIL_PIT_ID = IT.PIT_ID
                                                 AND RL.PRL_ID = @PRLID
                                 GROUP BY PIT_Category
                                 ORDER BY PIT_Category
                               ), 'No');
            END;
    END;