﻿
CREATE VIEW [InStep].[vR9FileControl] AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,6) AS BatchCount
		,SUBSTRING(SourceRecord,8,6) AS BlockCount
		,SUBSTRING(SourceRecord,14,8) AS EntryORAddendaCount
		,SUBSTRING(SourceRecord,22,10) AS EntryHash
		,SUBSTRING(SourceRecord,32,12) AS TotalDebitEntryDollarAmountinFile
		,SUBSTRING(SourceRecord,44,12) AS TotalCreditEntryDollarAmountinFile
		,SUBSTRING(SourceRecord,56,39) AS Reserved
FROM	InStep.ReturnFileData
WHERE	RecordType = 9





