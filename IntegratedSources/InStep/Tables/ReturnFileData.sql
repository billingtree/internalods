﻿CREATE TABLE [InStep].[ReturnFileData] (
    [ID]                  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileReferenceCodeID] INT           NOT NULL,
    [ConfigNumber]        TINYINT       NOT NULL,
    [LineNumber]          INT           NOT NULL,
    [BlockNumber]         SMALLINT      NOT NULL,
    [RecordType]          TINYINT       NOT NULL,
    [EntryORAddendaType]  NCHAR (3)     NULL,
    [IsCCDOffset]         BIT           NULL,
    [ReturnCode]          NCHAR (3)     NULL,
    [SourceRecord]        NVARCHAR (94) NOT NULL,
    [UsabilityIndex]      SMALLINT      NULL,
    [MashupOutcome]       TINYINT       NULL,
    [InsertDate]          DATETIME      CONSTRAINT [DF_ReturnFileData_InsertDate] DEFAULT (getdate()) NOT NULL,
    [CRM_PPDID]           NVARCHAR (36) NULL,
    [ProcessingNumber]    NVARCHAR (8)  NULL,
    CONSTRAINT [PK_ReturnFileData] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ReturnFileData_PaymentProcessing] FOREIGN KEY ([CRM_PPDID]) REFERENCES [SalesForce].[PaymentProcessing] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordType]
    ON [InStep].[ReturnFileData]([RecordType] ASC)
    INCLUDE([FileReferenceCodeID], [BlockNumber], [SourceRecord]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordType_EntryORAddendaType_UsabilityIndex]
    ON [InStep].[ReturnFileData]([RecordType] ASC, [EntryORAddendaType] ASC, [UsabilityIndex] ASC)
    INCLUDE([FileReferenceCodeID], [BlockNumber], [ReturnCode], [SourceRecord], [MashupOutcome]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_RecordType_UsabilityIndex]
    ON [InStep].[ReturnFileData]([RecordType] ASC, [UsabilityIndex] ASC)
    INCLUDE([ID], [ConfigNumber], [SourceRecord], [MashupOutcome]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_FileReferenceCodeID_BlockNumber]
    ON [InStep].[ReturnFileData]([FileReferenceCodeID] ASC, [BlockNumber] ASC)
    INCLUDE([ID], [RecordType], [UsabilityIndex], [MashupOutcome]) WITH (FILLFACTOR = 80);

