﻿CREATE TABLE [Billing].[ACHReturnsByReturnDate] (
    [RowID]             INT            IDENTITY (1, 1) NOT NULL,
    [FileName]          NVARCHAR (255) NOT NULL,
    [ConfigNumber]      TINYINT        NOT NULL,
    [AccountID]         INT            NULL,
    [ImportID]          VARCHAR (50)   NULL,
    [TransactionType]   VARCHAR (50)   NULL,
    [BatchID]           INT            NOT NULL,
    [InHouseID]         VARCHAR (30)   NULL,
    [SubmitDate]        DATE           NULL,
    [ConsumerFirstName] VARCHAR (150)  NULL,
    [ConsumerLastName]  VARCHAR (50)   NOT NULL,
    [Reference]         VARCHAR (50)   NULL,
    [CheckNumber]       VARCHAR (22)   NULL,
    [ReturnDate]        DATE           NOT NULL,
    [RoutingTransit]    VARCHAR (9)    NOT NULL,
    [AccountNumber]     VARCHAR (25)   NOT NULL,
    [Status]            VARCHAR (50)   NULL,
    [ReturnCode]        VARCHAR (3)    NOT NULL,
    [ReturnReason]      VARCHAR (60)   NULL,
    [DebitAmount]       MONEY          NOT NULL,
    [CreditAmount]      MONEY          NOT NULL,
    [AddendaInfo]       VARCHAR (80)   NULL,
    [ReturnDateYYYYMM]  INT            NOT NULL,
    [CRM_PPDID]         NVARCHAR (36)  NULL,
    [UsabilityIndex]    SMALLINT       NULL,
    [MashupOutcome]     TINYINT        NULL,
    [InsertDateTime]    DATETIME       CONSTRAINT [DF__ACHReturn__Inser__2A0E8198] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ACHReturnsByReturnDate] PRIMARY KEY CLUSTERED ([RowID] ASC) WITH (FILLFACTOR = 80)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'AccountID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PrivateData', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'ImportID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SecCodeInput', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'TransactionType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LegacyAccountBatchId', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'BatchID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AccountName', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'ConsumerLastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ReferenceData', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'Reference';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AccountIdent', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'CheckNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BankABA', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'RoutingTransit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BankAcctNo', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'AccountNumber';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TransStatusCodeDesc', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'Status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PaymentRelatedInfo', @level0type = N'SCHEMA', @level0name = N'Billing', @level1type = N'TABLE', @level1name = N'ACHReturnsByReturnDate', @level2type = N'COLUMN', @level2name = N'AddendaInfo';

