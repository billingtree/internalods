﻿



CREATE function  [dbo].[fnIsGlobalMerchantOnGateway] 
(@MerchantID varchar (500))
 returns char(1)
as
BEGIN
    DECLARE @Count nvarchar (500)
    DECLARE @ReturnResult char (1)
    
    SET @Count = 0
    SET @ReturnResult = 'N'
    
    SELECT @Count = COUNT(*)
    FROM USAePay.FileData USA 
    WHERE LEFT(USA.MerchantID,13) = RIGHT(@MerchantID,13)
     
    IF @Count = 0
        BEGIN
            SET @ReturnResult = 'N'
        END
    ELSE  
        SET @ReturnResult = 'Y'
   
    RETURN @ReturnResult
   
END    
 



