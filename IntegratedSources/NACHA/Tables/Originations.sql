﻿CREATE TABLE [NACHA].[Originations] (
    [SourceType]               INT             NOT NULL,
    [ReferenceCode]            NVARCHAR (8)    NULL,
    [ConfigNumber]             TINYINT         NULL,
    [FileCreationDate]         DATE            NULL,
    [FileCreationDateYYYYMM]   INT             NULL,
    [FileCreationTime]         TIME (0)        NULL,
    [ServiceClassCode]         NVARCHAR (3)    NULL,
    [CompanyName]              NVARCHAR (16)   NULL,
    [CompanyIdentification]    NVARCHAR (10)   NULL,
    [StandardEntryClassCode]   NVARCHAR (3)    NULL,
    [EffectiveEntryDate]       DATE            NULL,
    [CRM_PPDID]                NVARCHAR (36)   NULL,
    [ProcessingNumber]         NVARCHAR (8)    NULL,
    [BatchNumber]              NVARCHAR (7)    NULL,
    [FileReferenceCodeID]      INT             NOT NULL,
    [BlockNumber]              SMALLINT        NOT NULL,
    [TransactionCode]          NVARCHAR (2)    NULL,
    [Amount]                   NVARCHAR (10)   NULL,
    [IdentificationNumber]     NVARCHAR (15)   NULL,
    [ReceivingCompanyName]     NVARCHAR (22)   NULL,
    [TraceNumber]              NVARCHAR (15)   NULL,
    [UsabilityIndex]           SMALLINT        NULL,
    [MashupOutcome]            TINYINT         NULL,
    [FormattedAmount]          NUMERIC (22, 6) NULL,
    [MashupOutcomeDescription] VARCHAR (20)    NULL,
    [InsertDate]               SMALLDATETIME   CONSTRAINT [DF_Originations_InsertDate] DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [NCI_SourceType_FileCreationDateYYYYMM_CRM_PPDID_UsabilityIndex]
    ON [NACHA].[Originations]([SourceType] ASC, [FileCreationDateYYYYMM] ASC, [CRM_PPDID] ASC, [UsabilityIndex] ASC)
    INCLUDE([IdentificationNumber]);


GO
CREATE NONCLUSTERED INDEX [NCI_SourceType_FileCreationDateYYYYMM_UsabilityIndex]
    ON [NACHA].[Originations]([SourceType] ASC, [FileCreationDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([CRM_PPDID], [IdentificationNumber]);


GO
CREATE NONCLUSTERED INDEX [NCI_SourceType_FileCreationDateYYYYMM_UsabilityIndex_INC_CRM_PPDID_IdentificationNumber_FormattedAmount]
    ON [NACHA].[Originations]([SourceType] ASC, [FileCreationDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([CRM_PPDID], [IdentificationNumber], [FormattedAmount]);


GO
CREATE NONCLUSTERED INDEX [NCI_FileCreationDateYYYYMM_UsabilityIndex]
    ON [NACHA].[Originations]([FileCreationDateYYYYMM] ASC, [UsabilityIndex] ASC)
    INCLUDE([CRM_PPDID]) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [NCI_SourceType_FileCreationDateYYYYMM_UsabilityIndex_ReceivingCompanyName]
    ON [NACHA].[Originations]([SourceType] ASC, [FileCreationDateYYYYMM] ASC, [UsabilityIndex] ASC, [ReceivingCompanyName] ASC)
    INCLUDE([CRM_PPDID]);

