﻿




-- =============================================
-- Author:        Rick Stohle
-- Create date:   08-Dec-2015
-- Description:   Report identifies all Do Not Bill Customers for a given YearMonth and 
--                aggregates their Origination Volume/Count, their Batch Count, Return Count and UnAuthorized Return Count for ACH
--                and aggregates their Debit, Credit and Decline activity as 1 line item for Credit Card
-- =============================================
CREATE PROCEDURE [Billing].[spDoNotBillReport](
@RunYearMonth      INT,
@ProductLine       CHAR
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

      DECLARE @YearMonth             INT
	  DECLARE @ProductLineName       CHAR 
      SET @YearMonth = @RunYearMonth
	  SET @ProductLineName = @ProductLine

---------------------------------------------
--   Do Not Bill Customer Activity
---------------------------------------------
IF @ProductLineName = 'A'
		--------------------------------------------------------------------------------------------------------------
		--   ACH Transactions - Keep Originations, Batches, Returns and UnAuthorized Returns as separate columns
		--------------------------------------------------------------------------------------------------------------
		SELECT Orig.YYYYMM              AS YYYYMM
			  ,Orig.AccountName         AS AccountName
			  ,Orig.TrustName           AS TrustName
			  ,Orig.DoNotBillStartDate  AS DoNotBillStartDate
			  ,Orig.DoNotBillEndDate    AS DoNotBillEndDate
			  ,Orig.MID                 AS MID
			  ,Orig.SourceIdConfig      AS SourceIdConfig
			  ,Orig.MIDConfig           AS MIDConfig
			  ,Orig.TranAmount          AS OrigVolume
			  ,Orig.TranCount           AS OrigCount
			  ,Batch.TranCount          AS BatchCount
			  ,Rturn.TranCount          AS ReturnCount
			  ,Uturn.TranCount          AS UnAuthCount
		FROM (
				SELECT CPP.CPP_ID                                  AS CPP_ID
					  ,CST.CST_AccountName                         AS AccountName
					  ,CPA.CPA_TrustName                           AS TrustName
					  ,CPA.CPA_ProductLine                         AS ProductLine
					  ,CPA.CPA_DoNotBillStartDate                  AS DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate                    AS DoNotBillEndDate
					  ,CPP.CPP_MID                                 AS MID
					  ,CPP.CPP_SourceIdConfig                      AS SourceIdConfig
					  ,CONCAT(CPP.CPP_SourceIdConfig, CPP.CPP_MID) AS MIDConfig
					  ,CPP.CPP_RunDateYYYYMM                       AS YYYYMM
					  ,CDT.CDT_TranType                            AS TranType
					  ,SUM(ISNULL(CDT.CDT_TranAmount, 0))          AS TranAmount
					  ,SUM(ISNULL(CDT.CDT_TranCount, 0))           AS TranCount
				FROM Billing.CPA_CustomerProcessingAccount  AS CPA
				JOIN Billing.CST_Customer                   AS CST ON CPA.CPA_CST_ID = CST.CST_ID
				JOIN Billing.CPP_CustomerProcessingProfile  AS CPP on CPA.CPA_ID = CPP.CPP_CPA_ID
				LEFT JOIN Billing.CDT_CustomerDailyTran     AS CDT on CPP.CPP_ID = CDT.CDT_CPP_ID
																  AND CDT.CDT_TranType = 'Origination'
				WHERE CPA.CPA_DoNotBillYN = 'Y'
				  AND CPA.CPA_RunDateYYYYMM = @YearMonth
				  AND CPA.CPA_ProductLine = 'ACH'  
				GROUP BY CPP.CPP_ID
					  ,CST.CST_AccountName
					  ,CPA.CPA_TrustName  
					  ,CPA.CPA_ProductLine  
					  ,CPA.CPA_DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate 
					  ,CPP.CPP_MID  
					  ,CPP.CPP_SourceIdConfig  
					  ,CPP.CPP_RunDateYYYYMM   
					  ,CDT.CDT_TranType
			 )                                           AS Orig
		LEFT JOIN (
				SELECT CPP.CPP_ID                                  AS CPP_ID
					  ,CST.CST_AccountName                         AS AccountName
					  ,CPA.CPA_TrustName                           AS TrustName
					  ,CPA.CPA_ProductLine                         AS ProductLine
					  ,CPA.CPA_DoNotBillStartDate                  AS DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate                    AS DoNotBillEndDate
					  ,CPP.CPP_MID                                 AS MID
					  ,CPP.CPP_SourceIdConfig                      AS SourceIdConfig
					  ,CONCAT(CPP.CPP_SourceIdConfig, CPP.CPP_MID) AS MIDConfig
					  ,CPP.CPP_RunDateYYYYMM                       AS YYYYMM
					  ,CDT.CDT_TranType                            AS TranType
					  ,SUM(ISNULL(CDT.CDT_TranAmount, 0))          AS TranAmount
					  ,SUM(ISNULL(CDT.CDT_TranCount, 0))           AS TranCount
				FROM Billing.CPA_CustomerProcessingAccount  AS CPA
				JOIN Billing.CST_Customer                   AS CST ON CPA.CPA_CST_ID = CST.CST_ID
				JOIN Billing.CPP_CustomerProcessingProfile  AS CPP on CPA.CPA_ID = CPP.CPP_CPA_ID
				LEFT JOIN Billing.CDT_CustomerDailyTran     AS CDT on CPP.CPP_ID = CDT.CDT_CPP_ID
																  AND CDT.CDT_TranType = 'Batch'
				WHERE CPA.CPA_DoNotBillYN = 'Y'
				  AND CPA.CPA_RunDateYYYYMM = @YearMonth
				  AND CPA.CPA_ProductLine = 'ACH'  
				GROUP BY CPP.CPP_ID
					  ,CST.CST_AccountName
					  ,CPA.CPA_TrustName  
					  ,CPA.CPA_ProductLine  
					  ,CPA.CPA_DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate 
					  ,CPP.CPP_MID  
					  ,CPP.CPP_SourceIdConfig  
					  ,CPP.CPP_RunDateYYYYMM   
					  ,CDT.CDT_TranType
				)                                           AS Batch ON Orig.CPP_ID = Batch.CPP_ID
		LEFT JOIN (
				SELECT CPP.CPP_ID                                  AS CPP_ID
					  ,CST.CST_AccountName                         AS AccountName
					  ,CPA.CPA_TrustName                           AS TrustName
					  ,CPA.CPA_ProductLine                         AS ProductLine
					  ,CPA.CPA_DoNotBillStartDate                  AS DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate                    AS DoNotBillEndDate
					  ,CPP.CPP_MID                                 AS MID
					  ,CPP.CPP_SourceIdConfig                      AS SourceIdConfig
					  ,CONCAT(CPP.CPP_SourceIdConfig, CPP.CPP_MID) AS MIDConfig
					  ,CPP.CPP_RunDateYYYYMM                       AS YYYYMM
					  ,CDT.CDT_TranType                            AS TranType
					  ,SUM(ISNULL(CDT.CDT_TranAmount, 0))          AS TranAmount
					  ,SUM(ISNULL(CDT.CDT_TranCount, 0))           AS TranCount
				FROM Billing.CPA_CustomerProcessingAccount  AS CPA
				JOIN Billing.CST_Customer                   AS CST ON CPA.CPA_CST_ID = CST.CST_ID
				JOIN Billing.CPP_CustomerProcessingProfile  AS CPP on CPA.CPA_ID = CPP.CPP_CPA_ID
				LEFT JOIN Billing.CDT_CustomerDailyTran     AS CDT on CPP.CPP_ID = CDT.CDT_CPP_ID
																  AND CDT.CDT_TranType = 'Return'
				WHERE CPA.CPA_DoNotBillYN = 'Y'
				  AND CPA.CPA_RunDateYYYYMM = @YearMonth
				  AND CPA.CPA_ProductLine = 'ACH'  
				GROUP BY CPP.CPP_ID
					  ,CST.CST_AccountName
					  ,CPA.CPA_TrustName  
					  ,CPA.CPA_ProductLine  
					  ,CPA.CPA_DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate 
					  ,CPP.CPP_MID  
					  ,CPP.CPP_SourceIdConfig  
					  ,CPP.CPP_RunDateYYYYMM   
					  ,CDT.CDT_TranType
				)                                           AS Rturn ON Orig.CPP_ID = Rturn.CPP_ID
		LEFT JOIN (
				SELECT CPP.CPP_ID                                  AS CPP_ID
					  ,CST.CST_AccountName                         AS AccountName
					  ,CPA.CPA_TrustName                           AS TrustName
					  ,CPA.CPA_ProductLine                         AS ProductLine
					  ,CPA.CPA_DoNotBillStartDate                  AS DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate                    AS DoNotBillEndDate
					  ,CPP.CPP_MID                                 AS MID
					  ,CPP.CPP_SourceIdConfig                      AS SourceIdConfig
					  ,CONCAT(CPP.CPP_SourceIdConfig, CPP.CPP_MID) AS MIDConfig
					  ,CPP.CPP_RunDateYYYYMM                       AS YYYYMM
					  ,CDT.CDT_TranType                            AS TranType
					  ,SUM(ISNULL(CDT.CDT_TranAmount, 0))          AS TranAmount
					  ,SUM(ISNULL(CDT.CDT_TranCount, 0))           AS TranCount
				FROM Billing.CPA_CustomerProcessingAccount  AS CPA
				JOIN Billing.CST_Customer                   AS CST ON CPA.CPA_CST_ID = CST.CST_ID
				JOIN Billing.CPP_CustomerProcessingProfile  AS CPP on CPA.CPA_ID = CPP.CPP_CPA_ID
				LEFT JOIN Billing.CDT_CustomerDailyTran     AS CDT on CPP.CPP_ID = CDT.CDT_CPP_ID
																  AND CDT.CDT_TranType = 'UnAuth'
				WHERE CPA.CPA_DoNotBillYN = 'Y'
				  AND CPA.CPA_RunDateYYYYMM = @YearMonth
				  AND CPA.CPA_ProductLine = 'ACH'  
				GROUP BY CPP.CPP_ID
					  ,CST.CST_AccountName
					  ,CPA.CPA_TrustName  
					  ,CPA.CPA_ProductLine  
					  ,CPA.CPA_DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate 
					  ,CPP.CPP_MID  
					  ,CPP.CPP_SourceIdConfig  
					  ,CPP.CPP_RunDateYYYYMM   
					  ,CDT.CDT_TranType
				)                                          AS Uturn ON Orig.CPP_ID = Uturn.CPP_ID


ELSE
		-----------------------------------------------------------
		--   Credit Card - aggregate Debits, Credits and Declines
		-----------------------------------------------------------
		SELECT Orig.YYYYMM              AS YYYYMM
			  ,Orig.AccountName         AS AccountName
			  ,Orig.TrustName           AS TrustName
			  ,Orig.DoNotBillStartDate  AS DoNotBillStartDate
			  ,Orig.DoNotBillEndDate    AS DoNotBillEndDate
			  ,Orig.MID                 AS MID
			  ,Orig.SourceIdConfig      AS SourceIdConfig
			  ,Orig.MIDConfig           AS MIDConfig
			  ,SUM(Orig.TranAmount)     AS OrigVolume
			  ,SUM(Orig.TranCount)      AS OrigCount
			  ,''                       AS BatchCount
			  ,''                       AS ReturnCount
			  ,''                       AS UnAuthCount
		FROM (
				SELECT CPP.CPP_ID                                  AS CPP_ID
					  ,CST.CST_AccountName                         AS AccountName
					  ,CPA.CPA_TrustName                           AS TrustName
					  ,CPA.CPA_ProductLine                         AS ProductLine
					  ,CPA.CPA_DoNotBillStartDate                  AS DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate                    AS DoNotBillEndDate
					  ,CPP.CPP_MID                                 AS MID
					  ,CPP.CPP_SourceIdConfig                      AS SourceIdConfig
					  ,''                                          AS MIDConfig
					  ,CPP.CPP_RunDateYYYYMM                       AS YYYYMM
					  ,'AnyTran'                                   AS TranType
					  ,SUM(ISNULL(CDT.CDT_TranAmount, 0))          AS TranAmount
					  ,SUM(ISNULL(CDT.CDT_TranCount, 0))           AS TranCount
				FROM Billing.CPA_CustomerProcessingAccount  AS CPA
				JOIN Billing.CST_Customer                   AS CST ON CPA.CPA_CST_ID = CST.CST_ID
				JOIN Billing.CPP_CustomerProcessingProfile  AS CPP on CPA.CPA_ID = CPP.CPP_CPA_ID
				LEFT JOIN Billing.CDT_CustomerDailyTran     AS CDT on CPP.CPP_ID = CDT.CDT_CPP_ID
																  AND CDT.CDT_TranType IN ('Debit','Credit', 'Decline')
				WHERE CPA.CPA_DoNotBillYN = 'Y'
				  AND CPA.CPA_RunDateYYYYMM = @YearMonth
				  AND CPA.CPA_ProductLine = 'Credit Card'  
				GROUP BY CPP.CPP_ID
					  ,CST.CST_AccountName
					  ,CPA.CPA_TrustName  
					  ,CPA.CPA_ProductLine  
					  ,CPA.CPA_DoNotBillStartDate
					  ,CPA.CPA_DoNotBillEndDate 
					  ,CPP.CPP_MID  
					  ,CPP.CPP_SourceIdConfig  
					  ,CPP.CPP_RunDateYYYYMM   
					  ,CDT.CDT_TranType
			 )                                           AS Orig
        GROUP BY Orig.YYYYMM
			    ,Orig.AccountName  
			    ,Orig.TrustName  
			    ,Orig.DoNotBillStartDate 
			    ,Orig.DoNotBillEndDate 
			    ,Orig.MID 
			    ,Orig.SourceIdConfig 
			    ,Orig.MIDConfig

END