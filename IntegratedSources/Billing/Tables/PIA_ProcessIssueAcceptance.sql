﻿CREATE TABLE [Billing].[PIA_ProcessIssueAcceptance] (
    [PIA_ID]                         INT              IDENTITY (1, 1) NOT NULL,
    [PIA_PIT_ID]                     INT              NOT NULL,
    [PIA_CTF_CRMServiceProductFeeID] UNIQUEIDENTIFIER NULL,
    [PIA_CPP_CRMProcessingProfileID] UNIQUEIDENTIFIER NULL,
    [PIA_CPA_CRMProcessingAccountID] UNIQUEIDENTIFIER NULL,
    [PIA_CST_CRMAccountID]           UNIQUEIDENTIFIER NULL,
    [PIA_UserName]                   NVARCHAR (100)   NOT NULL,
    [PIA_AcceptedValue]              NVARCHAR (100)   NOT NULL,
    [PIA_InsertDateTime]             DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PIA_ProcessIssueAcceptance] PRIMARY KEY CLUSTERED ([PIA_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PIA_ProcessIssueAcceptance_PIT_ProcessIssueType] FOREIGN KEY ([PIA_PIT_ID]) REFERENCES [Billing].[PIT_ProcessIssueType] ([PIT_ID])
);

