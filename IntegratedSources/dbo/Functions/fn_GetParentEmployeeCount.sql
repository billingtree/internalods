﻿




CREATE function  [dbo].[fn_GetParentEmployeeCount] 
(@ParentAccountID nvarchar (36))
 RETURNS INTEGER
as
BEGIN
    DECLARE @HoldParentID nvarchar (36)
    DECLARE @ReturnParentEmployeeCount INT 
 
    SET @ReturnParentEmployeeCount = 0
    SET @HoldParentID = @ParentAccountID
    
    BEGIN

        SELECT @ReturnParentEmployeeCount = SUM(sfa.NumberOfEmployees)
        FROM SalesForce.Account sfa
        WHERE sfa.ID = @HoldParentID
           OR sfa.ParentId = @HoldParentID
    END 
   
    RETURN @ReturnParentEmployeeCount


/**********************
ALTER function  [dbo].[fn_GetParentEmployeeCount] 
(@ParentAccountID UniqueIdentifier)
 RETURNS INTEGER
as
BEGIN
    DECLARE @HoldParentID UniqueIdentifier
    DECLARE @ReturnParentEmployeeCount INT 
 
    SET @ReturnParentEmployeeCount = 0
    SET @HoldParentID = @ParentAccountID
    
    BEGIN

        SELECT @ReturnParentEmployeeCount = SUM(Acct.NumberOfEmployees)
        FROM CRMShadow.dbo.Account         AS Acct
        WHERE Acct.AccountId = @HoldParentID
           OR Acct.ParentAccountId = @HoldParentID
    END 
   
    RETURN @ReturnParentEmployeeCount
   
   
**************************/   
END    
 





