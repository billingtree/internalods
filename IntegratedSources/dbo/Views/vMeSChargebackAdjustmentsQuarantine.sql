﻿



CREATE VIEW [dbo].[vMeSChargebackAdjustmentsQuarantine]
AS
SELECT	ID
		,ReportReferenceCode
		,MerchantID
		,DBAName
		,ControlNumber
		,IncomingDate
		,CardNumber
		,ReferenceNumber
		,TransactionDate
		,TransactionAmount
		,TridentTransactionID
		,PurchaseID
		,ClientReferenceNumber
		,AuthorizationCode
		,AdjustmentDate
		,AdjustmentRefereneNum
		,Reason
		,FirstTime
		,ReasonCode
		,CBReferenceNumber
		,TerminalID
		,MashupOutcome
		,CASE MashupOutcome
			WHEN 1 THEN 'Spurious'
			WHEN 2 THEN 'UnMatched'
			WHEN 3 THEN 'Matched'
			WHEN 4 THEN 'Duplicates'
		END AS MashupOutcomeDescription
		,InsertDate
FROM	MeS.ChargebackAdjustments
WHERE	(UsabilityIndex = 0)




