﻿CREATE TABLE [Billing].[PIT_ProcessIssueType] (
    [PIT_ID]                INT            NOT NULL,
    [PIT_Name]              NVARCHAR (50)  NOT NULL,
    [PIT_Category]          NVARCHAR (50)  NOT NULL,
    [PIT_Description]       NVARCHAR (255) NOT NULL,
    [PIT_SubReportLocation] NVARCHAR (255) NULL,
    CONSTRAINT [PK_PIT_ProcessIssueType] PRIMARY KEY CLUSTERED ([PIT_ID] ASC) WITH (FILLFACTOR = 80)
);

