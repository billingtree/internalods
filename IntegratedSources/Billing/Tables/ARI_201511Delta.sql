﻿CREATE TABLE [Billing].[ARI_201511Delta] (
    [ProcessingNbr]   NVARCHAR (8)  NULL,
    [ConfigNbr]       TINYINT       NULL,
    [ItemNumber]      NVARCHAR (50) NULL,
    [InitialQuantity] INT           NULL,
    [InitialExtPrice] MONEY         NULL,
    [ProdQuantity]    INT           NULL,
    [ProdExtPrice]    MONEY         NULL,
    [DeltaQuantity]   INT           NULL,
    [DeltaExtPrice]   MONEY         NULL
);

