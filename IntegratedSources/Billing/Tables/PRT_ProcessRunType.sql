﻿CREATE TABLE [Billing].[PRT_ProcessRunType] (
    [PRT_ID]          INT            IDENTITY (1, 1) NOT NULL,
    [PRT_Name]        NVARCHAR (255) NOT NULL,
    [PRT_Description] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_PRT_ProcessRunType] PRIMARY KEY CLUSTERED ([PRT_ID] ASC) WITH (FILLFACTOR = 80)
);

