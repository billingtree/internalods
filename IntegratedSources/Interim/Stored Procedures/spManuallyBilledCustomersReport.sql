﻿
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [Interim].[spManuallyBilledCustomersReport]
    (
      @RunYear INT ,
      @RunMonth INT
    )

-- Insert statements for procedure here
AS
    BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @Year INT;
        DECLARE @Month INT;

        DECLARE @RunDate DATETIME;
        DECLARE @RunDateBeginMonth DATETIME;
      
        SET @Year = @RunYear;
        SET @Month = @RunMonth;
      
      
    --select CONVERT(DATETIME, CONCAT(EOMONTH(CONVERT(DATE, CONCAT('2014', '-', '10', '-', '01'))), ' 23:59:59'))  
    --select CONVERT(DATETIME, CONCAT('2014', '-', '10', '-', '01', ' 00:00:00'))  
      
        SET @RunDate = CONVERT(DATETIME, CONCAT(EOMONTH(CONVERT(DATE, CONCAT(@Year,
                                                              '-', @Month, '-',
                                                              '01'))),
                                                ' 23:59:59')); 
        SET @RunDateBeginMonth = CONVERT(DATETIME, CONCAT(@Year, '-', @Month,
                                                          '-', '01',
                                                          ' 00:00:00'));
      
        DECLARE @txtYear VARCHAR(4);
        DECLARE @txtMonth VARCHAR(2);
        DECLARE @YearMonth INT;
 
        SET @txtYear = CONVERT(VARCHAR(4), @Year);
        IF @Month < 10
            SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)); 
        ELSE
            SET @txtMonth = CONVERT(VARCHAR(2), @Month); 
    
        SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth));          
      
	      

-------------------------------------------------------------------------
--    This will drive through the MIDManuallyBilled table looking for 
--    Processing Profile ID's that are to be Manually Billed
--        ACH Counts first
--        Then CC Counts
--        Then ACH/CC Monthly Fees
--        Then ACH Batch Count
-------------------------------------------------------------------------

        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                ATI.FileCreationDate AS TranDate ,
                0 AS Rate ,
                SUM(ATI.FormattedAmount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(4) ACH Trans' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN NACHA.Originations AS ATI ON MB.CRM_Id = ATI.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND ATI.UsabilityIndex = 1
                AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
                AND ATI.FileCreationDateYYYYMM = @YearMonth
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                ATI.FileCreationDate
			--,Fee.SellingPrice  
        UNION ALL
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                ATI.FileCreationDate AS TranDate ,
                0 AS Rate ,
                SUM(ATI.FormattedAmount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(5) Returns' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN NACHA.Returns AS ATI ON PP.bt_ProcessingProfileId = ATI.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeACHReturn  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND ATI.UsabilityIndex = 1
                AND UPPER(ATI.StandardEntryClassCode) <> 'COR'
                AND ATI.ReturnCode NOT IN ( 'R05', 'R07', 'R10', 'R29', 'R51' )
                AND ATI.FileCreationDateYYYYMM = @YearMonth
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                ATI.FileCreationDate
			--,Fee.SellingPrice  
        UNION ALL
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                ATI.FileCreationDate AS TranDate ,
                0 AS Rate ,
                SUM(ATI.FormattedAmount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(7) UnAuth' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN NACHA.Returns AS ATI ON PP.bt_ProcessingProfileId = ATI.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeACHReturn  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND ATI.UsabilityIndex = 1
                AND UPPER(ATI.StandardEntryClassCode) <> 'COR'
                AND ATI.ReturnCode IN ( 'R05', 'R07', 'R10', 'R29', 'R51' )
                AND ATI.FileCreationDateYYYYMM = @YearMonth
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                ATI.FileCreationDate
			--,Fee.SellingPrice 
        UNION ALL
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                PP.bt_BankMID AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                USA.TransDate AS TranDate ,
                0 AS Rate ,
                SUM(USA.Amount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(1) Debits' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN USAePay.FileData AS USA ON PP.bt_ProcessingProfileId = USA.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND USA.UsabilityIndex = 1
                AND USA.TransDateYYYYMM = @YearMonth
                AND USA.TransType = 'S'
                AND USA.Result = 'A'
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                PP.bt_BankMID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                USA.TransDate
			--,Fee.SellingPrice 
        UNION ALL
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                PP.bt_BankMID AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                USA.TransDate AS TranDate ,
                0 AS Rate ,
                SUM(USA.Amount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(3) Declines' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN USAePay.FileData AS USA ON PP.bt_ProcessingProfileId = USA.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND USA.UsabilityIndex = 1
                AND USA.TransDateYYYYMM = @YearMonth
                AND USA.Result = 'D'
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                PP.bt_BankMID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                USA.TransDate
			--,Fee.SellingPrice  
        UNION ALL
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                PP.bt_BankMID AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                USA.TransDate AS TranDate ,
                0 AS Rate ,
                SUM(USA.Amount) AS TranAmount ,
                COUNT(*) AS TranCount ,
                COUNT(*) * 0 AS Revenue ,
                '(2) Credits' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                LEFT JOIN USAePay.FileData AS USA ON PP.bt_ProcessingProfileId = USA.CRM_PPDID
	--LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND USA.UsabilityIndex = 1
                AND USA.TransDateYYYYMM = @YearMonth
                AND USA.TransType = 'C'
                AND USA.Result = 'A'
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                PP.bt_BankMID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate ,
                USA.TransDate
			--,Fee.SellingPrice 
        UNION ALL    
/*****************************************************    
    SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
			       CONCAT(PP.bt_Config,'-',PP.bt_BankMID)              
			   ELSE
			       PP.bt_BankMID 
			END                                                AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,NULL                                                AS TranDate
		  ,0                                    AS Rate
		  ,NULL                                                AS TranAmount 
		  ,1                                                   AS TranCount
		  ,0                                    AS Revenue
		  ,'Monthly'                                           AS SourceData
    -- Didn't use MIDManuallyBilled here, since that table has potentially
    -- 2 Processing Accounts, in order to handle MIDs that are now Soft Deleted,
    -- but still have ODS transactions tied to those Soft Deletes.
    -- This will ensure only getting back 1 Monthly Fee record, not 2.
	FROM CRMShadow.MS.bt_ProcessingAccount          AS PA   
	LEFT JOIN CRMShadow.MS.bt_ProcessingProfile     AS PP   ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
    --LEFT JOIN CRM.vServiceProductFeeMonthly  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
	WHERE PA.bt_ManualBillingStartDate IS NOT NULL
	  AND PA.bt_ManualBillingStartDate <= @RunDate
	  AND (PA.bt_ManualBillingEndDate IS NULL OR PA.bt_ManualBillingEndDate >= @RunDateBeginMonth)
	  AND PA.StatusCodeName = 'Active'  
	  AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)
	  
	UNION ALL
*********************************************************/
        SELECT  PA.bt_AccountName AS ChildName ,
                A.Address1_StateOrProvince AS ChildState ,
                PA.bt_Name AS Name ,
                PA.bt_ProductLineName AS ProductLine ,
                PA.bt_ManualBillingStartDate AS ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate AS ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) AS MID ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END AS SourceConfig ,
                PP.bt_StartDate AS MIDStartDate ,
                PP.bt_EndDate AS MIDEndDate ,
                NULL AS TranDate ,
                0 AS Rate ,
                0 AS TranAmount ,
                COUNT(*) AS TranCount ,
                0 AS Revenue ,
                '(6) Batches' AS SourceData
        FROM    Interim.MIDManuallyBilled AS MB
                JOIN CRMShadow.MS.bt_ProcessingProfile AS PP ON MB.CRM_Id = PP.bt_ProcessingProfileId
                JOIN CRMShadow.MS.bt_ProcessingAccount AS PA ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
                JOIN CRMShadow.MS.Account AS A ON PA.bt_Account = A.AccountID
                JOIN (  -- Get Batch records from ATI 
                       SELECT DISTINCT
                                ATI.CRM_PPDID AS CRM_PPDID ,
                                ATI.FileReferenceCodeID AS FileRef ,
                                ATI.FileCreationDate AS CreateDate ,
                                ATI.BatchNumber AS BatchNumber
	                  --,ATI.BlockNumber          AS Block
                       FROM     NACHA.Originations AS ATI
                       WHERE    ATI.UsabilityIndex = 1
                                AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
                                AND YEAR(ATI.FileCreationDate) = @Year
                                AND MONTH(ATI.FileCreationDate) = @Month
                     ) AS ATI ON MB.CRM_Id = ATI.CRM_PPDID
        WHERE   MB.StartDate <= @RunDate
                AND ( MB.EndDate IS NULL
                      OR MB.EndDate >= @RunDateBeginMonth
                    )
                AND ( PP.bt_EndDate IS NULL
                      OR PP.bt_EndDate >= @RunDateBeginMonth
                    )
                AND PA.bt_ProductLineName = 'ACH'
        GROUP BY PA.bt_AccountName ,
                A.Address1_StateOrProvince ,
                PA.bt_Name ,
                PA.bt_ProductLineName ,
                PA.bt_ManualBillingStartDate ,
                PA.bt_ManualBillingEndDate ,
                CONCAT(PP.bt_Config, '-', PP.bt_BankMID) ,
                CASE WHEN PA.bt_ProductLineName = 'ACH' THEN PP.bt_Config
                     ELSE PP.bt_SourceId
                END ,
                PP.bt_StartDate ,
                PP.bt_EndDate;   



    END;

















