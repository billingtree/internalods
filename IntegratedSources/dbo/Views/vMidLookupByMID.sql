﻿
--DROP VIEW [dbo].[vMidLookupByMID]

CREATE VIEW [dbo].[vMidLookupByMID]
AS
      
SELECT DISTINCT
       MID                                 AS MID 
      ,CONVERT(INT, StartYYYYMM) AS StartYYYYMM
      ,CONVERT(INT, EndYYYYMM)   AS EndYYYYMM
FROM dbo.MidLookupUMS 
UNION
SELECT DISTINCT
       MID                                 AS MID
      ,CONVERT(INT, StartYYYYMM) AS StartYYYYMM
      ,CONVERT(INT, EndYYYYMM)   AS EndYYYYMM
FROM dbo.MidLookupMeS 



