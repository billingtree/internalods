﻿







CREATE VIEW [ATI].[vRODS] AS
SELECT	R1.ReferenceCode
		,R1.ConfigNumber
		,R1.FileCreationDate
		,R1.FileCreationDateYYYYMM
		,R1.FileCreationTime
		,R5.ServiceClassCode
		,R5.CompanyName
		,R5.CompanyIdentification
		,R5.StandardEntryClassCode
		,R5.EffectiveEntryDate
		,R6.*
		,CASE R6.TransactionCode
			WHEN 21 THEN CONVERT(numeric(16,2), R6.Amount)/100
			WHEN 31 THEN CONVERT(numeric(16,2), R6.Amount)/100       
			WHEN 26 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1
			WHEN 36 THEN CONVERT(numeric(16,2), R6.Amount)/100*-1              
			ELSE CONVERT(numeric(16,2), R6.Amount)/100     
		END AS FormattedAmount
		,CASE R6.MashupOutcome
			WHEN 0 THEN 'Matched'
			WHEN 1 THEN 'CRM Duplicates'
			WHEN 2 THEN 'Concordance Mismatch'
			WHEN 3 THEN 'CRM Mismatch'
		END AS MashupOutcomeDescription
FROM	(
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6ARCEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6CCDEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6COREntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyNameORIDNumber
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6CTXEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6PPDEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IdentificationNumber
					,ReceivingCompanyName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6RCKEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6TELEntryDetail
			WHERE	UsabilityIndex = 1
			UNION ALL
			SELECT	FileReferenceCodeID
					,BlockNumber
					,ReturnCode
					,TransactionCode
					,Amount
					,IndividualIdentificationNumber
					,IndividualName
					,UsabilityIndex
					,MashupOutcome
			FROM	ATI.vR6WEBEntryDetail
			WHERE	UsabilityIndex = 1
		) AS R6
			LEFT JOIN ATI.vR5BlockHeader AS R5
				ON R6.FileReferenceCodeID = R5.FileReferenceCodeID
				AND R5.BlockNumber=R6.BlockNumber
			LEFT JOIN ATI.vR1FileHeader AS R1
				ON R1.FileReferenceCodeID = R5.FileReferenceCodeID
