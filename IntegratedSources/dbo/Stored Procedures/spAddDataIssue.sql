﻿








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAddDataIssue]
(
 @DataIssue      NVARCHAR (2000)
,@CreatedBy      NVARCHAR (255)


)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 

    DECLARE @Issue      NVARCHAR (2000)
    DECLARE @Who        NVARCHAR (255)
	       
	SET @Issue = @DataIssue
	SET @Who = @CreatedBy


	--SELECT @Date = CreateDate
	--FROM dbo.RatesIgnoreAccount  
	--WHERE AccountNumber = @AcctNbr

    
		--IF @Date IS NULL  
		    BEGIN       
				BEGIN TRANSACTION
					INSERT INTO CRM.DIL_DataIssueLog  
					SELECT ''                         AS DIL_Priority
					      ,@Issue                     AS DIL_DataIssue
					      ,''                         AS DIL_ReportYN
						  ,'Identify'                 AS DIL_Status
						  ,''                         AS DIL_IssueOwner
						  ,NULL                       AS DIL_EstimatedClosedDate
						  ,''                         AS DIL_Comment
						  ,NULL                       AS DIL_ClosedOn
						  ,''                         AS DIL_ClosedBy
						  ,CONVERT(DATE, GetDate())   AS DIL_CreatedOn
						  ,@Who                       AS DIL_CreatedBy
						  ,NULL                       AS DIL_ModifiedOn
						  ,''                         AS DIL_ModifiedBy
						  ,''                         AS DIL_ReportLocationName
				COMMIT  
            END
	   -- ELSE
	   --     BEGIN
		--		BEGIN TRANSACTION
		--			DELETE dbo.RatesIgnoreAccount 
		--			WHERE AccountNumber = @AcctNbr                   
		--		COMMIT  		  
		--		SET @AcctNbr = 'Removed'  
	    --    END		


		SELECT ''                         AS DIL_Priority
		      ,@Issue                     AS DIL_DataIssue
		      ,''                         AS DIL_ReportYN
			  ,'Identify'                 AS DIL_Status
			  ,''                         AS DIL_IssueOwner
			  ,NULL                       AS DIL_EstimatedClosedDate
			  ,''                         AS DIL_Comment
			  ,NULL                       AS DIL_ClosedOn
			  ,''                         AS DIL_ClosedBy
			  ,CONVERT(DATE, GetDate())   AS DIL_CreatedOn
			  ,@Who                       AS DIL_CreatedBy
			  ,NULL                       AS DIL_ModifiedOn
			  ,''                         AS DIL_ModifiedBy
              ,''                         AS DIL_ReportLocationName
              
              
              
END














