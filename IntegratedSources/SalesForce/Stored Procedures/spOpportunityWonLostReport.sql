﻿



-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spOpportunityWonLostReport](
 @FromDate DATE
,@ToDate   DATE
,@Mode     CHAR(4) 
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
SET NOCOUNT ON; 

	DECLARE @StartDate DATE
	       ,@EndDate   DATE
	       ,@RunMode   CHAR(4)
	       ,@OppStage  CHAR(25)
	SET @StartDate=@FromDate
	SET @EndDate=@ToDate
    SET @RunMode=@Mode

    BEGIN
		IF @RunMode = 'Won' 
			SET @OppStage = 'Closed Won'
		ELSE
			SET @OppStage = 'Closed Lost'    
	END


		SELECT CASE PP.bt_ProductLineName
				   WHEN 'Credit Card' THEN 
					   'CC' 
				   WHEN 'ACH' THEN
					   'ACH'
				   ELSE
					   ' '
			   END                                                        AS ProductLine
			  ,CASE
				   WHEN A.ParentAccountIdName > '' THEN
					   A.ParentAccountIdName
				   ELSE 
					   A.Name
			   END                                                        AS Parent            
			  ,A.Name                                                     AS Child
			  ,PP.bt_BankMID                                              AS MID
			  --,PP.bt_ProcessingProfileId                                  AS ProcessingProfileId
			  ,CASE
				   WHEN SO.bt_ApplicationStageName > ' ' THEN
					   SO.bt_ApplicationStageName
				   ELSE 
					   CONVERT(NVARCHAR, SO.bt_ApplicationStage)
			   END                                                        AS AppStage
			  ,Opp.Name                                                   AS OppName
			  ,A.OwnerIdName                                              AS AccountOwnerName
			  ,Opp.bt_TypeName                                            AS TypeName
			  ,Opp.bt_OpportunityStageName                                AS OpportunityStage
			  ,A.bt_SubTypeName                                           AS SubTypeName
			  ,Opp.ActualCloseDate                                        AS ActualCloseDate
			  ,Opp.StatusCodeName                                         AS OpportunityStatusReason
			  ,Opp.bt_TotalEstimatedMonthlyFees                           AS TotalEstimatedMonthlyFees
			  ,Opp.EstimatedValue                                         AS EstimatedRevenue
			  ,SO.bt_EstimatedMonthlyDebitVolume                          AS EstimatedMonthlyDebitVolume
			  ,A.NumberOfEmployees                                        AS NumberOfEmployees
		      ,A.bt_SICCodeName                                           AS Industry
		      ,PartProd.PartnerName                                       AS PartnerName
		      ,PartProd.PartnerSubType                                    AS PartnerSubType
		      ,PartProd.ServProdFeeStartDate                              AS ServiceProductFeeStartDate
		      ,PartProd.ServProdFeeEndDate                                AS ServiceProductFeeEndDate
		      ,PartProd.ServProdFeeSoftDelete                             AS ServiceProductFeeSoftDelete
		      --,L.bt_NumberOfEmployees                                     AS LeadNumberOfEmployees
		      --,L.bt_LeadSourceName                                        AS LeadSource
		      --,L.Subject                                                  AS LeadSubject
		      ,Opp.Description                                            AS DescriptionComments
		      ,OHSP.PriorStage                                            AS PriorStage
		FROM CRMShadow.MS.Opportunity                          AS Opp
		LEFT JOIN CRMShadow.MS.bt_ProcessingAccount            AS PA     ON Opp.OpportunityId = PA.bt_Opportunity
		LEFT JOIN CRMShadow.MS.bt_ProcessingProfile            AS PP     ON PA.bt_ProcessingAccountId = PP.bt_ProcessingAccount
		LEFT JOIN CRMShadow.MS.Account                         AS A      ON PA.bt_Account = A.AccountId  
		                                                                --AND A.OwnerIdName <> 'CRM Dev'  
		LEFT JOIN CRMShadow.MS.bt_ServiceOrder                 AS SO     ON PA.bt_CurrentServiceOrder = SO.bt_ServiceOrderId
	    LEFT JOIN CRM.vPartnerWithAllSubTypes                  AS PartProd   ON PA.bt_CurrentServiceOrder = PartProd.ServiceOrderId
	    --LEFT JOIN CRMShadow.MS.Lead                            AS L      ON Opp.OriginatingLeadId = L.LeadId
	    LEFT JOIN CRM.vOpportunityHistoryStagePrior            AS OHSP   ON Opp.OpportunityId = OHSP.OpportunityId
		WHERE CONVERT(DATE, Opp.ActualCloseDate) BETWEEN @StartDate AND @EndDate
		  AND Opp.bt_OpportunityStageName = @OppStage
		  AND Opp.OwnerIdName <> 'CRM Dev'



END












