﻿CREATE TABLE [SalesForce].[ProcessingStageHistory] (
    [Created_By_UserName__c]  NVARCHAR (80)  NULL,
    [CreatedById]             NVARCHAR (18)  NULL,
    [CreatedDate]             DATETIME       NULL,
    [Date_Stage_Completed__c] DATETIME       NULL,
    [Date_Stage_Entered__c]   DATETIME       NULL,
    [Id]                      NVARCHAR (36)  NOT NULL,
    [IsDeleted]               BIT            NULL,
    [LastModifiedById]        NVARCHAR (18)  NULL,
    [LastModifiedDate]        DATETIME       NULL,
    [Name]                    NVARCHAR (100) NULL,
    [Number_of_Days__c]       NVARCHAR (3)   NULL,
    [Previous_Stage__c]       NVARCHAR (80)  NULL,
    [Processing__c]           NVARCHAR (36)  NULL,
    [Stage_Update__c]         NVARCHAR (255) NULL,
    [SystemModstamp]          DATETIME       NULL,
    [Updated_By__c]           NVARCHAR (18)  NULL,
    [Updated_By_UserName__c]  NVARCHAR (80)  NULL,
    [InsertDate]              DATETIME       CONSTRAINT [DF_ProcessingStageHistory_InsertDate] DEFAULT (getdate()) NOT NULL,
    [UpdateDate]              DATETIME       NULL,
    CONSTRAINT [PK_ProcessingStageHistory] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_ProcessingStageHistory_PaymentProcessing] FOREIGN KEY ([Processing__c]) REFERENCES [SalesForce].[PaymentProcessing] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [NCI_Name]
    ON [SalesForce].[ProcessingStageHistory]([Name] ASC)
    INCLUDE([Date_Stage_Entered__c], [Processing__c]) WITH (FILLFACTOR = 80);

