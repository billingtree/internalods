﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spConvenienceFeeAssociationReport](
@CustomerMerchantID   FLOAT,
@BTConvFeeMerchantID  FLOAT,
@Month                INT,
@Year                 INT
) WITH RECOMPILE

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @nCustomerMerchantID       FLOAT
    DECLARE @nBTConvFeeMerchantID      FLOAT
    
    SET @nCustomerMerchantID = @CustomerMerchantID
    SET @nBTConvFeeMerchantID = @BTConvFeeMerchantID      
    
    DECLARE @YearMonth                 INT
    DECLARE @txtYear                   VARCHAR (4)
    DECLARE @txtMonth                  VARCHAR (2)
 
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))       
    
    
SELECT USACustomer.SourceID                AS "CustomerSource"
      ,USACustomer.MerchantName            AS "CustomerName"
      ,USACustomer.MerchantID              AS "CustomerMerchantID"
      ,USACustomer.TransType               AS "CustomerTranType"
      ,CASE 
           WHEN USACustomer.Result = 'A' THEN 'Approved'
           WHEN USACustomer.Result = 'D' THEN 'Declined'
           WHEN USACustomer.Result = 'E' THEN 'Error'
           ELSE 'Unknown'                    
       END                                 AS "CustomerResult"
      ,USACustomer.TransDate               AS "CustomerTranDate"
      ,USACustomer.TransTime               AS "CustomerTranTime"
      ,USACustomer.AccountHolder           AS "CustomerAccountName"
      ,USACustomer.Invoice                 AS "CustomerInvoice"
      ,USACustomer.Amount                  AS "CustomerAmount"
      ,USACustomer.Description             AS "CustomerDescription"
      ,USACustomer.AVSZip                  AS "CustomerZip"
      ,BTConvFee.SourceID                  AS "BTSource"
      ,BTConvFee.MerchantName              AS "BTName"
      ,BTConvFee.MerchantID                AS "BTMerchantID"
      ,BTConvFee.TransType                 AS "BTTranType"
      ,CASE 
           WHEN BTConvFee.Result = 'A' THEN 'Approved'
           WHEN BTConvFee.Result = 'D' THEN 'Declined'
           WHEN BTConvFee.Result = 'E' THEN 'Error'
           WHEN BTConvFee.Result IS null THEN ' '
           ELSE 'Unknown'                    
       END                                 AS "BTResult"
      ,BTConvFee.TransDate                 AS "BTTranDate"
      ,BTConvFee.TransTime                 AS "BTTranTime"
      ,BTConvFee.AccountHolder             AS "BTAccountName"
      ,BTConvFee.Invoice                   AS "BTInvoice"
      ,BTConvFee.Amount                    AS "BTAmount"
      ,BTConvFee.Description               AS "BTDescription"
      ,BTConvFee.AVSZip                    AS "BTZip" 
      ,@YearMonth                          AS "ParmYYYYMM"
      ,CASE 
           WHEN USACustomer.Result <> BTConvFee.Result THEN 'Yes'
           ELSE ' '                    
       END                                 AS "ResultDiscrepancy"      
FROM USAePay.FileData      AS USACustomer
LEFT JOIN USAePay.FileData AS BTConvFee ON USACustomer.Invoice = BTConvFee.Invoice
                                  AND USACustomer.TransDate = BTConvFee.TransDate
                                  AND LOWER(USACustomer.AccountHolder) = 
                                      LOWER(BTConvFee.AccountHolder)
                                  --AND dbo.fnDiffInTimeInSeconds (USACustomer.TransTime, BTConvFee.TransTime) BETWEEN 0 and 45
                                  AND BTConvFee.MerchantID = @nBTConvFeeMerchantID
                                  AND BTConvFee.TransType = 'S'
                                  AND BTConvFee.UsabilityIndex = 1
                                  --AND BTConvFee.Result = 'A'
WHERE USACustomer.TransDateYYYYMM = @YearMonth
  AND USACustomer.MerchantID = @nCustomerMerchantID
  AND USACustomer.TransType = 'S'
  AND USACustomer.UsabilityIndex = 1
 -- AND USACustomer.Result = 'A'




END
