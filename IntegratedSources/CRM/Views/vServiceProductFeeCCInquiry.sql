﻿










CREATE VIEW [CRM].[vServiceProductFeeCCInquiry]
AS

--------------------------------------------
--  CC Platform Submission (Inquiry) Fee  --
--------------------------------------------
SELECT SPF.bt_Account             AS AccountId
      ,SPF.bt_Opportunity         AS OpportunityId
      ,SPF.bt_ProcessingAccount   AS ProcessingAccountId
      ,SPF.bt_ServiceOrder        AS ServiceOrderId
      ,SPF.bt_Product             AS ProductId
      ,SPF.bt_ServiceProductFeeId AS ServiceProductFeeId
      ,SPF.bt_FeeTypeName         AS FeeType
      ,SPF.bt_Amount              AS SellingPrice
      ,SPF.bt_IntegrationLevel    AS ProductFeeIntegrationLevel
      ,SPF.bt_StartDate           AS FeeStartDate
      ,SPF.bt_EndDate             AS FeeEndDate
      ,SPF.StatusCodeName         AS StatusCodeName
      ,SPF.CreatedOn              AS CreatedOn
      ,SPF.ModifiedOn             AS ModifiedOn
FROM [$(CRMShadow)].MS.bt_ServiceProductFee    AS SPF
WHERE SPF.bt_FeeTypeName IN ('CC Platform Submission Fee', 'CC Platform Submission Fee (CC only)')
  AND SPF.StatusCodeName = 'Active'
 







