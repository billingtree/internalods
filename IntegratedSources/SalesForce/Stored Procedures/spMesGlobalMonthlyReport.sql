﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spMesGlobalMonthlyReport](
@Month1 INT,
@Year1  INT,
@Mode   CHAR
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

    DECLARE @vMonth       INT
    DECLARE @vYear        INT
    DECLARE @vMode        CHAR (1)
    
    SET @vMonth = @Month1
    SET @vYear = @Year1
    SET @vMode = @Mode
    
    
--------------    For use of TransDateYYYYMM and BatchDate YYYYMM
    DECLARE @vYearMonth    INT
    DECLARE @txtYear       VARCHAR (4)
    DECLARE @txtMonth      VARCHAR (2)
    
 
    SET @txtYear = CONVERT(VARCHAR (4), @vYear)
    IF @vMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @vMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @vMonth) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))      
    
     
    BEGIN
    IF @vMode = 'T'   --   Get by Transaction Date   
        ------------------------------------------------------------------
        --   This is MeS Monthly data;                                  --
        --   Sales are Positive and Credits are Negative $ Amounts      --                                   --
        ------------------------------------------------------------------
        SELECT sfp.CC_Merchant_number__c                          "Merchant ID", 
               sfa.Id                                             "AcctID", 
               sfa.Name                                           "Child",  
               SUM(MeS.TransactionAmount)                         "TotalVolume", 
               count(*)                                           "TotalCount", 
               sfa.Existing_Clients_Profile__c                    "Parent",
              'MeS'                                               "Type", 
               CONCAT(@vMonth,'-',@vYear)                         "MM-YYYY",
               @vMonth                                            "MM", 
               @vYear                                             "YYYY"
        FROM MeS.SettlementSummary              MeS
         LEFT JOIN SalesForce.PaymentProcessing sfp    ON MeS.CRM_PPDID = sfp.Id
         LEFT JOIN SalesForce.Account           sfa    ON sfp.Account__c = sfa.Id
        WHERE MeS.TransDateYYYYMM = @vYearMonth
          AND MeS.UsabilityIndex = 1
        GROUP BY sfp.CC_Merchant_number__c, 
                 sfa.Id, sfa.Name, 
                 sfa.Existing_Clients_Profile__c

        UNION ALL
        ---------------------------------------------------------
        --   This is Global Monthly data;                      --
        -- Sales are Tran Code 5 and Credits are Tran Code 06, --
        -- which have been formatted Positive and Negative     --                                    --
        ---------------------------------------------------------
        SELECT sfp.CC_Merchant_number__c               "Merchant ID", 
               sfa.Id                                  "AcctID", 
               sfa.Name                                "Child",  
               SUM(G.Amount)                           "TotalVolume", 
               count(*)                                "TotalCount", 
               sfa.Existing_Clients_Profile__c         "Parent", 
               'Global'                                "Type", 
               CONCAT(@vMonth,'-',@vYear)              "MM-YYYY",
               @vMonth                                 "MM", 
               @vYear                                  "YYYY"
        FROM Global.FileDataPersisted G
         LEFT JOIN SalesForce.PaymentProcessing sfp   ON G.CRM_PPDID = sfp.Id
         LEFT JOIN SalesForce.Account sfa             ON sfp.Account__c = sfa.Id
        WHERE G.TransDateYYYYMM = @vYearMonth
          AND G.UsabilityIndex = 1
        GROUP BY sfp.CC_Merchant_number__c, sfa.Id, 
                 sfa.Name, 
                 sfa.Existing_Clients_Profile__c
                 
    ELSE          ----------------------------------
                  --  Get records by Batch Date   --
                  ----------------------------------
        ------------------------------------------------------------------
        --   This is MeS Monthly data;                                  --
        --   Sales are Positive and Credits are Negative $ Amounts      --                                   --
        ------------------------------------------------------------------
        SELECT sfp.CC_Merchant_number__c                          "Merchant ID", 
               sfa.Id                                             "AcctID", 
               sfa.Name                                           "Child",  
               SUM(MeS.TransactionAmount)                         "TotalVolume", 
               count(*)                                           "TotalCount", 
               sfa.Existing_Clients_Profile__c                    "Parent",
              'MeS'                                               "Type", 
               CONCAT(@vMonth,'-',@vYear)                         "MM-YYYY",
               @vMonth                                            "MM", 
               @vYear                                             "YYYY"
        FROM MeS.SettlementSummary              MeS
         LEFT JOIN SalesForce.PaymentProcessing sfp    ON MeS.CRM_PPDID = sfp.Id
         LEFT JOIN SalesForce.Account           sfa    ON sfp.Account__c = sfa.Id
        WHERE MeS.BatchDateYYYYMM = @vYearMonth
          AND MeS.UsabilityIndex = 1
        GROUP BY sfp.CC_Merchant_number__c, 
                 sfa.Id, sfa.Name, 
                 sfa.Existing_Clients_Profile__c

        UNION ALL
              --   Batch Date (known as Deposit Date)    --
        ---------------------------------------------------------
        --   This is Global Monthly data;                      --
        -- Sales are Tran Code 5 and Credits are Tran Code 06, --
        -- which have been formatted Positive and Negative     --                                    --
        ---------------------------------------------------------
        SELECT sfp.CC_Merchant_number__c               "Merchant ID", 
               sfa.Id                                  "AcctID", 
               sfa.Name                                "Child",  
               SUM(G.Amount)                           "TotalVolume", 
               count(*)                                "TotalCount", 
               sfa.Existing_Clients_Profile__c         "Parent", 
               'Global'                                "Type", 
               CONCAT(@vMonth,'-',@vYear)              "MM-YYYY",
               @vMonth                                 "MM", 
               @vYear                                  "YYYY"
        FROM Global.FileDataPersisted G
         LEFT JOIN SalesForce.PaymentProcessing sfp   ON G.CRM_PPDID = sfp.Id
         LEFT JOIN SalesForce.Account sfa             ON sfp.Account__c = sfa.Id
        WHERE G.DepositDateYYYYMM = @vYearMonth
          AND G.UsabilityIndex = 1
        GROUP BY sfp.CC_Merchant_number__c, sfa.Id, 
                 sfa.Name, 
                 sfa.Existing_Clients_Profile__c
                                      
                 
    END                 


END

