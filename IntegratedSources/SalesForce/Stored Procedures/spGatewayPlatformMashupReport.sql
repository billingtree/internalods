﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spGatewayPlatformMashupReport](
@Month INT,
@Year  INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @vMonth int, @vYear int
	SET @vMonth=@Month
	SET @vYear=@Year

    -- Insert statements for procedure here
    
    -- Concatenate Year and Month in order to use the Index
    DECLARE @vYearMonth INT
    DECLARE @txtYear   VARCHAR (4)
    DECLARE @txtMonth  VARCHAR (2)
    
 
    SET @txtYear = CONVERT(VARCHAR (4), @vYear)
    IF @vMonth < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @vMonth)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @vMonth) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))       

-----------------------------------------------------------------------
--   Find exposure of what did not match on MeS
------------------------------------------------------------------------ 
SELECT USA.MerchantName                                AS USAMerchant
       ,USA.MerchantID                                 AS USAMerchantID
       ,USA.TransDate                                  AS USATranDate
       ,USA.AuthCode                                   AS USAAuthCode
       ,USA.Amount                                     AS USAAmount
       ,MeS.TransactionAmount                          AS MeSAmount
       ,MeS.AuthorizationCode                          AS MeSAuthCode                   
       ,MeS.TransactionDate                            AS MeSTranDate
       ,MeS.MerchantID                                 AS MeSMerchantID
       ,MeS.DBAName                                    AS MeSMerchant
       ,USA.AccountHolder                              AS USAAccountHolder
       ,USA.TransDateYYYYMM                            AS USATranDateYYYYMM 
       ,USA.TransType                                  AS USATranType
       ,USA.Result                                     AS USAResult
       ,USA.Platform                                   AS USAPlatform
       ,USA.CardType                                   AS CardType
       ,MeS.CRM_PPDID                                  AS "MeSID"
       ,USA.CRM_PPDID                                  AS "USAID"
FROM USAePay.FileData             USA  
  LEFT JOIN MeS.SettlementSummary MeS   ON --USA.CRM_PPDID = MeS.CRM_PPDID
                                         USA.MerchantID = MeS.MerchantID
                                     AND USA.AuthCode = MeS.AuthorizationCode
                                     AND MeS.UsabilityIndex = 1
                                     AND USA.TransDate = MeS.TransactionDate
                                     AND USA.Amount = MeS.TransactionAmount
WHERE USA.TransDateYYYYMM = @vYearMonth
  AND USA.TransType = 'S'
  AND USA.Result = 'A'
  AND USA.UsabilityIndex = 1
  AND USA.Platform = 'MeS'
  AND (UPPER(USA.CardType) LIKE 'VISA%' 
                OR 
       UPPER(USA.CardType) LIKE 'MASTER%' )         
  AND MeS.AuthorizationCode IS NULL
                    
  

END
