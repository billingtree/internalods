﻿











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spProcessStatusReport](
@YearMonth      INT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @vYearMonth      INT
    SET @vYearMonth = @YearMonth   

    -- Insert statements for procedure here
    
		SELECT PSL.PSL_ID
		      ,PRL.PRL_ID
		      ,PSL.PSL_RunDateYYYYMM
			  ,PRT.PRT_Name
			  ,PSL.PSL_Status
			  ,PSL.PSL_InsertDateTime
			  ,PRL.PRL_StartDateTime
			  ,PRL.PRL_EndDateTime
			  ,PIT.PIT_Category
			  ,PIT.PIT_Description
			  ,PIT.PIT_Name
			  ,PIT_SubReportLocation
			  ,CASE
				   WHEN PRL.PRL_StartDateTime = MaxPRL.MaxStartDate THEN 
					   'Y'        
				   ELSE
					   'N'
			   END               AS MostRecentRun
              ,PIL.PIL_Count     AS RecCount
/********************************************************************			  
			  ,CASE 
				   WHEN PRL.PRL_StartDateTime = MaxPRL.MaxStartDate 
										 AND 
						PIT.PIT_Name <> 'Records in ODS Quarantine' THEN -- Most Recent Run and Not ODS Quarantine Issue, show the counter
					   CONVERT(VARCHAR, RecCnt.RecCount) 
				   WHEN PRL.PRL_StartDateTime = MaxPRL.MaxStartDate 
										 AND 
						PIT.PIT_Name = 'Records in ODS Quarantine' THEN  -- Most Recent Run and ODS Quarantine Issue, show the ODS Q counter
					   CONVERT(VARCHAR, QCount.RecCount) 
				   WHEN PIT.PIT_Category IS NULL THEN                    -- If no Category, then there are no Errors
					   '--'    
				   ELSE                                                  -- Prior Run, no Count is available
					   'N/A'   
			   END               AS RecCount
********************************************************************/

		FROM Billing.PSL_ProcessStatusLog          AS PSL
		JOIN Billing.PRT_ProcessRunType            AS PRT   ON PSL.PSL_PRT_ID = PRT.PRT_ID
		JOIN Billing.PRL_ProcessRunList            AS PRL   ON PSL.PSL_ID = PRL.PRL_PSL_ID
		LEFT JOIN Billing.PIL_ProcessIssueList     AS PIL   ON PRL.PRL_ID = PIL.PIL_PRL_ID
		LEFT JOIN Billing.PIT_ProcessIssueType     AS PIT   ON PIL.PIL_PIT_ID = PIT.PIT_ID

		LEFT JOIN (			  
			  SELECT MAX(PRL2.PRL_StartDateTime)  AS MaxStartDate
			  FROM Billing.PSL_ProcessStatusLog     AS PSL2
			  LEFT JOIN Billing.PRL_ProcessRunList       AS PRL2   ON PSL2.PSL_ID = PRL2.PRL_PSL_ID
			  LEFT JOIN Billing.PRT_ProcessRunType       AS PRT2   ON PSL2.PSL_PRT_ID = PRT2.PRT_ID
			  WHERE PSL2.PSL_RunDateYYYYMM = @YearMonth
			    AND PRT2.PRT_Name IN ('CRM Pre-Bill Edit/Error','CRM Monthly Billing Production Run')
			 )                               AS MaxPRL  ON PRL.PRL_StartDateTime = MaxPRL.MaxStartDate 
/*******************************************************************************************
		LEFT JOIN (
			  SELECT PRL.PRL_PSL_ID 
					,PIL.PIL_ID
					,COUNT(*)                 AS RecCount
			  FROM Billing.PED_ProcessErrorDetail        AS PED
			  LEFT JOIN Billing.PIL_ProcessIssueList     AS PIL   ON PED.PED_PIL_ID = PIL.PIL_ID
			  LEFT JOIN Billing.PRL_ProcessRunList       AS PRL   ON PIL.PIL_PRL_ID = PRL.PRL_ID
			  GROUP BY PRL.PRL_PSL_ID
					  ,PIL.PIL_ID
			 )                               AS RecCnt  ON PSL.PSL_ID = RecCnt.PRL_PSL_ID 
													   AND PIL.PIL_ID = RecCnt.PIL_ID  
******************************************************************************************/													    
/************************************************************************		
		LEFT JOIN (                                               
			 SELECT SUM(All4.RecCount)       AS RecCount
			 FROM   (			
					--SELECT COUNT(*)             AS RecCount
					--FROM USAePay.FileData       AS USA
					--WHERE USA.TransDateYYYYMM = @YearMonth 
					--  AND USA.UsabilityIndex = 0
					--UNION ALL
					SELECT COUNT(*)             AS RecCount
					FROM USAePay.DailySummary       AS USA
					WHERE USA.TransDateYYYYMM = @YearMonth 
					  AND USA.UsabilityIndex = 0
					UNION ALL
					SELECT COUNT(*)             AS RecCount
					FROM MeS.SettlementSummary    AS MeS
					WHERE MeS.TransDateYYYYMM = @YearMonth 
					  AND MeS.UsabilityIndex = 0
					UNION ALL
					SELECT COUNT(*)             AS RecCount
					FROM NACHA.Originations       AS ATI
					WHERE ATI.FileCreationDateYYYYMM = @YearMonth 
					  AND ATI.UsabilityIndex = 0
					UNION ALL
					SELECT COUNT(*)             AS RecCount
					FROM NACHA.Returns       AS ATI
					WHERE ATI.FileCreationDateYYYYMM = @YearMonth 
					  AND ATI.UsabilityIndex = 0
					)   AS All4
				)                AS QCount      ON 1=1   
************************************************************************/				
				                                                                                  
		WHERE PSL.PSL_RunDateYYYYMM = @YearMonth
		
		
		
END






















