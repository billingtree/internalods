﻿CREATE TABLE [Billing].[CCL_CustomerContactList] (
    [CCL_ID]            INT              IDENTITY (1, 1) NOT NULL,
    [CCL_RunDateYYYYMM] INT              NOT NULL,
    [CCL_CST_ID]        INT              NOT NULL,
    [CCL_CRMContactID]  UNIQUEIDENTIFIER NOT NULL,
    [CCL_eMailAddress]  NVARCHAR (100)   NULL,
    [CCL_CreateDate]    SMALLDATETIME    DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CCL_CustomerContact] PRIMARY KEY CLUSTERED ([CCL_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CCL_CustomerContactList_CST_Customer] FOREIGN KEY ([CCL_CST_ID]) REFERENCES [Billing].[CST_Customer] ([CST_ID]) ON DELETE CASCADE
);

