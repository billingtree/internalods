﻿
CREATE function  [dbo].[fn_GetUltimateParent] 
(@AccountID nvarchar (18))
 returns varchar(255)
as
BEGIN
    DECLARE @ParentName varchar (255)
    DECLARE @ParentID nvarchar (18)
    DECLARE @HoldParentID nvarchar (18)
    DECLARE @ChildID nvarchar (18)
    DECLARE @ReturnName varchar (255) 
    DECLARE @StopLoop INT
 
    SET @ParentName = NULL
    SET @ReturnName = NULL
    SET @HoldParentID = @AccountID
    SET @StopLoop = 0
    
    WHILE (@StopLoop <= 10)
        BEGIN
            SET @StopLoop = @StopLoop + 1
                        
            SELECT @ParentID = sfa.ParentID, 
                   @ChildID = sfa.ID, 
                   @ParentName = sfa.Name 
            FROM SalesForce.Account sfa
            WHERE sfa.ID = @HoldParentID
            
            IF @ParentID = @ChildID or @ParentID is null  -- This is the Ultimate Parent
                BEGIN
                    SET @ReturnName = @ParentName
                    SET @StopLoop = 11
                END
            ELSE  
                 SET @HoldParentID = @ParentID
       
        END 
   
    RETURN @ReturnName
   
END    
 
