﻿

CREATE VIEW [InStep].[vR5BlockHeader] AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,3) AS ServiceClassCode
		,SUBSTRING(SourceRecord,5,16) AS CompanyName
		,SUBSTRING(SourceRecord,21,20) AS CompanyDiscretionaryData
		,SUBSTRING(SourceRecord,41,10) AS CompanyIdentification
		,SUBSTRING(SourceRecord,51,3) AS StandardEntryClassCode
		,SUBSTRING(SourceRecord,54,10) AS CompanyEntryDescription
		--,CONVERT(date,SUBSTRING(SourceRecord,64,6)) AS CompanyDescriptiveDate
		,SUBSTRING(SourceRecord,64,6) AS CompanyDescriptiveDate
		,CONVERT(date,SUBSTRING(SourceRecord,70,6)) AS EffectiveEntryDate
		,SUBSTRING(SourceRecord,76,3) AS SettlementDateINJulian
		,SUBSTRING(SourceRecord,79,1) AS OriginatorStatusCode
		,SUBSTRING(SourceRecord,80,8) AS OriginatingDFIIdentification
		,SUBSTRING(SourceRecord,88,7) AS BatchNumber
		,CRM_PPDID
		,ProcessingNumber
		,UsabilityIndex
		,MashupOutcome
FROM	InStep.ReturnFileData
WHERE	RecordType = 5

