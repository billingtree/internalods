﻿-- =============================================
-- Author:      Rick Stohle
-- Create date: Oct-2015
-- Modified:    22-Dec-2015
-- Description: Report on those customers flagged as Manually Billed, aggregate their CDT counts
--              Detailed records are returned to the SSRS report where aggregation occurs
--              Add Customer State and their Most Recent Fees (from CTF)
-- =============================================
CREATE PROCEDURE [Billing].[spManuallyBilledCustomersReport] ( @RunYearMonth INT )

-- Insert statements for procedure here
AS
    BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @YearMonth INT;
        SET @YearMonth = @RunYearMonth;


--------------------------------------------------------------------------------------
--    Drive through the CPA looking for Manually Billed accounts and get CDT counts
--------------------------------------------------------------------------------------

        SELECT  CPA.CPA_TrustName AS TrustName ,
                CST.CST_AccountName AS Name ,
                A.address1_stateorprovince AS CustomerState ,
                TranFee.RecentFee AS TranFee ,
                BatchFee.RecentFee AS BatchFee ,
                ReturnFee.RecentFee AS ReturnFee ,
                UnAuthFee.RecentFee AS UnAuthFee ,
                MonthFee.RecentFee AS MonthFee ,
                CPA.CPA_ProductLine AS ProductLine ,
                CPA.CPA_ManualBillStartDate AS ManualBillingStartDate ,
                CPA.CPA_ManualBillEndDate AS ManualBillingEndDate ,
                CASE WHEN CPA.CPA_ProductLine = 'ACH'
                     THEN CONCAT(CPP.CPP_SourceIdConfig, '-', CPP.CPP_MID)
                     ELSE CPP.CPP_MID
                END AS MID ,
                CPP.CPP_SourceIdConfig AS SourceConfig ,
                CDT.CDT_TranDate AS TranDate ,
                CDT.CDT_TranAmount AS TranAmount ,
                CDT.CDT_TranCount AS TranCount ,
                CASE WHEN CDT.CDT_TranType = 'Debit' THEN '(1) Debits'
                     WHEN CDT.CDT_TranType = 'Credit' THEN '(2) Credits'
                     WHEN CDT.CDT_TranType = 'Decline' THEN '(3) Declines'
                     WHEN CDT.CDT_TranType = 'Origination'
                     THEN '(4) ACH Trans'
                     WHEN CDT.CDT_TranType = 'Return' THEN '(5) Returns'
                     WHEN CDT.CDT_TranType = 'Batch' THEN '(6) Batches'
                     WHEN CDT.CDT_TranType = 'UnAuth' THEN '(7) UnAuth'
                     ELSE NULL
                END AS SourceData
        FROM    Billing.CPA_CustomerProcessingAccount AS CPA
                LEFT JOIN Billing.CST_Customer AS CST ON CPA.CPA_CST_ID = CST.CST_ID
                LEFT JOIN Billing.CPP_CustomerProcessingProfile AS CPP ON CPA.CPA_ID = CPP.CPP_CPA_ID
                LEFT JOIN Billing.CDT_CustomerDailyTran AS CDT ON CPP.CPP_ID = CDT.CDT_CPP_ID
                                                              AND CDT.CDT_TranType IN (
                                                              'Origination',
                                                              'Return',
                                                              'Batch',
                                                              'UnAuth',
                                                              'Debit',
                                                              'Credit',
                                                              'Decline' )
                LEFT JOIN CRM.account AS A ON CST.CST_CRMAccountID = A.accountid
                LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                    'TranFee' AS FeeType ,
                                    CTF_FeeAmount AS RecentFee ,
                                    MAX(CTF_StartDate) AS LatestStartDate
                            FROM    Billing.CTF_CustomerTranFee
                            WHERE   CTF_RunDateYYYYMM = @YearMonth
                                    AND CTF_FeeType = 'BillingTree Gateway Submission Fee'
                            GROUP BY CTF_CPA_ID ,
                                    CTF_FeeAmount
                          ) AS TranFee ON CPA.CPA_ID = TranFee.CPA_ID
                LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                    'BatchFee' AS FeeType ,
                                    CTF_FeeAmount AS RecentFee ,
                                    MAX(CTF_StartDate) AS LatestStartDate
                            FROM    Billing.CTF_CustomerTranFee
                            WHERE   CTF_RunDateYYYYMM = @YearMonth
                                    AND CTF_FeeType = 'Per Batch Fee/Offset'
                            GROUP BY CTF_CPA_ID ,
                                    CTF_FeeAmount
                          ) AS BatchFee ON CPA.CPA_ID = BatchFee.CPA_ID
                LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                    'ReturnFee' AS FeeType ,
                                    CTF_FeeAmount AS RecentFee ,
                                    MAX(CTF_StartDate) AS LatestStartDate
                            FROM    Billing.CTF_CustomerTranFee
                            WHERE   CTF_RunDateYYYYMM = @YearMonth
                                    AND CTF_FeeType = 'Return Fee'
                            GROUP BY CTF_CPA_ID ,
                                    CTF_FeeAmount
                          ) AS ReturnFee ON CPA.CPA_ID = ReturnFee.CPA_ID
                LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                    'UnAuthFee' AS FeeType ,
                                    CTF_FeeAmount AS RecentFee ,
                                    MAX(CTF_StartDate) AS LatestStartDate
                            FROM    Billing.CTF_CustomerTranFee
                            WHERE   CTF_RunDateYYYYMM = @YearMonth
                                    AND CTF_FeeType = 'Un-Authorized Return Fee'
                            GROUP BY CTF_CPA_ID ,
                                    CTF_FeeAmount
                          ) AS UnAuthFee ON CPA.CPA_ID = UnAuthFee.CPA_ID
                LEFT JOIN ( SELECT  CTF_CPA_ID AS CPA_ID ,
                                    'MonthlyFee' AS FeeType ,
                                    SUM(CTF_FeeAmount) AS RecentFee ,
                                    MAX(CTF_StartDate) AS LatestStartDate
                            FROM    Billing.CTF_CustomerTranFee
                            WHERE   CTF_RunDateYYYYMM = @YearMonth
                                    AND CTF_FeeType = 'BillingTree Monthly Fee'
                            GROUP BY CTF_CPA_ID
                          ) AS MonthFee ON CPA.CPA_ID = MonthFee.CPA_ID
        WHERE   CPA.CPA_RunDateYYYYMM = @YearMonth
                AND CPA.CPA_ManualBillYN = 'Y';



    END;




