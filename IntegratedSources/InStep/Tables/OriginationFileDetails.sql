﻿CREATE TABLE [InStep].[OriginationFileDetails] (
    [ReferenceCode] INT            NOT NULL,
    [FileName]      NVARCHAR (255) NOT NULL,
    [ModifiedDate]  DATETIME       NULL,
    [Size]          NCHAR (10)     NULL,
    [Checksum]      NCHAR (10)     NULL,
    [LineCount]     INT            NULL,
    [RecordCount]   INT            NULL,
    [UploadDate]    DATETIME       CONSTRAINT [DF_OriginationFileDetails_UploadDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OriginationFileDetails] PRIMARY KEY CLUSTERED ([ReferenceCode] ASC) WITH (FILLFACTOR = 80)
);

