﻿


CREATE FUNCTION  [dbo].[fnDateToJulian] 
(@DateIn DATE)
 RETURNS INT
AS
BEGIN
    DECLARE @txtYear         CHAR (4)
    DECLARE @txtDays         VARCHAR(3)
    DECLARE @ReturnResult    CHAR (7)
    
    SET @txtYear = YEAR(@DateIn)  
    SET @txtDays = DATEPART(dy, @DateIn)
    SET @txtDays = RIGHT('000' + @txtDays,3)
        
    SET @ReturnResult = @txtYear + @txtDays

    RETURN @ReturnResult
   
END    
 




