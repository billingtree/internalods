﻿CREATE TABLE [Billing].[PRI_ProcessRunIssue] (
    [PRI_ID]     INT IDENTITY (1, 1) NOT NULL,
    [PRI_PRT_ID] INT NOT NULL,
    [PRI_PIT_ID] INT NOT NULL,
    CONSTRAINT [PK_PRI_ProcessRunIssue] PRIMARY KEY CLUSTERED ([PRI_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PRI_ProcessRunIssue_PIT_ProcessIssueType] FOREIGN KEY ([PRI_PIT_ID]) REFERENCES [Billing].[PIT_ProcessIssueType] ([PIT_ID]),
    CONSTRAINT [FK_PRI_ProcessRunIssue_PRT_ProcessRunType] FOREIGN KEY ([PRI_PRT_ID]) REFERENCES [Billing].[PRT_ProcessRunType] ([PRT_ID])
);

