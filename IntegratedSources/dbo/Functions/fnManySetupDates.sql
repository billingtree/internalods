﻿




CREATE FUNCTION  [dbo].[fnManySetupDates] 
(@ProcessingRecordID nvarchar (18))
 RETURNS char(1)
AS
BEGIN
    DECLARE @Count nvarchar (500)
    DECLARE @ReturnResult char (1)
    
    SET @Count = 0
    SET @ReturnResult = ' '
    
    SELECT @Count = COUNT(*)
    FROM SalesForce.ProcessingStageHistory PSH        WITH (NOLOCK)
    WHERE PSH.Processing__c = @ProcessingRecordID
      AND PSH.Name = 'Set Up'
      AND PSH.IsDeleted = 0
     
    IF @Count = 1
        BEGIN
            SET @ReturnResult = ' '
        END
    ELSE  
        SET @ReturnResult = 'Y'
   
    RETURN @ReturnResult
   
END    
 

