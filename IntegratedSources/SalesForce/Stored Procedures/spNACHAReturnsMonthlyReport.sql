﻿






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spNACHAReturnsMonthlyReport](
@Month1 INT,
@Year1  INT
)

    -- Insert statements for procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Month int, @Year int
	SET @Month=@Month1
	SET @Year=@Year1	

    -- Concatenate Year and Month in order to use the Index
    DECLARE @vYearMonth INT
    DECLARE @txtYear    VARCHAR (4)
    DECLARE @txtMonth   VARCHAR (2)
     
    SET @txtYear = CONVERT(VARCHAR (4), @Year)
    IF @Month < 10 
        SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
    ELSE
        SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
    SET @vYearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))   	

    
-------------------------------------------------------------------------
--     ATI - get all returns for the Month and Year
-------------------------------------------------------------------------    
SELECT ATI.ConfigNumber                     AS ConfigNbr
       ,ATI.FileCreationDate                AS CreationDate
       ,ATI.FileCreationTime                AS CreationTime
       ,ATI.ProcessingNumber                AS ProcessingNbr
  --     ,ATI.CompanyName                     AS ACHName
  --     ,ATI.BlockNumber                     AS BatchNumber
  --     ,ATI.StandardEntryClassCode          AS SEC
  --     ,ATI.TransactionCode                 AS TranCode
       ,SUM(ATI.FormattedAmount)            AS ReturnTotal
       ,Count(*)                            AS CountTotal
       ,'ATI'                               AS Source
       
FROM NACHA.Returns  ATI 

WHERE ATI.FileCreationDateYYYYMM = @vYearMonth
  AND ATI.UsabilityIndex = 1
  AND ATI.SourceType = 4
    
GROUP BY ATI.ConfigNumber
         ,ATI.FileCreationDate 
         ,ATI.FileCreationTime
         ,ATI.ProcessingNumber 
  --       ,ATI.CompanyName
  --       ,ATI.BlockNumber
  --       ,ATI.StandardEntryClassCode
  --       ,ATI.TransactionCode

UNION ALL

-------------------------------------------------------------------------
--     InStep - get all returns for the Month and Year
-------------------------------------------------------------------------    
SELECT  InStep.ConfigNumber                    AS ConfigNbr
       ,InStep.FileCreationDate                AS CreationDate
       ,InStep.FileCreationTime                AS CreationTime
       ,InStep.ProcessingNumber                AS ProcessingNbr
 --      ,InStep.CompanyName                     AS ACHName
 --      ,InStep.BlockNumber                     AS BatchNumber
 --      ,InStep.StandardEntryClassCode          AS SEC
 --      ,InStep.TransactionCode                 AS TranCode
       ,SUM(InStep.FormattedAmount)            AS ReturnTotal
       ,Count(*)                               AS CountTotal
       ,'InStep'                               AS Source
       
FROM NACHA.Returns  InStep 

WHERE InStep.FileCreationDateYYYYMM = @vYearMonth
  AND InStep.UsabilityIndex = 1
  AND InStep.SourceType = 1
  
GROUP BY InStep.ConfigNumber
        ,InStep.FileCreationDate 
        ,InStep.FileCreationTime
        ,InStep.ProcessingNumber 
 --        ,InStep.CompanyName
 --        ,InStep.BlockNumber
 --        ,InStep.StandardEntryClassCode
 --        ,InStep.TransactionCode


END




















