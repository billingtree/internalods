﻿
CREATE VIEW [dbo].[vGlobalODS]
AS
SELECT	FileName
		,LineNumber
		,SourceID
		,FileDate
		,FileTime
		,ScanCharge
		,DepositID
		,Hierarchy
		,MerchantID
		,CashLetterNumber
		,CardAcceptorID
		,TranCode
		,CardholderNumber
		,Amount
		,AuthCode
		,ReferenceNumber
		,TranDate
		,TranTime
		,AuthSource
		,TermCapability
		,POSEntryMode
		,AuthMCC
		,CardActTermInd
		,PrepaidIndicator
		,MOTOIndicator
		,ServiceDevelopment
		,CardholderIDMethod
		,VISATransID
		,AuthAmount
		,AuthCurrency
		,AuthResponse
		,ValidationCode
		,AuthCharID
		,ReqPayServiceValue
		,MarketSpecificAuth
		,AuthDate
		,CentralTimeIndicator
		,AVSResponse
		,AuthorizationAmount
		,PurchIDFormat
		,PurchID
		,LodgingNoShow
		,LodgingExtraCharges
		,LodgingCheckInDate
		,CarRentalNoShow
		,CarRentalExtraCharge
		,CarRentalCheckOutDate
		,CardType
		,ChargeType
		,MCInterchangeLevel
		,VISAInterchangeLevel
		,CPSQualTrans
		,ErrorCode
		,DowngradeCode
		,MerchantName
		,DepositDate
		,VISAProductID
		,MashupOutcome
		,CASE MashupOutcome
			WHEN 1 THEN 'Spurious'
			WHEN 2 THEN 'UnMatched'
			WHEN 3 THEN 'Matched'
			WHEN 4 THEN 'Duplicates'
		END AS MashupOutcomeDescription
FROM	Global.vFileData
WHERE	(UsabilityIndex = 1)
