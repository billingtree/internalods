﻿





CREATE VIEW [dbo].[vRevenueMeSByYear]
AS

SELECT AllYears.MID              AS MID
      ,AllYears.SourceId         AS SourceId
      ,AllYears.Name             AS Name
      ,AllYears.CRM_ID           AS CRM_ID
      ,AllYears.TranDate         AS TranDate
      ,AllYears.Source           AS Source
      ,SUM(AllYears.Revenue)     AS Revenue
FROM
    (
		SELECT DISTINCT 
			 MeS.MID                     AS MID
			,MeSLookup.SourceId          AS SourceId
			,MeS.Name                    AS Name
			,MeSLookup.CRM_Id            AS CRM_ID
			,YEAR(MeS.TranDate)          AS TranDate
			,SUM(MeS.Revenue)            AS Revenue
			,'MeS'                       AS Source  
		FROM dbo.RevenueMeS20112012         AS MeS
		LEFT JOIN dbo.RevenueMeSLookup      AS MeSLookup ON MeS.MID = MeSLookup.MID
		WHERE MeS.Revenue <> 0  
		GROUP BY MeS.MID
			  ,MeSLookup.SourceId
			  ,MeS.Name
			  ,MeSLookup.CRM_Id
			  ,YEAR(MeS.TranDate)
		      
		UNION ALL

		SELECT DISTINCT 
			 MeS.MID                     AS MID
			,MeSLookup.SourceId          AS SourceId
			,MeS.Name                    AS Name
			,MeSLookup.CRM_Id            AS CRM_ID
			,YEAR(MeS.TranDate)          AS TranDate
			,SUM(MeS.Revenue)            AS Revenue
			,'MeS'                       AS Source  
		FROM dbo.RevenueMeS20122013         AS MeS
		LEFT JOIN dbo.RevenueMeSLookup      AS MeSLookup ON MeS.MID = MeSLookup.MID
		WHERE MeS.Revenue <> 0  
		GROUP BY MeS.MID
			  ,MeSLookup.SourceId
			  ,MeS.Name
			  ,MeSLookup.CRM_Id
			  ,YEAR(MeS.TranDate)
			  
		UNION ALL
        -- Initially omit these 6 Accounts
		SELECT DISTINCT 
			 MeS.MID                     AS MID
			,MeS.SourceId                AS SourceId
			,MeS.CRMActLevelName_GPParentName
			                             AS Name
			,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
			                             AS CRM_ID
			,YEAR(MeS.TranDate)          AS TranDate
			,SUM(MeS.RevenueAmount)      AS Revenue
			,'MeS'                       AS Source  
		FROM dbo.MeSRevenueMonthlyGLFeed AS MeS
		WHERE MeS.RevenueAmount <> 0  
		  AND MeS.CRMActLevelName_GPParentName NOT IN ('Orbit Leasing, Inc.'
		                                              ,'Security Auto Loans'
		                                              ,'First Financial Asset Management, Inc. (FFAM)'
		                                              ,'Reliable Auto Finance, Inc.'
		                                              ,'BillingTree Admin'
		                                              ,'Coastal Credit, LLC')
		GROUP BY MeS.MID
			  ,MeS.SourceId
			  ,MeS.CRMActLevelName_GPParentName
			  ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
			  ,YEAR(MeS.TranDate)
			  
		UNION ALL
        -- Now, include these 6 Accounts, but only when their Revenue is a positive amount
		SELECT DISTINCT 
			 MeS.MID                     AS MID
			,MeS.SourceId                AS SourceId
			,MeS.CRMActLevelName_GPParentName
			                             AS Name
			,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
			                             AS CRM_ID
			,YEAR(MeS.TranDate)          AS TranDate
			,SUM(MeS.RevenueAmount)      AS Revenue
			,'MeS'                       AS Source  
		FROM dbo.MeSRevenueMonthlyGLFeed AS MeS
		WHERE MeS.RevenueAmount > 0  
		  AND MeS.CRMActLevelName_GPParentName IN ('Orbit Leasing, Inc.'
		                                          ,'Security Auto Loans'
		                                          ,'First Financial Asset Management, Inc. (FFAM)'
		                                          ,'Reliable Auto Finance, Inc.'
		                                          ,'BillingTree Admin'
		                                          ,'Coastal Credit, LLC')
		GROUP BY MeS.MID
			  ,MeS.SourceId
			  ,MeS.CRMActLevelName_GPParentName
			  ,MeS.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
			  ,YEAR(MeS.TranDate)			  

			  
    ) AS AllYears
GROUP BY AllYears.MID 
        ,AllYears.SourceId
        ,AllYears.Name
        ,AllYears.CRM_ID
        ,AllYears.TranDate
        ,AllYears.Source              






