﻿CREATE TABLE [dbo].[RevenueATI2011JanJun] (
    [TranDate]       DATETIME       NULL,
    [TranDateYYYYMM] FLOAT (53)     NULL,
    [Config]         FLOAT (53)     NULL,
    [MID]            FLOAT (53)     NULL,
    [Name]           NVARCHAR (255) NULL,
    [TranCount]      FLOAT (53)     NULL,
    [TranRate]       FLOAT (53)     NULL,
    [ReturnCount]    FLOAT (53)     NULL,
    [ReturnRate]     FLOAT (53)     NULL,
    [MonthFee]       FLOAT (53)     NULL,
    [TranFee]        FLOAT (53)     NULL,
    [ReturnFee]      FLOAT (53)     NULL,
    [Revenue]        FLOAT (53)     NULL
);

