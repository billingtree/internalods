﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [SalesForce].[spUSAePayMerchantDetailLinkedReport](
@StartYearMonth INT,
@EndYearMonth  INT,
@MerchantID FLOAT
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @vStartYearMonth int, @vEndYearMonth int, @vMerchantID FLOAT
	SET @vStartYearMonth=@StartYearMonth
	SET @vEndYearMonth=@EndYearMonth
	SET @vMerchantID=@MerchantID	
    -- Insert statements for procedure here

------------------------------------------------------------------------
--   This is USAePay detail records for a given Merchant and Month    --                                   --
------------------------------------------------------------------------
  BEGIN
      SELECT usa.MerchantID                        "MerchantID", 
             usa.USAePayID                         "SourceID", 
             usa.Platform                          "Platform", 
             usa.MerchantName                      "MerchantName", 
             CASE 
                 WHEN usa.TransType = 'A' and usa.Result = 'A' THEN 'Auth'
                 WHEN usa.TransType = 'C' and usa.Result = 'A' THEN 'Credit'
                 WHEN usa.TransType = 'S' and usa.Result = 'A' THEN 'Sale'
                 WHEN usa.TransType IN ('A','C','S') and usa.Result = 'D' THEN 'Decline'
                 ELSE 'Unknown'                    
             END                                   "TranType",
             usa.TransDate                         "TransactionDate",
             usa.TransTime                         "TransactionTime",
             usa.Amount                            "TransactionAmount", 
             usa.CardType                          "CardType", 
             usa.AccountHolder                     "AccountName",
             usa.AuthCode                          "AuthorizationCode"
        FROM USAePay.FileData usa
        WHERE usa.TransDateYYYYMM BETWEEN @vStartYearMonth AND @vEndYearMonth
          and usa.UsabilityIndex = 1
          and CONVERT(NUMERIC (16,0), LEFT(usa.MerchantID, 13)) = 
              CONVERT(NUMERIC (16,0), @vMerchantID)    
          and usa.TransType IN ('A','C','S') -- An Authorization, Credit or Sale
          and (usa.Result = 'A'              -- Approved
                    OR
               usa.Result = 'D')             -- Declined 
                    
   END

END
