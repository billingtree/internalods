﻿


-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [Billing].[spManuallyBilledCustomersDetailReport](
@RunYearMonth    INT,
@MID             NVARCHAR (50),
@ConfigSource    NVARCHAR (50)
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      DECLARE @YearMonth          INT
      DECLARE @BankMID            NVARCHAR (50)
      DECLARE @ConfigSourceID     NVARCHAR (50)
      
      SET @YearMonth = @RunYearMonth
      SET @BankMID = @MID
      SET @ConfigSourceID = @ConfigSource
      

IF LEN(@BankMID) = 4 
-------------------------------------------------------------------------
--    The supplied MID is for ACH
--        ACH Tran Counts first, Need to use ACHTransByBatchDate in order to get InHouseID
--        Then Returns, Need to use ACHReturnsByReturnDate in order to get InHouseID
--        Then UnAuths, Need to use ACHReturnsByReturnDate in order to get InHouseID
--        Then Batch Count, can use CDT
-------------------------------------------------------------------------
	SELECT CST.CST_AccountName                                 AS ChildName
	      ,CPA.CPA_TrustName                                   AS Name
		  ,CPA.CPA_ProductLine                                 AS ProductLine
		  ,CPA.CPA_ManualBillStartDate                         AS ManualBillingStartDate
		  ,CPA.CPA_ManualBillEndDate                           AS ManualBillingEndDate
		  ,CONCAT(CPP.CPP_SourceIdConfig,'-',CPP.CPP_MID)      AS MID
		  ,CPP.CPP_SourceIdConfig                              AS SourceConfig
		  ,CPP.CPP_StartDate                                   AS MIDStartDate                    
		  ,CPP.CPP_EndDate                                     AS MIDEndDate
		  ,ATI.BatchDate                                       AS BatchDate
		  ,ATI.CheckAmount                                     AS TranAmount 
		  ,ATI.CheckAmount                                     AS DebitQuantity 
		  ,ATI.InHouseID                                       AS InHouseID
		  ,ATI.AccountName                                     AS AccountHolder
		  ,'(4) ACH Trans'                                     AS SourceData
	FROM Billing.ACHTransByBatchDate                AS ATI 
    JOIN Billing.CPP_CustomerProcessingProfile      AS CPP  ON ATI.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
	                                                        AND CPP.CPP_RunDateYYYYMM = @YearMonth
    JOIN Billing.CPA_CustomerProcessingAccount      AS CPA  ON CPP.CPP_CPA_ID = CPA.CPA_ID
	JOIN Billing.CST_Customer                       AS CST  ON CPA.CPA_CST_ID = CST.CST_ID
	WHERE ATI.BatchDateYYYYMM = @YearMonth
      AND ATI.AccountID = @BankMID
      AND ATI.ConfigNumber = @ConfigSourceID
      AND ATI.Reference NOT LIKE 'OFFSET%'

	UNION ALL

	SELECT CST.CST_AccountName                                 AS ChildName
	      ,CPA.CPA_TrustName                                   AS Name
		  ,CPA.CPA_ProductLine                                 AS ProductLine
		  ,CPA.CPA_ManualBillStartDate                         AS ManualBillingStartDate
		  ,CPA.CPA_ManualBillEndDate                           AS ManualBillingEndDate
		  ,CONCAT(CPP.CPP_SourceIdConfig,'-',CPP.CPP_MID)      AS MID
		  ,CPP.CPP_SourceIdConfig                              AS SourceConfig
		  ,CPP.CPP_StartDate                                   AS MIDStartDate                    
		  ,CPP.CPP_EndDate                                     AS MIDEndDate
		  ,ATI.ReturnDate                                      AS BatchDate
		  ,ATI.DebitAmount * -1                                AS TranAmount 
		  ,0                                                   AS DebitQuantity 
		  ,ATI.InHouseID                                       AS InHouseID
		  ,CONCAT(ATI.ConsumerFirstName
		         ,' '
				 ,ATI.ConsumerLastName)                        AS AccountHolder
		  ,'(5) Returns'                                       AS SourceData
	FROM Billing.ACHReturnsByReturnDate             AS ATI
    JOIN Billing.CPP_CustomerProcessingProfile      AS CPP  ON ATI.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
	                                                       AND CPP.CPP_RunDateYYYYMM = @YearMonth
    JOIN Billing.CPA_CustomerProcessingAccount      AS CPA  ON CPP.CPP_CPA_ID = CPA.CPA_ID
	JOIN Billing.CST_Customer                       AS CST  ON CPA.CPA_CST_ID = CST.CST_ID
	WHERE ATI.ReturnDateYYYYMM = @YearMonth
      AND ATI.AccountID = @BankMID
      AND ATI.ConfigNumber = @ConfigSourceID
	  AND ATI.ReturnCode NOT IN ('R05','R07','R10','R29','R51')

	UNION ALL

	SELECT CST.CST_AccountName                                 AS ChildName
	      ,CPA.CPA_TrustName                                   AS Name
		  ,CPA.CPA_ProductLine                                 AS ProductLine
		  ,CPA.CPA_ManualBillStartDate                         AS ManualBillingStartDate
		  ,CPA.CPA_ManualBillEndDate                           AS ManualBillingEndDate
		  ,CONCAT(CPP.CPP_SourceIdConfig,'-',CPP.CPP_MID)      AS MID
		  ,CPP.CPP_SourceIdConfig                              AS SourceConfig
		  ,CPP.CPP_StartDate                                   AS MIDStartDate                    
		  ,CPP.CPP_EndDate                                     AS MIDEndDate
		  ,ATI.ReturnDate                                      AS BatchDate
		  ,ATI.DebitAmount * -1                                AS TranAmount 
		  ,0                                                   AS DebitQuantity 
		  ,ATI.InHouseID                                       AS InHouseID
		  ,CONCAT(ATI.ConsumerFirstName
		         ,' '
				 ,ATI.ConsumerLastName)                        AS AccountHolder
		  ,'(7) UnAuth'                                        AS SourceData
	FROM Billing.ACHReturnsByReturnDate             AS ATI
    JOIN Billing.CPP_CustomerProcessingProfile      AS CPP  ON ATI.CRM_PPDID = CPP.CPP_CRMProcessingProfileID
	                                                       AND CPP.CPP_RunDateYYYYMM = @YearMonth
    JOIN Billing.CPA_CustomerProcessingAccount      AS CPA  ON CPP.CPP_CPA_ID = CPA.CPA_ID
	JOIN Billing.CST_Customer                       AS CST  ON CPA.CPA_CST_ID = CST.CST_ID
	WHERE ATI.ReturnDateYYYYMM = @YearMonth
      AND ATI.AccountID = @BankMID
      AND ATI.ConfigNumber = @ConfigSourceID
	  AND ATI.ReturnCode IN ('R05','R07','R10','R29','R51')
		
    UNION ALL    


	SELECT CST.CST_AccountName                                 AS ChildName
	      ,CPA.CPA_TrustName                                   AS Name
		  ,CPA.CPA_ProductLine                                 AS ProductLine
		  ,CPA.CPA_ManualBillStartDate                         AS ManualBillingStartDate
		  ,CPA.CPA_ManualBillEndDate                           AS ManualBillingEndDate
		  ,CONCAT(CPP.CPP_SourceIdConfig,'-',CPP.CPP_MID)      AS MID
		  ,CPP.CPP_SourceIdConfig                              AS SourceConfig
		  ,CPP.CPP_StartDate                                   AS MIDStartDate                    
		  ,CPP.CPP_EndDate                                     AS MIDEndDate
		  ,CDT.CDT_TranDate                                    AS BatchDate
		  ,''                                                  AS TranAmount 
		  ,''                                                  AS DebitQuantity 
		  ,''                                                  AS InHouseID
		  ,''                                                  AS AccountHolder
		  ,'(6) Batches'                                       AS SourceData
	FROM Billing.CDT_CustomerDailyTran              AS CDT
    JOIN Billing.CPP_CustomerProcessingProfile      AS CPP  ON CDT.CDT_CPP_ID = CPP.CPP_ID
    JOIN Billing.CPA_CustomerProcessingAccount      AS CPA  ON CPP.CPP_CPA_ID = CPA.CPA_ID
	JOIN Billing.CST_Customer                       AS CST  ON CPA.CPA_CST_ID = CST.CST_ID
	WHERE CDT.CDT_RunDateYYYYMM = @YearMonth
	  AND CDT.CDT_TranType = 'Batch'
	  AND CPP.CPP_MID = @BankMID
	  AND CPP.CPP_SourceIdConfig = @ConfigSourceID



ELSE  

-------------------------------------------------------------------------
--    Credit Card Detail Transactions are not available
--       due to the USAePay Daily Summary (which feeds CDT)
--       does not have detail
-------------------------------------------------------------------------
	SELECT 'Daily Summary - No Detail'   AS ChildName
	      ,'Daily Summary - No Detail'   AS Name
		  ,'Credit Card'                 AS ProductLine
		  ,NULL                          AS ManualBillingStartDate
		  ,NULL                          AS ManualBillingEndDate
		  ,@BankMID                      AS MID
		  ,@ConfigSourceID               AS SourceConfig
		  ,NULL                          AS MIDStartDate                    
		  ,NULL                          AS MIDEndDate
		  ,NULL                          AS BatchDate
		  ,NULL                          AS TranAmount 
		  ,NULL                          AS DebitQuantity 
		  ,NULL                          AS InHouseID
		  ,NULL                          AS AccountHolder
		  ,'Daily Summary - No Detail'   AS SourceData



END



