﻿CREATE TABLE [dbo].[RevenueCategoryByMID] (
    [CRM_PPDID]           UNIQUEIDENTIFIER NULL,
    [ParentAccount]       NVARCHAR (255)   NULL,
    [ChildAccount]        NVARCHAR (255)   NULL,
    [MID]                 NVARCHAR (255)   NULL,
    [EndDate]             DATETIME         NULL,
    [CreatedByName]       NVARCHAR (200)   NULL,
    [ConfigSource]        NVARCHAR (255)   NULL,
    [IsAtThisStage]       NVARCHAR (255)   NULL,
    [FirstActivityDate]   DATE             NULL,
    [SIC Code]            NVARCHAR (255)   NULL,
    [RevenueCategory]     TINYINT          NULL,
    [RevenueCategoryName] NVARCHAR (255)   NULL,
    [CreatedDate]         SMALLDATETIME    CONSTRAINT [DF_RevenueCategoryByMID_CreatedDate] DEFAULT (getdate()) NOT NULL
);

