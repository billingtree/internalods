﻿CREATE TABLE [dbo].[CancellationMID] (
    [btProcessingProfileId] UNIQUEIDENTIFIER NULL,
    [ParentAccount]         NVARCHAR (255)   NULL,
    [ChildAccount]          NVARCHAR (255)   NULL,
    [ProductLine]           NVARCHAR (255)   NULL,
    [MID]                   NVARCHAR (60)    NULL,
    [ConfigSource]          NVARCHAR (60)    NULL,
    [EndDate]               DATE             NULL,
    [InitiatedBy]           NVARCHAR (255)   NULL,
    [OnThisDate]            DATE             NULL,
    [CreatedDate]           SMALLDATETIME    CONSTRAINT [DF_CancellationMID_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [RecCounter]            INT              NULL
);

