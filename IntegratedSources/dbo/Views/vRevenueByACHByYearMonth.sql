﻿








CREATE VIEW [dbo].[vRevenueByACHByYearMonth]
AS

SELECT DISTINCT ACHRev.TranDate      AS TranDate
               ,ACHRev.CRM_ID        AS CRM_ID
               ,ACHRev.Source        AS Source
               ,SUM(ACHRev.Revenue)  AS Revenue
FROM 
  (
  -- ATI 2008 - 2010 Revenue
  SELECT ATI.TranDateYYYYMM   AS TranDate
        ,ATILookup.CRM_Id     AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.Revenue)     AS Revenue 			  
  FROM dbo.RevenueATI20082010      AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID
                                              AND ATI.Config = ATILookup.Config
  GROUP BY ATI.TranDateYYYYMM 
          ,ATILookup.CRM_Id 
  
  UNION ALL
  -- ATI 2011 Jan-Jun Revenue
  SELECT ATI.TranDateYYYYMM   AS TranDate
        ,ATILookup.CRM_Id     AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.Revenue)     AS Revenue 			  
  FROM dbo.RevenueATI2011JanJun    AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID
                                              AND ATI.Config = ATILookup.Config
  GROUP BY ATI.TranDateYYYYMM 
          ,ATILookup.CRM_Id 
 
  UNION ALL
  -- ATI 2011 Jul-Dec Revenue
  SELECT ATI.TranDateYYYYMM   AS TranDate
        ,ATILookup.CRM_Id     AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.Revenue)     AS Revenue 			  
  FROM dbo.RevenueATI2011JulDec    AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID
                                              AND ATI.Config = ATILookup.Config
  GROUP BY ATI.TranDateYYYYMM 
          ,ATILookup.CRM_Id  
  
  UNION ALL
  -- ATI 2012 Revenue
  SELECT ATI.TranDateYYYYMM   AS TranDate
        ,ATILookup.CRM_Id     AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.Revenue)     AS Revenue 			  
  FROM dbo.RevenueATI2012          AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID
                                              AND ATI.Config = ATILookup.Config
  GROUP BY ATI.TranDateYYYYMM 
          ,ATILookup.CRM_Id  
  
  UNION ALL
  -- ATI 2013 Revenue
  SELECT ATI.TranDateYYYYMM   AS TranDate
        ,ATILookup.CRM_Id     AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.Revenue)     AS Revenue 			  
  FROM dbo.RevenueATI2013          AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID
                                              AND ATI.Config = ATILookup.Config
  GROUP BY ATI.TranDateYYYYMM 
          ,ATILookup.CRM_Id 
          
  UNION ALL
  --                             ATI Revenue from the GL Feed Table
  SELECT CONCAT(YEAR(ATI.TranDate), MONTH(ATI.TranDate))     
                               AS TranDate
        ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
                              AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.RevenueAmount)     AS Revenue 
  FROM dbo.ATIDailyRevenueGLFeed AS ATI
  WHERE MONTH(ATI.TranDate) > 9  
  GROUP BY CONCAT(YEAR(ATI.TranDate), MONTH(ATI.TranDate)) 
          ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
  UNION ALL
  SELECT CONCAT(YEAR(ATI.TranDate), 0, MONTH(ATI.TranDate))     
                               AS TranDate
        ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
                              AS CRM_ID
        ,'ATI'                AS Source
        ,SUM(ATI.RevenueAmount)     AS Revenue 
  FROM dbo.ATIDailyRevenueGLFeed AS ATI
  WHERE MONTH(ATI.TranDate) < 10  
  GROUP BY CONCAT(YEAR(ATI.TranDate), 0, MONTH(ATI.TranDate)) 
          ,ATI.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard  
            
  
  UNION ALL   
  -- InStep Revenue
  SELECT InStep.TranDateYYYYMM   AS TranDate
        ,InStepLookup.CRM_Id     AS CRM_ID
        ,'InStep'                AS Source
        ,SUM(InStep.Revenue)     AS Revenue 			  
  FROM dbo.RevenueInStep            AS InStep
  LEFT JOIN dbo.RevenueInStepLookup AS InStepLookup ON InStep.MID = InStepLookup.MID
  WHERE InStep.Revenue <> 0
  GROUP BY InStep.TranDateYYYYMM 
          ,InStepLookup.CRM_Id  
          
  ) AS ACHRev
  
GROUP BY ACHRev.TranDate 
      ,ACHRev.CRM_ID   
      ,ACHRev.Source  





