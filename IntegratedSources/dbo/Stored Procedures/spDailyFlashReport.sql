﻿








-- =============================================
-- Author:		Rick Stohle
-- Create date: 02-Jun-2014
-- Description:	Procedure to get Billing Tree Customers by with Revenue during a Year/Month parameters
-- =============================================
CREATE PROCEDURE [dbo].[spDailyFlashReport] 
(
@FromDate  DATE,
@ToDate    DATE
)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    DECLARE @FromThisDate      DATE
    DECLARE @ToThisDate        DATE
 
    SET @FromThisDate = @FromDate
    SET @ToThisDate = @ToDate
              
    -- Insert statements for procedure here


------------------------------------------------------------------------------------------------------------------------
--                         USAePay Daily Summary to get Transaction Volume and Count by Date 
-----------------------------------------------------------------------------------------------------------------------
                        
	SELECT USA.TransDate                           AS TranDate
		  ,'90060-00-20-0000-00'                   AS GLSegment
		  ,SUM(ISNULL(USA.Sales, 0)) +
		   SUM(ISNULL(USA.Credits, 0)) +
		   SUM(ISNULL(USA.Declines, 0))            AS Debit
	      ,''                                      AS Credit
		  ,'Credit Card Transactions (USAePay)'    AS Description
	FROM USAePay.DailySummary         AS USA
	WHERE CONVERT(DATE, USA.TransDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
	  AND USA.FileName LIKE 'USAePay%'  
	GROUP BY USA.TransDate 

	UNION ALL

	SELECT USA.TransDate                                   AS TranDate
		  ,'90070-00-20-0000-00'                           AS GLSegment
		  ,SUM(ISNULL(USA.Amount, 0))                      AS Debit
	      ,''                                              AS Credit
	      ,'Credit Card Transaction $ Volume (USAePay)'    AS Description
	FROM USAePay.DailySummary         AS USA
	WHERE CONVERT(DATE, USA.TransDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
	  AND USA.FileName LIKE 'USAePay%'  
	GROUP BY USA.TransDate 


    UNION ALL

------------------------------------------------------------------------------------------------------------------------
--                         PayRazr Daily Summary to get Transaction Volume and Count by Date 
-----------------------------------------------------------------------------------------------------------------------
                        
	SELECT USA.TransDate                           AS TranDate
		  ,'90062-00-20-0000-00'                   AS GLSegment
		  ,SUM(ISNULL(USA.Sales, 0)) +
		   SUM(ISNULL(USA.Credits, 0)) +
		   SUM(ISNULL(USA.Declines, 0))            AS Debit
	      ,''                                      AS Credit
		  ,'Credit Card Transactions (PayRazr)'    AS Description
	FROM USAePay.DailySummary         AS USA
	WHERE CONVERT(DATE, USA.TransDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
	  AND USA.FileName LIKE 'PayRazr%' 
	GROUP BY USA.TransDate 

	UNION ALL

	SELECT USA.TransDate                                   AS TranDate
		  ,'90072-00-20-0000-00'                           AS GLSegment
		  ,SUM(ISNULL(USA.Amount, 0))                      AS Debit
	      ,''                                              AS Credit
	      ,'Credit Card Transaction $ Volume (PayRazr)'    AS Description
	FROM USAePay.DailySummary         AS USA
	WHERE CONVERT(DATE, USA.TransDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
	  AND USA.FileName LIKE 'PayRazr%' 
	GROUP BY USA.TransDate 


    UNION ALL

------------------------------------------------------------------------------------------------------------------------
--                   MeS Transaction Volume and Count by Date (Sales Only, exclude Credits and AmEx and Discover)
------------------------------------------------------------------------------------------------------------------------
	SELECT MeS.TranDate                       AS TranDate
		  ,'90061-00-20-0000-00'              AS GLSegment
		  ,MeS.MeSCount                       AS Debit
		  ,''                                 AS Credit
		  ,'Credit Card Transactions (MeS)'   AS Description
		  
	FROM (			        
			SELECT MeS.BatchDate              AS TranDate
				  ,COUNT(*)                   AS MeSCount
				  ,SUM(MeS.TransactionAmount) AS MeSVolume
			FROM MeS.SettlementSummary    AS MeS
			WHERE CONVERT(DATE, MeS.BatchDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
			  AND MeS.CardType NOT IN ('AM','DS')
			  AND MeS.TransactionAmount > 0
			GROUP BY MeS.BatchDate 
         )  AS MeS
     
     UNION ALL 
     
	 SELECT MeS.TranDate                              AS TranDate
		  ,'90071-00-20-0000-00'                      AS GLSegment
		  ,MeS.MeSVolume                              AS Debit
		  ,''                                         AS Credit
		  ,'Credit Card Transaction $ Volume (MeS)'   AS Description		  
	 FROM (			        
			SELECT MeS.BatchDate              AS TranDate
				  ,COUNT(*)                   AS MeSCount
				  ,SUM(MeS.TransactionAmount) AS MeSVolume
			FROM MeS.SettlementSummary    AS MeS
			WHERE CONVERT(DATE, MeS.BatchDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
			  AND MeS.CardType NOT IN ('AM','DS')
			  AND MeS.TransactionAmount > 0
			GROUP BY MeS.BatchDate 
          )  AS MeS
         
      UNION ALL
--------------------------------------------------------------------------------
--                   ATI Transaction Count by Date
--------------------------------------------------------------------------------              
	  SELECT ATI.FileCreationDate                           AS TranDate
		    ,'90060-00-10-0000-00'                          AS GLSegment
		    ,COUNT(*)                                       AS Debit
		    ,''                                             AS Credit
		    ,'ACH Transactions'                             AS Description
	  FROM NACHA.Originations      AS ATI
	  WHERE CONVERT(DATE, ATI.FileCreationDate) BETWEEN CONVERT(DATE, @FromThisDate) AND CONVERT(DATE, @ToThisDate)
	    AND CompanyName NOT LIKE 'BillingTree%'
	    AND CompanyName NOT LIKE 'BillingTreeCCFee%'
	    AND CompanyName NOT LIKE 'BillingTreeFee%'   
	    AND CompanyName NOT LIKE 'BillingTreeSettl%'
	    AND CompanyName NOT LIKE 'BillingTreeSetUp%'
	    AND CompanyName NOT LIKE 'BillngTreeCnv%' 
	    AND CompanyName NOT LIKE 'BT Conv%'
	  GROUP BY ATI.FileCreationDate     
 

 
       
 
 
END













