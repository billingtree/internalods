﻿





CREATE VIEW [dbo].[vUSAePayQuarantine]
AS
SELECT	ID
		,FileName
		,TransType
		,Result
		,ApiType
		,TransDate
		,TransDateYYYYMM
		,TransTime
		,Software
		,SourceID
		,AuthCode
		,MerchantName
		,USAePayID
		,MerchantID 
		,Platform
		,RefNum
		,AccountHolder
		,Amount
		,Invoice
		,OrderID
		,Description
		,Comments
		,Currency
		,Tax
		,Tip
		,Shipping
		,Subtotal
		,CardType
		,AVSZip
		,CustomerID
		,ErrorCode
		,ErrorReason
		,MashupOutcome
		,CASE MashupOutcome
			WHEN 1 THEN 'Spurious'
			WHEN 2 THEN 'UnMatched'
			WHEN 3 THEN 'Matched'
			WHEN 4 THEN 'Duplicates'
		END AS MashupOutcomeDescription
		,InsertDate
FROM	USAePay.FileData
WHERE	(UsabilityIndex = 0)






