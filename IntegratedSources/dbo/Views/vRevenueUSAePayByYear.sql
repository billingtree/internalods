﻿





CREATE VIEW [dbo].[vRevenueUSAePayByYear]
AS

SELECT DISTINCT 
	   USAePay.CCGNumber               AS MID
	  ,NULL                            AS SourceId
	  ,USAePay.Name                    AS Name
	  ,USAePayLookup.CRM_Id            AS CRM_ID
	  ,YEAR(USAePay.TranDate)          AS TranDate
	  ,SUM(USAePay.Revenue)            AS Revenue
	  ,'USAePay'                   AS Source  
FROM dbo.RevenueUSAePay20082010     AS USAePay
LEFT JOIN dbo.RevenueCCGLookup      AS USAePayLookup ON USAePay.CCGNumber = USAePayLookup.CCGNumber
WHERE USAePay.Revenue <> 0  
GROUP BY USAePay.CCGNumber
        ,USAePay.Name
        ,USAePayLookup.CRM_Id
        ,YEAR(USAePay.TranDate)
	  
UNION ALL

SELECT DISTINCT 
	   USAePay.CCGNumber               AS MID
	  ,NULL                            AS SourceId
	  ,USAePay.Name                    AS Name
	  ,USAePayLookup.CRM_Id            AS CRM_ID
	  ,YEAR(USAePay.TranDate)          AS TranDate
	  ,SUM(USAePay.Revenue)            AS Revenue
	  ,'USAePay'                   AS Source  
FROM dbo.RevenueUSAePay2011         AS USAePay
LEFT JOIN dbo.RevenueCCGLookup      AS USAePayLookup ON USAePay.CCGNumber = USAePayLookup.CCGNumber
WHERE USAePay.Revenue <> 0  
GROUP BY USAePay.CCGNumber
        ,USAePay.Name
        ,USAePayLookup.CRM_Id
        ,YEAR(USAePay.TranDate)
	  
UNION ALL

SELECT DISTINCT 
	   USAePay.CCGNumber               AS MID
	  ,NULL                            AS SourceId
	  ,USAePay.Name                    AS Name
	  ,USAePayLookup.CRM_Id            AS CRM_ID
	  ,YEAR(USAePay.TranDate)          AS TranDate
	  ,SUM(USAePay.Revenue)            AS Revenue
	  ,'USAePay'                   AS Source  
FROM dbo.RevenueUSAePay2012         AS USAePay
LEFT JOIN dbo.RevenueCCGLookup      AS USAePayLookup ON USAePay.CCGNumber = USAePayLookup.CCGNumber
WHERE USAePay.Revenue <> 0  
GROUP BY USAePay.CCGNumber
        ,USAePay.Name
        ,USAePayLookup.CRM_Id
        ,YEAR(USAePay.TranDate)

UNION ALL

SELECT DISTINCT 
	   USAePay.CCGNumber               AS MID
	  ,NULL                            AS SourceId
	  ,USAePay.Name                    AS Name
	  ,USAePayLookup.CRM_Id            AS CRM_ID
	  ,YEAR(USAePay.TranDate)          AS TranDate
	  ,SUM(USAePay.Revenue)            AS Revenue
	  ,'USAePay'                   AS Source  
FROM dbo.RevenueUSAePay2013         AS USAePay
LEFT JOIN dbo.RevenueCCGLookup      AS USAePayLookup ON USAePay.CCGNumber = USAePayLookup.CCGNumber
WHERE USAePay.Revenue <> 0  
GROUP BY USAePay.CCGNumber
        ,USAePay.Name
        ,USAePayLookup.CRM_Id
        ,YEAR(USAePay.TranDate)

UNION ALL

SELECT DISTINCT 
	   USAePay.MID                     AS MID
	  ,NULL                            AS SourceId
	  ,USAePay.CRMActLevelName_GPParentName
	                                   AS Name
	  ,USAePay.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
	                                   AS CRM_ID
	  ,YEAR(USAePay.TranDate)          AS TranDate
	  ,SUM(USAePay.RevenueAmount)      AS Revenue
	  ,'USAePay'                       AS Source  
FROM dbo.USAePayRevenueMonthlyGLFeed  AS USAePay

WHERE USAePay.RevenueAmount <> 0  
GROUP BY USAePay.MID
        ,USAePay.CRMActLevelName_GPParentName
        ,USAePay.CRMProcessingRecordNum_GPExtenderFieldOnCustomerCard
        ,YEAR(USAePay.TranDate)
        
        



