﻿









CREATE VIEW [dbo].[vCCMIDsFromRevenueFiles]
AS

SELECT DISTINCT CAST(CCMID.RevMID AS NVARCHAR (255) )        AS RevMID
               ,CCMID.RevName       AS RevName
               ,CCMID.CRM_MID       AS CRM_MID
               ,CCMID.CRM_ID        AS CRM_ID
               ,CCMID.Source        AS Source
FROM 
  (
  -- Get MIDs from MeS 2011/2012 Revenue File
  SELECT DISTINCT 
         CAST(CONVERT(NUMERIC, MeS.MID) AS NVARCHAR (255))  AS RevMID
        ,MeS.Name                    AS RevName
        ,CAST(MeSLookup.CRM_MID AS NVARCHAR (255))           AS CRM_MID
        ,MeSLookup.CRM_Id            AS CRM_ID
        ,'MeS'                       AS Source			  
  FROM dbo.RevenueMeS20112012      AS MeS
  LEFT JOIN dbo.RevenueMeSLookup   AS MeSLookup ON MeS.MID = MeSLookup.MID
  
  UNION ALL
  -- Get MIDs from MeS 2012/2013 Revenue File
  SELECT DISTINCT 
         CAST(CONVERT(NUMERIC, MeS.MID) AS NVARCHAR (255))  AS RevMID
        ,MeS.Name                    AS RevName
        ,CAST(MeSLookup.CRM_MID AS NVARCHAR (255))           AS CRM_MID
        ,MeSLookup.CRM_Id            AS CRM_ID
        ,'MeS'                       AS Source		
  FROM dbo.RevenueMeS20122013      AS MeS
  LEFT JOIN dbo.RevenueMeSLookup   AS MeSLookup ON MeS.MID = MeSLookup.MID	
   
  UNION ALL  
  -- Get MIDs from Orion Revenue All Years
  SELECT DISTINCT
         CAST(Orion.MID AS NVARCHAR (255)) AS RevMID
        ,Orion.Name                   AS RevName
        ,CAST(OrionLookup.CRM_MID AS NVARCHAR (255))          AS CRM_MID
        ,OrionLookup.CRM_Id           AS CRM_ID
        ,'Orion'                      AS Source
  FROM dbo.RevenueOrion            AS Orion
  LEFT JOIN dbo.RevenueOrionLookup AS OrionLookup ON Orion.MID = OrionLookup.MID

  UNION ALL
  -- Get MIDs from UMS 2008 to 2010 Revenue File
  SELECT DISTINCT
         CAST(UMS.MID AS NVARCHAR (255))  AS RevMID
        ,UMS.Name                    AS RevName
        ,CAST(UMSLookup.CRM_MID AS NVARCHAR (255))           AS CRM_MID
        ,UMSLookup.CRM_Id            AS CRM_ID
        ,'UMS'                       AS Source
  FROM dbo.RevenueUMS20082010      AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  
  UNION ALL    
  -- Get MIDs from UMS 2011 Revenue File
  SELECT DISTINCT
         CAST(UMS.MID AS NVARCHAR (255))  AS RevMID
        ,UMS.Name                    AS RevName
        ,CAST(UMSLookup.CRM_MID AS NVARCHAR (255))           AS CRM_MID
        ,UMSLookup.CRM_Id            AS CRM_ID
        ,'UMS'                       AS Source
  FROM dbo.RevenueUMS2011          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  
  UNION ALL  
  -- Get MIDs from UMS 2012 Revenue File
  SELECT DISTINCT
         CAST(UMS.MID AS NVARCHAR (255))  AS RevMID
        ,UMS.Name                    AS RevName
        ,CAST(UMSLookup.CRM_MID AS NVARCHAR (255))           AS CRM_MID
        ,UMSLookup.CRM_Id            AS CRM_ID
        ,'UMS'                       AS Source
  FROM dbo.RevenueUMS2012          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0
  
  UNION ALL  
  -- Get MIDs from UMS 2013 Revenue File
  SELECT DISTINCT
         CAST(UMS.MID AS NVARCHAR (255))  AS RevMID
        ,UMS.Name                    AS RevName
        ,CAST(UMSLookup.CRM_MID AS NVARCHAR (255))   AS CRM_MID
        ,UMSLookup.CRM_Id            AS CRM_ID
        ,'UMS'                       AS Source
  FROM dbo.RevenueUMS2013          AS UMS
  LEFT JOIN dbo.RevenueUMSLookup   AS UMSLookup ON UMS.MID = UMSLookup.MID
  WHERE UMS.Revenue <> 0

  UNION ALL  
  -- Get MIDs from USAePay 2008 through 2010 Revenue File
  SELECT DISTINCT
         CAST(USA.CCGNumber AS NVARCHAR (255))   AS RevMID
        ,USA.Name                    AS RevName
        ,CCG.CRM_MID                 AS CRM_MID
        ,CCG.CRM_Id                  AS CRM_ID
        ,'USAePay'                   AS Source
  FROM dbo.RevenueUSAePay20082010        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
      
  UNION ALL  
  -- Get MIDs from USAePay 2011 Revenue File
  SELECT DISTINCT
         CAST(USA.CCGNumber AS NVARCHAR (255))   AS RevMID
        ,USA.Name                    AS RevName
        ,CCG.CRM_MID                 AS CRM_MID
        ,CCG.CRM_Id                  AS CRM_ID
        ,'USAePay'                   AS Source
  FROM dbo.RevenueUSAePay2011        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
          
  UNION ALL 
  -- Get MIDs from USAePay 2012 Revenue File
  SELECT DISTINCT
         CAST(USA.CCGNumber AS NVARCHAR (255))   AS RevMID
        ,USA.Name                    AS RevName
        ,CCG.CRM_MID                 AS CRM_MID
        ,CCG.CRM_Id                  AS CRM_ID
        ,'USAePay'                   AS Source			  
  FROM dbo.RevenueUSAePay2012        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber

  UNION ALL   
  -- Get MIDs from USAePay 2013 Revenue File
  SELECT DISTINCT
         CAST(USA.CCGNumber AS NVARCHAR (255))   AS RevMID
        ,USA.Name                    AS RevName
        ,CCG.CRM_MID                 AS CRM_MID
        ,CCG.CRM_Id                  AS CRM_ID
        ,'USAePay'                   AS Source
  FROM dbo.RevenueUSAePay2013        AS USA
  LEFT JOIN dbo.RevenueCCGLookup     AS CCG  ON USA.CCGNumber = CCG.CCGNumber
          			  
  ) AS CCMID
  










