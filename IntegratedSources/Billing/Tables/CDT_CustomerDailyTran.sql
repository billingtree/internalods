﻿CREATE TABLE [Billing].[CDT_CustomerDailyTran] (
    [CDT_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [CDT_CPP_ID]        INT          NOT NULL,
    [CDT_RunDateYYYYMM] INT          NOT NULL,
    [CDT_TranDate]      DATE         NOT NULL,
    [CDT_CTF_ID]        INT          NULL,
    [CDT_TranType]      VARCHAR (50) NOT NULL,
    [CDT_TranAmount]    MONEY        NOT NULL,
    [CDT_TranCount]     INT          NOT NULL,
    [CDT_PRT_ID]        INT          NOT NULL,
    [CCL_CreateDate]    DATETIME     CONSTRAINT [DF_CDT_CustomerDailyTran_CCL_CreateDate_1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CDT_CustomerDailyTran] PRIMARY KEY CLUSTERED ([CDT_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_CDT_CustomerDailyTran_CPP_CustomerProcessingProfile] FOREIGN KEY ([CDT_CPP_ID]) REFERENCES [Billing].[CPP_CustomerProcessingProfile] ([CPP_ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_CDT_CustomerDailyTran_CTF_CustomerTranFee] FOREIGN KEY ([CDT_CTF_ID]) REFERENCES [Billing].[CTF_CustomerTranFee] ([CTF_ID])
);

