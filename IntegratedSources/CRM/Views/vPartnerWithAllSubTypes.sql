﻿



CREATE VIEW [CRM].[vPartnerWithAllSubTypes]
AS

-------------------------------------------------------------------------------------
--   By Service Order Id - Get all Partner Types and All of their Sub Types        --
-------------------------------------------------------------------------------------
SELECT DISTINCT SPF.bt_ServiceOrder             AS ServiceOrderId
               ,SPF.bt_StartDate                AS ServProdFeeStartDate
               ,SPF.bt_EndDate                  AS ServProdFeeEndDate
               ,SPF.StatusCodeName              AS ServProdFeeSoftDelete
               ,Acct.Name                       AS PartnerName
			   ,Acct.bt_TypeName                AS PartnerType
			   ,Acct.bt_SubTypeName             AS PartnerSubType
			   ,Prod.bt_TypeName                AS ProdType
			   ,Prod.Name                       AS ProdName
			   ,Prod.bt_Version                 AS ProdVersion
FROM [$(CRMShadow)].MS.bt_ServiceProductFee   AS SPF
JOIN [$(CRMShadow)].MS.Product                AS Prod ON SPF.bt_Product = Prod.ProductId
												AND Prod.bt_EndDate IS NULL
JOIN [$(CRMShadow)].MS.Account                AS Acct ON Prod.bt_Provider = Acct.AccountId 
												AND Acct.Name NOT LIKE 'BillingTree%'                                             
WHERE SPF.bt_ServiceOrder IS NOT NULL






