﻿CREATE TABLE [ATI].[Concordance] (
    [ID]               INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileName]         VARCHAR (50)  NOT NULL,
    [ConfigNumber]     TINYINT       NOT NULL,
    [ProcessingNumber] SMALLINT      NOT NULL,
    [ACHName]          NVARCHAR (50) NOT NULL,
    [InsertDate]       DATE          CONSTRAINT [DF_Concordance_InsertDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]        BIT           CONSTRAINT [DF_Concordance_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Concordance] PRIMARY KEY CLUSTERED ([ConfigNumber] ASC, [ProcessingNumber] ASC, [ACHName] ASC) WITH (FILLFACTOR = 80)
);

