﻿












-- =============================================
-- Author:        Rick Stohle
-- Create date: 22-Jul-2014
-- Description:   Procedure to read in all of Finance's Revenue files, 
--                that have been mashed up to CRM inside the Views
--                so that we are able to get the State on the Account table
--                and then report on Revenue for the years prior to 2014
-- =============================================
CREATE PROCEDURE [dbo].[spAccountsByStateWithRevenueYearMonth] 
(
@ProductLine VARCHAR (11),
@State       VARCHAR (50),
@Year        VARCHAR (4)
)

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
    SET NOCOUNT ON;


    DECLARE @ThisProduct       VARCHAR (11)
    DECLARE @ThisState         VARCHAR (50)
    DECLARE @StartYear         INT
   -- DECLARE @EndYear           INT
 
    SET @ThisProduct = @ProductLine
    
    SET @ThisState = LTRIM(RTRIM(@State))

    SET @StartYear = @Year
/*****    
    IF @Year = 'All' 
        BEGIN
            SET @StartYear = 2008
            SET @EndYear = 2016
        END  
    ELSE
        BEGIN
            SET @StartYear = CONVERT(INT, @Year)
            SET @EndYear = CONVERT(INT, @Year)
        END
****/
    
    -- Insert statements for procedure here
    

IF @ThisState = 'All States'    
    IF @ThisProduct = 'CC'
		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                              AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                        AS RevFileName 
			  ,''                        AS RevMID
		FROM dbo.vRevenueByCCByYearMonth             AS Rev
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear 
				    				
    ELSE

-------------------------------------------
--  ACH Only Report Query
--------------------------------------------
    IF @ThisProduct = 'ACH'

		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                                 AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                                         AS RevFileName 
			  ,''                                         AS RevMID
		FROM dbo.vRevenueByACHByYearMonth            AS Rev 
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear
	      				
    ELSE

    --------------------------------------------------------------------------------------------------------------
    --	Get CC and ACH
    --------------------------------------------------------------------------------------------------------------
		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                              AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                        AS RevFileName 
			  ,''                        AS RevMID
		FROM dbo.vRevenueByCCByYearMonth             AS Rev
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear 
        
        UNION ALL

		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                                 AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                                         AS RevFileName 
			  ,''                                         AS RevMID
		FROM dbo.vRevenueByACHByYearMonth            AS Rev 
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear


ELSE

	-----------------------------------------------------------------------------------
	--   Do the Same as above, but get the data by the selected State 
	----------------------------------------------------------------------------------	   

    IF @ThisProduct = 'CC'
		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                              AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                        AS RevFileName 
			  ,''                        AS RevMID
		FROM dbo.vRevenueByCCByYearMonth             AS Rev
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear 
          AND Acc.Address1_StateOrProvince = @ThisState
				    				
    ELSE

-------------------------------------------
--  ACH Only Report Query
--------------------------------------------
    IF @ThisProduct = 'ACH'

		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                                 AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                                         AS RevFileName 
			  ,''                                         AS RevMID
		FROM dbo.vRevenueByACHByYearMonth            AS Rev 
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear
          AND Acc.Address1_StateOrProvince = @ThisState

	      				
    ELSE

    --------------------------------------------------------------------------------------------------------------
    --	Get CC and ACH
    --------------------------------------------------------------------------------------------------------------
		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                              AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                        AS RevFileName 
			  ,''                        AS RevMID
		FROM dbo.vRevenueByCCByYearMonth             AS Rev
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear 
          AND Acc.Address1_StateOrProvince = @ThisState


        UNION ALL
    
		SELECT CASE 
				   WHEN Acc.StatusCodeName = 'Active' THEN
					   ''
				   ELSE
					   'Y'
			   END                                                 AS SoftDeleted
			  ,Acc.Name                                            AS AccountName
			  ,Acc.bt_StatusName                                   AS AccountStatusName
			  ,Acc.bt_SubTypeName                                  AS AccountSubType
			  ,Acc.Address1_Composite                              AS PrimaryComposite
			  ,Acc.Address1_StateOrProvince                        AS PrimaryState
			  ,Acc.Address2_Composite                              AS SecondaryComposite
			  ,Acc.Address2_StateOrProvince                        AS SecondaryState
			  ,PA.bt_Name                                          AS PAName
			  ,PA.StatusCodeName                                   AS PAStatus
			  ,PP.StatusCodeName                                   AS ProfileStatus
			  ,PP.bt_ProductLineName                               AS ProductLine
			  ,PP.bt_BankMID                                       AS MID
			  ,CASE 
				   WHEN PP.bt_ProductLineName = 'ACH' THEN
					   PP.bt_Config
				   ELSE
					   PP.bt_SourceId  
			   END                                                 AS ConfigSourceID   
			  ,Rev.Source                                       AS Source
			  ,Rev.TranDate                                     AS TranDate
			  ,Rev.Revenue                                      AS Revenue
			  ,''                                         AS RevFileName 
			  ,''                                         AS RevMID
		FROM dbo.vRevenueByACHByYearMonth            AS Rev 
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile  AS PP   ON Rev.CRM_Id = PP.bt_ProcessingProfileId
		LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount  AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
		LEFT JOIN [$(CRMShadow)].MS.Account               AS Acc  ON PA.bt_Account = Acc.AccountId
        WHERE LEFT(Rev.TranDate, 4) = @StartYear
          AND Acc.Address1_StateOrProvince = @ThisState
   
    

END


















