﻿CREATE TABLE [dbo].[InitialProcessingDateByMID] (
    [CRM_PPDID]             UNIQUEIDENTIFIER NULL,
    [InitialProcessingDate] DATE             NULL,
    [CreatedDate]           SMALLDATETIME    CONSTRAINT [DF_InitialProcessingDateByMID_CreatedDate] DEFAULT (getdate()) NOT NULL
);

