﻿CREATE TABLE [USAePay].[Cost] (
    [YearMonth]       INT            NULL,
    [MerchantNum]     NVARCHAR (255) NULL,
    [Company]         NVARCHAR (255) NULL,
    [TotalCollected]  INT            NULL,
    [ResellerCost]    INT            NULL,
    [CollectionFee]   INT            NULL,
    [ResidualPayout]  INT            NULL,
    [DateActivated]   DATE           NULL,
    [MainGLSegment]   NVARCHAR (10)  CONSTRAINT [DF_Cost_MainGLSegment] DEFAULT ((41000)) NULL,
    [CRM_PPDID]       NVARCHAR (36)  NULL,
    [USAePaySourceID] NVARCHAR (255) NULL,
    [InsertDate]      SMALLDATETIME  CONSTRAINT [DF__Costs__InsertDat__2512604C] DEFAULT (getdate()) NULL
);

