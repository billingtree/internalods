﻿CREATE TABLE [dbo].[ATIWeeklyCosts] (
    [FileReferenceCode] INT           NULL,
    [ConfigID]          NVARCHAR (2)  NULL,
    [AccountID]         NVARCHAR (4)  NULL,
    [TotalTransactions] INT           NULL,
    [TotalReturns]      INT           NULL,
    [Corrections]       INT           NULL,
    [Addenda]           INT           NULL,
    [StartDate]         DATE          NULL,
    [EndDate]           DATE          NULL,
    [TransactionsCosts] MONEY         NULL,
    [ReturnsCosts]      MONEY         NULL,
    [CorrectionsCosts]  MONEY         NULL,
    [MainGLSegment]     NVARCHAR (10) CONSTRAINT [DF_ATIWeeklyCosts_MainGLSegment] DEFAULT ((42000)) NULL,
    [InsertDate]        SMALLDATETIME CONSTRAINT [DF__ATIWeeklyCosts__Inser__31AD415B] DEFAULT (getdate()) NULL
);

