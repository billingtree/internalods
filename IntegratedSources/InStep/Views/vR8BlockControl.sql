﻿
CREATE VIEW [InStep].[vR8BlockControl] AS
SELECT	ID
		,FileReferenceCodeID
		,ConfigNumber
		,LineNumber
		,BlockNumber
		,EntryORAddendaType
		,IsCCDOffset
		,LEFT(SourceRecord,1) AS RecordTypeCode
		,SUBSTRING(SourceRecord,2,3) AS ServiceClassCode
		,SUBSTRING(SourceRecord,5,6) AS EntryORAddendaCount
		,SUBSTRING(SourceRecord,11,10) AS EntryHash
		,SUBSTRING(SourceRecord,21,12) AS TotalDebitEntryDollarAmount
		,SUBSTRING(SourceRecord,33,12) AS TotalCreditEntryDollarAmount
		,SUBSTRING(SourceRecord,45,10) AS CompanyIdentification
		,SUBSTRING(SourceRecord,55,19) AS MessageAuthenticationCode
		,SUBSTRING(SourceRecord,74,6) AS Reserved
		,SUBSTRING(SourceRecord,80,8) AS OriginatingDFIIdentification
		,SUBSTRING(SourceRecord,88,7) AS BatchNumber
FROM	InStep.ReturnFileData
WHERE	RecordType = 8





