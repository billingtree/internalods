﻿




CREATE VIEW [dbo].[vACHMIDsFromRevenueFiles]
AS

SELECT DISTINCT CAST(ACHMID.RevMID AS NVARCHAR (255) )        AS RevMID
               ,ACHMID.RevName       AS RevName
               ,ACHMID.CRM_MID       AS CRM_MID
               ,ACHMID.CRM_ID        AS CRM_ID
               ,ACHMID.Source        AS Source
FROM 
  (
  -- Get MIDs from InStep Revenue File
  SELECT DISTINCT 
         CONVERT(NVARCHAR, InStep.MID)  AS RevMID
        ,InStep.Name                    AS RevName
        ,InStepLookup.CRM_MID           AS CRM_MID
        ,InStepLookup.CRM_Id            AS CRM_ID
        ,'InStep'                       AS Source  
  FROM dbo.RevenueInStep              AS InStep
  LEFT JOIN dbo.RevenueInStepLookup   AS InStepLookup ON InStep.MID = InStepLookup.MID
  WHERE InStep.Revenue <> 0
    
  UNION ALL
  -- Get MIDs from ATI 2008 to 2010 Revenue File
  SELECT DISTINCT 
         CONCAT(CONVERT(NVARCHAR, ATI.MID), '-', CONVERT(NVARCHAR, ATI.Config))
                                     AS RevMID
        ,ATI.Name                    AS RevName
        ,ATILookup.CRM_MID           AS CRM_MID
        ,ATILookup.CRM_Id            AS CRM_ID
        ,'ATI'                       AS Source  	
  FROM dbo.RevenueATI20082010      AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID	
                                               AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
       
  UNION ALL
  -- Get MIDs from ATI 2011 JanJun Revenue File
  SELECT DISTINCT 
         CONCAT(CONVERT(NVARCHAR, ATI.MID), '-', CONVERT(NVARCHAR, ATI.Config))
                                     AS RevMID
        ,ATI.Name                    AS RevName
        ,ATILookup.CRM_MID           AS CRM_MID
        ,ATILookup.CRM_Id            AS CRM_ID
        ,'ATI'                       AS Source  	
  FROM dbo.RevenueATI2011JanJun    AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID	
                                               AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
   
  UNION ALL  
  -- Get MIDs from ATI 2011 JulDec Revenue File
  SELECT DISTINCT 
         CONCAT(CONVERT(NVARCHAR, ATI.MID), '-', CONVERT(NVARCHAR, ATI.Config))
                                     AS RevMID
        ,ATI.Name                    AS RevName
        ,ATILookup.CRM_MID           AS CRM_MID
        ,ATILookup.CRM_Id            AS CRM_ID
        ,'ATI'                       AS Source  	
  FROM dbo.RevenueATI2011JulDec    AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID	
                                               AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0

  UNION ALL
  -- Get MIDs from ATI 2012 Revenue File
  SELECT DISTINCT 
         CONCAT(CONVERT(NVARCHAR, ATI.MID), '-', CONVERT(NVARCHAR, ATI.Config))
                                     AS RevMID
        ,ATI.Name                    AS RevName
        ,ATILookup.CRM_MID           AS CRM_MID
        ,ATILookup.CRM_Id            AS CRM_ID
        ,'ATI'                       AS Source  	
  FROM dbo.RevenueATI2012          AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID	
                                               AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0
  
  UNION ALL  
  -- Get MIDs from ATI 2013 Revenue File
  SELECT DISTINCT 
         CONCAT(CONVERT(NVARCHAR, ATI.MID), '-', CONVERT(NVARCHAR, ATI.Config))
                                     AS RevMID
        ,ATI.Name                    AS RevName
        ,ATILookup.CRM_MID           AS CRM_MID
        ,ATILookup.CRM_Id            AS CRM_ID
        ,'ATI'                       AS Source  	
  FROM dbo.RevenueATI2013          AS ATI
  LEFT JOIN dbo.RevenueATILookup   AS ATILookup ON ATI.MID = ATILookup.MID	
                                               AND ATI.Config = ATILookup.Config
  WHERE ATI.Revenue <> 0

          			  
  ) AS ACHMID
  








