﻿<?xml version="1.0" encoding="utf-8"?>
<ComponentItem xmlns:rdl="http://schemas.microsoft.com/sqlserver/reporting/2010/01/reportdefinition" xmlns:rd="http://schemas.microsoft.com/SQLServer/reporting/reportdesigner" Name="Tablix3" xmlns="http://schemas.microsoft.com/sqlserver/reporting/2010/01/componentdefinition">
  <Properties>
    <Property Name="Type">Tablix</Property>
    <Property Name="ThumbnailSource">iVBORw0KGgoAAAANSUhEUgAAAIIAAAAKCAYAAAB44IZIAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAETSURBVFhH7ZgLDoMgEETFi3o1PalliGum210+JjUYecnIqLMGFEjTsK7rPkWWZZm2bYN9hQd31HheuKvO8wDnYAdoWR5W/mlYYxB5WFmtFrAAAdfWPIszLOZY3NmMIP0I6RBCzH0TLx/ul9Z8j1hjYKzxlGqE2ndxpQ/Aq+N86zeasUUgoOXRmu8RvCSrz7nxyLhL1GSA9x5ZFrk6oeUbIQvmdHwZcTs0V0xphZbug5pMj6SJILMCbcmDUqZ3DzAZsErQsr9Soz3Q9Z6XttVLa3lQyrAXsGecAuw1nNV6Cla/rWuMvsfnrFrihDnz+hksjZVhAXm2JwZZkH4sxpuDlzP+R/hzjeeFu+pyHowdYRCZpg8O0if6JsiRRgAAAABJRU5ErkJggg==</Property>
    <Property Name="ThumbnailMimeType">image/png</Property>
  </Properties>
  <RdlFragment>
    <rdl:Report>
      <rdl:AutoRefresh>0</rdl:AutoRefresh>
      <rdl:DataSources>
        <rdl:DataSource Name="bidwsqldev01">
          <rdl:DataSourceReference>/Data Sources/IntegratedSourcesDB</rdl:DataSourceReference>
          <rd:SecurityType>None</rd:SecurityType>
          <rd:DataSourceID>b4274951-e6e2-4814-8bf9-2d7dc90375f4</rd:DataSourceID>
        </rdl:DataSource>
      </rdl:DataSources>
      <rdl:DataSets>
        <rdl:DataSet Name="DataSet1">
          <rdl:Query>
            <rdl:DataSourceName>bidwsqldev01</rdl:DataSourceName>
            <rdl:CommandText>SELECT
  vwQuarantineReport.Source
  ,vwQuarantineReport.Name
  ,vwQuarantineReport.SourceID
  ,vwQuarantineReport.MerchantID
  ,vwQuarantineReport.TransType
  ,vwQuarantineReport.Reason
  ,vwQuarantineReport.[Count]
FROM
  vwQuarantineReport</rdl:CommandText>
            <rd:DesignerState>
              <QueryDefinition xmlns="http://schemas.microsoft.com/ReportingServices/QueryDefinition/Relational">
                <SelectedColumns>
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="Source" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="Name" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="SourceID" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="MerchantID" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="TransType" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="Reason" />
                  <ColumnExpression ColumnOwner="vwQuarantineReport" ColumnName="Count" />
                </SelectedColumns>
              </QueryDefinition>
            </rd:DesignerState>
          </rdl:Query>
          <rdl:Fields>
            <rdl:Field Name="Source">
              <rdl:DataField>Source</rdl:DataField>
              <rd:TypeName>System.String</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="Name">
              <rdl:DataField>Name</rdl:DataField>
              <rd:TypeName>System.String</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="SourceID">
              <rdl:DataField>SourceID</rdl:DataField>
              <rd:TypeName>System.String</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="MerchantID">
              <rdl:DataField>MerchantID</rdl:DataField>
              <rd:TypeName>System.String</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="TransType">
              <rdl:DataField>TransType</rdl:DataField>
              <rd:TypeName>System.String</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="Reason">
              <rdl:DataField>Reason</rdl:DataField>
              <rd:TypeName>System.Int16</rd:TypeName>
            </rdl:Field>
            <rdl:Field Name="Count">
              <rdl:DataField>Count</rdl:DataField>
              <rd:TypeName>System.Int32</rd:TypeName>
            </rdl:Field>
          </rdl:Fields>
        </rdl:DataSet>
      </rdl:DataSets>
      <rdl:ReportSections>
        <rdl:ReportSection>
          <rdl:Body>
            <rdl:ReportItems>
              <rdl:Tablix Name="Tablix3">
                <rdl:TablixBody>
                  <rdl:TablixColumns>
                    <rdl:TablixColumn>
                      <rdl:Width>0.87625in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>3.61458in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>0.94444in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>1.49778in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>1in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>0.69792in</rdl:Width>
                    </rdl:TablixColumn>
                    <rdl:TablixColumn>
                      <rdl:Width>0.79167in</rdl:Width>
                    </rdl:TablixColumn>
                  </rdl:TablixColumns>
                  <rdl:TablixRows>
                    <rdl:TablixRow>
                      <rdl:Height>0.39188in</rdl:Height>
                      <rdl:TablixCells>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox16">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>Source</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox16</rd:DefaultName>
                              <rdl:ZIndex>5</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox17">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>Name</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox17</rd:DefaultName>
                              <rdl:ZIndex>1</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox18">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>SourceID / Config#</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox18</rd:DefaultName>
                              <rdl:ZIndex>7</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox19">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>MerchantID / Processing#</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox19</rd:DefaultName>
                              <rdl:ZIndex>8</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox24">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>Tran Type</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox24</rd:DefaultName>
                              <rdl:ZIndex>12</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox25">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>Reason</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox25</rd:DefaultName>
                              <rdl:ZIndex>13</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Textbox50">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>Count</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:TextDecoration>Underline</rdl:TextDecoration>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Textbox50</rd:DefaultName>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                      </rdl:TablixCells>
                    </rdl:TablixRow>
                    <rdl:TablixRow>
                      <rdl:Height>0.22396in</rdl:Height>
                      <rdl:TablixCells>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Source">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!Source.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Source</rd:DefaultName>
                              <rdl:ZIndex>2</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Name">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!Name.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Name</rd:DefaultName>
                              <rdl:ZIndex>4</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="SourceID">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!SourceID.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>SourceID</rd:DefaultName>
                              <rdl:ZIndex>3</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="MerchantID">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!MerchantID.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>MerchantID</rd:DefaultName>
                              <rdl:ZIndex>9</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="TransType">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!TransType.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style />
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>TransType</rd:DefaultName>
                              <rdl:ZIndex>10</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Reason">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!Reason.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style>
                                    <rdl:TextAlign>Left</rdl:TextAlign>
                                  </rdl:Style>
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Reason</rd:DefaultName>
                              <rdl:ZIndex>11</rdl:ZIndex>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                        <rdl:TablixCell>
                          <rdl:CellContents>
                            <rdl:Textbox Name="Count">
                              <rdl:CanGrow>true</rdl:CanGrow>
                              <rdl:KeepTogether>true</rdl:KeepTogether>
                              <rdl:Paragraphs>
                                <rdl:Paragraph>
                                  <rdl:TextRuns>
                                    <rdl:TextRun>
                                      <rdl:Value>=Fields!Count.Value</rdl:Value>
                                      <rdl:Style>
                                        <rdl:FontSize>9pt</rdl:FontSize>
                                        <rdl:Format>#,0;(#,0)</rdl:Format>
                                      </rdl:Style>
                                    </rdl:TextRun>
                                  </rdl:TextRuns>
                                  <rdl:Style>
                                    <rdl:TextAlign>Left</rdl:TextAlign>
                                  </rdl:Style>
                                </rdl:Paragraph>
                              </rdl:Paragraphs>
                              <rd:DefaultName>Count</rd:DefaultName>
                              <rdl:Style>
                                <rdl:Border>
                                  <rdl:Style>None</rdl:Style>
                                </rdl:Border>
                                <rdl:PaddingLeft>2pt</rdl:PaddingLeft>
                                <rdl:PaddingRight>2pt</rdl:PaddingRight>
                                <rdl:PaddingTop>2pt</rdl:PaddingTop>
                                <rdl:PaddingBottom>2pt</rdl:PaddingBottom>
                              </rdl:Style>
                            </rdl:Textbox>
                          </rdl:CellContents>
                        </rdl:TablixCell>
                      </rdl:TablixCells>
                    </rdl:TablixRow>
                  </rdl:TablixRows>
                </rdl:TablixBody>
                <rdl:TablixColumnHierarchy>
                  <rdl:TablixMembers>
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                    <rdl:TablixMember />
                  </rdl:TablixMembers>
                </rdl:TablixColumnHierarchy>
                <rdl:TablixRowHierarchy>
                  <rdl:TablixMembers>
                    <rdl:TablixMember>
                      <rdl:KeepWithGroup>After</rdl:KeepWithGroup>
                    </rdl:TablixMember>
                    <rdl:TablixMember>
                      <rdl:Group Name="Details" />
                    </rdl:TablixMember>
                  </rdl:TablixMembers>
                </rdl:TablixRowHierarchy>
                <rdl:DataSetName>DataSet1</rdl:DataSetName>
                <rdl:Top>0.35764in</rdl:Top>
                <rdl:Left>0.12375in</rdl:Left>
                <rdl:Height>0.61584in</rdl:Height>
                <rdl:Width>9.42264in</rdl:Width>
                <rdl:Style>
                  <rdl:Border>
                    <rdl:Style>None</rdl:Style>
                  </rdl:Border>
                </rdl:Style>
                <ComponentMetadata>
                  <ComponentId>367fabc2-a004-4cd5-96e8-594cabc2c80b</ComponentId>
                </ComponentMetadata>
              </rdl:Tablix>
            </rdl:ReportItems>
            <rdl:Height>0in</rdl:Height>
            <rdl:Style />
          </rdl:Body>
          <rdl:Width>0in</rdl:Width>
          <rdl:Page>
            <rdl:Style />
          </rdl:Page>
        </rdl:ReportSection>
      </rdl:ReportSections>
      <rd:ReportUnitType>Invalid</rd:ReportUnitType>
      <rd:ReportID>ee206d19-b972-40a2-a15b-e6312aaa9d78</rd:ReportID>
    </rdl:Report>
  </RdlFragment>
</ComponentItem>