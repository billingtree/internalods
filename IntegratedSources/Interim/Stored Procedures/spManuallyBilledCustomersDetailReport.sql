﻿





-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE [Interim].[spManuallyBilledCustomersDetailReport](
@RunYear         INT,
@RunMonth        INT,
@MID             NVARCHAR (50),
@ConfigSource    NVARCHAR (50)
)

-- Insert statements for procedure here

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      DECLARE @Year               INT
      DECLARE @Month              INT
      DECLARE @BankMID            NVARCHAR (50)
      DECLARE @ConfigSourceID     NVARCHAR (50)
      DECLARE @RunDateBeginMonth  DATETIME
      
      SET @Year=@RunYear
      SET @Month=@RunMonth
      
      SET @RunDateBeginMonth = CONVERT(DATETIME, CONCAT(@Year, '-', @Month, '-', '01', ' 00:00:00'))
      
      IF @MID IS NOT NULL
          SET @BankMID = @MID
          
      IF @ConfigSource IS NOT NULL         
          SET @ConfigSourceID = @ConfigSource
      
      
      DECLARE @txtYear   VARCHAR (4)
      DECLARE @txtMonth  VARCHAR (2)
      DECLARE @YearMonth INT
 
      SET @txtYear = CONVERT(VARCHAR (4), @Year)
      IF @Month < 10 
          SET @txtMonth = CONCAT('0', CONVERT(VARCHAR(1), @Month)) 
      ELSE
          SET @txtMonth = CONVERT(VARCHAR(2), @Month) 
    
      SET @YearMonth = CONVERT(INT, CONCAT(@txtYear, @txtMonth))          
      
      

IF LEN(@BankMID) = 4 
-------------------------------------------------------------------------
--    The supplied MID is for ACH
--        ACH Tran Counts first
--        Then Returns
--        Then UnAuths
--        Then Monthly Fees
--        Then Batch Count
-------------------------------------------------------------------------
	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,CONCAT(PP.bt_Config,'-',PP.bt_BankMID)              AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,ATI.FileCreationDate                                AS TranDate
		  ,ATI.FormattedAmount                                 AS TranAmount 
		  ,ATI.FormattedAmount                                 AS DebitQuantity 
		  ,ATI.TraceNumber                                     AS TraceNumber
		  ,ATI.ReceivingCompanyName                            AS AccountHolder
		  ,'(4) ACH Trans'                                     AS SourceData
	FROM NACHA.Originations                         AS ATI 
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP   ON ATI.CRM_PPDID = PP.bt_ProcessingProfileId
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	WHERE ATI.UsabilityIndex = 1
      AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
	  AND ATI.FileCreationDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_Config = @ConfigSourceID
	  AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)


	UNION ALL

	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,CONCAT(PP.bt_Config,'-',PP.bt_BankMID)              AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,ATI.FileCreationDate                                AS TranDate
		  ,ATI.FormattedAmount                                 AS TranAmount 
		  ,0                                                   AS DebitQuantity 
		  ,ATI.TraceNumber                                     AS TraceNumber
		  ,ATI.ReceivingCompanyName                            AS AccountHolder
		  ,'(5) Returns'                                       AS SourceData
	FROM NACHA.Returns                                AS ATI  
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile       AS PP   ON ATI.CRM_PPDID = PP.bt_ProcessingProfileId
   	LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount       AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	WHERE ATI.UsabilityIndex = 1
	  AND UPPER(ATI.StandardEntryClassCode) <> 'COR'
	  AND ATI.ReturnCode NOT IN ('R05','R07','R10','R29','R51')
	  AND ATI.FileCreationDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_Config = @ConfigSourceID
	  AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)


	UNION ALL

	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,CONCAT(PP.bt_Config,'-',PP.bt_BankMID)              AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,ATI.FileCreationDate                                AS TranDate
		  ,ATI.FormattedAmount                                 AS TranAmount 
		  ,0                                                   AS DebitQuantity 
		  ,ATI.TraceNumber                                     AS TraceNumber
		  ,ATI.ReceivingCompanyName                            AS AccountHolder
		  ,'(7) UnAuth'                                        AS SourceData
	FROM NACHA.Returns                                AS ATI  
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile       AS PP   ON ATI.CRM_PPDID = PP.bt_ProcessingProfileId
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount       AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	WHERE ATI.UsabilityIndex = 1
	  AND UPPER(ATI.StandardEntryClassCode) <> 'COR'
	  AND ATI.ReturnCode IN ('R05','R07','R10','R29','R51')
	  AND ATI.FileCreationDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_Config = @ConfigSourceID
	  AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)

			
    UNION ALL    


	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,CONCAT(PP.bt_Config,'-',PP.bt_BankMID)              AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,ATI.CreateDate                                      AS TranDate
		  ,NULL                                                AS TranAmount 
		  ,NULL                                                AS DebitQuantity 
		  ,''                                                  AS TraceNumber
		  ,''                                                  AS AccountHolder
		  ,'(6) Batches'                                       AS SourceData
	FROM [$(CRMShadow)].MS.bt_ProcessingProfile          AS PP   
	LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId	
	JOIN (  -- Get Batch records from ATI 
	        	SELECT DISTINCT ATI.CRM_PPDID   AS CRM_PPDID
	        	      ,ATI.FileReferenceCodeID  AS FileRef
	        	      ,ATI.FileCreationDate     AS CreateDate
	        	      ,ATI.BatchNumber          AS BatchNumber
	                  --,ATI.BlockNumber          AS Block
	            FROM NACHA.Originations                    AS ATI
	            WHERE ATI.UsabilityIndex = 1
		          AND UPPER(ATI.IdentificationNumber) NOT LIKE '%OFFSET%'
		          AND YEAR(ATI.FileCreationDate) = @Year
		          AND MONTH(ATI.FileCreationDate) = @Month 
		       )                                    AS ATI  ON PP.bt_ProcessingProfileId = ATI.CRM_PPDID
	WHERE PP.bt_BankMID = @BankMID
	  AND PP.bt_Config = @ConfigSourceID
	  AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)
 

ELSE  

--IF LEN(@BankMID) > 4
-------------------------------------------------------------------------
--    The supplied MID is for Credit Card
--        Debits (Approved Sales) first
--        Then Declines
--        Then Credits
--        Then Monthly Fees
-------------------------------------------------------------------------
	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,PP.bt_BankMID                                       AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,USA.TransDate                                       AS TranDate
		  ,USA.Amount                                          AS TranAmount 
		  ,USA.Amount                                          AS DebitQuantity
		  ,''                                                  AS TraceNumber
		  ,USA.AccountHolder                                   AS AccountHolder
		  ,'(1) Debits'                                        AS SourceData
	FROM USAePay.FileData                           AS USA  
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP   ON USA.CRM_PPDID = PP.bt_ProcessingProfileId
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_SourceId = @ConfigSourceID
      AND USA.TransType = 'S'
      AND USA.Result = 'A'
      AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)


    UNION ALL

	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,PP.bt_BankMID                                       AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,USA.TransDate                                       AS TranDate
		  ,USA.Amount                                          AS TranAmount 
		  ,0                                                   AS DebitQuantity
		  ,''                                                  AS TraceNumber
		  ,USA.AccountHolder                                   AS AccountHolder
		  ,'(3) Declines'                                      AS SourceData
	FROM USAePay.FileData                           AS USA  
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP   ON USA.CRM_PPDID = PP.bt_ProcessingProfileId
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId
	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_SourceId = @ConfigSourceID
      AND USA.Result = 'D'
      AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)


    UNION ALL 
  
	SELECT PA.bt_AccountName                                   AS ChildName
	      ,PA.bt_Name                                          AS Name
		  ,PA.bt_ProductLineName                               AS ProductLine
		  ,PA.bt_ManualBillingStartDate                        AS ManualBillingStartDate
		  ,PA.bt_ManualBillingEndDate                          AS ManualBillingEndDate
		  ,PP.bt_BankMID                                       AS MID
		  ,CASE 
			   WHEN PA.bt_ProductLineName = 'ACH' THEN
				   PP.bt_Config
			   ELSE
				   PP.bt_SourceId 
		   END                                                 AS SourceConfig
		  ,PP.bt_StartDate                                     AS MIDStartDate                    
		  ,PP.bt_EndDate                                       AS MIDEndDate
		  ,USA.TransDate                                       AS TranDate
		  ,USA.Amount                                          AS TranAmount 
		  ,0                                                   AS DebitQuantity
		  ,''                                                  AS TraceNumber
		  ,USA.AccountHolder                                   AS AccountHolder
		  ,'(2) Credits'                                       AS SourceData
	FROM USAePay.FileData                           AS USA  
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingProfile     AS PP   ON USA.CRM_PPDID = PP.bt_ProcessingProfileId
    LEFT JOIN [$(CRMShadow)].MS.bt_ProcessingAccount     AS PA   ON PP.bt_ProcessingAccount = PA.bt_ProcessingAccountId
	LEFT JOIN CRM.vServiceProductFeeGateway  AS Fee  ON PA.bt_ProcessingAccountId = Fee.ProcessingAccountId

	WHERE USA.UsabilityIndex = 1
	  AND USA.TransDateYYYYMM = @YearMonth
	  AND PP.bt_BankMID = @BankMID
	  AND PP.bt_SourceId = @ConfigSourceID
      AND USA.TransType = 'C'
      AND USA.Result = 'A'
      AND (PP.bt_EndDate IS NULL OR PP.bt_EndDate >= @RunDateBeginMonth)

  





END















