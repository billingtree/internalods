﻿CREATE TABLE [SF].[Processing_Stage_History__c] (
    [Created_By_UserName__c]  NVARCHAR (80)  NULL,
    [CreatedById]             NVARCHAR (18)  NULL,
    [CreatedDate]             DATETIME       NULL,
    [Date_Stage_Completed__c] DATETIME       NULL,
    [Date_Stage_Entered__c]   DATETIME       NULL,
    [Id]                      NVARCHAR (18)  NULL,
    [IsDeleted]               BIT            NULL,
    [LastModifiedById]        NVARCHAR (18)  NULL,
    [LastModifiedDate]        DATETIME       NULL,
    [Name]                    NVARCHAR (80)  NULL,
    [Number_of_Days__c]       NVARCHAR (3)   NULL,
    [Previous_Stage__c]       NVARCHAR (80)  NULL,
    [Processing__c]           NVARCHAR (18)  NULL,
    [Stage_Update__c]         NVARCHAR (255) NULL,
    [SystemModstamp]          DATETIME       NULL,
    [Updated_By__c]           NVARCHAR (18)  NULL,
    [Updated_By_UserName__c]  NVARCHAR (80)  NULL
);

