﻿






CREATE VIEW [dbo].[vATIWeeklyCosts]
AS
SELECT	FileReferenceCode
		,ConfigID AS ConfigNumber
		,AccountID AS ProcessingNumber
--		,TotalTransactions
----------------------------- Origination Costs
--        ,CASE ConfigId
--			WHEN '60'
--				THEN TotalTransactions * .115
--			ELSE TotalTransactions * .15
--         END										AS TotalTransactionCost
--		,TotalReturns
----------------------------- Return Costs
--        ,CASE ConfigId
--			WHEN '60'
--				THEN ISNULL(TotalReturns,0) * .09
--			ELSE ISNULL(TotalReturns,0) * .2
--         END										AS TotalReturnCost
--		,Corrections
----------------------------  Correction Costs
--        ,CASE ConfigId
--			WHEN '60'
--				THEN ISNULL(Corrections,0) * .09
--			ELSE ISNULL(Corrections,0) * .2
--         END										AS TotalCorrectionCost
		--,Addenda
		--,CASE ConfigId
		--	WHEN '60'
		--		THEN TotalTransactions * .115
		--	ELSE TotalTransactions * .15
  --       END +	CASE ConfigId
		--			WHEN '60'
		--				THEN ISNULL(TotalReturns,0) * .09
		--			ELSE ISNULL(TotalReturns,0) * .2
		--		END +	CASE ConfigId
		--					WHEN '60'
		--						THEN ISNULL(Corrections,0) * .09
		--					ELSE ISNULL(Corrections,0) * .2
		--				 END AS TotalCost
		,TransactionsCosts + ReturnsCosts + CorrectionsCosts AS TotalCost
		,StartDate
		,EndDate
		,MainGLSegment
		,InsertDate
FROM	dbo.ATIWeeklyCosts
WHERE	ISNULL(TotalTransactions,0) > 0
		OR ISNULL(TotalReturns,0) > 0
		OR ISNULL(Corrections,0) > 0
--ORDER BY ConfigId
--		,AccountId

