﻿CREATE TABLE [dbo].[USAePayRaw2012] (
    [YYYYMM]     FLOAT (53)     NULL,
    [GatewayNo]  NVARCHAR (255) NULL,
    [Merchant]   NVARCHAR (255) NULL,
    [Sales]      FLOAT (53)     NULL,
    [Credits]    FLOAT (53)     NULL,
    [Declines]   FLOAT (53)     NULL,
    [MerchantID] NVARCHAR (255) NULL,
    [SourceID]   NVARCHAR (255) NULL,
    [CRMID]      NVARCHAR (255) NULL
);

