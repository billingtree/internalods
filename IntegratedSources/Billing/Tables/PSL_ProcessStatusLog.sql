﻿CREATE TABLE [Billing].[PSL_ProcessStatusLog] (
    [PSL_ID]             INT           IDENTITY (1, 1) NOT NULL,
    [PSL_PRT_ID]         INT           NOT NULL,
    [PSL_RunDateYYYYMM]  INT           NOT NULL,
    [PSL_Status]         NVARCHAR (50) NOT NULL,
    [PSL_InsertDateTime] DATETIME      CONSTRAINT [DF_PSL_ProcessStatusLog_InsertDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PSL_ProcessStatusLog] PRIMARY KEY CLUSTERED ([PSL_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_PSL_ProcessStatusLog_PRT_ProcessRunType] FOREIGN KEY ([PSL_PRT_ID]) REFERENCES [Billing].[PRT_ProcessRunType] ([PRT_ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI_PRTID_RunDate]
    ON [Billing].[PSL_ProcessStatusLog]([PSL_RunDateYYYYMM] ASC, [PSL_PRT_ID] ASC) WITH (FILLFACTOR = 80);

